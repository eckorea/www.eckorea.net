@include('top')

<body>
<div class="page-wrapper">
    <main class="main">
        <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                </ol>
            </div>
            <!-- End .container --> 
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <ul class="checkout-steps">
                        <li>
                            <div class="blank20"></div>
                            <h2 class="step-title">Int'l Shipping Fee Payment</h2>
                            <form id="payment_form" method="post" action="/order/payment/shipping">
                                <input type="hidden" name="amount" value="0">
                                <input type="hidden" name="od_id" value="{{$data[0]->od_id}}">
                                {{ csrf_field() }}
                                 
                                 <div class="form-group required-field">
                                    <label>Order Total(Previous paid amount)</label>
                                    <div class="form-control-tooltip">
                                        ${{number_format($data[0]->od_receipt_price1/$data[0]->od_rate1,2)}}
                                        <span class="input-tooltip" data-toggle="tooltip" data-placement="right"><i class="icon-question-circle"></i></span> </div>
                                    <!-- End .form-control-tooltip --> 
                                </div>
                                 
                                 <div class="form-group required-field">
                                    <label>Shipping Fee </label>
                                    <div class="form-control-tooltip">
                                        ${{number_format($data[0]->od_send_cost2 / Config::get('rate.usd'),2)}}
                                        <span class="input-tooltip" data-toggle="tooltip" data-placement="right"><i class="icon-question-circle"></i></span> </div>
                                    <!-- End .form-control-tooltip --> 
                                </div>
                                <!-- End .form-group -->
                                
                                 <div class="form-group required-field">
                                    <label>Sales Tax (Order Total + Shipping Fee) * n%</label>
                                    <div class="form-control-tooltip">
                                        $ {{number_format($data[0]->od_sales_tax / Config::get('rate.usd'),2)}}
                                        <span class="input-tooltip" data-toggle="tooltip" data-placement="right"><i class="icon-question-circle"></i></span> </div>
                                    <!-- End .form-control-tooltip --> 
                                </div>
                                <!-- End .form-group -->
                                
                                 <div class="form-group required-field extra-topline">
                                    <label>Int'l Shipping Fee</label>
                                    <div class="form-control-tooltip">
                                        $ {{number_format(($data[0]->od_send_cost2 + $data[0]->od_sales_tax) / Config::get('rate.usd'),2)}}
                                        <span class="input-tooltip" data-toggle="tooltip" data-placement="right"><i class="icon-question-circle"></i></span> </div>
                                    <!-- End .form-control-tooltip --> 
                                </div>
                                <!-- End .form-group -->
                                
                                
                                <div class="form-group required-field">
                                    <label>Card Number </label>
                                    <div class="form-control-tooltip">
                                        <input type="text" class="form-control" required placeholder="1234-1234-1234-1234" name="card_number">
                                        <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span> </div>
                                    <!-- End .form-control-tooltip --> 
                                </div>
                                <!-- End .form-group -->
                                
                                <div class="form-group required-field">
                                    <label>Expire Date / CVC </label>
                                    <div class="form-control-tooltip">
                                        <input type="text" class="form-control" required placeholder="MM" name="card_mm" maxlength="2" style="width:80px; display:inline-block;">
                                        /
                                        <input type="text" class="form-control" required placeholder="YY" name="card_yy" maxlength="2" style="width:80px; display:inline-block;">
                                        <input type="text" class="form-control" required placeholder="CVV" name="card_cvv" maxlength="4" style="width:80px; display:inline-block;">
                                        <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span> </div>
                                    <!-- End .form-control-tooltip --> 
                                </div>
                                <!-- End .form-group -->
                                
                                <div class="form-group required-field">
                                    <label>Card Holder </label>
                                    <div class="form-control-tooltip">
                                        <input type="text" class="form-control" required placeholder="JHON LEE" name="card_holder">
                                        <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span> </div>
                                    <!-- End .form-control-tooltip --> 
                                </div>
                                <!-- End .form-group -->
                            </form>
                        </li>
                    </ul>
                </div>
                <!-- End .col-lg-8 -->
                
                <div class="col-lg-4">
                    <div class="order-summary">
                        <h3>Summary</h3>
                        <div class="" >
                            <table class="table table-mini-cart">
                                <tbody>
                                
                                @foreach($data as $row)
                                <tr>
                                    <td class="product-col"><figure class="product-image-container"> <a href="@if($row->it_url) {{$row->it_url}} @else /mall/item/{{$row->it_id}} @endif" class="product-image"> @if($row->it_thumbnail_url) <img src="{{$row->it_thumbnail_url}}" alt="product"> @else <img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg" alt="product"> @endif </a> </figure>
                                        <div>
                                            <h2 class="product-title"> <a href="@if($row->it_url) {{$row->it_url}} @else /mall/item/{{$row->it_id}} @endif">{{$row->it_name}}</a> </h2>
                                            <span class="product-qty">Qty: {{$row->ct_qty}} / KRW {{number_format($row->ct_price*$row->ct_qty,0)}}</span> </div></td>
                                </tr>
                                @endforeach
                                
                                <tr>
                                <td>
									<div class="cart-summary">
										<table class="table table-totals">
											<tbody>
												<tr>
													<td>Subtotal</td>
													<td>KRW {{number_format($data[0]->od_cart_price)}}</td>
												</tr>
												<tr>
													<td>Local Shipping</td>
													<td>KRW {{number_format($data[0]->od_send_cost1)}}</td>
												</tr>
												<tr>
													<td>
														Service Fee
														<div class="tooltip2"><a href="#"><img src="/image/team/g_dot.png"></a>
															<span class="tooltiptext2">Service Fee는 item 갯수로 부과되며 1-3개까지는 $1.9 4-7개까지는 item당 $0.5입니다. </span>
														</div>																					
													</td>
													<td>USD {{number_format($data[0]->od_service_fee/$data[0]->od_rate1,2)}} <!--(USD {{Config::get('service_fee.fee')}} * Item)--></td>
												</tr>
												<tr>
													<td>Item Subtotal</td>
													<td>KRW {{number_format($data[0]->od_receipt_price1)}}</td>
												</tr>
												<tr>
													<td>
														Exchange Rates
														<div class="tooltip2"><a href="#"><img src="/image/team/g_dot.png"></a>
															<span class="tooltiptext2">EC KOREA의 환율정보는 하나은행의 고시환율이 적용된 값임을 알려드립니다. </span>
														</div>
													</td>
													<td>1USD = {{number_format($data[0]->od_rate1)}} KRW</td>
												</tr>
												<tr>
													<td>Order Total (USD) </td>
													<td>USD {{number_format(($data[0]->od_receipt_price1) / $data[0]->od_rate1 ,2)}}</td>
												</tr>
											</tbody>
										</table>
									</div>
                               
                                </td>
                                </tr>  
                                
                                </tbody>                                
                            </table>
                        </div>
                        <!-- End #order-cart-section --> 
                    </div>
                    <!-- End .order-summary --> 
                </div>
                <!-- End .col-lg-4 --> 
            </div>
            <!-- End .row -->
            
            <div class="row">
                <div class="col-lg-8">
                    <div class="float-right">
                        <!--<a href="/payment" class="btn btn-primary float-right">NEXT</a>--> 
                        <button type="submit" id="order_payment" class="btn btn-outline-secondary">NEXT</button>
					</div>

					
                    <!-- End .checkout-steps-action --> 
                </div>
                <!-- End .col-lg-8 --> 
            </div>
            <!-- End .row --> 
        </div>
        <!-- End .container -->
        
        <div class="mb-6"></div>
        <!-- margin --> 
    </main>
    <!-- End .main --> 
</div>
<!-- End .page-wrapper --> 

@include('footer') 
<script>
function valid_order()
{
	var flag = true;
	$("input[type='text']").each(function(index, element) {
        if($(this).prop('required'))
		{			
			if($(this).val() == '')
			{
				alert('필수데이터를 입력해주세요.');
				$(this).focus();	
				flag = false;			
				return false;
			}						
		}
		
    });
	
	return flag;
}

$(document).ready(function(e) {
    $("#order_payment").click(function(e) {
		
		if(valid_order())
		{		
       		$("#payment_form").submit();
		}
    });
});
</script>
</body>

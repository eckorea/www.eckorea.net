@include('top')

<body>
	<div class="page-wrapper">
	<main class="main">
		<nav aria-label="breadcrumb" class="breadcrumb-nav">
			<div class="container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
				</ol>
			</div>
			<!-- End .container --> 
		</nav>

		<div class="container">
			<div class="row">
				<div class="col-lg-9 order-lg-last dashboard-content">
					<h2>EVENT</h2>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{{$data[0]->wr_title}}
							</div>
							<!-- End .form-group -->

							<div class="form-group">
								{!!$data[0]->wr_content!!}
							</div>
							<!-- End .form-group -->							
                           
							<!-- End .form-footer --> 
						</div>
					<!-- End .col-md-8 --> 
					</div>
					<!-- End .row --> 
				</div>
				<!-- End .col-lg-9 --> 

			@include('cscenter_left') 
			</div>
			<!-- End .row --> 
		</div>
		<!-- End .container -->
    
		<div class="mb-5"></div>
    <!-- margin --> 
	</main>
  <!-- End .main --> 
</div>
<!-- End .page-wrapper --> 
@include('footer')
</body>

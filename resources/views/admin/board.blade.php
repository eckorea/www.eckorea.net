@include('admin.top')
<div class="app-main"> @include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>게시글 관리.</div>
                    </div>
                </div>
            </div>
            <!--
            <div class="main-card mb-3 card">
                <div class="card-body">
                
                    <div class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <form method="get" action="">
                                <select name="qkind" class="select_css">                                      
                                    <option value="email" @if(Request::query('qkind') == 'email') selected @endif>이메일</option>                                    
                                </select>                                
                            </div>
                           <input type="text" class="form-control" name="q" value="{{Request::query('q')}}">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">검색</button>
                            </div>
                            </form>
                        </div>
                    </div>
                
                </div>
            </div>
                -->
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID</th>                                       
                                        <th class="text-center">TITLE</th>
                                        <th class="text-center">WRITER</th>
                                        <th class="text-center">DATE</th>                                        
                                        <th class="text-center">수정</th>
                                        <th class="text-center">글보기</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)
                                    <tr>
                                        <td>{{$row->wr_id}}</td>                                       
                                        <td>{{$row->wr_title}}</td>
                                        <td>{{$row->wr_writer}}</td>
                                        <td>{{$row->created_at}}</td>
                                        <td>
                                        <a href="/admin/board/view/{{$row->wr_id}}?wr_board={{$row->wr_board}}">[수정]</a>
                                        <a href="/admin/board/delete/{{$row->wr_id}}" onclick="return confirm('삭제하시겠습니까?');">[삭제]</a>
                                        </td>
                                        <td><a href="/mall/board/{{$row->wr_id}}" target="_blank">보기</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>
                            <button><a href="/admin/board/new?wr_board={{Request::query('wr_board')}}">게시글 추가</a></button>
                            </div>
                        </div>
                        <div style="text-align:center">
                            <div class="card-body">
                                <nav>
                                    <ul class="pagination">
                                    {{ $data->links() }}                                     
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.footer') </div>
</div>

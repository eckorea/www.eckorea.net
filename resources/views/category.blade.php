@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MALL</li>
                        @foreach($ca_tree as $each_ca)
                        <li class="breadcrumb-item" aria-current="page">{{$each_ca['ca_name']}}</li>
                        @endforeach
					</ol>
				</div>
				<!-- End .container --> 
			</nav>

			<div class="container">
				<div class="row">				
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>{{$ca_name}}</h2>
						<form method="get">
                        <input type="hidden" name="page" value="{{Request::query('page')}}">
						<nav class="toolbox">
							<div class="toolbox-left">
								<div class="toolbox-item toolbox-sort">
									<div class="select-custom">
                                    	<select name="orderby" class="form-control" onChange="submit();">
											<!--<option value="menu_order">Default sorting</option>-->
											<option value="popularity" @if(Request::query('orderby') == 'popularity') selected @endif >Sort by popularity</option>
											<option value="price" @if(Request::query('orderby') == 'price') selected @endif>Sort by price: low to high</option>
											<option value="price-desc" @if(Request::query('orderby') == 'price-desc') selected @endif>Sort by price: high to low</option>
										</select>
                                    
									</div><!-- End .select-custom -->
								</div><!-- End .toolbox-item -->
							</div><!-- End .toolbox-left -->
						</nav>
                        </form>

						<div class="row row-sm">
							
                            @foreach($data as $row)
                            @php
                            $arr_it_url = json_decode($row->it_img_json);
                            @endphp
                            <div class="col-6 col-md-4 col-xl-3">
								<div class="product-default">
									@if($row->it_type == 'in')	                                
                                    <figure>                                    
										<a href="/mall/item/{{$row->it_id}}">
                                        @if(isset($arr_it_url[0]))
                                        <img src="{{$row->it_thumbnail_url}}" style="height:250px; width:100%">
                                        @endif
                                        <!--
                                        <img src="/image/products/product-4.jpg">
                                        -->
                                        </a>
									</figure>
                                    @else                                   
                                    <figure>                                                                        
                                    @if(isset($arr_it_url[0]))
                                    <a href="{{$row->it_url}}" target="_blank">
                                    <img src="{{$arr_it_url[0]}}">
                                    </a>
                                    @endif
									
									</figure>                                    
                                    @endif
									<div class="product-details">
										<h2 class="product-title">
											<a href="@if($row->it_type == 'in')	/mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif ">{{$row->it_name}}</a>
										</h2>
										<div class=" RT" style="width:100%">
											<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2)}}</span><br>
											<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
										</div>
									</div><!-- End .product-details -->
								</div>
							</div><!-- End .col-xl-3 -->
						    @endforeach
						</div>

						<nav class="toolbox toolbox-pagination">
							<ul class="pagination">
								{{ $data->links() }}            
								<!--
								<li class="page-item disabled"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a> </li>
								<li class="page-item active"> <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a> </li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">4</a></li>
								<li class="page-item"><a class="page-link" href="#">5</a></li>
								<li class="page-item"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a> </li>
								-->
							</ul>
						</nav>
					</div><!-- End .col-lg-9 -->			



					@include('mall_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
		<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

	@include('footer')
</body>

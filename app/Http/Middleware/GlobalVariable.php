<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class GlobalVariable
{
    public function handle($request, Closure $next)
	{
		//상수처럼 자주 쓰는 데이터 전역변수화 하기 위한 미들웨어
		//서비스피
		$setting_data = DB::table('define_settings')
				->where([			
				['setting_key', '=', 'service_fee']
				])->limit(1)->get();
		$service_fee = $setting_data[0]->setting_value;		
		
		$setting_data = DB::table('define_settings')
				->where([			
				['setting_key', '=', 'service_fee_min']
				])->limit(1)->get();
		$service_fee_min = $setting_data[0]->setting_value;		
		
		$setting_data = DB::table('define_settings')
				->where([			
				['setting_key', '=', 'service_fee_min_price']
				])->limit(1)->get();
		$service_fee_min_price = $setting_data[0]->setting_value;		
		
		//전역변수 설정
		Config::set(['service_fee' => ['fee' => $service_fee, 'min'=>$service_fee_min, 'min_price'=>$service_fee_min_price]]);
		return $next($request);
	}
}

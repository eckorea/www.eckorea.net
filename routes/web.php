<?php
/*
|----------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

use Illuminate\Support\Facades\Cookie;

Route::get('/phpinfo', function () {
    phpinfo();
});

Route::get('/', 'MallController@indexWithPartner');
/*
Route::get('/test', function () {
   //return view('test');
   //parserCoupang_v1('html');
   //$result = getSSsearchData(urlencode('샤오미'),1);   
   //print_r($result[0]);
   
});*/

Route::get('/test', 'TestController@paser');
Route::get('/excel_test', 'ExcelController@test');
Route::get('/excel_test2', 'ExcelController@ex_test');

//결제
Route::get('/payment', 'TestController@cancel');

Route::get('/testindex', 'MallController@index');

//임시 결과 페이지
Route::get('/result/{txt}', function ($txt) {
	echo $txt;  
});

//언어변환
Route::get('/lang/{locale}', function ($locale) {
	
	//로케일 셋
	session()->put('locale', $locale);
    return redirect()->back();
	
	//echo request()->headers->get('referer'); exit;
	//App::setLocale($locale);
	//$setted_locale = App::getLocale();	
	//echo $setted_locale; exit;
	//return redirect($_SERVER['HTTP_REFERER']);
	
});

//Email 인증
Route::get('/verify/resend', 'MemberController@verify_resend');
Route::get('/verify/email', 'MemberController@verify');

//비번찾기
Route::get('/password_reset', function()
{
	return view('find_password');
});
Route::post('/password_reset', 'MemberController@password_reset');

//로그인
Route::get('/login', function () {
	$ckey = Cookie::get('SBUSERKEY');

    if(isset($_GET['PID'])) {
        $_PID = $_GET['PID'];
        Cookie::queue(Cookie::forget('cpsKEY'));
        Cookie::queue('cpsKEY', $_PID, 60*24*3);
        return view('login')->with('SBUSERKEY',$ckey)->with('cpsKEY',$_PID);
    }
    return view('login')->with('SBUSERKEY',$ckey);
});
Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

//회원가입
Route::post('/join', 'MemberController@store');
Route::get('/joined',function()
{
	return view('register_ok');
});

//게시판
Route::get('/notice', 'BoardController@list');
Route::get('/notice_write', function()
{
	return view('notice_write');
});
Route::post('/notice_write', 'BoardController@store');
Route::get('/notice_view/{wr_id}', 'BoardController@view');
Route::get('/notice_edit/{wr_id}', 'BoardController@view');
Route::post('/notice_edit/{wr_id}', 'BoardController@edit');
Route::get('/notice_delete/{wr_id}', 'BoardController@destroy');

Route::get('/event', 'BoardController@list');
Route::get('/event_view/{wr_id}', 'BoardController@view');

//Contact
Route::get('/contact', function()
{
	return view('contact');
});
Route::post('/contact', 'ContactController@store');

//Review
Route::get('/mall/review', 'MallController@index_review');

//PAGE VIEW
Route::get('/mall/board/{wr_id}', 'MallController@view_board');

//MALL
Route::get('/mall', 'MallController@index');
Route::get('/mall/category/{ca_id}', 'MallController@category_item');
Route::get('/mall/category/kyobo/{ca_id}', 'MallController@category_kyobo_item');
Route::get('/mall/item/{it_id}', 'MallController@view_item');
Route::get('/mall/search/{category}/{keyword}', 'MallController@search');
Route::get('/mall/search/{category}/{keyword}/{page}', 'MallController@search_data');
Route::get('/mall/faq', 'MallController@index_faq');
Route::get('/mall/faq/{faq_id}', 'MallController@view_faq');
Route::get('/mall/kculture', 'MallController@section_kculture');
Route::get('/mall/kculture/kstudy', 'MallController@study_kculture');
Route::get('/mall/kculture/{kc_board}', 'MallController@index_kculture');
Route::get('/mall/kculture/view/{kc_id}', 'MallController@view_kculture');
Route::get('/mall/guide/{guide_id}', 'MallController@view_guide');
Route::get('/mall/shipping_fee', function()
{
	return view('shipping_fee');
});
Route::get('/mall/exchange_rate', function()
{
	return view('exchagerate');
});
Route::get('/mall/about', function()
{
	return view('about');
});

Route::get('/mall/terms', 'MallController@view_terms');
Route::get('/mall/policy', 'MallController@view_policy');

//User Order
Route::get('/order/payment/domestic/approve', 'OrderController@payment_approve'); //로그인이 풀린상태로 컨펌들어오는 경우가 있어서 결제를 최우선순위로 하여 빼두었음.
Route::post('/order/payment/domestic/approve', 'OrderController@payment_approve'); //로그인이 풀린상태로 컨펌들어오는 경우가 있어서 결제를 최우선순위로 하여 빼두었음.

Route::middleware(['auth.user'])->group(function () {
	//임시 외부 카트 데이터 입력페이지
	Route::get('/test/cart', 'ChromeApiController@cart_test');

	Route::post('/order/cart', 'OrderController@add_cart');
	Route::post('/order/cart/update', 'OrderController@update_cart');
	Route::get('/order/cart', 'OrderController@view_cart');
	Route::get('/order/checkout', 'OrderController@checkout');
	Route::post('/order/payment', 'OrderController@payment');
	Route::post('/order/payment/domestic', 'OrderController@domestic_payment_request');
	
	Route::post('/order/shipping_rate', 'OrderController@shipping_rate');
	Route::get('/order/shipping_rate', 'OrderController@shipping_rate');
	
	Route::get('/order/checkout/shipping/{od_id}', 'OrderController@checkout_shipping');
	Route::post('/order/payment/shipping', 'OrderController@payment_shipping');
	Route::get('/order/list', 'OrderController@index');
	Route::get('/order/view/{od_id}', 'OrderController@view');
	Route::post('/order/edit/address','OrderController@edit_address');
	Route::get('/my/coupon', 'MallController@coupon_list');
	Route::get('/my/message', 'MallController@message_list');
	Route::get('/my/message/view/{id}', 'MallController@message_view');
	Route::get('/my/address', 'MallController@address_list');
	Route::get('/my/address/add', function()
	{
		return view('shippingaddr_add');
	});
	Route::post('/my/address/add', 'MallController@address_add');
	Route::get('/my/address/delete/{id}', 'MallController@address_delete');
	Route::get('/my/address/view/{id}', 'MallController@address_view');
	Route::get('/my/address/default/{id}', 'MallController@address_default');
	Route::get('/my/password/change', function()
	{
		return view('changepw');
	});
	Route::post('/my/password/change', 'MallController@password_change');
	Route::post('/mall/review', 'MallController@post_review');
	Route::get('/mall/review/delete/{review_id}', 'MallController@delete_review');
	Route::post('/mall/qna', 'MallController@post_qna');
	Route::get('/mall/qna/delete/{qna_id}', 'MallController@delete_qna');
	
	//inquiry
	Route::get('/inquiry', 'BoardController@list');
	Route::get('/inquiry_write', function()
	{
		return view('inquiry_write');
	});
	Route::post('/inquiry_write', 'BoardController@store');
	Route::get('/inquiry_view/{wr_id}', 'BoardController@view');
	Route::get('/inquiry/modify/{wr_id}', 'BoardController@view');
	Route::post('/inquiry/modify/{wr_id}', 'BoardController@edit');
	Route::get('/inquiry/delete/{wr_id}', 'BoardController@destroy');
});

//Admin------
Route::middleware(['auth.admin'])->group(function () {
	//Route::middleware(['first', 'second'])->group(function () { 나중에 관리자 로그인 체크 미들웨어를 그룹핑 하여 사용하자
	//Route::prefix('admin')->group(function () { //prefix를 쓰면 admin 덧붙이지 않아도 됨
	
	//Member
	Route::get('/admin/member', 'MemberController@list');
	Route::get('/admin/member/view/{id}', 'MemberController@show');
	Route::post('/admin/member/edit', 'MemberController@update');
	Route::get('/admin/member/delete/{id}', 'MemberController@delete');

	//Message
	Route::get('/admin/message', 'MessageController@index');
	Route::get('/admin/message/send', function()
	{
		return view('admin/message_send');
	});
	Route::post('/admin/message/send', 'MessageController@store');
	Route::get('/admin/message/view/{id}', 'MessageController@show');
	
	//Coupon
	Route::get('/admin/coupon', 'CouponController@index');
	Route::get('/admin/coupon/send', function()
	{
		return view('admin/coupon_send');
	});
	Route::post('/admin/coupon/send', 'CouponController@store');
	Route::get('/admin/coupon/view/{id}', 'CouponController@show');
	Route::get('/admin/coupon/delete/{id}', 'CouponController@destroy');
		
	//Admin Order	
	Route::get('/admin/buy', 'OrderController@index_buy_admin');
	Route::get('/admin/warehouse', 'OrderController@index_warehouse_admin');
	Route::get('/admin/packing', 'OrderController@index_packing_admin');
	Route::get('/admin/shipping', 'OrderController@index_shipping_admin');
	Route::get('/admin/order/view/{od_id}', 'OrderController@show_admin');
	Route::post('/admin/order/edit', 'OrderController@edit_order');	
	Route::post('/admin/order/make_ship_invoice', 'OrderController@make_ship_invoice');	
	Route::post('/admin/status/update', 'OrderController@update_ct_status');
	Route::post('/admin/status/update_order', 'OrderController@update_order_status');
	Route::post('/admin/cart/edit', 'OrderController@edit_cart');	
	Route::post('/admin/shipping/refund/{od_id}', 'OrderController@refund_shipping');
	
	//Contents
	Route::get('/admin/faq', 'ContentsController@faq_index');
	Route::get('/admin/faq/new/', 'ContentsController@faq_view');
	Route::get('/admin/faq/view/{faq_id}', 'ContentsController@faq_view');	
	Route::get('/admin/faq/delete/{faq_id}', 'ContentsController@faq_delete');	
	Route::post('/admin/faq/edit', 'ContentsController@faq_edit');
	
	Route::get('/admin/kculture', 'ContentsController@kculture_index');
	Route::get('/admin/kculture/new/', 'ContentsController@kculture_view');
	Route::get('/admin/kculture/view/{kc_id}', 'ContentsController@kculture_view');	
	Route::get('/admin/kculture/delete/{kc_id}', 'ContentsController@kculture_delete');	
	Route::post('/admin/kculture/edit', 'ContentsController@kculture_edit');
	
	Route::get('/admin/guide', 'ContentsController@guide_index');
	Route::get('/admin/guide/new/', 'ContentsController@guide_view');
	Route::get('/admin/guide/view/{faq_id}', 'ContentsController@guide_view');	
	Route::get('/admin/guide/delete/{faq_id}', 'ContentsController@guide_delete');	
	Route::post('/admin/guide/edit', 'ContentsController@guide_edit');
	
	Route::get('/admin/board', 'ContentsController@board_index');
	Route::get('/admin/board/new/', 'ContentsController@board_view');
	Route::get('/admin/board/view/{faq_id}', 'ContentsController@board_view');	
	Route::get('/admin/board/delete/{faq_id}', 'ContentsController@board_delete');	
	Route::post('/admin/board/edit', 'ContentsController@board_edit');
	
	//TERMS
	Route::get('/admin/terms', 'ContentsController@terms_view');
	Route::post('/admin/terms', 'ContentsController@terms_edit');
	
	//POLICY
	Route::get('/admin/policy', 'ContentsController@policy_view');
	Route::post('/admin/policy', 'ContentsController@policy_edit');
	
	//카테고리 관리
	Route::get('/admin/category', 'CategoryController@index');
	Route::get('/admin/category/delete/{ca_id}', 'CategoryController@destroy');
	Route::get('/admin/category/new', 'CategoryController@show');
	Route::get('/admin/category/{type}/{ca_id}', 'CategoryController@show');
	Route::post('/admin/category/edit', 'CategoryController@edit');
	Route::post('/admin/category/add', 'CategoryController@store');
	Route::post('/admin/category/new', 'CategoryController@store');
	
	//상품관리
	Route::get('/admin/item', 'ItemController@index');
	Route::get('/admin/item/delete/{it_id}', 'ItemController@destroy');	
	Route::get('/admin/item/new', 'ItemController@show');
	Route::get('/admin/item/{type}/{it_id}', 'ItemController@show');	
	Route::post('/admin/item/edit', 'ItemController@edit');
	Route::post('/admin/item/new', 'ItemController@store');
	
	//배너관리
	Route::get('/admin/banner', 'ContentsController@banner_index');
	Route::get('/admin/banner/new/', 'ContentsController@banner_view');
	Route::get('/admin/banner/view/{banner_id}', 'ContentsController@banner_view');	
	Route::get('/admin/banner/delete/{banner_id}', 'ContentsController@banner_delete');	
	Route::post('/admin/banner/edit', 'ContentsController@banner_edit');
	
	//발주서관리
	Route::get('/admin/exceldown/{type}', 'ExcelController@download');
	Route::post('/admin/excelup/{type}', 'ExcelController@upload');
});

//크롬익스텐션
Route::get('/chrome/login', 'ChromeApiController@login');
Route::get('/chrome/cart', 'ChromeApiController@cart');
Route::post('/chrome/cart', 'ChromeApiController@cart');

//에디터업로드
Route::post('/editor/upload', 'ContentsController@editor_upload');

//이미지뷰어
Route::get('/mall/image', 'MallController@img_view');

//레이아웃 뷰
Route::get('/layout/{file}', function ($file) {
	if($file)
	{
    	return view($file);
	}
	else
	{
		echo "요청하신 레이아웃 파일을 찾을수 없습니다";
	}
});


Route::get('/layout/admin/{file}', function ($file) {
	if($file)
	{
    	return view('admin/'.$file);
	}
	else
	{
		echo "요청하신 레이아웃 파일을 찾을수 없습니다";
	}
});

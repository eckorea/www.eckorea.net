<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;
use App\Item;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Libraries\PayErom;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Order $order, Request $request)
    {
		$mb_email = session('mb_email');
        $qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
		$qstatus = array_key_exists('status',$qs) ? $qs['status'] : '';
		
		$data = $order->join('carts','carts.od_id', '=', 'orders.od_id')
		->where([
		['carts.mb_email','=',$mb_email],
		['orders.od_status','!=','paystart']
		]);
		
		//상태별
		if($qstatus) 
		{
			if($qstatus == 'sended')
			{
				$data = $data->where([
				['orders.od_status', '=', 'sended']
				]);
			}
			else if($qstatus == 'sending')
			{
				$data = $data->where([
				['orders.od_status', '!=', 'sended'],
				['carts.ct_ship_no', '!=', '']
				]);
				//$row->od_status != 'sended' && $row->ct_ship_no != ''
			}
			else
			{
				$data = $data->where('carts.ct_house_status', $qstatus);
			}
		}
		
		//검색어				
		if($qkind == 'it_name') $data = $data->where('carts.it_name','LIKE',$q.'%');
		if($qkind == 'mb_email') $data = $data->where('carts.mb_email','LIKE',$q.'%');
		if($qkind == 'od_id') $data = $data->where('carts.od_id','LIKE',$q.'%');				
		
		//바인딩	
		$data = $data
		->orderBy('orders.created_at', 'desc')
		->paginate(10)->appends($request->query());		
        return view('/order')->with(['data'=>$data, 'query'=>$qs]);
    }
	
	public function index_admin()
	{
		
	}
	
	public function index_buy_admin(Order $order, Request $request)
	{
		$qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
		$qstatus = array_key_exists('status',$qs) ? $qs['status'] : '';
		$qorigin = array_key_exists('origin',$qs) ? $qs['origin'] : '';
		
		$data = $order->join('carts','carts.od_id', '=', 'orders.od_id');		
		
		//상태별
		if($qstatus) $data = $data->where('carts.ct_house_status', $qstatus);
		
		//사이트
		if($qorigin) $data = $data->where('carts.ct_origin', $qorigin);
		
		//검색어				
		if($qkind == 'it_name') $data = $data->where('carts.it_name','LIKE',$q.'%');
		if($qkind == 'mb_email') $data = $data->where('carts.mb_email','LIKE',$q.'%');
		if($qkind == 'od_id') $data = $data->where('carts.od_id','LIKE',$q.'%');		
						
		$data = $data
		->orderBy('orders.created_at', 'desc')
		->paginate(10)->appends($request->query());				
        return view('/admin/buy_item')->with(['data'=>$data]);
	}
	
	public function index_warehouse_admin(Order $order, Request $request)
	{
		$qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
		$qstatus = array_key_exists('status',$qs) ? $qs['status'] : '';
		
		$data = $order->join('carts','carts.od_id', '=', 'orders.od_id');		
		
		//상태별
		if($qstatus) $data = $data->where('carts.ct_house_status', $qstatus);
		
		//검색어				
		if($qkind == 'it_name') $data = $data->where('carts.it_name','LIKE',$q.'%');
		if($qkind == 'mb_email') $data = $data->where('carts.mb_email','LIKE',$q.'%');
		if($qkind == 'od_id') $data = $data->where('carts.od_id','LIKE',$q.'%');				
		
		//바인딩	
		$data = $data
		->orderBy('orders.created_at', 'desc')
		->paginate(10)->appends($request->query());		
        return view('/admin/center_item')->with(['data'=>$data, 'query'=>$qs]);
	}
	
	public function index_packing_admin(Order $order, Request $request)
	{
		$qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
		$qstatus = array_key_exists('status',$qs) ? $qs['status'] : '';
		
		$data = $order->join('carts','carts.od_id', '=', 'orders.od_id');		
		
		//상태별
		if($qstatus) $data = $data->where('carts.ct_house_status', $qstatus);
		
		//검색어				
		if($qkind == 'it_name') $data = $data->where('carts.it_name','LIKE',$q.'%');
		if($qkind == 'mb_email') $data = $data->where('carts.mb_email','LIKE',$q.'%');
		if($qkind == 'od_id') $data = $data->where('carts.od_id','LIKE',$q.'%');				
		
		//바인딩	
		$data = $data
		->orderBy('orders.created_at', 'desc')
		->paginate(10)->appends($request->query());		
        return view('/admin/packing')->with(['data'=>$data, 'query'=>$qs]);
	}
	
	public function index_shipping_admin(Order $order, Request $request)
	{
		$qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
		$qstatus = array_key_exists('status',$qs) ? $qs['status'] : '';
		
		$data = $order;
						
		//상태별
		if($qstatus) 
		{
			if($qstatus == 'sended') 
			{
				$data = $data->where('od_status', $qstatus);
			}
			else
			{
				$data = $data->where([
				['od_ship_paid', '=', $qstatus],
				['od_status', '!=', 'sended']
				]);
			}
		}
		
		//검색어				
		if($qkind == 'od_id') $data = $data->where('od_id','LIKE',$q.'%');		
		if($qkind == 'mb_email') $data = $data->where('mb_email','LIKE',$q.'%');		
		
		//바인딩	
		$data = $data
		->orderBy('orders.created_at', 'desc')
		->paginate(10)->appends($request->query());		
        return view('/admin/shipping')->with(['data'=>$data, 'query'=>$qs]);
	}
	
	public function add_cart(Cart $cart, Request $request)
	{
		$mb_email = session('mb_email');				
		$it_id = $request->input('it_id');
		$direct = $request->input('direct');
		
		$arr_option = array();
		if($request->input('it_option1')) array_push($arr_option, 'Option1:'.$request->input('it_option1'));
		if($request->input('it_option2')) array_push($arr_option, 'Option2:'.$request->input('it_option2'));
		
		if($direct == 'Y')
		{
			//카트 셀렉트 업데이트
			$cart->where([
			['mb_email', '=', $mb_email],
			['ct_status', '=', 'shopping']
			])->update(['ct_select' => 'N']);
		}

		//Item Info Load
		$item = DB::table('items')->where('it_id',$it_id)->get();
		
		//Variable
		$ct_origin = $item[0]->it_origin;
		$it_name = $item[0]->it_name;
		$it_option = implode(',',$arr_option);
		$ct_price = $item[0]->it_price;
		$ct_krw_price = $item[0]->it_price;
		$it_whole_price = $item[0]->it_whole_price;
		$ct_qty = $request->input('qty');
		$arr_it_img = json_decode($item[0]->it_img_json);
		$it_thumbnail_url = '';
		if(isset($arr_it_img[0])) $it_thumbnail_url = $arr_it_img[0];
		
		//카트 테이블 검색해서 it_id와 shopping 상태가 같은게 있으면 qty만 update 해줌
		$cart_data = $cart->where([
		['mb_email', '=', $mb_email],
		['ct_status', '=', 'shopping'],
		['it_id', '=', $it_id],
		['it_option', '=', $it_option]
		])->get();
		
		$cart_old_qty = 0;
		if(isset($cart_data[0])) $cart_old_qty = $cart_data[0]->ct_qty;
		
		//같은 상품이 이미 카트에 있는경우 갯수만 증가
		if($cart_old_qty)
		{
			$cart->where([
			['mb_email', '=', $mb_email],
			['ct_status', '=', 'shopping'],
			['it_id', '=', $it_id]
			])
			->update
			(['ct_qty' => ($cart_old_qty+$ct_qty), 'ct_select' => 'Y']);
		}
		else
		{
			$cart->insert(['mb_email'=>$mb_email, 
			'ct_type'=>'in', 
			'ct_origin'=>$ct_origin,
			'it_id'=>$it_id, 
			'it_name'=>$it_name, 
			'it_option'=>$it_option, 
			'ct_price'=>$ct_price, 
			'ct_krw_price'=>$ct_krw_price, 
			'it_whole_price'=>$it_whole_price, 
			'ct_qty'=>$ct_qty,
			'ct_status'=>'shopping',
			//'it_thumbnail_url'=>$it_thumbnail_url,
			'it_thumbnail_url'=>$item[0]->it_thumbnail_url,
			'created_at'=>Carbon::now()
			]);

            /**
             * TODO : esummer, 2021-05-27, 사용자 카트에 추가시 푸시 PUSH 메시지 전송
             */
            //sendMobileNotification($request, $mb_email, 'add_cart', '[eckorea.net] Your items has been successfully added', '/orders/cart');
        }


        if($direct == 'Y')
		{
			return redirect('/order/checkout');
		}
		else
		{
			return redirect('/order/cart');
		}
	}
	
	public function update_cart(Cart $cart, Request $request)
	{
		//AJAX
		$mb_email = session('mb_email');
		$method = $request->input('method');
		$ct_id = $request->input('ct_id');

		if($method == 'select')
		{
			$select_yn = $request->input('select_yn');
			
			$cart->where([
			['ct_id', '=', $ct_id],
			['mb_email', '=', $mb_email],
			['ct_status', '=', 'shopping']
			])->update(['ct_select' => $select_yn]);
			
			return response()->json(array('result'=> 1, 'msg'=> 'done'), 200);
		}
		else if($method == 'select_all')
		{
			$select_yn = $request->input('select_yn');
			
			$cart->where([
			['mb_email', '=', $mb_email],
			['ct_status', '=', 'shopping']
			])->update(['ct_select' => $select_yn]);
			
			return response()->json(array('result'=> 1, 'msg'=> 'done'), 200);
		}
		else if($method == 'delete')
		{
			$cart->where([
			['ct_id', '=', $ct_id],
			['mb_email', '=', $mb_email],
			['ct_status', '=', 'shopping']
			])->delete();
						
			return response()->json(array('result'=> 1, 'msg'=> 'done'), 200);
		}
		else if($method == 'edit_qty')
		{
			$qty = $request->input('qty');
			
			$cart->where([
			['ct_id', '=', $ct_id],
			['mb_email', '=', $mb_email],
			['ct_status', '=', 'shopping']
			])->update(['ct_qty' => $qty]);
			
			return response()->json(array('result'=> 1, 'msg'=> 'done'), 200);
		}		
	}
	
	public function view_cart(Cart $cart, Request $request)
	{
		$mb_email = session('mb_email');		
		$data = $cart->where([
		['mb_email', '=', $mb_email],
		['ct_status', '=', 'shopping']
		])->get();
		
		$setting_data = DB::table('define_settings')
				->where([			
				['setting_key', '=', 'service_fee']
				])->get();
		$service_fee = $setting_data[0]->setting_value;
				
		return view('/cart')->with(['data'=>$data, 'service_fee' => $setting_data[0]->setting_value]);
	}
	
	public function shipping_rate(Request $request)
	{
		//AJAX
		$type = $request->type; //1 : with rate , 0:w/o rate
		$country = $request->country;
		$weight = $request->weight;
						
		$cursor = DB::table('shipping_rates')
		->select(DB::raw('sr_carrier, MIN(sr_weight) AS sr_weight , MIN(sr_price) AS sr_price'))
		->where([
		['sr_country','=', $country],
		['sr_use','=','Y']
		]);
			
		if($type == 1) //내부상품만 주문한경우, 아니면 carrier name 만 사용한다
		{
			$cursor = $cursor->where([['sr_weight', '>=', $weight]]);
		}
				
		$data = $cursor->groupBy('sr_carrier')
		->get();
		
		return response()->json(array('result'=> 1, 'shipping_data'=>$data), 200);
	}
	
	public function checkout(Cart $cart, Request $request)
	{
		$flag_in_item = true; //도서(교보) 상품만 있을경우 true
		$flag_mixed_item = false; //도서(교보)외 상품 섞여있는 경우 true
		$total_weight = 0; 
		
		$mb_email = session('mb_email');		
		
		//카트데이터
		$data = $cart->where([			
				['mb_email', '=', $mb_email],
				['ct_status', '=', 'shopping'],
				['ct_select', '=', 'Y']
				])->get();
		
		//내부상품만 결제 하는지 확인
		foreach($data as $row)
		{			
			//교보(kyobo) 상품있는경우 mix 처리
			//if($row->ct_type == 'in')
			if($row->ct_origin == 'kyobo')
			{
				$flag_mixed_item = true;
			}
			
			//도서(kyobo)외 상품이 있는경우
			if($row->ct_origin != 'kyobo')
			{
				$flag_in_item = false;
				//break;
			}
			
			//무게 정보 로드 (in type 일 경우에만 상품 정보 검색 가능)
			$item_data = DB::table('items')->select('it_weight')->where('it_id',$row->it_id)->get();
			if(isset($item_data[0])) $it_weight = $item_data[0]->it_weight; else $it_weight = 0;
			$total_weight += $it_weight * $row->ct_qty;
		}
		
		
		//쿠폰데이터		
		$cp_data = DB::table('coupons')
				->where([			
				['mb_email', '=', $mb_email],
				['cp_used_date', '=', NULL]
				])->get();
		
		//배송사		
		$carrier_data = DB::table('define_carriers')
						->get();
		
		//사이트 설정				
		$setting_data = DB::table('define_settings')
				->where([			
				['setting_key', '=', 'service_fee']
				])->get();
				
		//기본배송지 설정
		$default_addr_data = DB::table('user_addrs')
		->where([
		['mb_email','=',session('mb_email')],
		['addr_default','=','Y']
		])->limit(1)->get();
				
		return view('/checkout')->with(['data'=>$data, 'cp_data'=>$cp_data, 'carrier_data'=>$carrier_data, 'service_fee' => $setting_data[0]->setting_value, 'default_addr_data'=> $default_addr_data, 
		'flag_in_item'=>$flag_in_item, 'flag_mixed_item'=>$flag_mixed_item, 'total_weight' => $total_weight]);
	}
	
	public function domestic_payment_request(Request $request, Cart $cart, Item $item)
	{
		//AJAX
		$card_code = $request->input('card_code');
		$installment_months = $request->input('installment_months');
		$mb_email = session('mb_email');		
		$card_number = str_replace("-","",$request->input('card_number'));
		$amount_due = $request->input('amount_due');
		$od_first_name = $request->input('od_first_name');
		$od_last_name = $request->input('od_last_name');
		$od_addr1 = $request->input('od_addr1');
		$od_addr2 = $request->input('od_addr2');
		$od_city = $request->input('od_city');
		$od_state = $request->input('od_state');
		$od_zip = $request->input('od_zip');
		$od_country = $request->input('od_country');
		$od_tel = $request->input('od_phone');
		$od_hp = $request->input('od_hp');
		$od_coupon = $request->input('od_coupon');
		$od_hope_carrier = $request->input('od_hope_carrier');
		$od_delivery_option = $request->input('od_delivery_option');
		if($od_country != 'South Korea') $od_delivery_option = ''; //한국 배송에만 해당 옵션 사용됨
		$get_intl_shipping = $request->input('od_intl_shipping');
		$get_sales_tax = $request->input('od_sales_tax');
		
		//회원 ID 넘버, 결제사에서 필수로 요청함
		$member_data = DB::table('members')
		->where([
		['mb_email','=',$mb_email]		
		])->limit(1)->get();		
		
		$mb_no = $member_data[0]->mb_no;
				
		//service fee 데이터		
		$service_fee = Config::get('service_fee.fee');
		$service_fee = $service_fee	* Config::get('rate.usd');
				
		$cart_data = $cart->where([			
					['mb_email', '=', $mb_email],
					['ct_status', '=', 'shopping'],
					['ct_select', '=', 'Y']
					])->get();
		
		//계산값 초기화			
		$od_cart_price = 0;
		$od_cart_coupon = 0;
		$od_service_fee = 0;
		$od_send_cost = 0;
		$od_send_cost2 = 0;
		$od_item_count = 0;
		
		//1차결제만 하는 주문 변수 초기화
		$flag_in_item = true;
		$od_total_weight = 0;
		$od_sales_tax = 0;
		$od_intl_shipping = 0;
		$od_item_short_name = '';
		
		//카트데이터 계산
		$arr_ct_id ;
		foreach($cart_data as $row)
		{
			//내부상품만 주문했는지 확인
			if($row->ct_type != 'in') $flag_in_item = false;
					
			$od_cart_price += ($row->ct_price * $row->ct_qty);
			//$od_service_fee +=	($service_fee * $row->ct_qty);
			$od_service_fee += $service_fee;
			$od_send_cost += $row->ct_send_cost;
			$od_item_count += 1;
			$arr_ct_id[] = $row->ct_id;
			if($od_item_short_name == '') $od_item_short_name = mb_substr($row->it_name,0,64).'...';
		}

		/*
		//회원 ID 넘버, 결제사에서 필수로 요청함
		$member_data = DB::table('members')
		->where([
		['mb_email','=',$mb_email]		
		])->limit(1)->get();		
		
		$mb_no = $member_data[0]->mb_no;
				
		//service fee 데이터		
		$service_fee = Config::get('service_fee.fee');
		$service_fee = $service_fee	* Config::get('rate.usd');
				
		$cart_data = $cart->where([			
					['mb_email', '=', $mb_email],
					['ct_status', '=', 'shopping'],
					['ct_select', '=', 'Y']
					])->get();
		
		//계산값 초기화			
		$od_cart_price = 0;
		$od_cart_coupon = 0;
		$od_service_fee = 0;
		$od_send_cost = 0;
		$od_item_count = 0;
		$od_item_short_name = '';
		
		$arr_ct_id;
		foreach($cart_data as $row)
		{
			$od_cart_price += ($row->ct_price * $row->ct_qty);
			$od_service_fee +=	$service_fee;
			$od_send_cost += $row->ct_send_cost;
			$od_item_count += 1;
			$arr_ct_id[] = $row->ct_id;
			if($od_item_short_name == '') $od_item_short_name = substr($row->it_name,0,64).'...';
		}*/
		
		//지정된 상품 개수보다 적으면 기본 서비스피로 부과, 상수값이 달러 기준으로 되어있으므로 재설정 할때 환율 다시 곱해줌
		if($od_item_count <= Config::get('service_fee.min')) $od_service_fee = Config::get('service_fee.min_price') * Config::get('rate.usd');
		$od_service_fee = round($od_service_fee); //절사처리

		//쿠폰데이터
		if($od_coupon)
		{
			$cp_data = DB::table('coupons')
					->where([			
					['mb_email', '=', $mb_email],
					['cp_used_date', '=', NULL],
					['id', '=', $od_coupon]
					])->get();

			if(!isset($cp_data[0]))
			{
				return response()->json(array('result'=> 0, 'msg'=> '선택하신 쿠폰을 찾을수 없습니다. 결제가 중단됩니다'), 200);
				exit;
			}
			else
			{
				$od_cart_coupon = $cp_data[0]->cp_amount;
				$od_cart_coupon = round($od_cart_coupon * Config::get('rate.usd'));	//USD -> KRW 환전시 절사처리
			}
		}	
		
		//구매금액 토탈
		$od_total = round($od_cart_price + $od_send_cost + $od_service_fee); // PG 측에서 KRW 소숫점 단위를 허용안하기 때문에 절사한다. 달러를 KRW로 바꾸는 부분은 대부분 절사처리 해줘야 할듯
		
		//최종 결제금액 (상품금액 + 국내배송비 + 서비스차지 - 쿠폰금액)		
		$od_pay_due = $od_total - $od_cart_coupon ;	
		
		//1차결제만 하는 주문일경우 무게와, 국제배송비 계산
		if($flag_in_item == true)
		{
			$sales_tax_rate = 0;
			//주문 무게 계산, it_weight 없을떄 로직을 프론트와 일치시켜야 하는것 주의.
			foreach($cart_data as $row)
			{
				$it_data = $item->where('it_id',$row->it_id)->limit(1)->get();
				$od_total_weight += $it_data[0]->it_weight * $row->ct_qty;
			}
			
			//국제 배송비 계산
			$shipping_data = DB::table('shipping_rates')
			->select(DB::raw('MIN(sr_price) AS sr_price'))
			->where([
			['sr_country','=', $od_country],
			['sr_carrier','=', $od_hope_carrier],		
			['sr_weight', '>=', $od_total_weight]
			])->get();				
			
			$od_intl_shipping = $shipping_data[0]->sr_price;
			$od_send_cost2 = $od_intl_shipping;
			
			//국가별 Sales Tax 계산 (주문금액 - 쿠폰 + 서비스피 + 국제배송료) * 국가별 요율
			$sales_tax_data = DB::table('define_countries')
			->select('country_sales_tax')
			->where([
			['country_name','=', $od_country]
			])->get();
			
			$sales_tax_rate = $sales_tax_data[0]->country_sales_tax;			
			$od_sales_tax = round(($od_pay_due + $od_intl_shipping) * $sales_tax_rate);
			
			//*********** 1차결제만 할 경우 최종 결제금액 ***************/
			$od_pay_due = $od_pay_due + $od_intl_shipping + $od_sales_tax;
		}
		
		//프론트쪽 계산과 백엔드쪽 계산이 일치하는지 확인하기 위한 값
		$amount_due_with_extra = $amount_due - $od_cart_coupon + $get_intl_shipping + $get_sales_tax;
			
		//로그 입력
		$raw_input = $request->all();
		$arr_raw_input = print_r($raw_input,true);
		
		$result = DB::table('process_logs')
				->insert(['route'=> 'payment',
				'raw'=>$arr_raw_input, 
				'mb_email'=>$mb_email, 
				'ip'=>$_SERVER['REMOTE_ADDR']
				]);
		
		//-------Valid check--------					
		//amount 비교 검증		
		//if($amount_due != $od_total)
		if($amount_due_with_extra != $od_pay_due)
		{
			return response()->json(array('result'=> 0, 'msg'=> '결제 금액이 상이합니다. 관리자에게 문의하여주십시오'), 200);
			exit;
		}
						
		//주문서 생성하고
		$new_od_id = date("ymd-His-").str_pad(mt_rand(0,9999),4,"0",STR_PAD_LEFT);	
		
		/*
		$result = DB::table('orders')
		->insert(['od_id'=>$new_od_id, 
		'od_item_count'=> $od_item_count,
		'mb_email'=>$mb_email, 
		'od_first_name'=>$od_first_name, 
		'od_last_name'=>$od_last_name, 
		'od_tel'=>$od_tel, 
		'od_hp'=>$od_hp, 
		'od_country'=>$od_country, 
		'od_city'=>$od_city, 
		'od_state'=>$od_state, 
		'od_zip'=>$od_zip, 
		'od_addr1'=>$od_addr1, 
		'od_addr2'=>$od_addr2, 
		'od_service_fee'=>$od_service_fee,
		'od_send_cost1'=>$od_send_cost,
		'od_cart_price'=>$od_cart_price, 
		'od_cart_coupon'=>$od_cart_coupon, 
		'od_hope_carrier'=>$od_hope_carrier,
		'od_delivery_option'=>$od_delivery_option,
		'od_status' => 'paystart',
		'created_at'=>now()
		]);*/
		
		$result = DB::table('orders')
		->insert(['od_id'=>$new_od_id, 
		'od_item_count'=> $od_item_count,
		'mb_email'=>$mb_email, 
		'od_first_name'=>$od_first_name, 
		'od_last_name'=>$od_last_name, 
		'od_tel'=>$od_tel, 
		'od_hp'=>$od_hp, 
		'od_country'=>$od_country, 
		'od_city'=>$od_city, 
		'od_state'=>$od_state, 
		'od_zip'=>$od_zip, 
		'od_addr1'=>$od_addr1, 
		'od_addr2'=>$od_addr2, 
		'od_service_fee'=>$od_service_fee,
		'od_send_cost1'=>$od_send_cost,
		'od_send_cost2'=>$od_send_cost2,
		'od_cart_price'=>$od_cart_price, 
		'od_cart_coupon'=>$od_cart_coupon, 
		'od_hope_carrier'=>$od_hope_carrier,
		'od_delivery_option'=>$od_delivery_option,
		'od_sales_tax' => $od_sales_tax,
		'od_weight_due'=> $od_total_weight,
		'od_status' => 'paystart',
		'created_at'=>Carbon::now()
		]);
		
			
		if($result)
		{			
			//결제 로그 선 삽입
			$result = DB::table('pay_logs')
			->insert([
			'od_id' => $new_od_id,
			'pay_pg'=> 'erompay_auth',
			'pay_type'=> 'charge',
			'pay_method'=>'card',
			'pay_card_code'=>$card_code, 
			'pay_for'=>'1st_charge',
			'pay_amount'=>$od_pay_due, 
			'pay_tid'=>'', 
			'pay_return_code'=>'', 
			'pay_status'=>'ready', 
			'pay_raw'=>'', 			
			'created_at'=>now()
			]);
			
			if($result)
			{			
				//결제요청				
				$pay = new PayErom();	
				$pay_result = $pay->domesticPayment($mb_no, $card_code, $installment_months, $new_od_id, $od_pay_due, $od_item_short_name, $od_coupon, $flag_in_item);				
				$arr_pay_result = json_decode($pay_result,true);	
				$pay_result_code = $arr_pay_result['result_code'];
				$pay_result_message = $arr_pay_result['result_message'];
				
				//결과값 결제로그 삽입	
				$result = DB::table('pay_logs')
				->insert([
				'od_id' => $new_od_id,
				'pay_pg'=> 'erompay_auth',
				'pay_type'=> 'charge',
				'pay_method'=>'card', 
				'pay_card_code'=>$card_code, 
				'pay_for'=>'1st_charge',
				'pay_amount'=>$od_pay_due, 
				'pay_tid'=>'', 
				'pay_return_code'=>$pay_result_code, 
				'pay_status'=>'done', 
				'pay_raw'=>$pay_result, 			
				'created_at'=>now()
				]);				
							
				if($pay_result_code == 'TS1000')
				{
					//요청성공
					foreach($arr_ct_id as $tgt_ct_id)
					{
						$cart->where([			
						['ct_id', '=', $tgt_ct_id]
						])->update(['od_id' => $new_od_id]);
					}

                    /**
                     * TODO : esummer, 2021-05-06, 사용자 결제시 파트너 시스템으로 결제정보 넘김
                     */

                    try {
                        $_PID = Cookie::get('cpsKEY');
                        if($_PID != '') {
                            setOrderPartners($_PID, $new_od_id, $mb_email);
                        }
                    } catch (Exception $error) {

                    }

					return response()->json(array('result'=> 1, 'msg'=> 'done','payment_url'=>$arr_pay_result['payment_url']), 200);
				}
				else
				{
					//실패시 상단에서 생성한 오더 레코드 삭제
					$result = DB::table('orders')
					->where('od_id', $new_od_id)
					->delete();
				
					return response()->json(array('result'=> 0, 'err_code'=>$pay_result_code, 'msg'=> $pay_result_message), 200);
				}							
			}
			else
			{
				//실패시 상단에서 생성한 오더 레코드 삭제
				$result = DB::table('orders')
				->where('od_id', $new_od_id)
				->delete();
				
				return response()->json(array('result'=> 0, 'msg'=> '결제 로그 생성 실패(Ready)'), 200);	
			}					
		}
		else
		{
			return response()->json(array('result'=> 0, 'msg'=> '주문서 생성에 문제가 발생하였습니다'), 200);	
		}
		
		return false;	
	}
	
	public function payment_approve(Cart $cart, Request $request)
	{
		//echo "<img src='/image/hori_loading.gif'> Payment Processing... Please wait...";
		
		//$mb_email = session('mb_email');
		$new_od_id = $request->input('order_id');	
		$pay_tid = $request->input('transaction_id');
		$pay_result_code = $request->input('result_code');
		$pay_result_message = $request->input('result_message');
		$pay_result_amount = $request->input('processing_amount');
		$od_pay_due = $request->input('processing_amount');
		$od_coupon = '';
		$flag_in_item = false;
		
		if(!$new_od_id) 
		{
			echo '필수데이터가 존재하지 않습니다';
			exit;
		}
		
		$od_data = DB::table('orders')
		->where('od_id', $new_od_id)
		->get();
		
		$od_receipt_price1 = $od_data[0]->od_cart_price + $od_data[0]->od_service_fee + $od_data[0]->od_send_cost1 - $od_data[0]->od_cart_coupon;
		$od_receipt_price2 = $pay_result_amount - $od_receipt_price1;
		
		//결제 모듈에서 커스텀 필드값이 리턴이 안되는것 같아, 일단 1차만 결제하는 주문을 구분하는 방법은 1차 결제 금액과 리턴된 금액의 차이가 있으면 국제 배송비도 이미 냈다고 판단한다.
		if($od_receipt_price2 > 0) $flag_in_item = true;
	
		if($pay_result_code == 'TS1000')
		{
			//카트 status 업데이트 . 인증결제의 경우 요청시 미리 카트에 설정해둔 od_id를 기준으로 결제상태를 변경한다. 관련 데이터를 별도의 테이블로 분리 해도 될것 같다.
			$cart->where([			
			['od_id', '=', $new_od_id]
			])->update(['ct_status' => 'paid', 'ct_house_status' => 'buy_wait']);
						
			//주문서 업데이트
			$result = DB::table('orders')
			->where('od_id', $new_od_id)
			->update(['od_pay_id1'=>$pay_tid,'od_status'=>'paid', 'od_receipt_price1' => $od_receipt_price1, 'od_rate1'=> Config::get('rate.usd')]);
			
			//1차결제만 하는 주문의 경우 2차 배송비 관련 결제완료 정보도 입력해준다.
			if($flag_in_item == true)
			{
				$result = DB::table('orders')
				->where('od_id', $new_od_id)
				->update(['od_pay_id2'=>$pay_tid,'od_ship_paid'=>'Y', 'od_receipt_price2' => $od_receipt_price2, 'od_rate2'=> Config::get('rate.usd')]);
			}
			
			//쿠폰사용처리
			if($od_coupon)
			{
				DB::table('coupons')
				->where([			
				//['mb_email', '=', $mb_email],
				['id', '=', $od_coupon],
				['cp_used_od_id', '=', $new_od_id]
				])
				->update(['cp_used_date'=> now()]);
			}
		}
		else
		{
			//실패시 상단에서 생성한 오더 레코드 삭제
			$result = DB::table('orders')
			->where('od_id', $new_od_id)
			->delete();
			
			echo "Payment Failed ($pay_result_code : $pay_result_message)"; 
		}
		
		//결과값 결제로그 삽입	
		$pay_raw = print_r($request->all(),true);
		$result = DB::table('pay_logs')
		->insert([
		'od_id' => $new_od_id,
		'pay_pg'=> 'erompay_auth',
		'pay_type'=> 'charge',
		'pay_method'=>'card', 
		'pay_for'=>'1st_charge',
		'pay_amount'=>$od_pay_due, 
		'pay_tid'=>$pay_tid, 
		'pay_return_code'=>$pay_result_code, 
		'pay_status'=>'done', 
		'pay_raw'=>$pay_raw, 			
		'created_at'=>now()
		]);
		
		//redirect
		if($pay_result_code == 'TS1000')
		{
			/*echo "<script>window.opener.location.href='/order/view/$new_od_id';self.close();</script>";
			return redirect("/order/view/$new_od_id");*/
			return view('/pay_result')->with(['new_od_id'=>$new_od_id]);
			exit;
		}	
	}
	
	public function payment(Cart $cart, Item $item, Request $request)
	{
		//결제 애니메이션 노출
		//echo "<img src='/image/hori_loading.gif'> <br>결제 진행중... 결제가 종료될때까지 기다려주십시오.<br>";		
				
		$mb_email = session('mb_email');		
		$card_number = str_replace("-","",$request->input('card_number'));
		$card_month = $request->input('card_mm');
		$card_year = $request->input('card_yy');
		$card_cvv = $request->input('card_cvv');
		$card_holder = $request->input('card_holder');
		$amount_due = $request->input('amount_due');		
		$od_first_name = $request->input('od_first_name');
		$od_last_name = $request->input('od_last_name');
		$od_addr1 = $request->input('od_addr1');
		$od_addr2 = $request->input('od_addr2');
		$od_city = $request->input('od_city');
		$od_state = $request->input('od_state');
		$od_zip = $request->input('od_zip');
		$od_country = $request->input('od_country');
		$od_tel = $request->input('od_phone');
		$od_hp = $request->input('od_hp');
		$od_coupon = $request->input('od_coupon');
		$od_hope_carrier = $request->input('od_hope_carrier');		
		$od_delivery_option = $request->input('od_delivery_option');
		if($od_country != 'South Korea') $od_delivery_option = ''; //한국 배송에만 해당 옵션 사용됨
		$get_intl_shipping = $request->input('od_intl_shipping');
		$get_sales_tax = $request->input('od_sales_tax');
				
		//service fee 데이터		
		$service_fee = Config::get('service_fee.fee');
		$service_fee = $service_fee	* Config::get('rate.usd');
				
		$cart_data = $cart->where([			
					['mb_email', '=', $mb_email],
					['ct_status', '=', 'shopping'],
					['ct_select', '=', 'Y']
					])->get();
		
		//계산값 초기화			
		$od_cart_price = 0;
		$od_cart_coupon = 0;
		$od_service_fee = 0;
		$od_send_cost = 0;
		$od_send_cost2 = 0;
		$od_item_count = 0;
		
		//1차결제만 하는 주문 변수 초기화
		$flag_in_item = true;
		$od_total_weight = 0;
		$od_sales_tax = 0;
		$od_intl_shipping = 0;
		
		//카트데이터 계산
		$arr_ct_id ;
		foreach($cart_data as $row)
		{
			//내부상품만 주문했는지 확인
			if($row->ct_type != 'in') $flag_in_item = false;
					
			$od_cart_price += ($row->ct_price * $row->ct_qty);
			//$od_service_fee +=	($service_fee * $row->ct_qty);
			$od_service_fee += $service_fee;
			$od_send_cost += $row->ct_send_cost;
			$od_item_count += 1;
			$arr_ct_id[] = $row->ct_id;
		}	
		
		//지정된 상품 개수보다 적으면 기본 서비스피로 부과, 상수값이 달러 기준으로 되어있으므로 재설정 할때 환율 다시 곱해줌
		if($od_item_count <= Config::get('service_fee.min')) $od_service_fee = Config::get('service_fee.min_price') * Config::get('rate.usd');
		$od_service_fee = round($od_service_fee); //절사처리
		
		//쿠폰데이터
		if($od_coupon)
		{
			$cp_data = DB::table('coupons')
					->where([			
					['mb_email', '=', $mb_email],
					['cp_used_date', '=', NULL],
					['id', '=', $od_coupon]
					])->get();

			if(!isset($cp_data[0]))
			{
				echo "선택하신 쿠폰을 찾을수 없습니다. 결제가 중단됩니다";
				exit;
			}
			else
			{
				$od_cart_coupon = $cp_data[0]->cp_amount;
				$od_cart_coupon = round($od_cart_coupon * Config::get('rate.usd'));	//USD -> KRW 환전시 절사처리
			}
		}	
		
		//구매금액 토탈
		$od_total = round($od_cart_price + $od_send_cost + $od_service_fee); // PG 측에서 KRW 소숫점 단위를 허용안하기 때문에 절사한다. 달러를 KRW로 바꾸는 부분은 대부분 절사처리 해줘야 할듯
		
		//**************** 최종 결제금액 (상품금액 + 국내배송비 + 서비스차지 - 쿠폰금액)************/		
		$od_pay_due = $od_total - $od_cart_coupon;	
				
		//1차결제만 하는 주문일경우 무게와, 국제배송비 계산
		if($flag_in_item == true)
		{
			$sales_tax_rate = 0;
			//주문 무게 계산, it_weight 없을떄 로직을 프론트와 일치시켜야 하는것 주의.
			foreach($cart_data as $row)
			{
				$it_data = $item->where('it_id',$row->it_id)->limit(1)->get();
				$od_total_weight += $it_data[0]->it_weight * $row->ct_qty;
			}
			
			//국제 배송비 계산
			$shipping_data = DB::table('shipping_rates')
			->select(DB::raw('MIN(sr_price) AS sr_price'))
			->where([
			['sr_country','=', $od_country],
			['sr_carrier','=', $od_hope_carrier],		
			['sr_weight', '>=', $od_total_weight]
			])->get();				
			
			$od_intl_shipping = $shipping_data[0]->sr_price;
			$od_send_cost2 = $od_intl_shipping;
			
			//국가별 Sales Tax 계산 (주문금액 - 쿠폰 + 서비스피 + 국제배송료) * 국가별 요율
			$sales_tax_data = DB::table('define_countries')
			->select('country_sales_tax')
			->where([
			['country_name','=', $od_country]
			])->get();
			
			$sales_tax_rate = $sales_tax_data[0]->country_sales_tax;			
			$od_sales_tax = round(($od_pay_due + $od_intl_shipping) * $sales_tax_rate);
			
			//*********** 1차결제만 할 경우 최종 결제금액 ***************/
			$od_pay_due = $od_pay_due + $od_intl_shipping + $od_sales_tax;
		}
		
		//프론트쪽 계산과 백엔드쪽 계산이 일치하는지 확인하기 위한 값
		$amount_due_with_extra = $amount_due - $od_cart_coupon + $get_intl_shipping + $get_sales_tax;
							
		//로그 입력
		$raw_input = $request->all();
		$arr_raw_input = print_r($raw_input,true);
		
		$result = DB::table('process_logs')
				->insert(['route'=> 'payment',
				'raw'=>$arr_raw_input, 
				'mb_email'=>$mb_email, 
				'ip'=>$_SERVER['REMOTE_ADDR']
				]);
		
		//-------Valid check--------					
		//amount 비교 검증		
		//if($amount_due != $od_total)
		if($amount_due_with_extra != $od_pay_due)
		{
			echo "결제 금액이 상이합니다. 관리자에게 문의하여주십시오";
			exit;
		}
						
		//주문서 생성하고
		$new_od_id = date("ymd-His-").str_pad(mt_rand(0,9999),4,"0",STR_PAD_LEFT);
				
		$result = DB::table('orders')
		->insert(['od_id'=>$new_od_id, 
		'od_item_count'=> $od_item_count,
		'mb_email'=>$mb_email, 
		'od_first_name'=>$od_first_name, 
		'od_last_name'=>$od_last_name, 
		'od_tel'=>$od_tel, 
		'od_hp'=>$od_hp, 
		'od_country'=>$od_country, 
		'od_city'=>$od_city, 
		'od_state'=>$od_state, 
		'od_zip'=>$od_zip, 
		'od_addr1'=>$od_addr1, 
		'od_addr2'=>$od_addr2, 
		'od_service_fee'=>$od_service_fee,
		'od_send_cost1'=>$od_send_cost,
		'od_send_cost2'=>$od_send_cost2,
		'od_cart_price'=>$od_cart_price, 
		'od_cart_coupon'=>$od_cart_coupon, 
		'od_hope_carrier'=>$od_hope_carrier,
		'od_delivery_option'=>$od_delivery_option,
		'od_sales_tax' => $od_sales_tax,
		'od_weight_due'=> $od_total_weight,
		'od_status' => 'paystart',
		'created_at'=>Carbon::now()
		]);
				
		if($result)
		{			
			//결제 로그 선 삽입
			$result = DB::table('pay_logs')
			->insert([
			'od_id' => $new_od_id,
			'pay_pg'=> 'erompay',
			'pay_type'=> 'charge',
			'pay_method'=>'card', 
			'pay_for'=>'1st_charge',
			'pay_amount'=>$od_pay_due, 
			'pay_tid'=>'', 
			'pay_return_code'=>'', 
			'pay_status'=>'ready', 
			'pay_raw'=>'', 			
			'created_at'=>Carbon::now()
			]);

            if($result)
			{			
				//결제 진행
				$pay = new PayErom();	
				$pay_result = $pay->approvePayment($card_number,$card_month,$card_year,$card_cvv,$card_holder,$new_od_id,$od_pay_due); //json data				
				$arr_pay_result = json_decode($pay_result,true);
				$pay_tid = $arr_pay_result['transaction_id'];
				$pay_result_code = $arr_pay_result['result_code'];
				$pay_result_message = $arr_pay_result['result_message'];
				
				if($pay_result_code == 'TS1000')
				{
					echo "결제성공";
					//카트 od id / status 업데이트
					foreach($arr_ct_id as $tgt_ct_id)
					{
						$cart->where([			
						['ct_id', '=', $tgt_ct_id]
						])->update(['od_id' => $new_od_id, 'ct_status' => 'paid', 'ct_house_status' => 'buy_wait']);
					}
					
					//주문서 업데이트
					$result = DB::table('orders')
					->where('od_id', $new_od_id)
					->update(['od_pay_id1'=>$pay_tid,'od_status'=>'paid', 'od_receipt_price1' => $od_pay_due, 'od_rate1'=> Config::get('rate.usd')]);
					
					//1차결제만 하는 주문의 경우 2차 배송비 관련 결제완료 정보도 입력해준다.
					if($flag_in_item == true)
					{
						$result = DB::table('orders')
						->where('od_id', $new_od_id)
						->update(['od_pay_id2'=>$pay_tid,'od_ship_paid'=>'Y', 'od_receipt_price2' => ($od_send_cost2+$od_sales_tax), 'od_rate2'=> Config::get('rate.usd')]);
					}
					
					//쿠폰사용처리
					if($od_coupon)
					{
						DB::table('coupons')
						->where([			
						['mb_email', '=', $mb_email],
						['id', '=', $od_coupon]
						])
						->update(['cp_used_date'=> Carbon::now()]);
					}

                    /**
                     * TODO : esummer, 2021-05-06, 사용자 결제시 파트너 시스템으로 결제정보 넘김
                     */

                    $_PID = Cookie::get('cpsKEY');
                    if($_PID != '') {
                        setOrderPartners($_PID, $new_od_id, $mb_email);
                    }

                    try{
                        /*이메일 인증에 성공하면 슬랙 메시지를 보내줌*/
                        sendSlackNotification("ORDER", 'Order Completed - #1 ' . $new_od_id, 'https://www.eckorea.net/admin/buy?status=buy_wait');
                    } catch(Exception $e){

                    } finally {

                    }

                    /**
                     * TODO : esummer, 2021-05-27, 사용자 결제시 푸시 PUSH 메시지 전송
                     */
                    $email = $mb_email;
                    sendMobileNotification(
                        $email
                        , 'order'
                        , '[eckorea.net] Your order has been successfully received'
                        , '[eckorea.net] Your order has been successfully received. Click the button below to check your order details.'
                        ,'/order/list'
                    );
				}
				else
				{
					//실패시 상단에서 생성한 오더 레코드 삭제
					$result = DB::table('orders')
					->where('od_id', $new_od_id)
					->delete();
					
					echo "Payment Failed ($pay_result_code : $pay_result_message)"; 
				}
				
				//결과값 결제로그 삽입	
				$result = DB::table('pay_logs')
				->insert([
				'od_id' => $new_od_id,
				'pay_pg'=> 'erompay',
				'pay_type'=> 'charge',
				'pay_method'=>'card', 
				'pay_for'=>'1st_charge',
				'pay_amount'=>$od_pay_due, 
				'pay_tid'=>$pay_tid, 
				'pay_return_code'=>$pay_result_code, 
				'pay_status'=>'done', 
				'pay_raw'=>$pay_result, 			
				'created_at'=>Carbon::now()
				]);
				
				//redirect
				if($pay_result_code == 'TS1000')
				{
                    return redirect("/order/view/$new_od_id");
				}				
			}
			else
			{
				//실패시 상단에서 생성한 오더 레코드 삭제
				$result = DB::table('orders')
				->where('od_id', $new_od_id)
				->delete();
					
				echo "결제 로그 생성 실패(Ready)";
			}					
		}
		else
		{
			echo "주문서 생성에 문제가 발생하였습니다";
		}				
	}

	public function checkout_shipping(Order $order, Request $request, $od_id)
	{
		$data = $order->join('carts','carts.od_id', '=', 'orders.od_id')->where('carts.od_id',$od_id)->get();
		
		return view('/extra_payment')->with(['data'=>$data]);
	}
	
	public function payment_shipping(Order $order, Request $request)
	{
		//결제 애니메이션 노출
		//echo "<img src='/image/hori_loading.gif'> <br>결제 진행중... 결제가 종료될때까지 기다려주십시오.<br>";		
		
		$od_id = $request->input('od_id');	
		$mb_email = session('mb_email');		
		$card_number = str_replace("-","",$request->input('card_number'));
		$card_month = $request->input('card_mm');
		$card_year = $request->input('card_yy');
		$card_cvv = $request->input('card_cvv');
		$card_holder = $request->input('card_holder');
		$amount = $request->input('amount');				
		//$od_coupon = $request->input('od_coupon');		
		
		$od_data = $order->where('od_id',$od_id)->get();
		$od_send_cost = $od_data[0]->od_send_cost2;
		$od_sales_tax = $od_data[0]->od_sales_tax;
		
		//$od_send_due = round(($od_send_cost + $od_sales_tax) * Config::get('rate.usd'));  //USD-> KRW 절사처리	
		$od_send_due = $od_send_cost + $od_sales_tax;
		$od_cart_coupon = 0;
		
		/*
		//쿠폰데이터
		if($od_coupon)
		{
			$cp_data = DB::table('coupons')
					->where([			
					['mb_email', '=', $mb_email],
					['cp_used_date', '=', NULL],
					['id', '=', $od_coupon]
					])->get();
					
			if($cp_data[0]->cp_amount == '')
			{
				echo "선택하신 쿠폰을 찾을수 없습니다. 결제가 중단됩니다";
				exit;
			}
			else
			{
				$od_cart_coupon = $cp_data[0]->cp_amount;
			}
		}*/	
			
		$od_pay_due = $od_send_due - $od_cart_coupon;	
		//Valid check	
		
		
		//로그 입력
		$raw_input = $request->all();
		$arr_raw_input = print_r($raw_input,true);
		
		$result = DB::table('process_logs')
				->insert(['route'=> 'payment',
				'raw'=>$arr_raw_input, 
				'mb_email'=>$mb_email, 
				'ip'=>$_SERVER['REMOTE_ADDR']
				]);	
		
		if($result)
		{			
			//결제 로그 선 삽입
			$result = DB::table('pay_logs')
			->insert([
			'od_id' => $od_id,
			'pay_pg'=> 'erompay',
			'pay_type'=> 'charge',
			'pay_method'=>'card', 
			'pay_for'=>'shipping',
			'pay_amount'=>$od_pay_due, 
			'pay_tid'=>'', 
			'pay_return_code'=>'', 
			'pay_status'=>'ready', 
			'pay_raw'=>'', 			
			'created_at'=>Carbon::now()
			]);
			
			if($result)
			{			
				//결제 진행
				$pay = new PayErom();	
				$pay_result = $pay->approvePayment($card_number,$card_month,$card_year,$card_cvv,$card_holder,$od_id,$od_pay_due); //json data				
				$arr_pay_result = json_decode($pay_result,true);
				$pay_tid = $arr_pay_result['transaction_id'];
				$pay_result_code = $arr_pay_result['result_code'];
				$pay_result_message = $arr_pay_result['result_message'];
				
				if($pay_result_code == 'TS1000')
				{
					echo "결제성공";		
					
					//주문서 업데이트
					$result = DB::table('orders')
					->where('od_id', $od_id)
					->update(['od_pay_id2' => $pay_tid, 'od_receipt_price2' => $od_pay_due, 'od_rate2'=> Config::get('rate.usd'), 'od_ship_paid'=>'Y']);
					
					//쿠폰사용처리
					/*
					if($od_coupon)
					{
						DB::table('coupons')
						->where([			
						['mb_email', '=', $mb_email],
						['id', '=', $od_coupon]
						])
						->update(['cp_used_date'=> Carbon::now()]);
					}*/
				}
				else
				{										
					echo "Payment Failed ($pay_result_code : $pay_result_message)"; 
				}
				
				//결과값 결제로그 삽입	
				$result = DB::table('pay_logs')
				->insert([
				'od_id' => $od_id,
				'pay_pg'=> 'erompay',
				'pay_type'=> 'charge',
				'pay_method'=>'card', 
				'pay_for'=>'shipping',
				'pay_amount'=>$od_pay_due, 
				'pay_tid'=>$pay_tid, 
				'pay_return_code'=>$pay_result_code, 
				'pay_status'=>'done', 
				'pay_raw'=>$pay_result, 			
				'created_at'=>Carbon::now()
				]);
				
				//redirect
				if($pay_result_code == 'TS1000')
				{
					return redirect("/order/view/$od_id");
				}				
			}
			else
			{					
				echo "결제 로그 생성 실패(Ready)";
			}					
		}
		else
		{
			echo "로그 생성에 문제가 발생하였습니다";
		}				
	}
	
	public function view(Order $order, Request $request, $od_id)
	{
		$mb_email = session('mb_email');
		$data = $order->join('carts','carts.od_id', '=', 'orders.od_id')->where('carts.od_id',$od_id)->get();
		$pay_data = DB::table('pay_logs')->where([
		['od_id', '=', $od_id],
		['pay_return_code', '=', 'TS1000']
		])->get();
		return view('/order_view')->with(['data'=>$data, 'pay_data'=>$pay_data]);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }
	
	public function show_admin(Order $order, $od_id)
    {
		$data = $order		
		->join('carts','carts.od_id', '=', 'orders.od_id')		
		//->select('orders.*','carts.it_name')
		->where('orders.od_id', $od_id)
		->get();
		
		$carrier_data = DB::table('define_carriers')
						->get();
		
		$ship_pay_log = DB::table('pay_logs')
						->where([
						['od_id', '=', $od_id],
						['pay_for','=', 'shipping'],
						['pay_return_code', '=', 'TS1000']
						])
						->limit(1)
						->get();
						
		$mod_log = DB::table('order_mod_logs')
						->where([
						['od_id', '=', $od_id]
						])
						->get();
				
		return view('/admin/order_view')->with(['data'=>$data, 'carrier_data'=>$carrier_data, 'ship_pay_log'=>$ship_pay_log, 'mod_log'=>$mod_log]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }
	
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
	
	public function update_order_status(Request $request, Order $order)
	{	
		//AJAX
		$method = $request->input('method');
		$od_id = $request->input('od_id');
		$status = $request->input('status');
		
		//update
		$order->where([
		['od_id', '=', $od_id]
		])->update(['od_status' => $status]);
		
		$raw_input = print_r($request->all(),true);
		$this->order_mod_log($od_id, null, $status, $raw_input);
		
		return response()->json(array('result'=> 1, 'msg'=> '변경완료'), 200);
	}
	
	public function update_ct_status(Request $request, Order $order, Cart $cart)
	{	
		//AJAX
		$method = $request->input('method');
		$ct_id = $request->input('ct_id');
		$status = $request->input('status');
		$ct_house_memo = $request->input('ct_house_memo');
		//$ct_local_ship_info = $request->input('ct_local_ship_info');
		$msg = '';
		
		//카트정보 로드
		$row = $cart->where([
		['ct_id', '=', $ct_id]
		])->get();		
		
		//환불처리 위한 데이터
		$od_id = $row[0]->od_id;
		$ct_pg_refunded = $row[0]->ct_pg_refunded;	
		
		$json_status_time = $row[0]['ct_house_status_time_json'];
		$arr_status_time = json_decode($json_status_time,true);
		$arr_status_time[$status] = date("Y-m-d H:i:s");
		$new_json_status_time = json_encode($arr_status_time);
		
		//상태메모 셋업, method 단위로 
		$json_status_memo = $row[0]['ct_house_memo_json'];
		$arr_status_memo = json_decode($json_status_memo,true);
		$arr_status_memo[$method] = $ct_house_memo;
		$new_json_status_memo = json_encode($arr_status_memo, JSON_UNESCAPED_UNICODE);
		
		if($method != '')
		{			
			//결제취소시 PG사에 결제금액 환불
			if($status == 'buy_cancel' && $ct_pg_refunded == 'N')
			{
				//주문서 정보 로드
				$od_data = $order->where('od_id',$od_id)->limit(1)->get();
				$service_fee = $od_data[0]->od_service_fee / $od_data[0]->od_item_count;
				//$service_fee_total = $service_fee * $row[0]->ct_qty;
				$service_fee_total = $service_fee;
				$ct_amount = ($row[0]->ct_krw_price * $row[0]->ct_qty) + $row[0]->ct_send_cost + $service_fee_total;				
				$pay_id = $od_data[0]->od_pay_id1;
				
				//PG 객체생성								
				$pay = new PayErom();
				$pay_result = $pay->refundPayment($pay_id,$od_id,$ct_amount,'1st_charge');
				
				//결과 분석
				$arr_pay_result = json_decode($pay_result,true);				
				$pay_result_code = $arr_pay_result['result_code'];
				$pay_result_message = $arr_pay_result['result_message'];
				
				if($pay_result_code == 'TS1000')
				{
					//카트 업데이트
					$cart->where([
					['ct_id', '=', $ct_id]
					])->update(['ct_pg_refunded' => 'Y', 'ct_status' => 'cancel']);
					
					//주문서 업데이트					
					$affected = DB::update("UPDATE orders SET od_cancel_price1 = IF(od_cancel_price1,od_cancel_price1,0) + $ct_amount WHERE od_id = ?", [$od_id]);							
					
					$msg = "{$ct_amount}원 환불 완료.";
					
					//해당 주문서의 모든 상품이 ct_status = cancel 이고  2차배송비까지 환불처리 되었으면 od_status = 'cancel'로 변경
					//$msg .= " 해당 주문서의 모든 상품결제와 배송비가 환불되었으므로 주문서상태가 취소로 변경되었습니다.";
					
				}
				else
				{
					$msg = "PG사 환불 처리에 실패하였습니다. $pay_result_code : $pay_result_message";	
					return response()->json(array('result'=> 1, 'msg'=> $msg), 200);
					exit;
				}			
			}
							
			//update
			$cart->where([
			['ct_id', '=', $ct_id]
			])->update(['ct_house_status' => $status, 'ct_house_memo_json' => $new_json_status_memo, 'ct_house_status_time_json' => $new_json_status_time]);
			
			$msg .= ' 주문상태 변경완료';			
		}
		else
		{
			exit;
		}
		
		$raw_input = print_r($request->all(),true);
		$this->order_mod_log($od_id, $ct_id, 'update_ct_status', $raw_input);
		
		return response()->json(array('result'=> 1, 'msg'=> $msg), 200);
	}
	
	public function edit_order(Request $request, Order $order, Cart $cart)
	{
		$od_id = $request->input('od_id');
		$od_country = $request->input('od_country');
		$od_first_name = $request->input('od_first_name');
		$od_last_name = $request->input('od_last_name');
		$od_tel = $request->input('od_tel');
		$od_addr1 = $request->input('od_addr1');
		$od_addr2 = $request->input('od_addr2');
		$od_city = $request->input('od_city');
		$od_state = $request->input('od_state');
		$od_zip = $request->input('od_zip');
		$od_weight = $request->input('od_weight');
		$od_width = $request->input('od_width');		
		$od_length = $request->input('od_length');
		$od_height = $request->input('od_height');
		$od_weight_due = $request->input('od_weight_due');
		$od_send_cost2 = $request->input('od_send_cost2');
		$od_sales_tax = $request->input('od_sales_tax');
		
		$order->where('od_id', $od_id)->update([
		'od_country' => $od_country,
		'od_first_name' => $od_first_name,
		'od_last_name' => $od_last_name,
		'od_tel' => $od_tel,
		'od_addr1' => $od_addr1,
		'od_addr2' => $od_addr2,
		'od_city' => $od_city,
		'od_state' => $od_state,
		'od_zip' => $od_zip,
		'od_weight' => $od_weight,
		'od_width' => $od_width,
		'od_length' => $od_length,
		'od_height' => $od_height,
		'od_weight_due' => $od_weight_due,
		'od_send_cost2' => $od_send_cost2,
		'od_sales_tax'=>$od_sales_tax
		]);
		
		$raw_input = print_r($request->all(),true);
		$this->order_mod_log($od_id, null,'edit_order',$raw_input);
		
		return redirect()->back();		
	}
	
	public function edit_address(Request $request, Order $order) //FOR CUSTOMER
	{
		$msg = '';
		$od_id = $request->input('od_id');
		$od_country = $request->input('od_country');
		$od_first_name = $request->input('od_first_name');
		$od_last_name = $request->input('od_last_name');
		$od_tel = $request->input('od_tel');
		$od_addr1 = $request->input('od_addr1');
		$od_addr2 = $request->input('od_addr2');
		$od_city = $request->input('od_city');
		$od_state = $request->input('od_state');
		$od_zip = $request->input('od_zip');
		
		$cursor = $order->where([
		['od_id', '=', $od_id],
		['mb_email', '=', session('mb_email')]
		]);
		
		$od_data = $cursor->get();
		$od_ship_paid = $od_data[0]->od_ship_paid;
		
		if($od_ship_paid != 'Y' && $od_ship_paid != 'C') //이미 지불했거나, 결제취소가 아닌경우에만 업데이트 가능하도록
		{
			$cursor->update([
			'od_country' => $od_country,
			'od_first_name' => $od_first_name,
			'od_last_name' => $od_last_name,
			'od_tel' => $od_tel,
			'od_addr1' => $od_addr1,
			'od_addr2' => $od_addr2,
			'od_city' => $od_city,
			'od_state' => $od_state,
			'od_zip' => $od_zip,
			'od_ship_paid'=>'N'
			]);
			
			$msg = "Address is changed";
		}
		else
		{
			$msg = "This order status can not change address";
		}
				
		$raw_input = print_r($request->all(),true);
		$this->order_mod_log($od_id, null,'edit_address',$raw_input);
				
		return redirect()->back()->with('jsAlert',$msg);		
	}
	
	public function edit_cart(Request $request, Order $order, Cart $cart)
	{
		$od_id = $request->input('od_id');
		$ct_id = $request->input('ct_id');
		$ct_carrier = $request->input('ct_carrier');
		$ct_ship_no = $request->input('ct_ship_no');
		
		$ct_local_ship_carrier = $request->input('ct_local_ship_carrier');
		$ct_local_ship_no = $request->input('ct_local_ship_no');
		
		$cart->where('ct_id', $ct_id)->update([
		'ct_carrier' => $ct_carrier,
		'ct_ship_no' => $ct_ship_no,
		'ct_local_ship_carrier' => $ct_local_ship_carrier,
		'ct_local_ship_no' => $ct_local_ship_no
		]);
		
		$raw_input = print_r($request->all(),true);
		$this->order_mod_log($od_id, $ct_id,'edit_cart',$raw_input);
		
		return redirect()->back();
	}
	
	public function make_ship_invoice(Request $request, Order $order)
	{
		$od_id = $request->input('od_id');
		$od_weight = $request->input('od_weight');
		$od_width = $request->input('od_width');		
		$od_length = $request->input('od_length');
		$od_height = $request->input('od_height');
		$od_weight_due = $request->input('od_weight_due');
		$od_send_cost2 = $request->input('od_send_cost2');
		
		$order->where('od_id', $od_id)->update([
		'od_weight' => $od_weight,
		'od_width' => $od_width,
		'od_length' => $od_length,
		'od_height' => $od_height,
		'od_weight_due' => $od_weight_due,
		'od_send_cost2' => $od_send_cost2,
		'od_ship_paid' => 'R'
		]);
		
		$raw_input = print_r($request->all(),true);
		$this->order_mod_log($od_id, null,'make_ship_invoice',$raw_input);

       /**
        * PUSH : esummer, 2021-05-28, 사용자 가입시 모바일 푸시 메시지 생성
        */
        $mb_email = $order->where('od_id', $od_id)->get()[0]->mb_email;
        sendMobileNotification($mb_email
        , "payment_ready"
        , "[eckorea.net] Your order is ready to ship! Please make a payment for your int’l shipping."
        , "[eckorea.net] Your order is ready to ship! All the goods you ordered are in stock, and inspection / packing is completed."
        , "/order/list");

		return response()->json(array('result'=> 1, 'msg'=> '견적제출완료'), 200);
	}
	
	public function refund_shipping(Request $request, Order $order, $od_id)
	{
		//주문서 정보 로드
		$od_data = $order->where('od_id',$od_id)->limit(1)->get();
		$od_rate = $od_data[0]->od_rate2; //결제 당시 기준환율				
		//$od_send_cost = $od_data[0]->od_send_cost2 * $od_rate;
		$od_send_cost = $od_data[0]->od_receipt_price2;
		$pay_id = $od_data[0]->od_pay_id2;						
		
		//PG 객체생성								
		$pay = new PayErom();
		$pay_result = $pay->refundPayment($pay_id,$od_id,$od_send_cost,'shipping');
		
		//결과 분석
		$arr_pay_result = json_decode($pay_result,true);				
		$pay_result_code = $arr_pay_result['result_code'];
		$pay_result_message = $arr_pay_result['result_message'];
		
		if($pay_result_code == 'TS1000')
		{
			//카트 업데이트
			$order->where([
			['od_id', '=', $od_id]
			])->update(['od_cancel_price2' => $od_send_cost, 'od_ship_paid' => 'C']);
						
			$msg = "{$od_send_cost}원 환불 완료.";
			
			//해당 주문서의 모든 상품이 ct_status = cancel 이고  2차배송비까지 환불처리 되었으면 od_status = 'cancel'로 변경
			//$msg .= " 해당 주문서의 모든 상품결제와 배송비가 환불되었으므로 주문서상태가 취소로 변경되었습니다.";
			
		}
		else
		{
			$msg = "PG사 환불 처리에 실패하였습니다. $pay_result_code : $pay_result_message";	
			return response()->json(array('result'=> 1, 'msg'=> $msg), 200);
			exit;
		}
		
		return response()->json(array('result'=> 1, 'msg'=> $msg), 200);
	}
	
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }	
	
	public function order_mod_log($od_id, $ct_id, $method, $raw)
	{
		$arr_visable = array('입고대기');
		if(in_array($method, $arr_visable)) $visable = 'Y';	else $visable = 'N';
		
		$result = DB::table('order_mod_logs')
		->insert([
		'od_id' => $od_id, 
		'ct_id' => $ct_id, 
		'method' => $method, 
		'raw' => $raw, 
		'visable' => $visable, 
		'writer' => session('mb_email'),
		'ip' => $_SERVER['REMOTE_ADDR']		
		]);
	}
}

<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Item $item, Request $request)
    {

        $qs = $request->query();
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';

		$chk_main_use = $request->it_main_use;

		$data = $item->leftJoin('categories','items.ca_id','=','categories.ca_id');

        if($chk_main_use == 'Y') {
            $data = $data->whereExists(function ($data) {
                $data->select(DB::raw(1))
                    ->from('recommend_items')
                    ->whereRaw('items.it_id = recommend_items.it_id');
            });
        }

        //검색어
        if($q != '') {
            if ($qkind == 'it_name') $data = $data->where('items.it_name', 'LIKE', "%$q%");
            if($qkind == 'it_id') $data = $data->where('items.it_id','=',"$q");
            if($qkind == 'ca_id') $data = $data->where('items.ca_id','=',"$q");
            if($qkind == 'it_origin') $data = $data->where('items.it_origin','=',"$q");
        }

        if ($request->it_use == "") {
            $request->it_use = "Y";
        }

        $chk_it_use = $request->it_use;
        if($chk_it_use == 'Y')
            $data = $data->where('items.it_use','=',"Y");

        //바인딩
		$data = $data
		->select('*','items.updated_at AS item_updated_at')
		->orderBy('items.updated_at', 'desc')
		->paginate(30)->appends($request->query());

		return view('/admin/item')->with(['data'=>$data]);
    }
	
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Item $item, Request $request)
    {
		$arr_img_url;
		foreach((array)array_filter($request->it_img_url) as $each_img)
		{
			$arr_img_url[] = $each_img;
		}
		
		$it_img_json = json_encode($arr_img_url);
		
        $new_item_id = $item->insertGetId([
		'it_type'=>$request->it_type,
		'it_origin'=>$request->it_origin,
		'it_id_extra'=>$request->it_id_extra,
		'ca_id'=>$request->ca_id,
		'ca_id_extra'=>$request->ca_id_extra,
		'it_name'=>$request->it_name,
		'it_price'=>$request->it_price,
		'it_whole_price'=>$request->it_whole_price,
		'it_intro'=>$request->it_intro,
		'it_desc'=>$request->it_desc,
		'it_weight'=>$request->it_weight,
		'it_width'=>$request->it_width,
		'it_height'=>$request->it_height,
		'it_length'=>$request->it_length,		
		'it_use'=>$request->it_use,
		'it_stock_qty'=>$request->it_stock_qty,
		'it_img_json'=>$it_img_json,
		'it_url'=>$request->it_url,
		'it_option1'=>$request->it_option1,
		'it_option2'=>$request->it_option2,
		'it_thumbnail_url'=>$request->it_thumbnail_url,
		'created_at'=>now()	
		]);
		
		$this->recommend_item_update($request, $new_item_id);	
		
		return redirect("/admin/item/edit/$new_item_id")->with('jsAlert', '입력하신 정보로 저장되었습니다');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item, $type, $it_id = '')
    {
		//$action_url = '';		
		$recommend_keys = array();
		if($type == 'edit')
		{			
			$data = $item->where('it_id',$it_id)->get();
			$recommend_data = DB::table('recommend_items')->where('it_id',$it_id)->get();			
			foreach($recommend_data as $row)
			{
				$recommend_keys[] = $row->reco_type;
			}
			$action_url = "/admin/item/$type";
		}		
		else
		{
			$data = '';
			$action_url = "/admin/item/new";
		}
		
        return view('/admin/item_view')->with(['action_url'=>$action_url, 'type'=>$type, 'data'=>$data, 'recommend_keys'=>$recommend_keys, 'it_id'=>$it_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item, Request $request)
    {
        $it_id = $request->it_id;
		$arr_img_url;
		foreach((array)array_filter($request->it_img_url) as $each_img)
		{
			$arr_img_url[] = $each_img;
		}
		
		$it_img_json = json_encode($arr_img_url);
		
		$item->where('it_id',$it_id)->update([
		'it_type'=>$request->it_type,
		'it_origin'=>$request->it_origin,
		'it_id_extra'=>$request->it_id_extra,
		'ca_id'=>$request->ca_id,
		'ca_id_extra'=>$request->ca_id_extra,
		'it_name'=>$request->it_name,
		'it_price'=>$request->it_price,
		'it_whole_price'=>$request->it_whole_price,
		'it_intro'=>$request->it_intro,
		'it_desc'=>$request->it_desc,
		'it_weight'=>$request->it_weight,
		'it_width'=>$request->it_width,
		'it_height'=>$request->it_height,
		'it_length'=>$request->it_length,		
		'it_use'=>$request->it_use,
		'it_stock_qty'=>$request->it_stock_qty,
		'it_img_json'=>$it_img_json,
		'it_url'=>$request->it_url,
		'it_option1'=>$request->it_option1,
		'it_option2'=>$request->it_option2,
		'it_thumbnail_url'=>$request->it_thumbnail_url
		]);
		
		//추천상품 데이터 업데이트
		$this->recommend_item_update($request, $it_id);		
		
		return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item, $it_id)
    {
        $deletedRows = $item->where('it_id', $it_id)->delete();
		return redirect()->back();
    }
	
	public function recommend_item_update($request, $it_id)
	{
		//추천아이템 DB 초기화후 입력
		DB::table('recommend_items')->where('it_id',$it_id)->delete();
		foreach((array) $request->it_recommend as $recommend)
		{
			DB::table('recommend_items')->insert([
			'it_id'=>$it_id,
			'reco_type'=> $recommend,
			'created_at'=>now()
			]);
		}
	}
}

<?php

namespace App\Http\Middleware;

use Closure, Session;

class AdminCheck
{
    public function handle($request, Closure $next)
	{
		if(session('mb_level') != 10)
		{
			$request->session()->flush();
			echo "관리자 계정만 접근가능합니다. 관리자 계정으로 로그인해주십시요 <a href='/login'>[로그인]</a>";
			exit;			
		}
		return $next($request);
	}	
}

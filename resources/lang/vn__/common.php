<?php

return [
    'myaccount' => 'MY ACCOUNT',
    'admin' => 'ADMIN',
	'login' => 'SIGN IN',
	'logout' => 'SIGN OUT',
	'cscenter' => 'CS CENTER',
	'search' => 'Search',
	'all'=>'ALL',
	'buy_wait' => 'Trước sự đặt hàng',
	'buy_done' => 'Mệnh lệnh đã hoàn thành',
	'buy_hold' => 'Sự chờ đợi sự đặt hàng',
	'buy_cancel' => 'Hủy bỏ sự đặt hàng',
	'warehouse_stock' => 'Xác định sự nhập kho/Kiểm tra số lượng & đóng gói',
	'warehouse_hold' => 'sự hoãn sự nhập kho',
	'warehouse_cancel' => 'Hủy bỏ sự nhập kho',
	'send_payment_wait' => 'Sự chờ đợi sự thanh toán',
	'send_paid' => 'Quá trình thanh toán giao hàng đã hoàn thành/ Sự chờ đợi gửi đi',
	'sending' => 'Shipping',
	'sended' => 'Shiped',
	'paid' => 'thanh toán hay không'
];

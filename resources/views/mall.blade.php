@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MALL</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>

			<div class="container">
				<div class="row">				
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>mall</h2>

						<nav class="toolbox">
							<div class="toolbox-left">
								<div class="toolbox-item toolbox-sort">
									<div class="select-custom">
										<select name="orderby" class="form-control">
											<option value="menu_order" selected="selected">Default sorting</option>
											<option value="popularity">Sort by popularity</option>
											<option value="price">Sort by price: low to high</option>
											<option value="price-desc">Sort by price: high to low</option>
										</select>
									</div><!-- End .select-custom -->
								</div><!-- End .toolbox-item -->
							</div><!-- End .toolbox-left -->
						</nav>

						<div class="row row-sm">
							<div class="col-6 col-md-4 col-xl-3">
								<div class="product-default">
									<figure>
										<a href="mall_view"><img src="/image/products/product-1.jpg"></a>
									</figure>
									<div class="product-details">

										<h2 class="product-title">
											<a href="mall_view">Product Short Name</a>
										</h2>
										<div class="price-box">
											<span class="product-price">$32.00</span>
										</div><!-- End .price-box -->

									</div><!-- End .product-details -->
								</div>
							</div><!-- End .col-xl-3 -->

							<div class="col-6 col-md-4 col-xl-3">
								<div class="product-default">
									<figure>
										<a href="mall_view"><img src="/image/products/product-2.jpg"></a>
									</figure>
									<div class="product-details">

										<h2 class="product-title">
											<a href="mall_view">Product Short Name</a>
										</h2>
										<div class="price-box">
											<span class="product-price">$32.00</span>
										</div><!-- End .price-box -->

									</div><!-- End .product-details -->
								</div>
							</div><!-- End .col-xl-3 -->

							<div class="col-6 col-md-4 col-xl-3">
								<div class="product-default">
									<figure>
										<a href="mall_view"><img src="/image/products/product-3.jpg"></a>
									</figure>
									<div class="product-details">
										<h2 class="product-title">
											<a href="mall_view">Product Short Name</a>
										</h2>
										<div class="price-box">
											<span class="product-price">$32.00</span>
										</div><!-- End .price-box -->

									</div><!-- End .product-details -->
								</div>
							</div><!-- End .col-xl-3 -->

							<div class="col-6 col-md-4 col-xl-3">
								<div class="product-default">
									<figure>
										<a href="mall_view"><img src="/image/products/product-4.jpg"></a>
									</figure>
									<div class="product-details">
										<h2 class="product-title">
											<a href="mall_view">Product Short Name</a>
										</h2>
										<div class="price-box">
											<span class="product-price">$32.00</span>
										</div><!-- End .price-box -->
									</div><!-- End .product-details -->
								</div>
							</div><!-- End .col-xl-3 -->
						
						</div>

						<nav class="toolbox toolbox-pagination">
							<ul class="pagination">
								<li class="page-item disabled"><a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a></li>
								<li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">4</a></li>
								<li class="page-item"><span>...</span></li>
								<li class="page-item"><a class="page-link" href="#">15</a></li>
								<li class="page-item">
								<a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a>
								</li>
							</ul>
						</nav>
					</div><!-- End .col-lg-9 -->			



					@include('mall_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
		<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

	@include('footer')
</body>

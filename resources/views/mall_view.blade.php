@include('top')
@php
$arr_it_url = json_decode($data[0]->it_img_json);
@endphp
<body>
	<div class="page-wrapper">

		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item" aria-current="page">MALL</li>
                        @foreach($ca_tree as $each_ca)
                        <li class="breadcrumb-item" aria-current="page">{{$each_ca['ca_name']}}</li>
                        @endforeach
					</ol>
				</div>
			</nav>

			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="product-single-container product-single-default">
							<div class="row">												
								<div class="col-lg-6 col-md-6 product-single-gallery">
									<div class="product-slider-container product-item">
										<div class=" owl-carousel owl-theme">
											<div class="product-item">
												<!--<img class="product-single-image" src="{{$arr_it_url[0]}}" />-->
                                                <img class="product-single-image" src="{{$data[0]->it_thumbnail_url}}" />
											</div>
										<!--<div class="product-item">
												<img class="product-single-image" src="/image/products/zoom/product-2.jpg" data-zoom-image="/image/products/zoom/product-2-big.jpg"/>
											</div>
											<div class="product-item">
												<img class="product-single-image" src="/image/products/zoom/product-3.jpg" data-zoom-image="/image/products/zoom/product-3-big.jpg"/>
											</div>
											<div class="product-item">
												<img class="product-single-image" src="/image/products/zoom/product-4.jpg" data-zoom-image="/image/products/zoom/product-4-big.jpg"/>
											</div>
											<div class="product-item">
												<img class="product-single-image" src="/image/products/zoom/product-4.jpg" data-zoom-image="/image/products/zoom/product-4-big.jpg"/>
											</div>
											<div class="product-item">
												<img class="product-single-image" src="/image/products/zoom/product-4.jpg" data-zoom-image="/image/products/zoom/product-4-big.jpg"/>
											</div>-->
										</div>
										<!-- End .product-single-carousel -->
										<!--<span class="prod-full-screen">
											<i class="icon-plus"></i>
										</span>-->
									</div>

									<div class="" id='carousel-custom-dots'>
										
									</div>
								</div><!-- End .col-lg-7 -->

								<div class="col-lg-6 col-md-6">
								<div class="product-single-details">
									<h1 class="product-title">{{$data[0]->it_name}}</h1>
									<div class="ratings-container">
										<div class="product-ratings">
                                        @if($data[0]->review_points > 0)
                                        <span class="ratings" style="width:{{($data[0]->review_points/$data[0]->review_counts) * 20}}%"></span><!-- End .ratings -->                                        
                                        @endif										
										</div><!-- End .product-ratings -->
									</div><!-- End .product-container -->

									<div class="price-box">
										<!--<span class="old-price">KRW 81</span>-->
                                        <span class="product-price">USD {{number_format($data[0]->it_price/ Config::get('rate.usd'), 2)}}</span>
										<span class="product-price">KRW {{number_format($data[0]->it_price)}}</span>
									</div><!-- End .price-box -->

									<div class="product-desc">
										<p>{{$data[0]->it_intro}}</p>
									</div><!-- End .product-desc -->
                                    <form id="it_form" method="post" action="/order/cart">	
                                    <input type="hidden" name="it_id" value="{{$it_id}}">
                                    <input type="hidden" id="direct" name="direct" value="N">
                                    {{ csrf_field() }}		
									<div class="product-filters-container">
                                
										<!--<div class="product-single-filter">
											<label>Colors</label>
											<select>
												<option>검은색</option>
											</select>
										</div>-->
                                        <!-- End .product-single-filter -->
									</div><!-- End .product-filters-container -->


									<div class="product-action product-all-icons">
										<div class="product-single-qty">
											<input class="horizontal-quantity form-control" type="text" name="qty">
										</div><!-- End .product-single-qty -->

										<div class="btn btn-outline-secondary" title="Add to Cart" id="carting">
											Add to Cart
										</div>
										<div class="btn btn-outline-secondary " title="Buy" id="buying">
											Buy
										</div>																			
									</div><!-- End .product-action -->	
                                    <div class="product-action product-all-icons">
                                    <ul>
                                    @if($data[0]->it_option1)
                                    <li>
                                    <label>Option 1</label>
                                    <select id="it_option1" class="it_option" name="it_option1">
                                    <option value="">Select Option</option>
                                    @foreach(explode("|",$data[0]->it_option1) as $option)
                                    	@if($option !='')<option value="{{$option}}">{{$option}}</option>@endif
                                    @endforeach
                                    </select>
                                    </li>
                                    @endif
                                    
                                    @if($data[0]->it_option2)
                                    <li>
                                    <label>Option 2</label>
                                    <select id="it_option2" class="it_option" name="it_option2">
                                    <option value="">Select Option</option>
                                    @foreach(explode("|",$data[0]->it_option2) as $option)
                                    	@if($option !='')<option value="{{$option}}">{{$option}}</option>@endif
                                    @endforeach
                                    </select>                                    
                                    </li>
                                    @endif
                                    </ul>                                   
                                    </div>
									</form>
								</div><!-- End .product-single-details -->
								</div><!-- End .col-lg-5 -->
								</div><!-- End .row -->
								</div><!-- End .product-single-container -->

								<div class="product-single-tabs scrolling-box">
									<!--<div class="sticky-header" role="tablist">
										<ul class="nav nav-tabs container">
											<li class="nav-item">
												<a class="nav-link" id="product-tab-desc" href="#product-desc-content" data-toggle="tab">Description</a>
											</li>
											
											
											<li class="nav-item">
												<a class="nav-link" id="product-tab-reviews" href="#product-reviews-content" data-toggle="tab">Reviews</a>
											</li>
										</ul>
									</div>-->
								<div class="tab-pane" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
									<div class="product-desc-content">
										<h3 class="text-uppercase heading-text-color font-weight-semibold">Description</h3>
										<p>{!! nl2br($data[0]->it_desc) !!}</p>
									</div><!-- End .product-desc-content -->
								</div><!-- End .tab-pane -->

								
								<!--<div class="tab-pane" id="product-tags-content" role="tabpanel" aria-labelledby="product-tab-tags">
                                <form method="post" action="/mall/qna">
                                {{ csrf_field() }}
                                            <input type="hidden" name="it_id" value="{{$it_id}}">
									<div class="product-tags-content">
										<h3 class="text-uppercase heading-text-color font-weight-semibold">Q&A</h3>
										@foreach($qna_data as $qna_row)
                                        <div class="review_id">{{substr($qna_row->mb_email,0,3)}}***</div>
                                       
                                        <div>
                                        {{$qna_row->qna_contents}}
                                        </div>
                                        @if($qna_row->mb_email == session('mb_email'))
                                        <a href="/mall/qna/delete/{{$qna_row->qna_id}}" onClick="return confirm('Delete?');">[삭제]</a>
                                        @endif
                                        @endforeach
                                        <div class="form-group mb-2">
                                        <textarea cols="5" rows="6" name="qna_text" class="form-control form-control-sm" placeholder="문의"></textarea>                                 
                                        </div>
                                        <div>
                                        <button type="submit">상품문의 등록</button>
                                        </div>
									</div>
                                </form>
								</div><!-- End .tab-pane -->

								<div class="tab-pane" id="product-reviews-content" role="tabpanel" aria-labelledby="product-tab-reviews">
									<div class="product-reviews-content">															
										<div class="add-product-review">
											<h3 class="text-uppercase heading-text-color font-weight-semibold">Reviews</h3>
											
                                            @foreach($review_data as $review_row)
                                            <div class="review_id">{{substr($review_row->mb_email,0,3)}}***</div>
											@if(isset($review_row->review_photo))<a href="/mall/image?url=/storage/{{$review_row->review_photo}}" target="_blank"><img src="/storage/{{$review_row->review_photo}}" style="height:150px;"></a>@endif
											<div>
                                            {{$review_row->review_contents}}
                                            </div>
                                            @if($review_row->review_video_url)
                                            <div>
                                            Video Url : <a href="{{$review_row->review_video_url}}" target="_blank">{{$review_row->review_video_url}}</a>
                                            </div>
                                            @endif
                                            @if($review_row->mb_email == session('mb_email') || session('mb_level') == 10)
                                            <a href="/mall/review/delete/{{$review_row->review_id}}" onClick="return confirm('Delete?');">[삭제]</a>
                                            @endif
                                            @endforeach
                                            
                                            <form method="post" action="/mall/review" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="it_id" value="{{$it_id}}">
											<div class="form-group">
												<label>Photo</label>
												<input type="file" accept="image/*" name="file_photo" class="form-control form-control-sm">
											</div><!-- End .form-group -->

											<div class="form-group">
												<label>Video url</label>
												<input type="text" name="video_url" class="form-control form-control-sm">
											</div><!-- End .form-group -->
											<div class="form-group mb-2">
												<label>Review <span class="required">*</span></label>
												<textarea cols="5" rows="6" name="review_text" class="form-control form-control-sm"></textarea>
											</div><!-- End .form-group -->
                                            
                                            <div class="form-group mb-2">
												<label>RATE <span class="required">*</span></label>
												<select name="rate">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                </select>
											</div><!-- End .form-group -->

											<input type="submit" class="btn btn-primary" value="Submit Review">
                                            </form>
										</div><!-- End .add-product-review -->
									</div><!-- End .product-reviews-content -->
								</div><!-- End .tab-pane -->

								</div><!-- End .product-single-tabs -->
								</div><!-- End .col-lg-9 -->							
								</div><!-- End .row -->
								</div><!-- End .container -->

								<div class="featured-section">
								<div class="container">
									

								<div class="owl-carousel owl-theme owl-nav-top" data-toggle="owl" data-owl-options="{
								'margin' : 20,
								'items' : 2,
								'responsive' : {
								'768': {
								'items': 3
								},
								'992': {
								'items': 4
								}
								}
								}" >
								

								


							</div>
					</div><!-- End .featured-products -->
				</div><!-- End .container -->
			</div><!-- End .featured-section -->
		</main><!-- End .main -->
        
	</div><!-- End .page-wrapper -->

	@include('footer')
<script>
function cart_valid()
{
	var flag = true;
	
	if($(".it_option").length > 0)
	{
		$(".it_option").each(function(index, element) {
            if($(this).val() == '')
			{
				flag = false;
				alert('Please Select Option');
				$(this).focus();
				return false;
			}
        });
	}

	//로그인 check
	if("{{session('mb_email')}}" == '')
	{
		location.href = '/login?return_url='+window.location.pathname;
		flag = false;
	}
	
	return flag;
}

$(document).ready(function(e) {
    $("#carting").click(function(e) {		
		if(cart_valid())
		{			
			$("#direct").val('N');
			$("#it_form").submit();			
		}		
    });
	
	$("#buying").click(function(e) {
		if(cart_valid())
		{
			$("#direct").val('Y');
			$("#it_form").submit();
		}
    });
});
</script>
</body>

        
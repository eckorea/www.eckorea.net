@include('admin.top')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>
<style>
.ck-editor__editable {
    min-height: 500px;
}
</style>
<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>상품관리</div>
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="tab-content">                            
							<div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                            <form method="post" action="{{$action_url}}" onsubmit="return before_item_submit();">
                             {{ csrf_field() }}
                            <input type="hidden" name="it_id" value="{{$it_id}}" />
								<div class="card-body">
                                
                                 <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품ID</label>
										<div class="col-sm-10">@if(isset($data[0])){{$data[0]->it_id}}@endif</div>
									</div>	
                                    
                                     <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">외부상품ID</label>
										<div class="col-sm-10"><input class="form-control" name="it_id_extra" value="@if(isset($data[0])){{$data[0]->it_id_extra}}@endif" ></div>
									</div>	
                                
                                <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품타입</label>
										<div class="col-sm-10">
                                        <select name="it_type">
                                        <option value="in" @if(isset($data[0]) && $data[0]->it_type == 'in') selected="selected" @endif>내부상품</option>
                                        <option value="out" @if(isset($data[0]) && $data[0]->it_type == 'out') selected="selected" @endif>외부상품</option>
                                        </select>                                        
                                        
                                        </div>
									</div>                                                                  
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품출처</label>
										<div class="col-sm-10"><input class="form-control" name="it_origin" value="@if(isset($data[0])){{$data[0]->it_origin}}@endif" ></div>
									</div>		
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">카테고리ID</label>
										<div class="col-sm-10"><input class="form-control" name="ca_id" value="@if(isset($data[0])){{$data[0]->ca_id}}@endif" ></div>
									</div>              											
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">외부카테고리ID</label>
										<div class="col-sm-10"><input class="form-control" name="ca_id_extra" value="@if(isset($data[0])){{$data[0]->ca_id_extra}}@endif" ></div>
									</div>
                                    
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품명</label>
										<div class="col-sm-10"><input class="form-control" name="it_name" value="@if(isset($data[0])){{$data[0]->it_name}}@endif" ></div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품가격</label>
										<div class="col-sm-10"><input class="form-control" name="it_price" value="@if(isset($data[0])){{$data[0]->it_price}}@endif" ></div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">공급가</label>
										<div class="col-sm-10"><input class="form-control" name="it_whole_price" value="@if(isset($data[0])){{$data[0]->it_whole_price}}@endif" ></div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">기본설명</label>
										<div class="col-sm-10"><input class="form-control" name="it_intro" value="@if(isset($data[0])){{$data[0]->it_intro}}@endif" ></div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품설명</label>
                                    
										<div class="col-sm-10">
                                        <input type="hidden" class="form-control" id="it_desc" name="it_desc" value="" >
                                        <div id="toolbar-container"></div>
                                        <div id="editor" style="border:solid 1px;">
                                           @if(isset($data[0])){!!$data[0]->it_desc!!}@endif
                                        </div>                                        
                                        
                                        
                                        </div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품무게</label>
										<div class="col-sm-10"><input class="form-control" name="it_weight" value="@if(isset($data[0])){{$data[0]->it_weight}}@endif" ></div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품가로</label>
										<div class="col-sm-10"><input class="form-control" name="it_width" value="@if(isset($data[0])){{$data[0]->it_width}}@endif" ></div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품높이</label>
										<div class="col-sm-10"><input class="form-control" name="it_height" value="@if(isset($data[0])){{$data[0]->it_height}}@endif" ></div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품깊이</label>
										<div class="col-sm-10"><input class="form-control" name="it_length" value="@if(isset($data[0])){{$data[0]->it_length}}@endif" ></div>
									</div>
                                    
                                     <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">옵션1 ('|' 로 구분)</label>
										<div class="col-sm-10"><input class="form-control" name="it_option1" value="@if(isset($data[0])){{$data[0]->it_option1}}@endif" placeholder="검정|노랑|빨강" ></div>
									</div>
                                     <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">옵션2 ('|' 로 구분)</label>
										<div class="col-sm-10"><input class="form-control" name="it_option2" value="@if(isset($data[0])){{$data[0]->it_option2}}@endif" placeholder="검정|노랑|빨강"></div>
									</div>
                                       
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">판매여부</label>
										<div class="col-sm-10">
                                        <select name="it_use">
                                        <option value="Y" @if(isset($data[0]) && $data[0]->it_use == 'Y') selected="selected" @endif>Y</option>
                                        <option value="N" @if(isset($data[0]) && $data[0]->it_use == 'N') selected="selected" @endif>N</option>
                                        </select>                                        
                                        
                                        </div>
									</div>   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">재고</label>
										<div class="col-sm-10"><input class="form-control" name="it_stock_qty" value="@if(isset($data[0])){{$data[0]->it_stock_qty}}@endif" ></div>
									</div>
                                    
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">썸네일이미지 URL</label>
										<div class="col-sm-10"><input class="form-control" name="it_thumbnail_url" value="@if(isset($data[0])){{$data[0]->it_thumbnail_url}}@endif" ></div>
									</div>
                                    
                                    <!--   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">순서</label>
										<div class="col-sm-10"><input class="form-control" name="ca_id" value="@if(isset($data[0])){{$data[0]->it_order}}@endif" ></div>
									</div>-->
                                    @php
                                    $arr_imgs = array();
                                    if(isset($data[0]))
                                    {
                                    	$arr_imgs = json_decode($data[0]->it_img_json);
                                    }
                                    @endphp   
                                    
                                    @foreach((array)$arr_imgs as $each_img)
                                    <div class="position-relative row form-group img_group"><label class="col-sm-2 col-form-label">상세 이미지URL</label>
										<div class="col-sm-10"><input class="form-control" name="it_img_url[]" value="{{$each_img}}" ></div>
									</div>
                                    @endforeach
                                    <div class="position-relative row form-group img_group"><label class="col-sm-2 col-form-label">상세 이미지URL <span onclick="add_img_url()">[추가]</span></label>
										<div class="col-sm-10"><input class="form-control" name="it_img_url[]" value="" ></div>
									</div>
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상품URL</label>
										<div class="col-sm-10"><input class="form-control" name="it_url" value="@if(isset($data[0])){{$data[0]->it_url}}@endif" ></div>
									</div>   
                                    
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">추천상품</label>
										<div class="col-sm-10">
                                        <ul>
                                        <li><input type="checkbox" name="it_recommend[]" value="featured" @if(in_array('featured',$recommend_keys)) checked="checked" @endif /> <label>FEATURED PRODUCTS</label></li>
                                        <li><input type="checkbox" name="it_recommend[]" value="bestseller" @if(in_array('bestseller',$recommend_keys)) checked="checked" @endif /> <label>BEST SELLER</label></li>
                                        <li><input type="checkbox" name="it_recommend[]" value="new" @if(in_array('new',$recommend_keys)) checked="checked" @endif /> <label>NEW ARRIVAL</label></li>
                                        <li><input type="checkbox" name="it_recommend[]" value="flex" @if(in_array('flex',$recommend_keys)) checked="checked" @endif /> <label>FLEXZONE</label></li>
                                        <li><input type="checkbox" name="it_recommend[]" value="must" @if(in_array('must',$recommend_keys)) checked="checked" @endif /> <label>KYOBO BEST SELLER</label></li>
                                        <li><input type="checkbox" name="it_recommend[]" value="hotdeal" @if(in_array('hotdeal',$recommend_keys)) checked="checked" @endif /> <label>HOT DEAL</label></li>
                                        </ul>
                                        
                                        </div>
									</div>   
                                    
									<button class="mb-2 mr-2 btn btn-light" type="button"><a href="/admin/item">목록</a></button>                                       
                                    <button class="mb-2 mr-2 btn btn-light" type="submit">저장</button>
								</div>
                             </form>									
							</div>
						</div>						
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>
<script>
function add_img_url()
{
	$(".img_group:last").after($(".img_group:last").clone());
}

class UploadAdapter {
    constructor(loader) {
        this.loader = loader;
    }

    upload() {
        return this.loader.file.then( file => new Promise(((resolve, reject) => {
            this._initRequest();
            this._initListeners( resolve, reject, file );
            this._sendRequest( file );
        })))
    }

    _initRequest() {
        const xhr = this.xhr = new XMLHttpRequest();
        xhr.open('POST', '/editor/upload', true);
        xhr.responseType = 'json';
    }

    _initListeners(resolve, reject, file) {
        const xhr = this.xhr;
        const loader = this.loader;
        const genericErrorText = '파일 업로드에 실패했습니다.'

        xhr.addEventListener('error', () => {reject(genericErrorText)})
        xhr.addEventListener('abort', () => reject())
        xhr.addEventListener('load', () => {
            const response = xhr.response
            if(!response || response.error) {
                return reject( response && response.error ? response.error.message : genericErrorText );
            }
			console.log(response);
            resolve({
                default: response.url //업로드된 파일 주소
            })
        })
    }

    _sendRequest(file) {
        const data = new FormData()
        data.append('upload',file)
		this.xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}');
        this.xhr.send(data);		
    }
}

function MyCustomUploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        return new UploadAdapter(loader)
    }
}

var myEditor;

DecoupledEditor
  .create( document.querySelector( '#editor' ),
  {
	  extraPlugins: [MyCustomUploadAdapterPlugin]
  })
  .then( editor => {
	 const toolbarContainer = document.querySelector( '#toolbar-container' ); 
	 toolbarContainer.appendChild( editor.ui.view.toolbar.element );
	 myEditor = editor;
  } )
  .catch( error => {
	 console.error( error );
  } );

function before_item_submit()
{
	var contents = myEditor.getData();
	$("#it_desc").val(contents);
}
</script>
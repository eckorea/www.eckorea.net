@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MY ACCOUNT</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>CHANGE PASSWORD</h2>

						<div class="row">

						<div class="col-md-12">
							<div class="heading">
								<p>Please enter the password you want to change.</p>
							</div>
							<!-- End .heading -->
							<form method="post" action="/my/password/change">
                            {{ csrf_field() }}
							<!--<input type="password" name="old_password" class="form-control" placeholder="Current Password" required>-->
							<input type="password" name="new_password" class="form-control" placeholder="Password" required>
							<input type="password" name="new_password_confirm" class="form-control" placeholder="Password Confirm" required>
							
							<!-- End .custom-checkbox -->

							<div class="form-footer">
								<button type="submit" class="btn btn-primary">CHANGE</button>
							</div>
                            </form>
							<!-- End .form-footer -->
						</div>
						<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('mypage_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

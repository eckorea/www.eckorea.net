@include('top')

<body>
<div class="page-wrapper">
  <main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
      <div class="container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Sign In</li>
        </ol>
      </div>
      <!-- End .container --> 
    </nav>
    <div class="page-header">
      <div class="container">
        <h2>Forgot your password?</h2>
		 <p>Enter the email address associated with your ECKOREA account, <br>then click Continue.</p>
      </div>
      <!-- End .container --> 
    </div>
    <!-- End .page-header -->
    
	
    <div class="container">
      <div class="">
        <div class="max_middle"> 
        <form method="post" action="/password_reset">
         {{ csrf_field() }}
          <input type="email" name="email" class="form-control" placeholder="Email Address" required>
          <div class="top_mar_bt">
			  <button type="submit" class="btn btn-primary">CONTINUE</button>
	      </div>
        </form>
			<!-- End .col-md-6 -->        
       
      </div>
      <!-- End .row --> 
    </div>
    <!-- End .container -->
    
    <div class="mb-5"></div>
    <!-- margin --> 
  </main>
  <!-- End .main --> 
</div>
<!-- End .page-wrapper --> 

@include('footer')
</body>

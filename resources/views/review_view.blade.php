@include('top')

<body>
	<div class="page-wrapper">
	<main class="main">
		<nav aria-label="breadcrumb" class="breadcrumb-nav">
			<div class="container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
				</ol>
			</div>
			<!-- End .container --> 
		</nav>

		<div class="container">
			<div class="row">
				<div class="col-lg-9 order-lg-last dashboard-content">
					<h2>REVIEW</h2>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="contact-name">Title</label>
								<input type="text" class="form-control" readonly>
							</div>
							<!-- End .form-group -->

							<div class="form-group">
								<label for="contact-message">Contents</label>
								<textarea cols="30" rows="1" class="form-control" readonly></textarea>
							</div>
							<!-- End .form-group -->

							<div class="form-group">
								<label for="contact-name">File</label>
								<img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg" alt="product">
							</div>

							<div class="form-footer">
								<button type="button" class="btn btn-primary">Modify</button>
								<button type="button" class="btn btn-primary">Delete</button>
							</div>
							<!-- End .form-footer --> 
						</div>
					<!-- End .col-md-8 --> 
					</div>
					<!-- End .row --> 
				</div>
				<!-- End .col-lg-9 --> 

			@include('cscenter_left') 
			</div>
			<!-- End .row --> 
		</div>
		<!-- End .container -->
    
		<div class="mb-5"></div>
    <!-- margin --> 
	</main>
  <!-- End .main --> 
</div>
<!-- End .page-wrapper --> 

@include('footer')
</body>

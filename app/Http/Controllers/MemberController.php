<?php

namespace App\Http\Controllers;

use App\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

use App\Mail\EmailVerification;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Member $member)
    {
       $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ]);

		//존재하는 Email인지 확인		
		$data = DB::table('members')		
		->where('mb_email', $request->input('email'))
		->get();
				
		if(isset($data[0]))
		{
			//중복 존재하는경우
			return redirect('/login')->with(['jsAlert'=>'Same ID already exists', 'email'=>'Same ID already exists']);
		}
		
		//패스워드 해싱
		$hashed_pass = Hash::make($request->input('password'));
		
		//뉴스레터 여부
		if(!$request->input('news_letter')) $news_letter = 0; else $news_letter = 1;
		
		//회원 데이터 생성
		$result = $member->insert([
		'mb_first_name' => $request->input('first_name'),
		'mb_last_name' => $request->input('last_name'),
		'mb_email' => $request->input('email'),
		'mb_password' =>$hashed_pass,
		'mb_news_letter' => $news_letter,
		'mb_recommend_id'=>$request->input('recommend_user'),
		'mb_ip'=>$_SERVER['REMOTE_ADDR'],
		'created_at'=>now()
		]);
		
		//sendmail
		if($result)
		{
            $this->verify_make($request->input('email'), 'join');

            /**
             * TODO : esummer, 2021-05-06, 사용자 가입시 파트너 시스템으로 결제정보 넘김
             */
            try{
                $_PID = Cookie::get('cpsKEY');
                /*echo "<script> alert('". $_PID ."') ; </script>";*/
                if($_PID != '') {
                    $_OPTION = ['UID' => $request->input('email')];
                    sendMarketingData($request, 'J', $_PID, $_OPTION);
                }
            } catch(Exception $e){
                $errorMessage = $e;
            } finally {

            }

            return redirect('/joined');
		}
		else
		{
			echo "fail";
			exit;
		}
    }
	
	/*public function member_verify(Request $request, Member $member)
	{
		$result = $this->verify($request);
		
		if($result)
		{
		}
		else
		{
		}
	}*/
	
	public function verify(Request $request, Member $member)
	{
		$type = $request->type;
		$key = $request->key;
		$email = $request->email;
		$msg = '';
		$btn_name = '';
		$btn_url = '';
        $title = '';
		
		$cursor = DB::table('email_verify')->where([
		['veri_type','=',$type],
		['veri_email','=',$email],
		['veri_key','=',$key],
		['veri_used','=','N'],
		['veri_time_limit','>',date('Y-m-d H:i:s')]		
		]);
				
		$data = $cursor->get();
		
		if(isset($data[0]->veri_id))
		{		
			//update;
			$cursor->update(['veri_used'=>'Y']);
			if($type == 'join')
			{
				$member
				->where([
				['mb_email','=',$data[0]->veri_email]
				])
				->update(['mb_verified'=>'Y']);
				$msg = "Verified. You Can Login";
				$btn_name = 'Login';
				$btn_url = '/login';


				try{
                    /*이메일 인증에 성공하면 슬랙 메시지를 보내줌*/
                    sendSlackNotification("JOIN", 'EMail verified - #1 ' . $data[0]->veri_email, 'https://www.eckorea.net/');
                } catch(Exception $e){

                } finally {

                }

			}
			else if($type == 'password_reset') //1회성 로그인 시켜준뒤, 패스워드 변경페이지로 이동시킴
			{
				//회원 DB에 존재하는 EMAIL 인지 확인
				$member_data = $member->where('mb_email',$data[0]->veri_email)->get();
				
				if($member_data[0]->mb_email == $email)
				{
					$request->session()->put('mb_email', $data[0]->veri_email);
					$request->session()->put('mb_level', $member_data[0]->mb_level);
					return redirect('/my/password/change');	
				}
				else
				{
					$msg = "Can't find information....";
				}				
			}			
		}
		else
		{
			$msg = "Used or expired code. Resend Verification Email?";
			$btn_name = 'Resend Email';
			$btn_url = "/verify/resend?email=$email&type=$type";
		}
		
		return view('register_msg')->with([
		'msg'=>$msg,
		'title'=>$title,
		'btn_name'=>$btn_name,
		'btn_url'=>$btn_url
		]);
	}
	
	public function password_reset(Request $request, Member $member)
	{
		$email = $request->email;
		$title = __('common.need_email_verification1');
		$msg = __('common.email_veri_resent2');
		//$msg = 'Verification email sent for password reset. Please check email.';
		$btn_name = 'Login';
		$btn_url = '/login';
		
		$this->verify_make($email, 'password_reset');
		
		return view('register_msg')->with([
		'title'=>$title,
		'msg'=>$msg,
		'btn_name'=>$btn_name,
		'btn_url'=>$btn_url
		]);
	}
	
	public function verify_resend(Request $request)
	{
		$type = $request->type;
		$key = $request->key;
		$email = $request->email;
		
		$title = __('common.email_veri_resent1');
		$msg = __('common.email_veri_resent2');
		//$msg = 'Resent. Please check email.';
		$btn_name = 'Login';
		$btn_url = '/login';
				
		$this->verify_make($email, $type);
		
		return view('register_msg')->with([
		'title'=>$title,
		'msg'=>$msg,
		'btn_name'=>$btn_name,
		'btn_url'=>$btn_url
		]);
	}
	
	public function verify_make($email, $type, $sub_title ='')
	{
		//key gen
		$hased_key = Hash::make(time().rand(0,100));		
		$limit_time = date("Y-m-d H:i:s", mktime(date('H')+1,date('i'),date('s'),date('n'),date('j'),date('Y')));
			
		$result = DB::table('email_verify')->insert([
		'veri_type'=>$type,
		'veri_email'=>$email,
		'veri_key'=>$hased_key,
		'veri_time_limit'=>$limit_time
		]);
		
		if($result) 
		{
			//sendmail
			$veri_url = 'https://'.$_SERVER['HTTP_HOST']."/verify/email?email=$email&type=$type&key=$hased_key";
			if($type == 'join')
			{
				Mail::to($email)->send(new EmailVerification($veri_url));
			}
			else if($type == 'password_reset')
			{
				Mail::to($email)->send(new EmailVerification($veri_url, 'PASSWORD FIND'));
			}
		}		
	}
	
	public function list(Member $member, Request $request)
    {		
		$qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
				
		if($qkind == 'email') $data = $member->where('mb_email','LIKE',$q.'%');	
		else if($qkind == 'name') $data = $member->where('mb_last_name','LIKE',$q.'%')->orWhere('mb_first_name', 'LIKE', $q.'%');		
		else $data = $member;
				
		$data = $data
		->orderBy('created_at', 'desc')
		->paginate(10);		
		//->appends($request->query());
        return view('/admin/member')->with('data',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member, $mb_no)
    {
        $data = $member		
		->where('mb_no', $mb_no)
		->get();
		return view('/admin/member_view')->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $mb_id = $request->input('mb_no');
        $row = $member
            ->where('mb_no',$mb_id)
            ->update(['mb_first_name'=>$request->input('first_name'), 'mb_last_name'=> $request->input('last_name')]);

        //비밀번호 초기화의 경우
        $password = $request->input('password');
        if($password != '')
        {
            $hashed_password = Hash::make($request->input('password'));
            $member->where('mb_no',$mb_id)->update(['mb_password'=>$hashed_password]);
        }

        return redirect("/admin/member/view/$mb_id/");
    }

    /**
     * @param Member $member
     * @param $mb_no
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Member $member, $mb_no)
    {
        $row = $member
            ->where('mb_no',$mb_no)
            ->update(['deleted_at'=> Carbon::now()]);
        $data = $member
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('/admin/member')->with('data',$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        //
    }
}

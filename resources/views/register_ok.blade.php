@include('top')

<body>
    <div class="page-wrapper">
        <main class="main">            
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Login</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="about-section">
                <div class="container">
                    <h2 class="title">@lang('common.need_email_verification1')</h2>
                    <p>
					<!--	Membership completed. <br>
						Experience easy and fast shopping in Korea.<br>
                        Verfication email sent. You can login after verification. Please check email.-->
                        @lang('common.need_email_verification2')
					</p>

					<div>
                        <a href="/"><button type="submit" class="btn btn-primary">GO TO HOME</button></a>
                    </div><!-- End .form-footer -->
                </div><!-- End .container -->
            </div><!-- End .about-section --> 
        </main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	@include('footer')
    
</body>

        
<!DOCTYPE html>
<html lang="ko">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>EC KOREA</title>
</head>
<body>
고객 인증 이메일 입니다.
아래 링크를 누르시면 인증이 완료됩니다.
<a href="{{$veri_url}}">인증하기</a>
</body>
</html>

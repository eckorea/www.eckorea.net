@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">GUIDE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>Customs duty rates</h2>

						<div class="row">
							<div class="col-md-12">	
								<h4 class="title">관세용어</h4>
								<p>
									텍스트로 주실수 있으신가요
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">품목별 관세율표</h4>
								<p>
									관세법 별표로 규정된 수입물품의 품목별 관세율을 하나의 표로 묶어놓은 것을 말합니다. 관세율은 품목별로 기본세율과 잠정세율이 규정되어 있습니다. 현재 세계적으로 통용되는 상품분류방식은 SITC방식과 CCCN방식이 있습니다.
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">CUSTOMS TERMS</h4>
								<p>
									텍스트로 주실수 있으신가요
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">TARIFF RATE TABLE BY ITEM</h4>
								<p>
									This refers to a table of tariff rates for each item of imported goods defined by the attached Table of the Customs Act. The tariff rate stipulates the basic tax rate and the provisional tax rate for each item. Currently, there are SITC and CCCN methods that are used worldwide.
								</p>

								

								
							</div>
							<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('guide_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

@include('top')

<body>
    <div class="page-wrapper">
        <main class="main">
            <div class="home-top-container">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="banner banner-image">
                                <a href="#">
                                    <img src="/image/banners/banner-1.jpg" alt="banner">
                                </a>
                            </div><!-- End .banner -->
                        </div><!-- End .col-lg-5 -->

                        <div class="col-lg-7 top-banners">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="banner banner-image">
                                        <a href="#">
                                            <img src="/image/banners/banner-2.jpg" alt="banner">
                                        </a>
                                    </div><!-- End .banner -->

                                    <div class="banner banner-image">
                                        <a href="#">
                                            <img src="/image/banners/banner-3.jpg" alt="banner">
                                        </a>
                                    </div><!-- End .banner -->
                                </div><!-- End .col-sm-6 -->
                                <div class="col-sm-6">
                                    <div class="banner banner-image">
                                        <a href="#">
                                            <img src="/image/banners/banner-4.jpg" alt="banner">
                                        </a>
                                    </div><!-- End .banner -->
                                </div><!-- End .col-sm-6 -->
                            </div><!-- End .row -->
                        </div><!-- End .col-lg-7 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .home-top-container -->

           
            

            <div class="partners-container">
                <div class="container">
                    <div class="partners-carousel owl-carousel owl-theme">
                        <a href="#" class="partner">
                            <img src="/image/logos/1.png" alt="logo">
                        </a>
                        <a href="#" class="partner">
                            <img src="/image/logos/2.png" alt="logo">
                        </a>
                        <a href="#" class="partner">
                            <img src="/image/logos/3.png" alt="logo">
                        </a>
                        <a href="#" class="partner">
                            <img src="/image/logos/4.png" alt="logo">
                        </a>
                        <a href="#" class="partner">
                            <img src="/image/logos/5.png" alt="logo">
                        </a>
                        <a href="#" class="partner">
                            <img src="/image/logos/2.png" alt="logo">
                        </a>
                        <a href="#" class="partner">
                            <img src="/image/logos/1.png" alt="logo">
                        </a>
                    </div><!-- End .partners-carousel -->
                </div><!-- End .container -->
            </div><!-- End .partners-container -->
        </main><!-- End .main -->       
    </div><!-- End .page-wrapper -->

	@include('footer')
    
</body>

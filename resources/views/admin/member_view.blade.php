@include('admin.top')

<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>회원관리</div>
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="tab-content">                            
							<div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
								<div class="card-body">	
                                <form method="post" action="/admin/member/edit">
                                {{ csrf_field() }}
                                	<input type="hidden" name="mb_no" value="{{$data[0]->mb_no}}" />
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">이메일</label>
										<div class="col-sm-10"><input class="form-control" readonly="readonly" value="{{$data[0]->mb_email}}"></div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">FIRST NAME</label>
										<div class="col-sm-10"><input class="form-control" name="first_name" value="{{$data[0]->mb_first_name}}"></div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">LAST NAME</label>
										<div class="col-sm-10"><input class="form-control" name="last_name" value="{{$data[0]->mb_last_name}}"></div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">가입일</label>
										<div class="col-sm-10">{{date("Y-m-d",strtotime($data[0]->created_at))}}</div>
									</div>
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">추천인</label>
										<div class="col-sm-10">{{$data[0]->mb_recommend_id}}</div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">비밀번호 초기화</label>
										<div class="col-sm-10"><input class="form-control" name="password"></div>
									</div>		
									
									<div class="blank20"></div>
                                        
									<button type="submit" class="mb-2 mr-2 btn btn-primary">저장</button>                                        
									<button class="mb-2 mr-2 btn btn-light">취소</button>

									<a href="/admin/member/delete/{{$data[0]->mb_no}}" onclick="return confirm('삭제하시겠습니까?');"><div class="mb-2 mr-2 btn btn-light" id="_delete">삭제</div></a>
                                   </form>
								</div>									
							</div>
						</div>						
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>

@include('admin.top')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
<style>
.ck-editor__editable {
    min-height: 500px;
}
</style>
<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>KCULTER</div>
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="tab-content">                            
							<div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                            <form method="post" action="/admin/kculture/edit" enctype="multipart/form-data">
                             {{ csrf_field() }}
                            <input type="hidden" name="kc_id" value="@if(isset($data[0])){{$data[0]->kc_id}}@endif" />
								<div class="card-body">
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">게시판</label>
										<div class="col-sm-10">
                                        <select name="kc_board">
                                        @foreach(array_keys($kc_boards) as $board)
                                        <option value="{{$board}}" @if(isset($data[0]) && $data[0]->kc_board == $board) selected @endif >{{$board}}</option>                                      
                                        @endforeach
                                        </select>
                                  		</div>
									</div>                                   
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">제목</label>
										<div class="col-sm-10"><input class="form-control" name="kc_title" value="@if(isset($data[0])){{$data[0]->kc_title}}@endif" ></div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">내용</label>
										<div class="col-sm-10"><textarea id="kc_contents" name="kc_contents" class="form-control">@if(isset($data[0])){{$data[0]->kc_contents}}@endif</textarea></div>
									</div>
                                    
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">썸네일</label>
										<div class="col-sm-10"><input type="file" name="kc_thumbnail" class="form-control"></div>
									</div>								
									
									<button class="mb-2 mr-2 btn btn-light" type="button"><a href="/admin/kculture">목록</a></button>                                       
                                    <button class="mb-2 mr-2 btn btn-light" type="submit">저장</button>
								</div>
                             </form>									
							</div>
						</div>						
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>
<script>
class UploadAdapter {
    constructor(loader) {
        this.loader = loader;
    }

    upload() {
        return this.loader.file.then( file => new Promise(((resolve, reject) => {
            this._initRequest();
            this._initListeners( resolve, reject, file );
            this._sendRequest( file );
        })))
    }

    _initRequest() {
        const xhr = this.xhr = new XMLHttpRequest();
        xhr.open('POST', '/editor/upload', true);
        xhr.responseType = 'json';
    }

    _initListeners(resolve, reject, file) {
        const xhr = this.xhr;
        const loader = this.loader;
        const genericErrorText = '파일 업로드에 실패했습니다.'

        xhr.addEventListener('error', () => {reject(genericErrorText)})
        xhr.addEventListener('abort', () => reject())
        xhr.addEventListener('load', () => {
            const response = xhr.response
            if(!response || response.error) {
                return reject( response && response.error ? response.error.message : genericErrorText );
            }
			console.log(response);
            resolve({
                default: response.url //업로드된 파일 주소
            })
        })
    }

    _sendRequest(file) {
        const data = new FormData()
        data.append('upload',file)
		this.xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}');
        this.xhr.send(data);		
    }
}

function MyCustomUploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        return new UploadAdapter(loader)
    }
} 	
		
ClassicEditor
	.create( document.querySelector( '#kc_contents' ),
	{
		 extraPlugins: [MyCustomUploadAdapterPlugin]
	})
	.then(function()
	{
		console.log('ckeditor loaded');
	}
	)
	.catch( error => {
		console.error( error );
	});
</script>
<?php

namespace App\Http\Middleware;

use Closure;
use App\Libraries\PayErom;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class UpdateCurrencyRate
{
    public function handle($request, Closure $next)
	{
		//유효기간 만료되지 않은 데이터가 있는지 체크, 단 토요일, 일요일 제외		
		$data = DB::table('currency_rates')
		->where([
		['curr_expire','>=',now()]
		])
		->orderBy('curr_expire', 'desc')
		->limit(1)
		->get();
			
		if(!isset($data[0]->curr_expire) && date("N") < 6) //6은 토요일 7은 일요일
		{		
			//환율 업데이트
			$pay = new PayErom();	
			$result = $pay->getRate('USD','KRW');
			$arr_result = json_decode($result,true);	
			$display_currency = $arr_result['display_currency'];
			$processing_currency = $arr_result['processing_currency'];
			$exchnage_rate = $arr_result['exchange_rate'];
			$curr_expire = $arr_result['exchange_rate_end_time'];
			$processing_date = $arr_result['processing_date'];
			
			$data = DB::table('currency_rates')
			->insert([
			'curr_type1'=>$display_currency,
			'curr_type2'=>$processing_currency,
			'curr_rate'=>$exchnage_rate,
			'curr_expire'=>$curr_expire,
			'created_at'=>$processing_date
			]);
			
			$current_rate = $exchnage_rate;
		}
		else //주말
		{
			$data = DB::table('currency_rates')			
			->orderBy('curr_expire', 'desc')
			->limit(1)
			->get();
			
			$current_rate = $data[0]->curr_rate;
		}
		
		//환율 보정값 로드
		$usd_rate_spread = 0;
		$add_data = DB::table('define_settings')
		->where([
		['setting_key','=','usd_rate_spread']
		])
		->limit(1)
		->get();
		
		$usd_rate_spread = $add_data[0]->setting_value;
		
		//환율 데이터를 사용하는곳이 많으므로 전역변수화
		Config::set(['rate' => ['usd' => $current_rate+$usd_rate_spread]]);
		return $next($request);
	}
}

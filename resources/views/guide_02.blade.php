@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">GUIDE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>How to use</h2>

						<div class="row">
							<div class="col-md-12">	
								<h4 class="title">1. 회원가입</h4>
								<p>
									이씨코리아 메인페이지의 우측상단 ‘SIGN IN’ 또는 ‘프로필 아이콘’ 클릭! <br>
									이름, 이메일, <span class="red">연락처(배송과정 안내 메시지 수신용)</span> 를 입력하면 바로 회원가입 완료!<br>
									또는 SNS 계정 연동 회원가입도 가능합니다. 현재 연동가능한 계정은 Facebook, Google, Kakao, <span class="red">Apple </span>입니다.<br>
									마지막으로 회원가입을 완료하셨다면, 모바일 App 또는 PC용 통합 장바구니 Chrome App을 설치해주세요!<br>
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">2. 이씨코리아에서 쇼핑</h4>
								<p>
									찾으시는 상품이 있으신가요? 아니면 천천히 둘러보고 싶으신가요?<br>
									이씨코리아에서는 양질의 다양한 상품들을 제공하며, NAVER 상품 검색 서비스도 제공해드리고 있습니다!<br><br>

									특정 상품을 찾으신다면, 검색창에서 찾으시는 상품명 또는 키워드를 입력해주시고 검색조건을 선택해주세요!<br>
									NAVER 상품 검색을 선택하신 경우, NAVER SHOPPING에서 검색한 상품들을 보실 수 있으며,<br>
									기본 카테고리를 선택하신 경우, 이씨코리아의 상품을 검색하여 보여드립니다.<br><br>

									다양한 상품을 만나보고 싶으시다면, 검색창 밑 파란색 메뉴바의 카테고리를 이용하거나, 메인페이지 중간에 ECK CHOICE(MD추천상품)와 FLEX ZONE(카테고리별 핫한 상품)을 둘러보세요.<br>
									추가로 KYOBO BEST SELLER(최근 다른 회원들이 구매한 상품)도 함께 소개해 드리고있습니다!
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">3. 이씨코리아 장바구니 담기</h4>
								<p>
									구매하고 싶은 상품을 찾으셨다면, 이씨코리아 통합 장바구니에 담아주세요!<br>
									결제 주문 한 건에는 최대 7 종류의 상품을 담으실 수 있습니다.<br>
									상품 종류가 7개가 넘는 경우, 7개를 먼저 구매 신청하신 후 따로 분리 주문하여 주시기 바랍니다.<br><br>
									 
									외부 사이트(쿠팡, 네이버 스마트스토어) 상품인 경우,  상품의 옵션을 선택하신 뒤 통합 장바구니 담기 버튼을 눌러주세요.<br>
									자동적으로 이씨코리아에 주문 정보가 연동됩니다. 최종적으로 주문 정보를 다시 한번 확인해주시고,<br>
									개인 연락처를 포함한 배송 정보를 입력한 뒤 결제하시면 구매 완료!<br><br>

									<span class="red">(크롬 익스텐션 설치 & 이용방법 이미지로 보여주기)</span><br><br>

									이씨코리아 상품인 경우, 일반 쇼핑몰처럼 옵션 선택 후 장바구니에 담아주세요.<br>
									결제 전 주문 정보를 다시 한번 확인해주시고, 개인 연락처를 포함한 배송 정보를 입력한 뒤 결제하시면 구매 완료!<br>
									EC KOREA 에서 결제 가능한 카드는<span class="red"> 0000,0000,0000 </span> 로 국내/해외 카드사 모두 지원합니다.<br>
									결제가 안되는 경우, 00000@ecplaza.net 으로 메일을 보내주시기 바랍니다.<br><br>

									<span class="red">이 때, 배송 국가에 따라서 배송 불가 품목은 결제할 수 없으니 참고 바랍니다!  </span>
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">4. 주문상품 한국 내 배송</h4>
								<p>
									주문이 접수되면, 회원님을 대신하여 상품을 구매해드리고 이씨코리아 물류 센터로 발송합니다.<br>
									주문 상품들의 구매 신청이 완료될 때마다 회원님께 알림 문자가 발송됩니다.<br><br>

									또한 이씨코리아 물류 센터에 상품이 모두 도착할 때까지 전 과정은 회원님의 연락처로 안내를 드립니다.<br>
									보다 자세한 배송 현황을 확인 하고 싶으시다면  MY ACCOUNT 에서 보실 수 있습니다.<br><br>

									회원님의 주문이 EC KOREA 영업시간 내 (09:00~18:00) 에 접수된 경우, 당일에 상품을 구매하여 약 2~3일 이내에  EC KOREA 물류 센터로 도착합니다.<br><br>

									회원님의 주문이 휴무일 또는 영업시간 외에 접수된 경우, 다음 첫 영업일에 상품 구매가 이루어집니다.
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">5. 주문상품 입고</h4>
								<p>
									회원님의 주문 상품이 물류 센터에 도착할 때마다 입고 처리를 위한 안내를 전송드리며, <br>
									만약 입고 시점에서 불가피한 사유(품절, 파손, 오배송 등)가 확인된 경우, 회원님께 교환/환불 안내가 전송됩니다.<br><br>

									입고는 모든 결제 주문 상품이 물류 센터로 도착해야 완료되는 점 참고 해 주시기 바랍니다.<br>
									먼저 물류센터에 도착한 상품이라도 동일 주문의 다른 상품들이 도착하지 않았다면, 출고되지 않습니다.<br><br>

									최종적으로 주문하신 모든 상품이 물류 센터로 도착하게 되면, 회원님께 입고 완료 알림이 전송됩니다.
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">6. 검수/패킹 후 출고</h4>
								<p>
									물류센터로 입고된 상품들은 출고 전 주문서와 비교해 상품의 이상유무를 확인하고, 무게측정이 이루어집니다.<br>
									측정된 무게를 기준으로 배송비가 책정되며, 최종 배송비는 회원님께 알림을 통해 전송해 드립니다. <br>
									이후 회원님의 주문에 맞게 재포장 작업을 실행하고, 전 과정 및 결과는 회원님의 이메일에 선택하신 유형으로 (사진, 영상) 전송됩니다.<br><br>

									검수/패킹 과정에서 문제가 없으셨다면 원하시는 배송 옵션을 선택한 뒤, 국제 배송비를 결제해주세요!<br>
									국제 배송비 결제가 확인되면, 회원님의 배송지로 국제 배송이 시작됩니다.<br>
									휴무일 또는 영업시간 외에 배송비 결제가 완료된 경우, 다음 첫 영업일을 기준으로 국제 배송이 시작됩니다.
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">7. 국제 배송(항공, 해상)</h4>
								<p>
									국제 배송은 회원님의 배송지, 선택하신 옵션, 그리고 상황에 따라 다르나 일반적으로 1주 정도 소요됩니다.<br>
									일반인 출입금지 지역과같이, 일부 특수한 주소의 경우, 배송이 불가능할 수 있으므로 이 점 참고해 주시기 바랍니다.<br>
									배송 조회는 운송장 번호가 등록된 이후부터  가능합니다.<br>
									<span class="red">특수한 경우의 중/대형 화물은 해상으로 운송됩니다.</span> (Inspection & Fees 중/대형 화물 안내 참조)<br>
									주문량 폭주 등 기타 예외적인 상황으로 인해 배송이 지연되어 배송 조회가 되지 않을 경우, 1:1 문의 해주시기 바랍니다.
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">8. 통관 및 관부가세 납부</h4>
								<p>
									주문 상품이 회원님의 국가로 도착하게 되면, 통관 절차가 진행 됩니다.<br>
									만약 결제 단계에서 차단되지 않은 통관불가품목이 포함 되어 있는 경우, 현지 통관에서 통보 없이 폐기될 수 있습니다.<br>
									주문 전, 각 나라 관세청 홈페이지에 제공되는 통관불가품목 목록을 참고하여 주문해주시기 바랍니다.<br>
									국가별 면세범위를 포함한 통관 기준에 따라 회원님의 주문 상품에 관부가세가 합산되어 청구될 수 있습니다.<br>
									청구된 관부가세는 회원님에게 문자 및 이메일로 안내되며, 관부가세 결제가 완료되어야 통관 절차가 종료됩니다.<br>
									자세한 내용은 <span class="red">Customs duty rates 및 List of restricted & prohibited items</span> 참고 바랍니다.
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">9. 현지 배송</h4>
								<p>
									주문 상품이 통관을 통과하면, 현지 운송업체를 통해 회원님의 배송지로 배송이 시작됩니다.<br>
									배송 조회는 국제 배송과 마찬가지로 통관이 종료되고 운송장 번호가 등록된 이후부터 가능합니다. 
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">10. 주문상품 수령</h4>
								<p>
									주문 상품을 모두 수령하셨다면, 이상유무를 확인해주시고 리뷰를 남겨주세요!<br>
									<span class="red">만약 상품에 이상이 있을 경우, Returns & Exchange 를 확인 해주시기 바랍니다! </span>
								</p>								

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">1. Sign Up</h4>
								<p>
									Click the ‘SIGN IN’ button or ‘Profile Icon’ in the upper right corner of the EC Korea main page!<br>
									Enter your name, e-mail, and other contact information (to receive a message regarding shipping details) to complete registration process!<br>
									You can also sign-up with an SNS account. Currently, Facebook, Google, Kakao, and Apple are the only available account that can be linked.<br><br>

									Lastly, once you have completed the registration, please install the mobile app or the integrated shopping cart Chrome App on youe PC before you start shopping. 
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">2. Shopping at EC Korea</h4>
								<p>
									Is there a product you're looking for? Or do you want take your time to browse around?<br>
									EC Korea offers a variety of high-quality products and NAVER search engine service!<br><br>

									If you are looking for a particular product, please enter the name or the keyword that match what you want in the search box and select provided criteria!<br>
									If you choose to search for products sold on NAVER, you can see the search results on NAVER SHOPPING.<br>
									If you select the basic category, you can see the search results on EC Korea.<br><br>

									If you want to see a variety of products, use the category under blue menu bar below the search box or use the ECK CHOICE (MD recommended products) and FLEX ZONE(hot products by category) that can be found at the center of the main page.<br>
									In addition, we're introducing KYOBO BEST SELLER (products that were recently purchased by other members)!
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">3. EC KOREA SHOPPING CART</h4>
								<p>
									When you find the product you want to purchase, please add it to the integrated shopping cart of EC Korea!<br>
									A payment order can contain up to 7 products (not by quantity).<br>
									If there are more than 7 product types, please purchase of 7 items prior to ordering remains products.<br><br>

									If it's a product from an external site (Coupang, Naver smartstore), please select an option for the product and press the "Put the integrated shopping cart"button.<br>
									Your order information will be automatically directed to EC Korea shopping cart. Before going to the next step, please check to make sure the order information is correct.<br>
									If you make a payment after entering the shipping and personal contact information, the order is completed!<br><br>

									(크롬 익스텐션 설치 & 이용방법 이미지로 보여주기)<br><br>

									For products found at EC Korea, please select the option and add it to your shopping cart as done in other shopping mall.<br>
									Please check the order information again before you make the payment, and enter the shipping and personal contact information, and complete the purchase!<br><br>

									Cards that can be used at EC KOREA are  <span class="red">0000,0000,0000 </span>, which are supported by both domestic and overseas card companies.<br>
									Please send an email to 00000@ecplaza.net  if the payment does not preceed.<br><br>

									<span class="red">Please note that you cannot make the payment for restricted items depending on the delivery country!</span>

								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">4. PRODUCT DELIVERY IN  KOREA</h4>
								<p>
									When the order is received, we will make the purchase on your behalf and the product will be sent to the EC Korea distribution center.<br>
									We will send the notification message, when the purchase request is completed for every ordered products.<br><br>

									In addition, the entire process will be tracked and  informed to the purchaser until all products arrive at the EC Korea distribution center.<br>
									You can also check the status of the products at detail under MY ACCOUNT page.<br><br>

									If your order is placed within EC KOREA business hours (09:00~18:00 KST), the purchase will be requested on the same day and will be arrived at the EC KOREA distribution center within 3 days.<br><br>

									If your order is placed on a holiday or outside business hours, the purchase request will be placed on the coming business day.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">5. PRODUCT WAREHOUSING</h4>
								<p>
									Whenever your ordered product arrives at the distribution center, we will send you a notification for warehousing process.<br>
									If an unavoidable reason (out of stock, damaged, misdelivery, etc.) is found at the time of arrival, an exchange/refund information will be sent to you.<br><br>

									Please note that warehousing is not completed until all payment orders arrive at the distribution center.<br>
									Even if the a product arrives at the distribution center first, it will not be shippied if other products of the same order have not arrived.<br><br>

									When all of the products arrive at the distribution center, we will send you the completion notification. 
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">6. INSPECTION/PACKING/SHIPPING</h4>
								<p>
									Products received at the distribution center are compared with the purchase order before shipment to check for product fault and weigh them.<br>
									Shipping costs will be determined based on the measured weight, and the final shipping cost will be sent via notification.<br>
									Re-packaging will be carried out based on your order, and all processes and results will be sent to your email with the options you selected (photo, video).<br><br>

									If there is no problem with the inspection/packing process, please select the shipping option you want and make a payment for the international shipping fee!<br>
									Once the international shipping fee payment is confirmed, international shipping will begin and will go to your shipping address.<br>
									If payment is made on holidays or outside business hours, international shipping will begin at the coming business day.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">7. INTERNATIONAL SHIPPING (AIR, SEA)</h4>
								<p>
									International shipping usually takes about a week depending on your shipping address, options, and circumstances.<br>
									Please note that delivery may not be possible for some unique addresses, such as areas where the public is not allowed to enter.<br>
									Tracking is possible after the waybill number is registered.<br>
									In special circumstances, such as medium/large cargoes, the products will be shipped by sea (see Inspection & Fees Medium/Large Cargo Guidance).<br>
									If delivery is delayed due to other exceptional circumstances such as congestion in an order quantity, please contact our 1:1 customer service. 
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">8. CUSTOMS CLEARANCE AND GOVERNMENT TAX PAYMENT</h4>
								<p>
									When the ordered product arrives in your country, customs clearance process will proceed.<br>
									If a partial restrict article is not blocked at the payment process, it can be discarded without notification from the local customs.<br>
									Before placing an order, please refer to the list of restrict items provided on the website of each country's customs office.<br>
									Your order may be charged with additional surtaxes based on customs clearance criteria including the country's tax-free range.<br>
									We will send you the notification of the requested customs tax, and the customs clearance process will be done only after payment of the customs tax is completed.<br>
									For more information, please refer to <span class="red">Custom duty rates, and  List of restricted & protected items.</span>
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">9. LOCAL SHIPPING</h4>
								<p>
									Once the order has passed customs clearance, delivery will be initiated to your shipping address through the local carrier services.<br>
									As with international shipping, tracking is possible after the waybill number is registered.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">10. RECEIVE ORDER PRODUCTS</h4>
								<p>
									If you have received all of the items you ordered, please check if there are any faults and  leave a review!<br>
									<span class="red">If there is a problem with the product, please check Returns & Exchange!</span>
								</p>

								<div class="blank10"></div><div class="blank10"></div>


								
							</div>
							<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('guide_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

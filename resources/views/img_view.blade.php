<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<!-- 모바일 대응을 위한 뷰포트 기본 설정 -->
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<style>
html {
	height: -webkit-fill-available;
}
body {
	margin: 0px; /* 브라우저 자체 기본 margin 없애기 */
	display: flex;
	justify-content: center; /* 자식 요소를 가로 가운데 정렬 */
	align-items: center; /* 자식 요소를 세로 가운데 정렬 */
	height: 100vh; /* '-webkit-fill-available'이 유효하지 않을 경우를 대비 */
	height: -webkit-fill-available;
}
.intro {
	
	width: 100%;
	padding: 15px 0px;
	text-align: center;
	color: #ffffff;
}
</style>
</head>
<body>
<div class="intro">
    <div> <img src="{{$url}}" style="max-width:100%;">
        <div style="margin-top:10px;">
            <button onclick="window.close();">Close</button>
        </div>
    </div>
</div>
</body>
</html>
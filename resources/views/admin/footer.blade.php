		<div class="app-wrapper-footer">
			<div class="app-footer">
				<div class="app-footer__inner">
					<div class="app-footer-left">
						<ul class="nav">
							<li class="nav-item">
								<a href="javascript:void(0);" class="nav-link">
									하단정보
								</a>
							</li>
							
						</ul>
					</div>					
				</div>
			</div>
		</div> 
	</div>
<script type="text/javascript" src="/css/assets/scripts/main.js"></script>
<script type="text/javascript" src="/js/assets/js/jquery.min.js"></script>
<script>
$(document).ready(function(e) {
	
	//좌측 메뉴 포커싱 스크립트 시작
	var alias = new Object;
	alias['/admin/kculture/new'] = '/admin/kculture';
	alias['/admin/faq/new'] = '/admin/faq';
	alias['/admin/board/new?wr_board=event'] = '/admin/board?wr_board=event';
	
	//쿼리스트링 있는경우
	var q = new URLSearchParams(location.search).get('status');
	var wr_board = new URLSearchParams(location.search).get('wr_board');
	
	var url = location.href;
	var host = location.hostname;
	var protocol = location.protocol;
	var query = url.split('?')[1];
	var pathname = location.pathname;	
	var addable = '';
	if(q) addable = '?status='+q;
	if(wr_board) addable = '?wr_board='+wr_board;	
	var match_url = pathname+addable;
	if(alias[match_url]) match_url = alias[match_url];
	//매칭
	$(".vertical-nav-menu a").each(function(index, element) {
		
        if($(this).attr('href') == match_url)
		{
			$(this).css('color','red');
			$(this).css('font-weight','bold');
			$(this).closest('ul').addClass('mm-show');
		}
    });
	//좌측 메뉴 포커싱 스크립트 종료
});
</script>
</body>
</html>

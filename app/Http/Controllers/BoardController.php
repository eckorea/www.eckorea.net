<?php

namespace App\Http\Controllers;

use App\Board;
use Illuminate\Http\Request;
//use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

class BoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	protected $wr_board, $list_url, $list_view, $view_view;
	
	public function __construct(Request $request)
    {
		if($request->is('notice*'))
		{
			$this->wr_board = 'notice';
			$this->list_url = '/notice/';
			$this->list_view = 'notice';
			$this->view_view = 'notice_view';
		}
		else if($request->is('inquiry*'))
		{
			$this->wr_board = 'inquiry';			
			$this->list_url = '/inquiry/';
			$this->list_view = 'inquiry';
			$this->view_view = 'inquiry_view';
		}
		else if($request->is('event*'))
		{
			$this->wr_board = 'event';			
			$this->list_url = '/event/';
			$this->list_view = 'event';
			$this->view_view = 'event_view';
		}
		else
		{
			//$this->wr_board = 'notice';
		}
		
	}
	 
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$path = isset($request->upfile) ? $request->upfile->store('inquiry','public') : '';		       
		Board::create([
		'wr_board' => $this->wr_board,
		'wr_title' => $request->input('title'),
		'wr_content' => $request->input('contents'),
		'wr_writer' => session('mb_email'),
		'wr_file' => $path
		]);
				
		return redirect($this->list_url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function list(Board $board)
    {
		$cursor = $board
		->where('wr_board', $this->wr_board);
		
		if($this->wr_board == 'inquiry' && session('mb_level') < 10)
		{
			$cursor = $cursor->where('wr_writer',session('mb_email'));
		}		
		
		$data = $cursor
		->where('wr_board', $this->wr_board)
		->orderBy('created_at', 'desc')
		->paginate(5);		
        return view($this->list_view)->with('data',$data);
    }
	
	public function view(Board $board, Request $request, $wr_id)
    {
		//echo Route::currentRouteName(); exit;
		//$request->path(); exit;				
		
		$cursor = $board;
		
		if($this->wr_board == 'inquiry' && session('mb_level') < 10)
		{
			$cursor = $cursor->where('wr_writer',session('mb_email'));
		}

		$data = $cursor
		->where('wr_board', $this->wr_board)
		->where('wr_id', $wr_id)
		->get();
		
		if($request->is('notice_edit/*')) {
    		return view('notice_edit')->with('data',$data);
		}
		else if($request->is('notice_view/*')) 
		{
			return view('notice_view')->with('data',$data);
		}
		else if($request->is('event_view/*')) 
		{
			return view('event_view')->with('data',$data);
		}
		else if($request->is('inquiry_view/*')) 
		{
			return view('inquiry_view')->with('data',$data);
		}
		else if($request->is('inquiry/modify/*')) 
		{
			return view('inquiry_edit')->with('data',$data);
		}	        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function edit(Board $board, Request $request, $wr_id)
    {
		$row = $board
		->where('wr_id',$wr_id)
		->update(['wr_title'=>$request->input('title'), 'wr_content'=> $request->input('contents')]);		
		return redirect("/{$this->view_view}/$wr_id/");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Board $board)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Board  $board
     * @return \Illuminate\Http\Response
     */
    public function destroy(Board $board, $wr_id)
    {
        $deletedRows = $board->where('wr_id', $wr_id)->delete();
		return redirect($this->list_view);
    }
}

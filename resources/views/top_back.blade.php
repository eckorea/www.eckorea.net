<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Porto - Bootstrap eCommerce Template TEST">
    <meta name="author" content="SW-THEMES">       
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
  
    
    <script type="text/javascript">
        WebFontConfig = {
            google: { families: [ 'Open+Sans:300,400,600,700,800','Poppins:300,400,500,600,700' ] }
        };
        (function(d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = '/js/assets/js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>
    
    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="/css/assets/css/bootstrap.min.css">

    <!-- Main CSS File -->
    <link rel="stylesheet" href="/css/assets/css/style.min.css">
    <link rel="stylesheet" type="text/css" href="/css/assets/vendor/fontawesome-free/css/all.min.css">
</head>

<div class="page-wrapper">
	<div class="top-notice text-white bg-dark">
		<div class="container text-center">
			<h5 class="d-inline-block mb-0 mr-1">Get Up to <b>40% OFF</b> New-Season Styles</h5>
			<a href="category.html" class="category">MEN</a>
			<a href="category.html" class="category ml-2 mr-2">WOMEN</a>
			<small class="ml-1">* Limited time only.</small>
			<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		</div><!-- End .container -->
	</div>
	<header class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-left header-dropdowns">
					<div class="header-dropdown">
						<a href="#">USD</a>
						<div class="header-menu">
							<ul>
								<li><a href="#">EUR</a></li>
								<li><a href="#">USD</a></li>
							</ul>
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->

					<div class="header-dropdown">
						<a href="#"><img src="/image/flags/en.png" alt="England flag">ENG {{@session('locale')}}</a>
						<div class="header-menu">
							<ul>
								<li><a href="/lang/en"><img src="/image/flags/en.png" alt="England flag">ENG</a></li>
								<li><a href="/lang/ko"><img src="/image/flags/fr.png" alt="Korea flag">KOR</a></li>
							</ul>
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->                       
				</div><!-- End .header-left -->

				<div class="header-right">
					<p class="welcome-msg">[{{@session('mb_email')}}] @lang('common.welcome')! </p>

					<div class="header-dropdown dropdown-expanded">
						<a href="#">Links</a>
						<div class="header-menu">
							<ul>
								<li><a href="my-account.html">MY ACCOUNT </a></li>
								<li><a href="/notice">CS CENTER</a></li>
								<li><a href="/login">LOG IN</a></li>
								<li><a href="/admin/member">ADMIN</a></li>
							</ul>
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->
				</div><!-- End .header-right -->
			</div><!-- End .container -->
		</div><!-- End .header-top -->

		<div class="header-middle sticky-header">
			<div class="container">
				<div class="header-left">
					<a href="/" class="logo">
						<img src="/image/logo.png" alt="Porto Logo">
					</a>

					<nav class="main-nav">
						<ul class="menu sf-arrows">
							<li>
								<a href="mall" class="sf-with-ul">MALL</a>
								<ul>
									<li><a href="#">Submenu1</a></li>
									<li><a href="#">Submenu1</a></li>
								</ul>
							</li>
							<li class=""><a href="product.html" class="sf-with-ul">REVIEW</a></li>								
							<li class=""><a href="product.html" class="sf-with-ul">BLOG</a></li>						
							<li class=""><a href="product.html" class="sf-with-ul">GUIDE</a></li>						
							<li class=""><a href="about" class="sf-with-ul">ABOUT US</a></li>						
						</ul>
					</nav>

					<div class="header-search">
						<a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
						<form action="#" method="get">
							<div class="header-search-wrapper">
								<div class="select-custom">
									<select id="cat" name="cat">
										<option value="4">BOOK</option>
										<option value="12">NO BOOK</option>										
									</select>
								</div><!-- End .select-custom -->
								<input type="search" class="form-control" name="q" id="q" placeholder="Search..." required>
								
								<button class="btn" type="submit"><i class="icon-search"></i></button>
							</div><!-- End .header-search-wrapper -->
						</form>
					</div><!-- End .header-search -->
				</div><!-- End .header-left -->

				<div class="header-right">
					<button class="mobile-menu-toggler" type="button">
						<i class="icon-menu"></i> ALL
					</button>
					
					<div class="dropdown cart-dropdown">
						<a href="/order/cart" class="dropdown-toggle">
							<span class="cart-count">2</span>
						</a>						
					</div><!-- End .dropdown -->
				</div><!-- End .header-right -->
			</div><!-- End .container -->
		</div><!-- End .header-middle -->
	</header><!-- End .header -->
</div><!-- End .page-wrapper -->


<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->
	<div class="mobile-menu-container">
		<div class="mobile-menu-wrapper">
			<span class="mobile-menu-close"><i class="icon-cancel"></i></span>
			<nav class="mobile-nav">
				<ul class="mobile-menu">
					<li>
						<a href="category.html">MALL</a>  
						<ul>
							<li>
								<a href="#">Submenu1</a>
								<ul>
									<li><a href="#">Submenu2</a></li>
									<li><a href="#">Submenu2</a></li>
								</ul>
							</li>
							<li><a href="#">Submenu1</a></li>
						</ul>
					</li>                    
					<li class=""><a href="product.html">REVIEW</a></li>								
					<li class=""><a href="product.html">BLOG</a></li>						
					<li class=""><a href="product.html">GUIDE</a></li>						
					<li class=""><a href="product.html">ABOUT US</a></li>								 
				</ul>
			</nav><!-- End .mobile-nav -->

			<div class="social-icons">
				<a href="#" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
				<a href="#" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
				<a href="#" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
			</div><!-- End .social-icons -->
		</div><!-- End .mobile-menu-wrapper -->
	</div><!-- End .mobile-menu-container -->
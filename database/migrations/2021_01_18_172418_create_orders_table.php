<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';
			$table->bigIncrements('id');
			//$table->bigInteger('od_id')->primary();
			$table->string('od_id',32)->primary();
			$table->text('od_json_pay_id')->nullable();
			$table->string('mb_email');
			$table->string('od_first_name', 64)->nullable();
			$table->string('od_last_name', 64)->nullable();
			$table->string('od_tel', 64)->nullable();
			$table->string('od_hp', 64)->nullable();
			$table->string('od_country', 64);
			$table->string('od_city', 128)->nullable();
			$table->string('od_state', 64)->nullable();
			$table->string('od_zip', 16);
			$table->string('od_addr1');
			$table->string('od_addr2')->nullable();
			$table->float('od_cart_price', 10, 0)->default(0)->comment('카트총계(USD)');
			$table->float('od_cart_coupon', 10, 0)->default(0)->comment('카트적용쿠폰금액(USD)');
			$table->float('od_weight', 10, 0)->nullable()->comment('실제무게');
			$table->float('od_width', 10, 0)->nullable()->comment('가로');
			$table->float('od_length', 10, 0)->nullable()->comment('세로');
			$table->float('od_height', 10, 0)->nullable()->comment('높이');
			$table->float('od_weight_due', 10, 0)->nullable()->comment('적용무게');
			$table->float('od_send_cost1', 10, 0)->default(0)->comment('1차배송비');
			$table->float('od_send_cost2', 10, 0)->default(0)->nulladble->comment('2차배송비');
			$table->float('od_receipt_price1', 10, 0)->default(0)->comment('1차결제금액(USD)');
			$table->float('od_receipt_price2', 10, 0)->default(0)->comment('2차결제액(보통2차배송비)');
			$table->float('od_cancel_price', 10, 0)->nullable()->comment('주문취소금액(USD)');
			$table->text('od_mod_history')->nullable()->comment('주문변경로그');
			$table->text('od_memo')->nullable();
			$table->dateTime('created_at');
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			$table->SoftDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}

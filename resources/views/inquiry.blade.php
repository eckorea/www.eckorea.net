@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>

			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>INQUIRY</h2>
						@foreach($data as $row)         
						<article class="post">
							<div class="post-body">
								<div class="post-date"> <span class="day">{{date('d',strtotime($row->created_at))}}</span> <span class="month">{{date('M',strtotime($row->created_at))}}</span> </div>
								<!-- End .entry-date -->
								<div class="post-title"> <a href="inquiry_view/{{$row->wr_id}}">{{$row->wr_title}}</a> </div>
								<div class="post-content">
									<p>{{$row->wr_content}}</p>
									<a href="inquiry_view/{{$row->wr_id}}" class="read-more">Read More <i class="icon-angle-double-right"></i></a> 
								</div>
								<!-- End .entry-content -->

								<div class="post-meta"> <span><i class="icon-calendar"></i>{{$row->created_at}}</span> ({{$row->wr_writer}})</div>
								<!-- End .entry-meta --> 
							</div>
							<!-- End .entry-body --> 
						</article>          
						@endforeach   

						<div>
							<a href="/inquiry_write"><button type="button" class="btn btn-primary">Submit</button></a>
						</div><!-- End .form-footer -->
						<nav class="toolbox toolbox-pagination">

							<ul class="pagination">
							{{ $data->links() }}            
							<!--
							<li class="page-item disabled"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a> </li>
							<li class="page-item active"> <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a> </li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item"><a class="page-link" href="#">4</a></li>
							<li class="page-item"><a class="page-link" href="#">5</a></li>
							<li class="page-item"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a> </li>
							-->
							</ul>
						</nav>
					</div>
					<!-- End .col-lg-9 -->

					@include('cscenter_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
		<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

	@include('footer')
</body>

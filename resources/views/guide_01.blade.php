@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">GUIDE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>Introduction</h2>

						<div class="row">
							<div class="col-md-12">	
								<h4 class="title">역직구 구매대행이란?</h4>
								<p>
									역직구 구매대행이란 해외에 계신 교민분들 또는 한류팬들을 위해 한국 쇼핑몰의 원하시는 상품을 대신 주문하여 배송까지 제공하는 통합 서비스입니다. 
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">구매대행을 하는 이유</h4>
								<p>
									한국 쇼핑몰에서 회원가입이 어려우신 분, 과도한 배송비가 걱정이신 분, 간편하게 한국 상품을 구매하고 싶으신 분들 등의 다양한 요구에 맞추어 주문 절차의 모든 것을 대신하여 책임지고 수행해주기 때문에, 편리하고 쉽습니다! 
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">이씨코리아의 차별점</h4>
								<p>
									타사 대비 <span class="red">00%</span> 저렴한 배송비. <br>
									Samsung SDS Cello Square 및 Charm 3PL(Specialized 3PL Company) 와의 파트너십을 통한 체계적인 물류 시스템.<br>
									독특하고 가성비 있는 한국 제품의 공급업체들과 직접 제휴를 통한 일반 쇼핑몰과 차별되는 상품들.<br><br>

									글로벌 무역 플랫폼 이씨플라자에서 출발해 이제는 역직구 서비스로 나아가 새롭고 행복한 경험을 제공하는 것, <br> 
									저희가 이씨코리아 회원분들께 드리고자 하는 가치입니다.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">What is <span class="red">Cross-border Shipping</span> agency?</h4>
								<p>
									A cross-border shipping agency is an integrated service that provides ordering and delivery services for the products that Korean residents overseas or K-Wave fans wanted at the Korean shopping mall.
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">Why do you need <span class="red">Cross-border Shipping</span> agency?</h4>
								<p>
									Cross-border shipping is convenient and easy to use because we take responsibility for all of the ordering process to meet various needs such as those who have difficulty registering an account in Korean shopping malls, are concerned about excessive shipping costs, and want to easily purchasable Korean products!
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">Uniqueness of EC Korea</h4>
								<p>
									<span class="red">00% </span> lower shipping cost than other companies.<br>
									A systematic logistics system … through partnership with Samsung SDS Cello Square and Charm Specialized 3PL Company.<br>
									The product that are distinctive from regular shopping malls with direct affiliation with suppliers who provide unique and cost-effective K-products.<br><br>

									Starting from the global trading platform, EC Plaza, we are expanding by introducing cross-border shipping service that will provide a new and happy experience.<br>
									That is the value we want to provide to all our EC Korea members.
								</p>
							</div>
							<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('guide_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

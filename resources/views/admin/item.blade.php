@include('admin.top')
<div class="app-main"> @include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>상품 관리</div>
                    </div>
                </div>
            </div>
           
            <div class="main-card mb-3 card">
                <div class="card-body">
                
                    <div class="form-inline">
                        <div class="input-group">
                          <form method="get" action="">
                            <div class="input-group-prepend">
                                <select name="qkind" class="select_css">
                                    <option value="it_name" @if(Request::query('qkind') == 'it_name') selected @endif>상품명</option>
                                    <option value="it_id" @if(Request::query('qkind') == 'it_id') selected @endif>상품ID</option>
                                    <option value="ca_id" @if(Request::query('qkind') == 'ca_id') selected @endif>카테고리ID</option>
                                    <option value="it_origin" @if(Request::query('qkind') == 'it_origin') selected @endif>쇼핑몰</option>
                                </select>
                                <input type="text" class="form-control" name="q" value="{{Request::query('q')}}">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-secondary">검색</button>
                                </div>
                            </div>
                             <div>
                              판매여부 <input type="checkbox" name="it_use" value="Y" @if(Request::query('it_use') == 'Y') checked @endif />
                              메인전시여부 <input type="checkbox" name="it_main_use" value="Y" @if(Request::query('it_main_use') == 'Y') checked @endif />
                             </div>
                          </form>
                        </div>
                    </div>
                
                </div>
            </div>
           
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">상품ID</th>
                                        <th class="text-center">카테고리</th>                                        
                                        <th class="text-center">상품명</th>
                                        <th class="text-center">카테고리ID</th>
                                        <th class="text-center">판매가격</th>
                                        <th class="text-center">판매여부</th>
                                        <th class="text-center">쇼핑몰</th>
                                        <th class="text-center">수정일자</th>
                                        <th class="text-center">수정</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)            
                                
                                    <tr>
                                        <td>{{$row->it_id}}</td>
                                        <td>{{$row->ca_name}}</td>
                                        <td>{{mb_substr($row->it_name,0,32)}}</td>
                                        <td>{{$row->ca_id}}</td>                     
                                        <td>{{number_format($row->it_price)}}원</td>
                                        <td>{{$row->it_use}}</td>
                                        <td>{{$row->it_origin}}</td>
                                        <td>{{$row->item_updated_at}}</td>                     
                                        <td>                                        
                                        <a href="/admin/item/edit/{{$row->it_id}}">[수정]</a>
                                        <a href="/admin/item/delete/{{$row->it_id}}" onclick="return confirm('삭제하시겠습니까?');">[삭제]</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>
                            <button><a href="/admin/item/new">상품 추가</a></button>
                            </div>
                        </div>
                        <div style="text-align:center">
                            <div class="card-body">
                                <nav>
                                    <ul class="pagination">
                                    {{ $data->links() }}                                    
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.footer') </div>
</div>

@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>REVIEW</h2>	
                        @foreach($data as $row)
                        @php
                        $arr_imgs = json_decode($row->it_img_json);
                        @endphp
						<article class="post">
							
                            <div class="post-body2">
								<div class="post-date2">
									<a href="/mall/item/{{$row->it_id}}"><img src="{{$arr_imgs[0]}}" alt="product"></a>
								</div>
								<!-- End .entry-date -->
								<div class="post-title2"> <a href="/mall/item/{{$row->it_id}}">{{$row->it_name}}</a> </div>
								<div class="post-content2">
									<p>{{$row->review_contents}}</p>
									<!--<a href="/admin/review_view" class="read-more">Read More <i class="icon-angle-double-right"></i></a> -->
								</div>
								<!-- End .entry-content -->

								<div class="post-meta2"> <span><i class="icon-calendar"></i>{{$row->created_at}}</span> ({{substr($row->mb_email,0,3)}}***)</div>
								<!-- End .entry-meta --> 
							</div>
							<!-- End .entry-body --> 
						</article>
						<!-- End .entry -->
                        @endforeach
                        
                        <nav class="toolbox toolbox-pagination">
							<ul class="pagination">
								{{ $data->links() }}            
								<!--
								<li class="page-item disabled"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a> </li>
								<li class="page-item active"> <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a> </li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">4</a></li>
								<li class="page-item"><a class="page-link" href="#">5</a></li>
								<li class="page-item"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a> </li>
								-->
							</ul>
						</nav>
                        
					</div>

					<!-- End .col-lg-9 -->

					@include('cscenter_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

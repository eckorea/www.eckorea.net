@include('admin.top')

<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>쿠폰내역</div>
					</div>
				</div>
			</div>
			
			<div class="main-card mb-3 card">
				<div class="card-body">
					<div class="form-inline">							
						<div class="input-group">
							<div class="input-group-prepend">
								<form method="get" action="">
                                <select name="qkind" class="select_css">                                        
                                    <option value="email" @if(Request::query('qkind') == 'email') selected @endif>이메일</option>                                    
                                </select>                                
                            </div>
                           <input type="text" class="form-control" name="q" value="{{Request::query('q')}}">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">검색</button>
                            </div>
                            </form>
						</div>
					</div>		
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="table-responsive">
							<table class="align-middle mb-0 table table-borderless table-striped table-hover">
								<thead>
								<tr>
									<th class="text-center">이메일</th>
									<th class="text-center">쿠폰명</th>
									<th class="text-center">쿠폰금액</th>
									<th class="text-center">발송일</th>
									<th class="text-center">사용일</th>
									<th class="text-center">비고</th>
								</tr>
								</thead>
								<tbody>
                                @foreach($data as $row)
									<tr>	
										<td>{{$row->mb_email}}</td>
										<td>{{$row->cp_name}}</td>
										<td>${{$row->cp_amount}}</td>
										<td>{{$row->created_at}}</td>
										<td>{{$row->cp_used_date}}</td>
										<td><a href="/admin/coupon/delete/{{$row->id}}" onclick="return confirm('삭제하시겠습니까?');">[삭제]</a></td>
									</tr>
                                @endforeach
								</tbody>
							</table>
						</div>  
						<div style="text-align:center">
							<div class="card-body">							
							<nav>
								<ul class="pagination">
                                {{ $data->links() }} 
                                <!--
									<li class="page-item"><a href="javascript:void(0);" class="page-link" ><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
									<li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
									<li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
									<li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
									<li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
									<li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
									<li class="page-item"><a href="javascript:void(0);" class="page-link" ><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>-->
								</ul>
							</nav>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>

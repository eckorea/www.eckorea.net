@include('top')

<!-- 2021-03-19 메인 슬라이더 교체를 위한 js 추가 -->
<script src="/css/assets/js/jssor.slider-28.1.0.min.js"></script>

<?php 
$mAgent = array("iPhone","iPod","Android","Blackberry", 
    "Opera Mini", "Windows ce", "Nokia", "sony" );
$chkMobile = false;
for($i=0; $i<sizeof($mAgent); $i++){
    if(stripos( $_SERVER['HTTP_USER_AGENT'], $mAgent[$i] )){
        $chkMobile = true;
        break;
    }
}
// if($chkMobile===true){
if(false){
   echo "<div style='background-color: rgba(0,0,0, 0.6);top:0;left:0;width:100%;height:100%;position:fixed;z-index:10000000'>
      <div style='transform: translate(-50%, -50%);top:50%;left:50%;position:fixed;z-index:100000000'>
         <div style='text-align:center'><a href='https://apps.apple.com/kr/app/ec-korea-shopping-cart/id1563172179'><img src='/css/assets/images/app_icon_01.png' ></a></div>
		 <div style='text-align:center; padding-top:20px'><a href='https://play.google.com/store/apps/details?id=net.eckorea.kshopping'><img src='/css/assets/images/app_icon_02.png' ></a></div>
		 <div style='text-align:center; padding-top:20px; color:#fff; font-size:18px; text-decoration:underline'> 앱 설치후 이용 가능합니다. </div>
      </div>
   </div>";
}
?>

				<script type="text/javascript">

                    window.onload = function() {
                        let browserInfo = navigator.userAgent.toLowerCase(); 

                        let storeDiv = "<div style='background-color: rgba(0,0,0, 0.6);top:0;left:0;width:100%;height:100%;position:fixed;z-index:10000000'>";
                        storeDiv += "<div style='transform: translate(-50%, -50%);top:50%;left:50%;position:fixed;z-index:100000000'>";
                        storeDiv += "    <div style='text-align:center'><a href='https://apps.apple.com/kr/app/ec-korea-shopping-cart/id1563172179'><img src='/css/assets/images/app_icon_01.png' ></a></div>";
                        storeDiv += "<div style='text-align:center; padding-top:20px'><a href='https://play.google.com/store/apps/details?id=net.eckorea.kshopping'><img src='/css/assets/images/app_icon_02.png' ></a></div>";
                        storeDiv += "<div style='text-align:center; padding-top:20px; color:#fff; font-size:18px; text-decoration:underline'> 앱 설치후 이용 가능합니다. </div>";
                        storeDiv += "</div>";
                        storeDiv += "</div>";

                        if(browserInfo.indexOf("android") > -1) {
                            if (typeof (window.ecplaza) === 'undefined') {
                                document.write(storeDiv);
                            }
                        } else if(browserInfo.indexOf("ip") > -1) {
                            if (window.location.hostname.indexOf("www.eckorea.net") > -1) {
                                /* 아이폰 모바일 앱은 www2.eckorea.net 으로 접근하기 때문에 모바일 웹브라우저는 여기를 지나감 */
                                document.write(storeDiv);
                            }
                        }
                    };

                    window.jssor_1_slider_init = function() {

							var jssor_1_SlideoTransitions = [
							  [{b:500,d:1000,x:0,e:{x:6}}],
							  [{b:-1,d:1,x:100,p:{x:{d:1,dO:9}}},{b:0,d:2000,x:0,e:{x:6},p:{x:{dl:0.1}}}],
							  [{b:-1,d:1,x:200,p:{x:{d:1,dO:9}}},{b:0,d:2000,x:0,e:{x:6},p:{x:{dl:0.1}}}],
							  [{b:-1,d:1,rX:20,rY:90},{b:0,d:4000,rX:0,e:{rX:1}}],
							  [{b:-1,d:1,rY:-20},{b:0,d:4000,rY:-90,e:{rY:7}}],
							  [{b:-1,d:1,sX:2,sY:2},{b:1000,d:3000,sX:1,sY:1,e:{sX:1,sY:1}}],
							  [{b:-1,d:1,sX:2,sY:2},{b:1000,d:5000,sX:1,sY:1,e:{sX:3,sY:3}}],
							  [{b:-1,d:1,tZ:300},{b:0,d:2000,o:1},{b:3500,d:3500,tZ:0,e:{tZ:1}}],
							  [{b:-1,d:1,x:20,p:{x:{o:33,r:0.5}}},{b:0,d:1000,x:0,o:0.5,e:{x:3,o:1},p:{x:{dl:0.05,o:33},o:{dl:0.02,o:68,rd:2}}},{b:1000,d:1000,o:1,e:{o:1},p:{o:{dl:0.05,o:68,rd:2}}}],
							  [{b:-1,d:1,da:[0,700]},{b:0,d:600,da:[700,700],e:{da:1}}],
							  [{b:600,d:1000,o:0.4}],
							  [{b:-1,d:1,da:[0,400]},{b:200,d:600,da:[400,400],e:{da:1}}],
							  [{b:800,d:1000,o:0.4}],
							  [{b:-1,d:1,sX:1.1,sY:1.1},{b:0,d:1600,o:1},{b:1600,d:5000,sX:0.9,sY:0.9,e:{sX:1,sY:1}}],
							  [{b:0,d:1000,o:1,p:{o:{o:4}}}],
							  [{b:1000,d:1000,o:1,p:{o:{o:4}}}]
							];

							var jssor_1_options = {
							  $AutoPlay: 1,
							  $CaptionSliderOptions: {
								$Class: $JssorCaptionSlideo$,
								$Transitions: jssor_1_SlideoTransitions
							  },
							  $ArrowNavigatorOptions: {
								$Class: $JssorArrowNavigator$
							  },
							  $BulletNavigatorOptions: {
								$Class: $JssorBulletNavigator$,
								$SpacingX: 16,
								$SpacingY: 16
							  }
							};

							var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

							/*#region responsive code begin*/

							var MAX_WIDTH = 1180;

							function ScaleSlider() {
								var containerElement = jssor_1_slider.$Elmt.parentNode;
								var containerWidth = containerElement.clientWidth;

								if (containerWidth) {

									var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

									jssor_1_slider.$ScaleWidth(expectedWidth);
								}
								else {
									window.setTimeout(ScaleSlider, 30);
								}
							}

							ScaleSlider();

							$Jssor$.$AddEvent(window, "load", ScaleSlider);
							$Jssor$.$AddEvent(window, "resize", ScaleSlider);
							$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
							/*#endregion responsive code end*/
						};
					</script>
				   
					<style>
						/*jssor slider loading skin double-tail-spin css*/
						.jssorl-004-double-tail-spin img {
							animation-name: jssorl-004-double-tail-spin;
							animation-duration: 1.6s;
							animation-iteration-count: infinite;
							animation-timing-function: linear;
						}

						@keyframes jssorl-004-double-tail-spin {
							from { transform: rotate(0deg); }
							to { transform: rotate(360deg); }
						}

						/*jssor slider bullet skin 031 css*/
						.jssorb031 {position:absolute;}
						.jssorb031 .i {position:absolute;cursor:pointer;}
						.jssorb031 .i .b {fill:#000;fill-opacity:0.6;stroke:#fff;stroke-width:1600;stroke-miterlimit:10;stroke-opacity:0.8;}
						.jssorb031 .i:hover .b {fill:#fff;fill-opacity:1;stroke:#000;stroke-opacity:1;}
						.jssorb031 .iav .b {fill:#fff;stroke:#000;stroke-width:1600;fill-opacity:.6;}
						.jssorb031 .i.idn {opacity:.3;}

						/*jssor slider arrow skin 051 css*/
						.jssora051 {display:block;position:absolute;cursor:pointer;}
						.jssora051 .a {fill:none;stroke:#000;stroke-width:360;stroke-miterlimit:10;}
						.jssora051:hover {opacity:.8;}
						.jssora051.jssora051dn {opacity:.5;}
						.jssora051.jssora051ds {opacity:.3;pointer-events:none;}
					</style>

	<div class="page-wrapper">
		
		<main class="main">
			
				<div class="container container-not-boxed">					

					<div class="home-products-container text-center">
					
					<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1180px;height:388px;overflow:hidden;visibility:hidden;">
						<!-- Loading Screen -->
						<div data-u="loading" class="jssorl-004-double-tail-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
							
						</div>
						<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1180px;height:388px;overflow:hidden;">
                        @foreach($banner_data as $banner)
                        <div>
                            <a href="{{$banner->banner_link}}"><img data-u="image" src="/storage/banner/{{$banner->banner_id}}" /></a>
                        </div>                        
                        @endforeach 
                        
                        
                        </div><a data-scale="0" href="https://www.jssor.com" style="display:none;position:absolute;">slider html</a>
						<!-- Bullet Navigator -->
						<div data-u="navigator" class="jssorb031" style="position:absolute;bottom:16px;right:16px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
							<div data-u="prototype" class="i" style="width:13px;height:13px;">
								<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
									<circle class="b" cx="8000" cy="8000" r="5800"></circle>
								</svg>
							</div>
						</div>
						<!-- Arrow Navigator -->
						<div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
							<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
								<polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
							</svg>
						</div>
						<div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
							<svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
								<polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
							</svg>
						</div>
					</div>
					<script type="text/javascript">jssor_1_slider_init();
					</script>
					<!-- #endregion Jssor Slider End -->

					<div class="blank20">&nbsp;</div>

					</div>
					
					
										
					
					
					<div class="home-products-container text-center">
						<!--<h3 class="main_ti_line">ECK CHOICE</h3>-->
						<div class="row">
							<div class="col-md-3 mb-2 ">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">Hot Deal</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3" data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['hotdeal'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>
						
							<div class="col-md-3 mb-2 ">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">Featured Products</h3>
                                    
                                    <div class="owl-carousel owl-theme nav-image-center nav-thin px-3 " data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
                                    @foreach($result['featured'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach    									
									</div>
								</div>
							</div>
							

							<div class="col-md-3 mb-2 ">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">Best Sellers</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3" data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['bestseller'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>

							<div class="col-md-3 mb-2 ">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">New Arrivals</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3" data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['new'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>

							
						</div>
					</div>

					<div class="blank20">&nbsp;</div><div class="blank20">&nbsp;</div>

					<div class="container">
						<div class="products-container bg-white text-center mb-2 pb-5">
							<h3 class="main_ti_line">KYOBO BEST SELLER</h3>
							<div class="row product-ajax-grid mb-2">							
								@foreach($result['must'] as $row)

								@php
								$arr_it_url = json_decode($row->it_img_json);
								@endphp
								<div class="col-6 col-sm-4 col-md-3 col-xl-5col">
									<div class="product-default inner-quickview inner-icon imgfix ">
									@if($row->it_type == 'in')
									<a href="/mall/item/{{$row->it_id}}">
									 @if(isset($arr_it_url[0]))
										<img src="{{$row->it_thumbnail_url}}" class="shadow">
										@endif
										<!--<img src="/css/assets/images/products/product-13.jpg">-->
									</a>
									@else                    

									<a href="{{$row->it_url}}">
										@if(isset($arr_it_url[0]))
										<img src="{{$row->it_thumbnail_url}}">
										@endif
									</a>
									@endif

										<div class="product-details MT">

											<h3 class="product-title">
												<a href="/mall/item/10">{{$row->it_name}}</a>
											</h3>
											<div class="ratings-container">
												<!--<div class="product-ratings">

													@if($row->review_points > 0)
													<span class="ratings" style="width:{{($row->review_points/$row->review_counts) * 20}}%"></span>                               
													@endif	

													<span class="tooltiptext tooltip-top"></span>
												</div>-->
											</div><!-- End .product-container -->	

											<div class=" RT" style="width:100%">
												<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
												<span class="product-price2">₩ {{number_format($row->it_price,0)}}</span>
											</div>
										</div><!-- End .product-details -->
									</div>
								</div><!-- End .col-xl-5col -->
								@endforeach
							</div><!-- End .row -->

						</div><!-- End .container-box -->
					</div><!-- End .container -->
					

					
					<div class="home-products-container text-center">
						<h3 class="main_ti_line">FLEX ZONE</h3>
						<div class="row">
							<div class="col-md-4 mb-2">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">K-POPS</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3 " data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['flex'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    if(substr($row->ca_id,0,2) != '10') continue; //데이터에 모든 카테고리 혼재되어 있으므로 ex 해당 안되는 카테고리는 노출 X
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>

							<div class="col-md-4 mb-2">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">K-GOODS</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3" data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['flex'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    if(substr($row->ca_id,0,2) != '02') continue; //데이터에 모든 카테고리 혼재되어 있으므로 ex 해당 안되는 카테고리는 노출 X
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>

							<div class="col-md-4 mb-2">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">K-FASHION</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3" data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['flex'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    if(substr($row->ca_id,0,2) != '03') continue; //데이터에 모든 카테고리 혼재되어 있으므로 ex 해당 안되는 카테고리는 노출 X
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4 mb-2">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">K-BEAUTY</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3 " data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['flex'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    if(substr($row->ca_id,0,2) != '04') continue; //데이터에 모든 카테고리 혼재되어 있으므로 ex 해당 안되는 카테고리는 노출 X
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>

							<div class="col-md-4 mb-2">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">K-FOOD</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3" data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['flex'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    if(substr($row->ca_id,0,2) != '05') continue; //데이터에 모든 카테고리 혼재되어 있으므로 ex 해당 안되는 카테고리는 노출 X
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>

							<div class="col-md-4 mb-2">
								<div class="home-products bg-white px-4 pb-2 pt-4">
									<h3 class="section-sub-title mt-1 m-b-1">K-LIVING</h3>

									<div class="owl-carousel owl-theme nav-image-center nav-thin px-3" data-owl-options="{
										'dots': false,
										'nav': true,
										'responsive': {
											'480': {
												'items': 2
											},
											'326': {
												'items': 2
											},
											'768': {
												'items': 1
											}
										}
									}">
									@foreach($result['flex'] as $row)
                                    @php
                                    $arr_it_url = json_decode($row->it_img_json);
                                    if(substr($row->ca_id,0,2) != '06') continue; //데이터에 모든 카테고리 혼재되어 있으므로 ex 해당 안되는 카테고리는 노출 X
                                    @endphp
										<div class="product-default new_main_lay" >
											<figure>
												 
                                                <a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">
                                                 @if(isset($arr_it_url[0]))
                                                    <img src="{{$row->it_thumbnail_url}}">
                                                 @endif                                                 
                                                </a>
                                            </figure>
											<div class="product-details">
												
												<h3 class="product-title">
													<a href="@if($row->it_type == 'in') /mall/item/{{$row->it_id}} @else {{$row->it_url}} @endif">{{$row->it_name}}</a>
												</h3>
												
												<div class=" RT" style="width:100%">
													<span class="product-price">$ {{number_format($row->it_price / Config::get('rate.usd'),2) }}</span><br>
													<span class="product-price2">₩ {{number_format($row->it_price)}}</span>
												</div>
											</div>
										</div>
                                    @endforeach
									</div>
								</div>
							</div>
						</div>
					</div> 
					

				</div><!-- End .container-not-boxed -->				



				<div class="container container-not-boxed">
					<!--
					<div class="banner banner-big-sale mb-5" data-parallax="{'speed': 3.2, 'enableOnMobile': true}" data-image-src="assets/images/banners/banner-4.jpg">
						<div class="banner-content row align-items-center py-3 mx-0">
							<div class="col-md-9 pt-3">
								<h2 class="text-white text-uppercase mb-md-0 px-3">
									<b class="d-inline-block mr-3 mb-1">Big Sale</b>
									All new fashion brands items up to 70% off
									<small class="text-transform-none align-middle">Online Purchases Only</small>
								</h2>
							</div>
							<div class="col-md-3 py-3 text-center text-md-right">
								<a class="btn btn-light btn-lg mr-3 ls-10" href="#">View Sale</a>
							</div>
						</div>
					</div>-->


				
					<div class="feature-boxes-container row mt-6 mb-1">
						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/order/list">
									<img src="/image/team/bottom_ic_03.png"></a>
									<div class="feature-box-content">
										<h3 class="mb-0 pb-1" style="color: #444!important;text-transform: none!important;">My page</h3>
									</div>
								</a>
							</div>
						</div>
						
						
						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/guide/4">
									<img src="/image/team/bottom_ic_06.png"></a>
									<div class="feature-box-content">
                                        <h3 class="mb-0 pb-1" style="color: #444!important;text-transform: none!important;">Guide</h3>
									</div>
								</a>
							</div>
						</div>
						

						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/faq">
									<img src="/image/team/bottom_ic_01.png"></a>
									<div class="feature-box-content">
                                        <h3 class="mb-0 pb-1" style="color: #444!important;text-transform: none!important;">FAQ</h3>
									</div>
								</a>
							</div>
						</div>
						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/review">
									<img src="/image/team/bottom_ic_02.png"></a>
									<div class="feature-box-content">
                                        <h3 class="mb-0 pb-1" style="color: #444!important;text-transform: none!important;">Reviews</h3>
									</div>
								</a>
							</div>
						</div>

						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/shipping_fee">
									<img src="/image/team/bottom_ic_04.png"></a>
									<div class="feature-box-content">
                                        <h3 class="mb-0 pb-1" style="color: #444!important;text-transform: none!important;">Shipping Fee</h3>
									</div>
								</a>
							</div>
						</div>

						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/exchange_rate">
									<img src="/image/team/bottom_ic_05.png"></a>
									<div class="feature-box-content">
                                        <h3 class="mb-0 pb-1" style="color: #444!important;text-transform: none!important;">Exchange Rate</h3>
									</div>
								</a>
							</div>
						</div>
					</div><!-- End .feature-boxes-container.row -->

				</div><!-- End .container-not-boxed -->
			</main><!-- End .main -->		
		</div>

		



	

	


@include('footer')

</html>
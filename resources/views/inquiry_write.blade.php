@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">>CS CENTER</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>

			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>INQUIRY</h2>
						<div class="row">
							<div class="col-md-12">
								<form method="post" action="inquiry_write" enctype="multipart/form-data">
									{{ csrf_field() }}
									<div class="form-group required-field">
										<label for="contact-name">Title</label>
										<input type="text" class="form-control" name="title" required>
									</div>
									<!-- End .form-group -->

									<div class="form-group required-field">
										<label for="contact-message">Contents</label>
										<textarea cols="30" rows="1" class="form-control" name="contents" required></textarea>
									</div>
									<!-- End .form-group -->

									<div class="form-group required-field">
										<label for="contact-name">File</label>
										<input type="file" accept="image/*" class="form-control" name="upfile" style="padding:5px;">
									</div>
									<!-- End .form-group -->

									<div class="form-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
									<!-- End .form-footer -->
								</form>
							</div>
							<!-- End .col-md-8 --> 
						</div>
						<!-- End .row --> 
					</div>
					<!-- End .col-lg-9 --> 

					@include('cscenter_left') 
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
		<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

	@include('footer')
</body>

@include('top')

<body>
    <div class="page-wrapper">
        <main class="main">            

            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">K-CULTURE</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="about-section">
                <div class="container">
                    <h2 class="title">K-STUDY</h2>
                    
                </div><!-- End .container -->
            </div><!-- End .about-section -->

			<div class="team-section">
                <div class="container">                  
                    <div class="row pt-3">
						
                        @foreach(array_keys($kc_group) as $group_key)
                        <div class="col-sm-4 col-lg-4">
							<div class="widget">
								<h4 class="widget-title">{{$kc_group[$group_key]}}</h4>
								
								<ul class="links link-parts row mb-0">
									<div class="link-part col-sm-12">
                                    @foreach(array_keys($kc_boards) as $board)                                    	
                                    	@if($kc_boards[$board] == $group_key)
                                    	<li><a href="/mall/kculture/{{$board}}">{{$board}}</a></li>
                                        @endif
                                    @endforeach																		
									</div>									
								</ul>
							</div><!-- End .widget -->							
						</div><!-- End .col-sm-6 -->
                        @endforeach			
						
					</div><!-- End .row -->			
                </div><!-- End .container -->
            </div><!-- End .team-section -->                       
        </main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	@include('footer')
    
</body>

        
@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">Search</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>

			<div class="container">
            <input type="hidden" id="page" value="1">
				<div class="row">				
					<div class="col-lg-12 order-lg-last dashboard-content">
						<h2>Search</h2>
						<div class="row row-sm">      
                                            
                        <!--clone용 template_product 삭제 금지-->                            
                            <div id="template_product" class="col-6 col-md-4 col-xl-3 wrap-product-default" style="display:none;">                                                        	
								<div class="product-default">								                       
                                    <figure>                                         
                                        <a href="#" class="product-img-link"><img class="product-img" src="/image/products/product-4.jpg" style="height:250px"></a>
									</figure>
									<div class="product-details">
										<h2 class="product-title">
											<a href="#" class="product-name">TEMPLATE</a>
										</h2>
										<div class=" RT" style="width:100%">
											<span class="product-price">$ 49.00</span><br>
											<span class="product-price2">₩ 10,000</span>
										</div><!-- End .price-box -->
									</div><!-- End .product-details -->                                    
								</div> 
							</div><!-- End .col-xl-3 -->	                          

						<!--<nav class="toolbox toolbox-pagination">
							<input id="btn_more" type="button" value="더보기" onClick="get_search_data('{{$keyword}}');">
						</nav>-->
						
					</div><!-- End .col-lg-9 -->		
                    
                     <div id="wrap_msg" style="text-align:center; font-weight:bold; font-size:3em; margin-bottom:50px; display:none;">
                        We are sorry!<br>
                        Your item cannot be found :(
                     </div>
					
                    <nav class="toolbox toolbox-pagination" id="wrap_btn_more">
                    	<!-- ajax 로딩 이미지-->		
					</nav>
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
		<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

	@include('footer')
</body>
<script>
<?php
	//adcr.naver , smartstore.naver.com, shopping.naver.com
	/*
	var allowed_host = Array('smartstore.naver.com', 'shopping.naver.com'); //해당 호스트만 노출됨
	*/
?>

var allowed_host = Array('smartstore.naver.com', 'shopping.naver.com'); //해당 호스트만 노출됨

// var allowed_host = Array(); //해당 호스트만 노출됨

var showed_item_cnt = 0; //현재까지 노출된 아이템 갯수
var showed_last_url = ''; //마지막 아이템 url
var continue_limit = 4; //추가 로드 최대 횟수
var continue_cnt = 0; //현재 추가 로드 횟수
var processing_page = 1; //현재 프론트 상에서의 실질적인 페이징
var page_limit = 40; //페이지당 보여지는 개수

function inArray(needle, haystack) {
var length = haystack.length;
for(var i = 0; i < length; i++) {
    if(haystack[i] == needle) return true;
}
return false;
}

function get_search_data(category, keyword)
{
var page = $("#page").val();

//AJAX
$.ajax({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
type: 'GET',
url: '/mall/search/'+category+'/'+keyword+'/'+page,
dataType: 'json',
timeout: 16000,
beforeSend: function()
{
    $("#wrap_btn_more").html("상품 정보를 불러오는 중입니다... <img src='/image/hori_loading.gif'>");
},
success: function(data) {
    //timeout 되면 다시 처리하는.. error 또는 fail때 함수 다시 로드
    console.log(data);
    $(data.entity).each(function(index, row) {
    //허용된 host만 노출
      var parser = document.createElement('a');
      parser.href = row.url;
      if(!inArray(parser.hostname,allowed_host) && category == 'SS')
      {
          //console.log(parser.hostname);
          return;
      }

    //허용된 host만 노출 END
      var modi_html = $("#template_product").clone();
      var usd_price = row.price / {{Config::get('rate.usd')}};
      modi_html.css("display","inline-block");
      modi_html.find(".product-img").attr('src',row.mainImgUrl);
      modi_html.find(".product-img-link").attr('href',row.url);
      modi_html.find(".product-name").attr('href',row.url).text(row.name);
      modi_html.find(".product-price").text('$ '+parseFloat(usd_price).toFixed(2));
      modi_html.find(".product-price2").text('₩ '+row.price);
      $(".wrap-product-default").last().after(modi_html);
      //추가 load 여부를 확인하기 위한 데이터
      showed_item_cnt++;
      showed_last_url = row.url;
    });

    //내부 페이징 카운터 UP
    $("#page").val((page*1)+1);

    //더보기 버튼 표시
    var btn_html = "<input id=\"btn_more\" type=\"button\" value=\"더보기\" onClick=\"get_search_data('{{$category}}', '{{$keyword}}');\">";
    $("#wrap_btn_more").html(btn_html);

    //check how many items viewed. 현재 아이템갯수와, 나와야할 갯수 비교 & 무한루프 방지용 리밋 조건을 걸음
    if(showed_item_cnt < (processing_page) * page_limit && continue_limit > continue_cnt)
    {
        console.log('Items less do more. Processing Page:' + processing_page +', Items:'+showed_item_cnt);
        continue_cnt++;

        //다시 로드 요청
        get_search_data('{{$category}}','{{$keyword}}');
    }
    else
    {
        console.log('Items Over Limit Done. Processing Page:' + processing_page +', Items:'+showed_item_cnt);
        processing_page++;
        continue_cnt = 0;

        //노출된 데이터 아무것도 없으면, 템플릿이 하나 숨겨져 있으므로 1개인 경우에는 추가된게 없는것이 됨
        if($(".product-default").length == 1)
        {
            $("#wrap_msg").show();
            $("#wrap_btn_more").hide();
        }
    }

    console.log('page'+$("#page").val());

},
error: function(data) {
    //보통은 타임아웃일 경우가 높다, 재로드
    if(continue_limit > continue_cnt)
    {
        console.log('error and retry');
        continue_cnt++;
        get_search_data('{{$keyword}}');
    }
}
});
}
$(document).ready(function(e) {
get_search_data('{{$category}}','{{$keyword}}');
});
</script>

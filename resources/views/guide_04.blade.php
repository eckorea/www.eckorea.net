@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">GUIDE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>Shipping fee information</h2>

						<div class="row">
							<div class="col-md-12">	
								<h4 class="title">배송요금표</h4>
								<p>
									위의 링크 클릭 시 Shipping Fee 페이지로 이동됩니다. 해당 페이지에서 배송요금표를 확인하실 수 있습니다.
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">배송비계산기</h4>
								<p>
									위의 링크 클릭 시 Shipping Fee 페이지로 이동됩니다. 해당 페이지에서 예상 배송비 견적을 확인하실 수 있습니다.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">SHIPPING FEE TABLE</h4>
								<p>
									Click the link above to go to the Shipping Fee page. You can check the shipping rate table on this page.
								</p>
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">SHIPPING FEE CALCULATOR</h4>
								<p>
									Click the link above to go to the Shipping Fee page. You can check the estimated shipping cost on this page.
								</p>

								
							</div>
							<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('guide_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

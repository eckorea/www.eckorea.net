@include('admin.top')
<div class="app-main"> 
	@include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>입고보류</div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">
                            
                                <select name="qkind">                                    
                                    <option value="">상품명</option>
                                    <option value="name">또 뭘로 검색?</option>
                                </select>
                            </div>
                            <input type="text" class="form-control" name="q" value="">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">검색</button>								
                            </div>							
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">이미지</th>
                                        <th class="text-center">상품정보</th>
                                        <th class="text-center">주문번호</th>
                                        <th class="text-center">주문인</th>
                                        <th class="text-center">신청일/입고보류일</th>
                                    </tr>
                                </thead>
                                <tbody>                               
									<tr>
										<td class="center_img"><img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg"></td>
										<td class="center_name">
											[쿠팡] - 입점판매자몰 <br>
											퍼실 딥클린 테크놀로지 라벤더젤 드럼용 리필 <br>
											<span>3L / 빨간색</span> <br>
											<p>2개</p>
										</td>
										<td>EC-100522</td>
										<td>
											elpis@gmail.com
											<div class="blank10"></div>
											<button class="mb-2 mr-2 btn btn-primary">입고</button>
										</td>
										<td>
											2020-12-31 <br>
											2020-01-04
										</td>
									</tr>  
									<tr>
										<td colspan="6" class="center_memo">입고메모  <span>입고보류시 남긴 메모가 여기에 보여집니다.</span></td>
									</tr>
									<tr>
										<td class="center_img"><img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg"></td>
										<td class="center_name">
											[쿠팡] - 입점판매자몰 <br>
											퍼실 딥클린 테크놀로지 라벤더젤 드럼용 리필 <br>
											<span>3L / 빨간색</span> <br>
											<p>2개</p>
										</td>
										<td>EC-100522</td>
										<td>
											elpis@gmail.com
											<div class="blank10"></div>
											정상입고처리
										</td>
										<td>
											2020-12-31 <br>
											2020-01-04
										</td>
									</tr>  
									<tr>
										<td colspan="6" class="center_memo">입고메모  <span>입고보류시 남긴 메모가 여기에 보여집니다.</span></td>
									</tr>
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        @include('admin.footer') 
	</div>
</div>

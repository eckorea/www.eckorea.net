<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Message $message, Request $request)
    {
        $qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
				
		if($qkind == 'email') $data = $message->where('ms_receiver','LIKE',$q.'%');			
		else $data = $message;
		
		$data = $data
		->orderBy('created_at', 'desc')
		->paginate(5);		
        return view('/admin/message')->with('data',$data);	
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$arr_mb_email = explode(',',$request->input('mb_email'));
		
		foreach($arr_mb_email as $mb_email)
		{
			Message::create([		
			'ms_sender' => session('mb_email'),
			'ms_receiver' => $mb_email,
			'ms_text' => $request->input('ms_text')
			]);
		}        
				
		return redirect('/admin/message');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message, $id)
    {
        $data = $message		
		->where('id', $id)
		->get();
		return view('/admin/message_view')->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}

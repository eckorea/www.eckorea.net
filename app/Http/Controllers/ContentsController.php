<?php
namespace App\Http\Controllers;

use App\Board;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ContentsController extends Controller
{
	//Mall Controller에서도 쓰고 있음, 변경시 같이 변경해줘야함
	public $kc_boards = array(
	'K-TREND'=>'1',
	'K-BEAUTY'=>'2',
	'K-COOK'=>'3',
	'Simple Tests & Quizzes'=>'41',
	'Ordinary conversations'=>'41',
	'Lectures'=>'41',
	'TOPIK'=>'41',
	'Habits'=>'42',
	'Superstitions'=>'42',
	'Traditions'=>'42',
	'Customs'=>'42',
	'Culture & History'=>'42',
	'Landmarks'=>'43',
	'Shopping'=>'43',
	'Foods'=>'43',
	'Scenery'=>'43',
	'Activities'=>'43'
	);
	
	public function editor_upload(Request $request)
	{		
		$path = $request->hasFile('upload') ? $request->upload->store('editor','public') : '';
		//if ($request->file('photo')->isValid())			
		/*
		{
			"error": {
				"message": "The image upload failed because the image was too big (max 1.5MB)."
			}
		}*/
		
		$arr_data = array('url'=>asset('storage/'.$path));	
		return response()->json($arr_data);
	}
	
    public function faq_index(Request $request)
	{
		$data = DB::table('faqs')
		->paginate(10)->appends($request->query());		
		return view('/admin/faq')->with(['data'=>$data]);	
	}
	
	public function faq_view(Request $request, $faq_id = '')
	{		
		$lang = $request->query('lang');
		$data = DB::table('faqs')->where('faq_id',$faq_id)->get();
		
		$title = '';
		$contents = '';
		
		if(isset($data[0]))
		{		
			$title = $data[0]->{"faq_title_".$lang};
			$contents = $data[0]->{"faq_contents_".$lang};
		}
		
		return view('/admin/faq_view')->with(['data'=>$data, 'title'=>$title, 'contents' => $contents]);	
		
		//$data = DB::table('faqs')->where('faq_id',$faq_id)->get();
		//return view('/admin/faq_view')->with(['data'=>$data]);	
	}
	
	public function faq_edit(Request $request)
	{
		$faq_lang = $request->input('faq_lang');
		$faq_id = $request->input('faq_id');
		$faq_group = $request->input('faq_group');
		$faq_title = $request->input('faq_title');
		$faq_contents = $request->input('faq_contents');
		
		if($faq_id)
		{
			$data = DB::table('faqs')
			->where('faq_id',$faq_id)
			->update([
			'faq_group'=>$faq_group,
			"faq_title_$faq_lang"=>$faq_title,
			"faq_contents_$faq_lang"=>$faq_contents,		
			]);
			
			return redirect()->back();
		}
		else
		{
			$new_id = DB::table('faqs')
			->insertGetId([
			'faq_group'=>$faq_group,
			"faq_title_$faq_lang"=>$faq_title,
			"faq_contents_$faq_lang"=>$faq_contents,
			'created_at'=>Carbon::now()	
			]);
			
			return redirect("/admin/faq/view/$new_id?lang=$faq_lang");
		}		
	}
	
	public function faq_delete(Request $request, $faq_id = '')
	{
		$data = DB::table('faqs')->where('faq_id',$faq_id)->delete();
		return redirect('/admin/faq');
	}
		
	/******************kculture****************/
	public function kculture_index(Request $request)
	{
		$qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
		
		$data = DB::table('kcultures');
		
		//검색어				
		if($qkind == 'kc_board') $data = $data->where('kc_board','LIKE',$q.'%');
		if($qkind == 'kc_title') $data = $data->where('kc_title','LIKE',$q.'%');
		
		$data = $data->paginate(10)->appends($request->query());		
		return view('/admin/kculture')->with(['data'=>$data]);	
	}
	
	public function kculture_view(Request $request, $kc_id = '')
	{		
		$data = DB::table('kcultures')->where('kc_id',$kc_id)->get();
		return view('/admin/kculture_view')->with(['data'=>$data, 'kc_boards'=>$this->kc_boards]);	
	}
	
	public function kculture_edit(Request $request)
	{
		$kc_id = $request->input('kc_id');
		$kc_board = $request->input('kc_board');
		$kc_title = $request->input('kc_title');
		$kc_contents = $request->input('kc_contents');
		
		$kc_thumbnail_path = '';
		$kc_thumbnail_path = $request->hasFile('kc_thumbnail') ? $request->kc_thumbnail->store('kculture','public') : '';
				
		if($kc_id)
		{
			$data = DB::table('kcultures')
			->where('kc_id',$kc_id)
			->update([			
			'kc_board'=>$kc_board,
			'kc_title'=>$kc_title,
			'kc_contents'=>$kc_contents			
			]);
			
			if($kc_thumbnail_path) $data->update(['kc_thumbnail'=>$kc_thumbnail_path]); //이 경우 기존 파일을 삭제하는것이 좋을듯함
			
			return redirect()->back();
		}
		else
		{
			$new_id = DB::table('kcultures')
			->insertGetId([			
			'kc_board'=>$kc_board,
			'kc_title'=>$kc_title,
			'kc_contents'=>$kc_contents,		
			'kc_thumbnail'=>$kc_thumbnail_path,
			'kc_writer'=>session('mb_email'),
			'created_at'=>Carbon::now()	
			]);
			
			return redirect("/admin/kculture/view/$new_id");
		}		
	}
	
	public function kculture_delete(Request $request, $faq_id = '')
	{
		$data = DB::table('kcultures')->where('kc_id',$faq_id)->delete();
		return redirect('/admin/kculture');
	}
	
	/************** GUIDE ************/
	public function guide_index(Request $request)
	{
		$data = DB::table('guides')
		->paginate(10)->appends($request->query());		
		return view('/admin/guide')->with(['data'=>$data]);	
	}
	
	public function guide_view(Request $request, $guide_id = '')
	{
		$lang = $request->query('lang');
		$data = DB::table('guides')->where('guide_id',$guide_id)->get();
		
		$title = '';
		$contents = '';
		
		if(isset($data[0]))
		{		
			$title = $data[0]->{"guide_title_".$lang};
			$contents = $data[0]->{"guide_contents_".$lang};
		}
		
		return view('/admin/guide_view')->with(['data'=>$data, 'title'=>$title, 'contents' => $contents]);	
	}
	
	public function guide_edit(Request $request)
	{
		$guide_lang = $request->input('guide_lang');
		$guide_id = $request->input('guide_id');
		$guide_group = $request->input('guide_group');
		$guide_title = $request->input('guide_title');
		$guide_contents = $request->input('guide_contents');

		if($guide_id)
		{
			$data = DB::table('guides')
			->where('guide_id',$guide_id)
			->update([
			'guide_group'=>$guide_group,
			"guide_title_$guide_lang"=>$guide_title,
			"guide_contents_$guide_lang"=>$guide_contents,		
			]);
			
			return redirect()->back();
		}
		else
		{
			$new_id = DB::table('guides')
			->insertGetId([
			'guide_group'=>$guide_group,
			"guide_title_$guide_lang"=>$guide_title,
			"guide_contents_$guide_lang"=>$guide_contents,		
			'created_at'=>Carbon::now()	
			]);
			
			return redirect("/admin/guide/view/$new_id?lang=$guide_lang");
		}		
	}
	
	public function guide_delete(Request $request, $guide_id = '')
	{
		$data = DB::table('guides')->where('guide_id',$guide_id)->delete();
		return redirect('/admin/guide');
	}
	
	/************** BOARD ************/
	public function board_index(Request $request, Board $board)
	{
		$wr_board = $request->query('wr_board');
		
		$data = $board
		->where('wr_board',$wr_board)
		->orderBy('wr_id','desc')
		->paginate(10)->appends($request->query());
		return view('/admin/board')->with(['data'=>$data]);	
	}
	
	public function board_view(Request $request, Board $board, $wr_id = '')
	{
		$data = $board->where('wr_id',$wr_id)->get();
		
		$title = '';
		$contents = '';
		
		if(isset($data[0]))
		{		
			$title = $data[0]->{"wr_title"};
			$contents = $data[0]->{"wr_content"};
		}
		
		return view('/admin/board_view')->with(['data'=>$data, 'title'=>$title, 'contents' => $contents]);	
	}
	
	public function board_edit(Request $request, Board $board)
	{
		$wr_board = $request->input('wr_board');		
		$wr_id = $request->input('wr_id');		
		$wr_title = $request->input('wr_title');
		$wr_contents = $request->input('wr_contents');

		if($wr_id) //수정
		{
			$data = $board
			->where('wr_id',$wr_id)
			->update([		
			"wr_id"=>$wr_id,
			"wr_content"=>$wr_contents,		
			]);
			
			return redirect()->back();
		}
		else //신규
		{
			$new_id = $board
			->insertGetId([
			"wr_board"=>$wr_board,
			"wr_id"=>$wr_id,
			"wr_title"=>$wr_title,
			"wr_content"=>$wr_contents,		
			"wr_writer"=>session('mb_email'),
			'created_at'=>now()	
			]);
			
			return redirect("/admin/board/view/$new_id?wr_board=$wr_board");
		}		
	}
	
	public function board_delete(Request $request, Board $board, $wr_id)
	{
		$cursor = $board->where('wr_id',$wr_id);
		$data = $cursor->get();
		$wr_board = $data[0]->wr_board;		
		$cursor->delete();
		
		return redirect("/admin/board?wr_board=$wr_board");
	}
	
	/************************ TERMS OF USE *****************************/
	public function terms_view(Request $request)
	{		
		$lang = $request->query('lang');
		$data = DB::table('terms')->orderBy('created_at','desc')->limit(1)->get();
		
		$contents = '';
		
		if(isset($data[0]))
		{				
			$contents = $data[0]->{"terms_".$lang};
		}		
		
		return view('/admin/terms_view')->with(['data'=>$data,'contents' => $contents]);	
	}
	
	public function terms_edit(Request $request)
	{
		$terms_id = $request->input('terms_id');	
		$terms_lang = $request->input('terms_lang');	
		$terms_contents = $request->input('terms_contents');
		
		$data = DB::table('terms')
		->where('terms_id',$terms_id)
		->update([
		"terms_$terms_lang"=>$terms_contents,		
		]);
		
		return redirect()->back();		
	}
	
	/************************ PRIVATE POLICY *****************************/
	public function policy_view(Request $request)
	{		
		$lang = $request->query('lang');
		$data = DB::table('polices')->orderBy('created_at','desc')->limit(1)->get();
		
		$contents = '';
		
		if(isset($data[0]))
		{				
			$contents = $data[0]->{"policy_".$lang};
		}		
		
		return view('/admin/policy_view')->with(['data'=>$data,'contents' => $contents]);	
	}
	
	public function policy_edit(Request $request)
	{
		$policy_id = $request->input('policy_id');	
		$policy_lang = $request->input('policy_lang');	
		$policy_contents = $request->input('policy_contents');
		
		$data = DB::table('polices')
		->where('policy_id',$policy_id)
		->update([
		"policy_$policy_lang"=>$policy_contents,		
		]);
		
		return redirect()->back();		
	}
	
	//************************** BANNER ***********************//
	public function banner_index(Request $request)
	{		
		$data = DB::table('banners')
		->orderBy('banner_id','desc')
		->paginate(10)->appends($request->query());
		return view('/admin/banner')->with(['data'=>$data]);	
	}
	
	public function banner_view(Request $request, $banner_id = '')
	{
		$data = DB::table('banners')->where('banner_id',$banner_id)->get();
		
		$title = '';
		$banner_order = 9999;
		$banner_link = '';
		
		if(isset($data[0]))
		{		
			$title = $data[0]->{"banner_name"};
			$banner_order = $data[0]->{"banner_order"};
			$banner_link = $data[0]->{"banner_link"};
		}
		
		return view('/admin/banner_view')->with(['data'=>$data, 'title'=>$title, 'banner_order'=>$banner_order, 'banner_link'=>$banner_link]);	
	}
	
	public function banner_edit(Request $request)
	{		
		$banner_id = $request->input('banner_id');		
		$banner_name = $request->input('banner_name');
		$banner_order = $request->input('banner_order');
		$banner_link = $request->input('banner_link');

		if($banner_id) //수정
		{
			$data = DB::table('banners')
			->where('banner_id',$banner_id)
			->update([		
			"banner_name"=>$banner_name,		
			"banner_order"=>$banner_order,
			"banner_link"=>$banner_link	
			]);
			
			$banner_path = isset($request->banner_img) ? $request->banner_img->storeAs('banner',$banner_id,'public') : '';
			
			/*
			if($banner_path)
			{
				$data = DB::table('banners')
				->where('banner_id',$banner_id)
				->update([		
				"banner_img"=>$banner_path,		
				]);
			}*/
			
			return redirect()->back();
		}
		else //신규
		{
			$new_id = DB::table('banners')
			->insertGetId([
			"banner_name"=>$banner_name,
			"banner_order"=>$banner_order,
			"banner_link"=>$banner_link,
			'created_at'=>now()	
			]);
			
			$banner_path = isset($request->banner_img) ? $request->banner_img->storeAs('banner',$new_id,'public') : '';
			
			return redirect("/admin/banner/view/$new_id");
		}		
	}
	
	public function banner_delete(Request $request, $banner_id)
	{
		$cursor = DB::table('banners')->where('banner_id',$banner_id);
		$cursor->delete();
		
		return redirect("/admin/banner");
	}
	
}

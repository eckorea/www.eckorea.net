@include('admin.top')
<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>카테고리</div>
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="tab-content">                            
							<div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                            <form method="post" action="/admin/category/{{$type}}">
                             {{ csrf_field() }}
                            <input type="hidden" name="ca_id" value="{{$ca_id}}" />
								<div class="card-body">                                                                
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">상위카테고리</label>
										<div class="col-sm-10">{{$ca_parent_name}}</div>
									</div>		
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">카테고리명</label>
										<div class="col-sm-10"><input class="form-control" name="ca_name" value="{{$ca_name}}" ></div>
									</div>              											
									
									<button class="mb-2 mr-2 btn btn-light" type="button"><a href="/admin/category">목록</a></button>                                       
                                    <button class="mb-2 mr-2 btn btn-light" type="submit">저장</button>
								</div>
                             </form>									
							</div>
						</div>						
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>
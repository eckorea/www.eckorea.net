@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">GUIDE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>Inspection & Fees</h2>

						<div class="row">
							<div class="col-md-12">	
								<h4 class="title">검수과정</h4>
								<p>
									이미지 자리인가요
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">검수 유의사항</h4>
								<p>
									이미지 자리인가요
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">INSPECTION PROCESS</h4>
								<p>
									이미지 자리인가요
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">SHIPPING FEE TABLE</h4>
								<p>
									이미지 자리인가요
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">INSPECTION NOTICE</h4>
								<p>
									이미지 자리인가요
								</p>

								
							</div>
							<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('guide_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

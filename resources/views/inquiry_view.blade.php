@include('top')

<body>
	<div class="page-wrapper">
	<main class="main">
		<nav aria-label="breadcrumb" class="breadcrumb-nav">
			<div class="container">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.html">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
				</ol>
			</div>
			<!-- End .container --> 
		</nav>

		<div class="container">
			<div class="row">
				<div class="col-lg-9 order-lg-last dashboard-content">
					<h2>INQUIRY</h2>
					<div class="row">
						<div class="col-md-12">
							<div class="post-meta"> <span><i class="icon-calendar"></i>{{$data[0]->created_at}}</span> ({{$data[0]->wr_writer}})</div>
							<div class="blank20"></div>
							<div class="form-group">
								<label for="contact-name">Title</label>
								<input type="text" class="form-control" readonly placeholder="{{$data[0]->wr_title}}">
							</div>
							<!-- End .form-group -->

							<div class="form-group">
								<label for="contact-message">Contents</label>
								<textarea cols="30" rows="1" class="form-control" readonly>{{$data[0]->wr_content}}</textarea>
							</div>
							<!-- End .form-group -->

							<div class="form-footer">
								<button type="submit" class="btn btn-primary" onClick="inquiry_modify('{{$data[0]->wr_id}}')">Modify</button>
								<button type="submit" class="btn" onClick="inquiry_delete('{{$data[0]->wr_id}}')">Delete</button>
								<button type="submit" class="btn btn-primary" onClick="inquiry_answer('{{$data[0]->wr_id}}')">Answer</button>
							</div>
							<!-- End .form-footer --> 
						</div>
						<!-- End .col-md-8 --> 
					</div>
					<!-- End .row --> 
				</div>
				<!-- End .col-lg-9 --> 

				@include('cscenter_left') 
			
			</div>
			<!-- End .row --> 
		</div>
		<!-- End .container -->

		<div class="mb-5"></div>
		<!-- margin --> 
	</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

	<script>
		function inquiry_delete(wr_id)
		{
			if(confirm('Delete?'))
			{
				location.href = '/inquiry/delete/'+wr_id;
			}
		}

		function inquiry_modify(wr_id)
		{
			location.href = '/inquiry/modify/'+wr_id;
		}
	</script>
	@include('footer')
</body>

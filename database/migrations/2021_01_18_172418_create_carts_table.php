<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carts', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';
			//$table->bigIncrements('id');
			//$table->integer('ct_id', true);
			$table->bigIncrements('ct_id');
			$table->string('od_id',32)->nullable();
			$table->string('mb_email');
			$table->enum('ct_type', array('in','out'))->comment('외부상품인지, 인하우스DB상품인지');
			$table->string('ct_origin', 64)->comment('상품출처(네이버/교보문고/ETC)');
			$table->string('it_url', 512)->nullable()->comment('외부 상품 URL');
			$table->integer('it_id')->nullable();
			$table->string('it_name');
			$table->string('it_option');
			$table->float('ct_price', 10, 0);
			$table->float('ct_krw_price', 10, 0)->comment('인풋시점의 한화금액');
			$table->integer('ct_qty');
			$table->string('ct_ship_method', 32)->nullable();
			$table->string('ct_carrier', 64)->nullable()->comment('배송회사');
			$table->string('ct_ship_no', 64)->nullable()->comment('송장번호');
			$table->float('ct_send_cost', 10, 0)->comment('배송비');
			$table->enum('ct_select', array('Y','N'))->default('Y')->comment('장바구니결제선택여부');
			$table->string('ct_memo', 512)->nullable();
			$table->string('ct_status', 32);
			$table->string('ct_house_order_status', 32)->comment('내부 주문 상태');
			$table->string('ct_house_status', 32)->comment('내부 입고 상태');
			$table->text('ct_house_memo')->nullable()->comment('입고 메모');
			$table->dateTime('created_at');
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			$table->SoftDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('carts');
	}

}

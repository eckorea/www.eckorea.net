<!-- 2021-05-11 추가 footer 소스 시작 -->

<div class="page-wrapper">
    <footer class="footer">
        <div class="footer-middle footer-take pt-3">
            <div class="container">
                <div class="row mt-3 mb-0">
                    <div class="col-lg-9 col-md-12 col-sm-12 pr-4">
                        <p class="talk-text"><strong>Kakako Chat Hours</strong> From 9:30am to 6:00pm (Lunch 12:00pm to 1:00pm). <a href="http://pf.kakao.com/_UxnFDK/chat"><span><img alt="" src="/image/team/icon-talk.png">Kakao Talk 1:1 Chat (톡)</span></a></p>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <p class="talk-text cus"><strong>CS Center </strong> +82-2-3433-4242 </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 m-b-4 footer-menu">
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="/image/team/logo-white.png" alt="eckorea" class="m-b-3">
                            <ul class="fmenu-ul">
                                <li><strong>ECPlaza Network Inc.</strong></li>
                                <li><strong>CEO :</strong> In-Kyu Park</li>
                                <li><strong>Address :</strong> 143-40, Gangdong-daero, Gangdong-gu, Seoul, 1F</li>
                                <li><strong>Director of Privacy :</strong> Dong-ha Lee</li>
                                <li><strong>Business license number :</strong> 212-81-46495</li>
                                <li><strong>Communication sales business report number :</strong> NO. 25-1173</li>
                            </ul>
                            <ul class="share-ul">
                                <li><a href="https://www.facebook.com/EC-KOREA-351853399217828" target="_blank"><img src="/image/team/icon-facebook.png" alt="facebook" class="i-facebook"></a></li>
                                <li><a href="https://www.instagram.com/ec_korea_mall/" target="_blank"><img src="/image/team/icon-instagram.png" alt="instragram" class="i-instragram"></a></li>
                                <li><a href="https://www.youtube.com/channel/UC9KXNIRP6nqCTkxulOW86sg" target="_blank"><img src="/image/team/icon-youtube.png" alt="youtube" class="i-youtube"></a></li>
                                <li><a href="https://blog.naver.com/cbt20korea" target="_blank"><img src="/image/team/icon-blog.png" alt="blog" class="i-blog"></a></li>
                            </ul>

                            <!-- End .container -->
                        </div><!-- End .widget -->
                        <div class="col-lg-6 col-xs-12">
                            <div class="widget row footer-links">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <p class="widget-title">EC korea</p>
                                    <ul class="foot-sub">
                                        <li><a href="/layout/about">About us</a></li>
                                        <li><a href="/mall/faq">FAQ</a></li>
                                        <li><a href="/mall/guide/4">Guide</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <p class="widget-title">Policy</p>
                                    <ul class="foot-sub">
                                        <li><a href="/mall/policy">Terms and Use</a></li>
                                        <li><a href="/mall/policy">Privacy Policy</a></li>
                                        <li><a href="/mall/terms">Copyright Regulations</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <p class="widget-title">Customer Service</p>
                                    <ul class="foot-sub">
                                        <li><a href="/notice">Notice</a></li>
                                        <li><a href="/event">Event</a></li>
                                        <li><a href="/inquiry">Inquiry</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- End .col-lg-9-->
                <div class="col-lg-12 row footer-copy">
                    <p>© All Rights Reserved. Made by <strong>ECPLAZA</strong></p>
                </div>
            </div><!-- End .row -->
        </div><!-- End .container -->
        <!-- End .footer-middle -->
    </footer>
</div><!-- End .page-wrapper -->
<!-- 2021-05-11 추가 footer 소스 끝 -->

<a id="scroll-top" href="#top" title="Top" role="button"><i class="icon-angle-up"></i></a>

<!-- Plugins JS File -->
<script src="/css/assets/js/jquery.min.js"></script>
<script src="/css/assets/js/bootstrap.bundle.min.js"></script>
<script src="/css/assets/js/plugins.min.js"></script>


<!-- Main JS File -->
<script src="/css/assets/js/main.min.js"></script>
<!--Tracking FROM eckorea-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-203303586-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-203303586-1');
</script>

<script>
$(document).ready(function(e) {
	
	//좌측 메뉴 포커싱 스크립트 시작
	var alias = new Object;
	/*alias['/notice_view'] = '/notice';
	alias['/admin/faq/new'] = '/admin/faq';
	alias['/admin/board/new?wr_board=event'] = '/admin/board?wr_board=event';*/
	
	//쿼리스트링 있는경우
	var q = new URLSearchParams(location.search).get('status');
	var wr_board = new URLSearchParams(location.search).get('wr_board');
	
	var url = location.href;
	var host = location.hostname;
	var protocol = location.protocol;
	var query = url.split('?')[1];
	var pathname = location.pathname;	
	var addable = '';
	if(q) addable = '?status='+q;
	if(wr_board) addable = '?wr_board='+wr_board;	
	var match_url = pathname+addable;
	if(alias[match_url]) match_url = alias[match_url];
	//매칭
	$(".cs_left a").each(function(index, element) {
		
        if($(this).attr('href') == match_url)
		{
			$(this).css('text-decoration','underline');
			$(this).css('font-weight','bold');			
		}
    });
	//좌측 메뉴 포커싱 스크립트 종료
});
</script>
</html>
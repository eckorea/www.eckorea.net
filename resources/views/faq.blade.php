@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>FAQ</h2>
						
						<div class="row pt-3">
							<div class="col-sm-6 col-lg-6">
								<div class="widget">
									<h4 class="widget-title">Basic Informations</h4>

									<ul class="links link-parts row mb-0">
										<div class="link-part col-sm-12">
                                        @foreach($data as $row)
                                        @if($row->faq_group == 'Basic Informations')
                                        <li><a href="/mall/faq/{{$row->faq_id}}">{{$row->{"faq_title_".app()->getLocale()} }}</a></li>                                        
                                        @endif
                                        @endforeach											
										</div>
										
									</ul>
								</div><!-- End .widget -->

								<div class="blank10">&nbsp;</div>

								<div class="widget">
									<h4 class="widget-title">Order & Payment</h4>

									<ul class="links link-parts row mb-0">
										<div class="link-part col-xl-12">
										@foreach($data as $row)
                                        @if($row->faq_group == 'Order & Payment')
                                        <li><a href="/mall/faq/{{$row->faq_id}}">{{$row->{"faq_title_".app()->getLocale()} }}</a></li>                                        
                                        @endif
                                        @endforeach												
										</div>
										
									</ul>
								</div><!-- End .widget -->
                                
							</div><!-- End .col-sm-6 -->						
                            
                            
                            <div class="col-sm-6 col-lg-6">
								<div class="widget">
									<h4 class="widget-title">Precautions for importation procedures</h4>

									<ul class="links link-parts row mb-0">
										<div class="link-part col-xl-12">
										@foreach($data as $row)
                                        @if($row->faq_group == 'Precautions for importation procedures')
                                        <li><a href="/mall/faq/{{$row->faq_id}}">{{$row->{"faq_title_".app()->getLocale()} }}</a></li>                                        
                                        @endif
                                        @endforeach		
										</div>
										
									</ul>
								</div><!-- End .widget -->

								<div class="blank10">&nbsp;</div>

								<div class="widget">
									<h4 class="widget-title">Shipping fee by country</h4>

									<ul class="links link-parts row mb-0">
										<div class="link-part col-xl-12">
										@foreach($data as $row)
                                        @if($row->faq_group == 'Shipping fee by country')
                                        <li><a href="/mall/faq/{{$row->faq_id}}">{{$row->{"faq_title_".app()->getLocale()} }}</a></li>                                        
                                        @endif
                                        @endforeach		
										</div>
										
									</ul>
								</div><!-- End .widget -->
							</div><!-- End .col-sm-6 -->	
							
						</div><!-- End .row -->			
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('cscenter_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

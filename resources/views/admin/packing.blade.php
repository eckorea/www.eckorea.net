@include('admin.top')
<div class="app-main"> 
	@include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>검수/패킹</div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">                            
                                <form method="get">
                                <input type="hidden" name="status" value="{{Request::query('status')}}" />
                                <select name="qkind" class="select_css">                                   
                                    <option value="od_id" @if(Request::query('qkind') == 'od_id') selected @endif>주문번호</option>
                                    <option value="mb_email" @if(Request::query('qkind') == 'mb_email') selected @endif>주문인</option>
                                    <option value="it_name" @if(Request::query('qkind') == 'it_name') selected @endif>상품명</option>                                    
                                </select>
                            </div>
                            <input type="text" class="form-control" name="q" value="">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">검색</button>								
                            </div>
                            <div class="input-group-append">
                                &nbsp;<button type="button" class="btn btn-secondary" onclick="location.href='/admin/packing?status={{Request::query('status')}}';">초기화</button>								
                            </div>
                            </form>							
                        </div>
                        <form method="post" action="/admin/excelup/packing" enctype="multipart/form-data" style="margin-left:20px;">
                        @csrf
                        <input type="file" name="excel_file" />
                        <button type="submit" style="margin-left:20px;">검수/패킹 목록 업로드</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">주문번호</th>
                                        <th class="text-center">상품정보</th>
                                        <th class="text-center">주문인</th>
										<th class="text-center">배송</th>
                                    </tr>
                                </thead>
                                <tbody> 
									@foreach($data as $row)
									<tr>
										<td class="center_order">
											{{$row->od_id}}<br>
											<a href="/admin/order/view/{{$row->od_id}}">[보기]</a>
										</td>
										<td class="center_name2">
											<div>
												<img src="{{$row->it_thumbnail_url}}">
												<div class="center_name_right">
													[{{convOriginToText($row->ct_origin)}}]<br>
													{{$row->it_name}} <br>
													{{$row->it_option}} <span>{{$row->ct_krw_price}}원 / {{$row->ct_qty}}개</span>
												</div>
												<div class="clear"></div>
											</div>
                                            <!--
											<div class="blank10"></div>
											<div>
												<img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg">
												<div class="center_name_right">
													[쿠팡] - 입점판매자몰 <br>
													퍼실 딥클린 테크놀로지 라벤더젤 드럼용 리필 <br>
													3L / 빨간색 <span>16,000원 / 2개</span>
												</div>
												<div class="clear"></div>
											</div>-->
										</td>
										<td>
											{{$row->mb_email}} <br>
											<b>{{$row->ct_carrier}} {{$row->ct_ship_no}}</b>
										</td>
										<td>
											<b>{{$row->od_country}}</b> <br>
											{{$row->ct_carrier}} <br>
											{{$row->od_weight_due}}kg /	$ {{$row->od_send_cost2}}
										</td>
									</tr>  	
                                    
                                    @endforeach					
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        @include('admin.footer') 
	</div>
</div>

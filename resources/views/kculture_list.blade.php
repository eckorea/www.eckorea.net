@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">K-CULTURE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">				
					<div class="col-lg-12 order-lg-last dashboard-content">								

						<div class="row row-sm">
                        
                            @foreach($data as $row)                      
                            <div class="col-6 col-md-4 col-xl-3">
                            <a href="/mall/kculture/view/{{$row->kc_id}}">
								<div class="product-default">								                       
                                    <figure>
                                    @if($row->kc_thumbnail)
                                    	@if(strpos($row->kc_thumbnail,'http://') === FALSE)
                                        <img src="/storage/{{$row->kc_thumbnail}}"  style="height:250px; width:100%">
                                        @else
                                        <img src="{{$row->kc_thumbnail}}"  style="height:250px; width:100%">
                                        @endif                                    
                                    @else
                                    <img src="/image/products/product-4.jpg">
                                    @endif                                         
                                        
									</figure>
									<div class="product-details">
										<div class="product-title">
											<span class="">{{$row->kc_title}}</span>
										</div><!-- End .price-box -->
									</div><!-- End .product-details -->
                                    
								</div>
                                </a>
							</div><!-- End .col-xl-3 -->						    
                            @endforeach
                            
						</div>					
					</div><!-- End .col-lg-9 -->							
				</div>
				<!-- End .row --> 
                {{ $data->links() }} 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
		<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

	@include('footer')
</body>

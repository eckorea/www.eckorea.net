
<style type="text/css">
	.mail_top {text-align:center}
	.mail_top img {width:250px}
	.mail_bottom {width:100%; border:1px dotted #dfdfdf; padding:10px; font-size:13px; line-height:150%; box-sizing:border-box;}
	.mail_bottom p {font-size:13px; text-align:center; margin-bottom:20px}
	.mail_bottom p a {padding:0px 10px; text-decoration:none; border-bottom:1px solid #000; padding:3px 10px;  color:#000; margin:0px 5px; font-weight:bold}
</style>

<!-- 메일 상단 -->
<div class="mail_top">
	<a href="https://www.eckorea.net" target="_blank"><img src="/image/team/mail_top_img.jpg"></a>
</div>
<!-- 메일 상단 -->

<!-- 메일 하단 -->
<div class="mail_bottom">
	<p>
		<a href="https://www.eckorea.net/mall/terms" target="_blank">이용약관</a>
		<a href="https://www.eckorea.net/mall/policy" target="_blank">개인정보정책</a>
		<a href="https://www.eckorea.net/mall/faq" target="_blank">도움말 및 지원</a>
	</p>
	이씨플라자㈜ <br>
	Tel : 02-3433-4242 <br>
	05398 서울시 강동구 강동대로 143-40, 1층 <br>
	대표자 : 박인규 | 개인정보보호 책임자:이동하 | 사업자등록번호:212-81-46495 | 통신판매업신고번호:제 25-1173호
</div>
<!-- 메일 하단 -->
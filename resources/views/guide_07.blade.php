@include('top')

<script type="text/javascript">
    function display_yn(val) {
        var arr_category_cd = Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T');
        
        for(var i=0 ; i<arr_category_cd.length ; i++) {
            $('#Div_'+arr_category_cd[i]).hide();
            $('#img_check_'+arr_category_cd[i]).hide();
            $('#item_'+arr_category_cd[i]).attr('style','cursor:pointer;');
        }
       
        $('#Div_'+val).show();
        $('#img_check_'+val).show();
        $('#item_'+val).attr('style','color:#d5175f;font-weight:bold;');
    }

</script>

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">GUIDE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>List of restricted & prohibited items</h2>

						<div class="row row-sm">
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_A" onclick="display_yn('A');" style="cursor:pointer">
									<div class="tab_list">미국</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_B" onclick="display_yn('B');" style="cursor:pointer">
									<div class="tab_list">캐나다</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_C" onclick="display_yn('C');" style="cursor:pointer">
									<div class="tab_list">멕시코</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_D" onclick="display_yn('D');" style="cursor:pointer">
									<div class="tab_list">호주</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_E" onclick="display_yn('E');" style="cursor:pointer">
									<div class="tab_list">뉴질랜드</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_F" onclick="display_yn('F');" style="cursor:pointer">
									<div class="tab_list">영국</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_G" onclick="display_yn('G');" style="cursor:pointer">
									<div class="tab_list">독일</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_H" onclick="display_yn('H');" style="cursor:pointer">
									<div class="tab_list">프랑스</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_I" onclick="display_yn('I');" style="cursor:pointer">
									<div class="tab_list">베트남</div>
								</span>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-2">
								<span id="item_J" onclick="display_yn('J');" style="cursor:pointer">
									<div class="tab_list">싱가폴</div>
								</span>
							</div><!-- End .col-xl-3 -->
						</div>
						
						<div>공통통관 불가품목 텍스트로 부탁드립니다</div>

						<div id="Div_A" style="display:none;">
							<h4 class="title">미국 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>								
						</div>
						<div id="Div_B" style="display:none;">
							<h4 class="title">캐나다 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>
						<div id="Div_C" style="display:none;">
							<h4 class="title">멕시코 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>
						<div id="Div_D" style="display:none;">
							<h4 class="title">호주 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>
						<div id="Div_E" style="display:none;">
							<h4 class="title">뉴질랜드 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>
						<div id="Div_F" style="display:none;">
							<h4 class="title">영국 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>
						<div id="Div_G" style="display:none;">
							<h4 class="title">독일 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>
						<div id="Div_H" style="display:none;">
							<h4 class="title">프랑스 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>
						<div id="Div_I" style="display:none;">
							<h4 class="title">베트남 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>
						<div id="Div_J" style="display:none;">
							<h4 class="title">싱가폴 통관 불가품목 및 제약사항</h4>
							<p>
								여기에 내용 들어갑니다
							</p>	
						</div>

						<h4 class="title">주의사항</h4>
						<p>
							배송 국가별 통관 불가 품목과 제약 사항이 상이하므로 주문전에 반드시 확인하여 주시기 바랍니다.
						</p>	

						<h4 class="title">PRECAUTIONS</h4>
						<p>
							Please make sure to check it before ordering as there are different restrictions and items that cannot be cleared by the shipping country.
						</p>	
						
					</div>

					<!-- End .col-lg-9 -->

					@include('guide_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 


<script>
    $(document).ready(function() {
        display_yn('A');
    })
</script>  
		

@include('footer')
</body>

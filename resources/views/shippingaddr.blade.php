@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MY ACCOUNT</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>SHIPPING ADDRESS</h2>

						<div class="row">
							<div class="col-md-12">	
								<div class="table-responsive basic">
									<table class="table table-striped table-hover ">
										<thead>
										<tr>
											<th class="text-center">COUNTRY</th>
											<th class="text-center">NAME</th>
											<th class="text-center">ADDRESS</th>
											<th class="text-center">상태</th>
                                            <th class="text-center">DEFALUT</th>
										</tr>
										</thead>
										<tbody>
                                        @foreach($data as $row)
										<tr>	
											<td>{{$row->addr_country}}</td>
											<td>{{$row->addr_first_name}} / {{$row->addr_last_name}}</td>
											<td>{{$row->addr_city}} / {{$row->addr_state}}</td>
											<td><a href="/my/address/view/{{$row->id}}">[VIEW]</a></td>
                                            <td>
                                            @if($row->addr_default == 'Y')
                                            Default
                                            @else
                                            <a href="/my/address/default/{{$row->id}}">SET AS DEFAULT</a>
                                            @endif                                            
                                            </td>
										</tr>
                                        @endforeach											
										</tbody>
									</table>
								</div>  
							</div>
							<!-- End .col-md-6 --> 
						</div>

						<div class="blank10">&nbsp;</div>

						<div>
							<a href="/my/address/add"><button type="button" class="btn btn-primary">Add new address</button></a>
						</div><!-- End .form-footer -->
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('mypage_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

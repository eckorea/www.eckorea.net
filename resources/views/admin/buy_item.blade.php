@include('admin.top')
<div class="app-main"> 
	@include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>구매관리</div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="form-inline">
                        <div class="input-group">							
                            <div class="input-group-prepend">                            
                            <form method="get">
                                <select name="qkind" class="select_css">                                   
                                    <option value="od_id" @if(Request::query('qkind') == 'od_id') selected @endif>주문번호</option>
                                    <option value="mb_email" @if(Request::query('qkind') == 'mb_email') selected @endif>주문인</option>
                                    <option value="it_name" @if(Request::query('qkind') == 'it_name') selected @endif>상품명</option>                                    
                                </select>
                            </div>
                            <input type="text" class="form-control" name="q" value="{{Request::query('q')}}" placeholder="검색어">                                       
                            
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">검색</button>								
                            </div>
                            <div class="input-group-append">
                                &nbsp;<button type="button" class="btn btn-secondary" onclick="location.href='/admin/buy?status={{Request::query('status')}}';">초기화</button>								
                            </div>					
                        </div>
                        
						<input type="radio" name="origin" value="CU" @if(Request::query('origin') == 'CU') checked @endif> 쿠팡
						<input type="radio" name="origin" value="SS" @if(Request::query('origin') == 'SS') checked @endif> 네이버
						<input type="radio" name="origin" value="kyobo" @if(Request::query('origin') == 'kyobo') checked @endif> 교보문고                    
                        
                         <div class="buy_rdo_list">                           
                            <ul>
								<li><input type="radio" id="buy_wait" name="status" value="buy_wait" @if(Request::query('status') == 'buy_wait') checked="checked" @endif /> <label for="buy_wait">주문전</label></li>
								<li><input type="radio" id="buy_done" name="status" value="buy_done" @if(Request::query('status') == 'buy_done') checked="checked" @endif /> <label for="buy_done">주문완료</label></li>
								<li><input type="radio" id="buy_hold" name="status" value="buy_hold" @if(Request::query('status') == 'buy_hold') checked="checked" @endif /> <label for="buy_hold">주문대기</label></li>
								<li><input type="radio" id="buy_cancel" name="status" value="buy_cancel" @if(Request::query('status') == 'buy_cancel') checked="checked" @endif /> <label for="buy_cancel">주문취소</label></li>
                            </ul>
                            </div>                        
                        </form>
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">주문번호</th>
                                        <th class="text-center">상품정보</th>
                                        <th class="text-center">주문인</th>
                                    </tr>
                                </thead>
                                <tbody> 
									@foreach($data as $row)
                                    
                                    @php
                                    $arr_status_memo = json_decode($row->ct_house_memo_json,true);
                                    @endphp
									<tr>
										<td class="center_order">
											{{$row->od_id}}<br>
											<a href="/admin/order/view/{{$row->od_id}}">[보기]</a>
										</td>
										<td class="center_name2">			
											<div class="form-row">
												<div class="col-md-2">
													<img src="{{$row->it_thumbnail_url}}">
												</div>
												<div class="col-md-6">
													<div class="mall">[{{convOriginToText($row->ct_origin)}}] <!--<input type="text" id="ct_local_ship_info_{{$row->ct_id}}" placeholder="외부송장정보" value="{{$row->ct_local_ship_info}}" />--></div>
													{{$row->it_name}} <br>
													<span class="option"> {{substr($row->it_option,0,50)}} </span> <span>{{$row->ct_krw_price}}원 / {{$row->ct_qty}}개</span>		<br>

                                                    <input type="text" id="buy_memo_{{$row->ct_id}}" placeholder="주문메모" value="@if(isset($arr_status_memo['buy'])) {{$arr_status_memo['buy']}} @endif" style="width:100%"/>
                                                    @if(isset($arr_status_memo['seller_msg']))
                                                    <div class="option">
                                                        판매자메모 : {{$arr_status_memo['seller_msg']}}
                                                    </div>

                                                    @endif
                                                </div>
												<div class="col-md-4">
													<div class="input-group-prepend">                            
														<select class="select_status select_css" id="select_status_{{$row->ct_id}}">                                    
															<option value="buy_wait" @if($row->ct_house_status == 'buy_wait') selected @endif >주문전</option>
															<option value="buy_done" @if($row->ct_house_status == 'buy_done') selected @endif>주문완료</option>
															<option value="buy_hold" @if($row->ct_house_status == 'buy_hold') selected @endif>주문대기</option>
															<option value="buy_cancel" @if($row->ct_house_status == 'buy_cancel') selected @endif>주문취소</option>
														</select>
                                                        <input type="button" value="업데이트" class="btn_status" data_idx="{{$row->ct_id}}" />
                                                        <input type="button" value="원본보기" onclick="window.open('{{$row->it_url}}');" />
													</div>
                                                   
												</div>
											</div>                                         
                                        </td>
										<td>
											{{$row->mb_email}}
										</td>
									</tr>  	
                                    @endforeach						
                                </tbody>
                            </table>
                        </div>    
                                                
                        <nav class="toolbox toolbox-pagination">
							<ul class="pagination">
								{{ $data->links() }}            
								<!--
								<li class="page-item disabled"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a> </li>
								<li class="page-item active"> <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a> </li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">4</a></li>
								<li class="page-item"><a class="page-link" href="#">5</a></li>
								<li class="page-item"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a> </li>
								-->
							</ul>
						</nav>
                                            
                    </div>
                </div>
            </div>
        </div>
        @include('admin.footer') 
	</div>
</div>
<script>
$(document).ready(function(e) {    
	$(".btn_status").on("click",function()
	{
		var method = 'buy';
		var ct_id = $(this).attr("data_idx");
		var status = $("#select_status_"+ct_id).val();
		var memo = $("#buy_memo_"+ct_id).val();
		var ct_local_ship_info = $("#ct_local_ship_info_"+ct_id).val();
		
		if(status == 'buy_cancel')
		{
			if(!confirm('주문취소시 PG사에 결제금액이 환불 요청됩니다. 진행하시겠습니까?'))
			{
				return false;
			}
		}
		
		//AJAX
		$.ajax({
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: 'POST',
		url: '/admin/status/update',
		data: {method:method, ct_id : ct_id, status : status, ct_house_memo : memo, ct_local_ship_info : ct_local_ship_info},
		dataType: 'json',
		success: function(data) {
			alert(data.msg);
		},
		error: function(data) {
			alert("error" +data);
	  }
	});
		
	});
});
</script>
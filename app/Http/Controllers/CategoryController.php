<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category, Request $request)
    {
		/*
		$data = $category->get();		
		foreach($data as $row)
		{
			$ca_id = $row->ca_id;
			$caled_level = strlen($row->ca_id) / 2;
			$od = ($caled_level-1) * 2;
			
			$parent = substr($ca_id,0,$od);
			
			echo "$ca_id | $parent | $caled_level <br>";
			
			
			$category->where('ca_id',$ca_id)->update([
			'ca_parent' => $parent,
			'ca_level' => $caled_level
			]);
		}
		*/	
		
		//SELECT * FROM `categories` ORDER BY cast(ca_id as unsigned) ASC
        $qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
				
		if($qkind == 'email') $data = $category->where('mb_email','LIKE',$q.'%');			
		else $data = $category;
		
		$data = $data
		->orderBy('ca_id', 'asc')
		->paginate(30);		
        return view('/admin/category')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Category $category,Request $request)
    {
		$ca_parent = trim($request->input('ca_id'));
		$ca_name = trim($request->input('ca_name'));

		//다음 카테고리 넘버 get
		$data = $category
		->selectRaw('MAX(ca_id) AS ca_id')
		->where('ca_parent',$ca_parent)
		->withTrashed()
		->get();
				
		$max_ca_id = $data[0]->ca_id;
		$ca_level = (strlen($ca_parent) / 2)+1;
		
		$tgt_no = (int)substr($max_ca_id,-2,2);
		$new_no = $tgt_no+1;
		$new_ca_id = $ca_parent.str_pad($new_no,2,'0',STR_PAD_LEFT); //최종 카테고리 넘버

		$category->insert([
			'ca_id'=>$new_ca_id,
			'ca_name'=>$ca_name,
			'ca_parent'=>$ca_parent,
			'ca_level'=>$ca_level,
			'created_at'=>now()	
			]);
		
		return redirect("/admin/category/edit/$new_ca_id")->with('jsAlert', '입력하신 정보로 저장되었습니다');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $Category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, $type, $ca_id = '')
    {
		$ca_name = '';
		$ca_parent_name = '';
		
		if($type == 'new')
		{
			$ca_id = '';
		}
		else if($type == 'add')
		{
			$data = $category		
			->where('ca_id', $ca_id)
			->get();			
			
			$ca_parent_name = $data[0]->ca_name;
		}
		else if($type == 'edit')
		{			
			$data = $category		
			->where('ca_id', $ca_id)
			->get();
			$ca_name = $data[0]->ca_name;
			$ca_parent = $data[0]->ca_parent;
			
			if($ca_parent)
			{
				$parent_data = $category		
				->where('ca_id', $ca_parent)
				->get();
				$ca_parent_name = $parent_data[0]->ca_name;
			}
		}
		else
		{
			echo 'error';
			exit;
		}
        		
		return view('/admin/category_view')->with(['type'=>$type,'ca_id'=>$ca_id, 'ca_name'=>$ca_name, 'ca_parent_name'=>$ca_parent_name]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $Category
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Category $category)
    {
        $ca_id = $request->ca_id;
		$ca_name = $request->ca_name;
		$category->where('ca_id',$ca_id)->update(['ca_name'=>$ca_name]);
		
		return redirect()->back()->with('jsAlert', '입력하신 정보로 저장되었습니다');;		
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $Category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $Category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, $ca_id)
    {
		$data = $category
		->where('ca_parent',$ca_id)
		->get();
		
		$child_cnt = count($data);
		
		if($child_cnt == 0)
		{
			$deletedRows = $category->where('ca_id', $ca_id)->delete();
			return redirect()->back();
		}
		else
		{
			return redirect()->back()->with('jsAlert', '하위카테고리가 아직 존재합니다.');
		}	
    }
}

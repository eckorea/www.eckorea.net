<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
//use Carbon\Carbon;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

//엑셀 로드용
class CargoData implements ToCollection
{
    public function collection(Collection $rows)
    {
      
    }
}

//엑셀 파일 내보내기
class CargoExport implements FromView
{
	private $exType;
		
	public function __construct($type) {
		$this->exType = $type;	
	}
	
    public function view(): View
    {
		$cart = new Cart();	
		$order = new Order();
		$cursor = $order->join('carts','carts.od_id', '=', 'orders.od_id')	
		->select(DB::raw('carts.*, orders.*, orders.created_at AS order_date'));
		
		if($this->exType == 'order')
		{
			$cursor = $cursor->where([
			['carts.ct_house_status','=','buy_done']
			]);
		}
		else if($this->exType == 'send_wait')
		{			
			$cursor = $cursor->where([
			['orders.od_ship_paid','=','Y'],
			['orders.od_status','!=','sended']
			]);
		}
				
		$data = $cursor->get();
		
		if($data->count() == 0)
		{
			echo "다운받을 데이터가 없습니다";
			exit;
		}		
		
		$output_data = array(); //최종 데이터 변수 초기화	
		foreach($data as $row)
		{
			$arr_od_id = explode('-',$row->od_id);
			$each_data = array(
			"STATUS"=>'',
			'발주번호'=>$row->od_id,
			'발주일'=>date("Y-m-d",strtotime($row->order_date)),
			'접수번호'=>'',
			'발송인'=>'',
			'발송인연락처'=>'',
			'수취인'=>$row->od_first_name.' '.$row->od_last_name,
			'수취인연락처'=>$row->od_tel,
			'국가'=>$row->od_country,
			'우편번호'=>$row->od_zip,
			'전체주소'=>$row->od_state.' '.$row->od_city.' '.$row->od_addr1.' '.$row->od_addr2,
			'시'=>$row->od_city,
			'구'=>$row->od_state,
			'상세주소1'=>$row->od_addr1,
			'상세주소2'=>$row->od_addr2,
			'주문처메모'=>$row->od_memo,
			'발송메모'=>'',
			'코드구분'=>'',
			'대표코드'=>'',
			'상품코드'=>$arr_od_id[2].$row->ct_id,
			'상품명'=>$row->it_name,
			'단가'=>$row->ct_price,
			'통화단위'=>'KRW',
			'수량'=>$row->ct_qty,
			'입고예정일'=>'',
			'입고일자'=>'',
			'입고수량'=>'',
			'미입고수량'=>'',
			'상품구분'=>'',
			'색상코드'=>'',
			'사이즈코드'=>'',
			'유통기한'=>'',
			'바코드'=>'',
			'입고운송장번호'=>$row->ct_local_ship_no,
			'입고택배사'=>$row->ct_local_ship_carrier,
			'구입채널'=>'',
			'입고사진1'=>'',
			'입고사진2'=>'',
			'입고사진3'=>'',
			'ISSUE'=>'',
			'발송운송장번호'=>$row->ct_ship_no,
			'발송택배사'=>$row->ct_carrier,
			'실무게'=>$row->od_weight,
			'부피무게'=>$row->od_weight,
			'부피사이즈'=>'',
			'박스번호'=>'',
			'LOT'=>'',
			'발송일'=>'',
			'메일주소'=>'',
			'제조국가'=>''
			);
			
			//기타 항목 루프
			for($ii=4; $ii <= 21; $ii++)
			{
				$each_data['기타'.$ii] = "";
			}
			
			//row merge
			array_push($output_data, $each_data);
		}
		
		//View 형태로 결과 노출시키면, excel download 쪽에서 알아서 처리함
        return view('excel.cargo')->with(['data'=>$output_data]);
    }
}

class ExcelController extends Controller
{
	//엑셀파일 로드
	public function upload(Request $request, $type)
	{
		$odController = new OrderController();
				
		if (!$request->hasFile('excel_file')) {
    		echo "파일이 업로드 되지 않았습니다";
			return;
		}
		
		$cart = new Cart();	
		$order = new Order();
					
		$array = Excel::toArray(new CargoData, $request->file('excel_file'));		
		//19:상품코드, 40 : 운송장번호 , 41 : 운송회사, 42: 실무게, 43: 부피무게
		foreach($array as $rows)
		{
			$inner_cnt = 1;
			foreach($rows as $row)
			{
				if($inner_cnt > 1)
				{	
					$od_id = $row[1];		
					$code_len = strlen($row[19]);
					$ct_id = substr($row[19],4,($code_len-4));
					$ship_no = $row[40];
					$carrier = $row[41];
					$real_weight = $row[42];
					$volume_weight = $row[43];					
				
					if($type == 'packing')
					{						
						//시간 데이터
						$ct_data = $cart->where('ct_id',$ct_id)->get();
						$json_status_time = $ct_data[0]['ct_house_status_time_json'];
						$arr_status_time = json_decode($json_status_time,true);
						$arr_status_time['warehouse_stock'] = date("Y-m-d H:i:s");
						$new_json_status_time = json_encode($arr_status_time);
						
						//카트 업데이트 ,, 상태 업데이트 시간 추가해야됨
						$cart->where('ct_id',$ct_id)->update([
						'ct_carrier'=>$carrier,
						'ct_ship_no'=>$ship_no,
						'ct_house_status'=>'warehouse_stock',
						'ct_house_status_time_json' => $new_json_status_time
						]);
						
						//오더업데이트
						$order->where('od_id',$od_id)->update([
						'od_weight'=>$real_weight,
						'od_weight_due'=>$real_weight
						]);
						
						//로깅
						$raw_input = print_r($row,true);
						$odController->order_mod_log($od_id, $ct_id, 'excel_update_ct_status', $raw_input);
					}
					else if($type == 'sended')
					{
						//오더업데이트
						$order->where('od_id',$od_id)->update([
						'od_status'=>'sended'
						]);
						
						//로깅
						$raw_input = print_r($row,true);
						$odController->order_mod_log($od_id, null, 'excel_sended', $raw_input);
					}
										
					echo $inner_cnt."행 처리완료 되었습니다.($od_id)<br>";
					
					
					/*echo 'ct_id:'.$ct_id;
					
					print_r($row);
					echo "<br><br>";
					*/
				}
				
				$inner_cnt++;
			}						
		}
		
		echo "<a onclick='history.back(-1);'>[돌아가기]</a>";
	}
	
	//엑셀파일 내보내기
	public function download($type)
	{
		return Excel::download(new CargoExport($type), date("YmdHis")."_$type.xlsx");
	}
	
	
	
	//엑셀파일 로드
	public function test()
	{		
		$array = Excel::toArray(new CargoData, 'storage/excel/master.xlsx');		
		
		foreach($array as $rows)
		{
			foreach($rows as $row)
			{
				print_r($row);
				echo "<br><br>";
			}						
		}
	}
	
	//엑셀파일 내보내기
	public function ex_test()
	{
		return Excel::download(new CargoExport('typeeee'), 'export.xlsx');
	}
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		$timestamps = false;
		
        Schema::create('members', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';
            $table->bigIncrements('mb_no');
			$table->integer('mb_level')->default(1);
			$table->string('mb_first_name',32);
			$table->string('mb_last_name',32);
			$table->string('mb_email',255)->unique();
			$table->string('mb_password',255);
			$table->boolean('mb_news_letter')->default(0);
			$table->ipAddress('mb_ip')->nullable();			
            $table->dateTime('created_at');
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			$table->SoftDeletes();			
			//$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}

<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ChromeApiController extends Controller
{	
    public function login(Request $request)
	{
		$enc_key = 'ewojc3nax2pg4';
		
		$email = $request->input('id');
		$password = $request->input('pw');
		
		//$enc_pass = base64_encode(openssl_encrypt('1234', "AES-128-ECB", $enc_key, OPENSSL_RAW_DATA));
		//echo urlencode($enc_pass);	
		
		$decrpyted_email = openssl_decrypt(base64_decode($email), "AES-128-ECB", $enc_key, OPENSSL_RAW_DATA);		
		$decrpyted_password = openssl_decrypt(base64_decode($password), "AES-128-ECB", $enc_key, OPENSSL_RAW_DATA);		
		
		$user_data = DB::table('members')->where([
			['mb_email', '=', $decrpyted_email]			
		])->get();
		
		if(isset($user_data[0]))
		{
			$saved_hash = $user_data[0]->mb_password;
			if (Hash::check($decrpyted_password, $saved_hash)) {
				//echo "해쉬비번맞음 (로그인처리)";
				//$request->session()->put('mb_email', $email);
				
				//userkey 생성
				$chrome_auth_key = Hash::make($email.time());
				
				//DB chrome_auth_key set
				DB::table('members')->where([
				['mb_email', '=', $email]			
				])->update(['chrome_auth_key'=>$chrome_auth_key, 'chrome_auth_mktime'=>date("Y-m-d H:i:s")]);
				
				//리턴 json
				$arr_result = array("rcode" => 1, "rmsg" => '로그인 성공', "userkey" => $chrome_auth_key, "plain_email" => $decrpyted_email, "plain_pass" => $decrpyted_password);
				
			}
			else
			{
				$arr_result = array("rcode" => 0, "rmsg" => '로그인 실패', "plain_email" => $decrpyted_email , "plain_pass" => $decrpyted_password);
				//echo "해쉬비밀번호틀림";
				//$request->session()->flush();
			}
		}
		else
		{
			$arr_result = array("rcode" => 0, "rmsg" => '로그인 실패', "plain_email" => $decrpyted_email , "plain_pass" => $decrpyted_password);
		}	
		
		echo json_encode($arr_result,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		exit;	
	}	
	
	public function cart(Request $request, Cart $cart)
	{		
		Log::debug('ChromePlugin Cart request option: '.print_r($request->all(),true));		
		Log::debug("ChromePlugin Cart request HEADER({$request->method()}): ".print_r($request->header(),true));
		
		$type = $request->input('type'); //스토어 타입  :   value : SS : 스마트스토어 , CU : 쿠팡 , PO : 우체국 hottracks : 핫트랙스
		$url = $request->input('url');  //상품 URL
		$option_raw = $request->input('options');  //옵션 및 수량 정보 ( 비정형 )
		$chrome_auth_key = urldecode($request->input('userkey'));
		$platform = $request->input('platform');  //상품 URL
		
		if($type == 'CU' || $type == 'hottracks' || $type == 'PO')
		{
			$arr_options = parserCoupang_v1($request);		
		}
		else if($type == 'SS')
		{
			$arr_options = parserSmartStore_v1($request);		
		}
		else
		{
			$arr_result = array("rcode" => 0, "rmsg" => '존재하지 않는 타입입니다.');
			echo json_encode($arr_result,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			exit;
		}		
					
		//존재하는 유저인지 확인
		$user_data = DB::table('members')->where([
			['chrome_auth_key', '=', $chrome_auth_key]			
		])->get();		
		/*
		$arr_result = array("rcode" => 1, "rmsg" => '상품이 카트에 저장되었습니다', "raw" => $request->all());
		echo json_encode($arr_result,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		exit;*/
		
		if(isset($user_data[0]) && $chrome_auth_key)
		{
			$mb_email = $user_data[0]->mb_email;

			//cart input			
			foreach((array)$arr_options as $options)
			{
				$cart_old_qty = 0;
				$cart_old_cursor = $cart->where([
				['mb_email','=', $mb_email],
				['it_url','=', $url],
				['it_option','=',$options['option']]
				]);
				
				$cart_data = $cart_old_cursor->get();
				
				if(isset($cart_data[0])) $cart_old_qty = $cart_data[0]->ct_qty;
				
				//같은 상품이 이미 카트에 있는경우 갯수만 증가
				if($cart_old_qty)
				{
					$cart_old_cursor
					->update
					(['ct_qty' => ($cart_old_qty+$options['count']), 'ct_select' => 'Y']);
				}
				else
				{
                    try{
                        if ($options['count'] == '' || $options['count'] == 0) {
                            Log::debug("ChromePlugin Cart Exception 품절 [Division by zero] - #1");

                            sendSlackNotification("CART", 'ChromePlugin Cart Exception 품절 [Division by zero] - #1 ' . $mb_email, 'https://www.eckorea.net/');

                            $arr_result = array("rcode" => -1, "rmsg" => '해당 상품은 품절되었습니다. 다른 상품을 담아주세요 :)');
                            echo json_encode($arr_result,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                            exit;
                        } else {
                            DB::table('carts')->insert(
                                ['mb_email' => $mb_email, 'ct_type' => 'out', 'ct_origin' => $type,  'it_url' => $url, 'it_thumbnail_url'=> $options['img'], 'it_name' => $options['name'], 'it_option'=>$options['option'], 'ct_option_raw'=>$option_raw, 'ct_price'=> $options['price']/$options['count'], 'ct_krw_price' => $options['price'], 'ct_qty' =>$options['count'], 'ct_send_cost' => $options['shipping'], 'ct_send_cost_cond' => $options['shipping_cond'], 'ct_send_cost_cond2' => $options['shipping_cond2'], 'ct_status' => 'shopping', 'created_at' => date("Y-m-d H:i:s")]
                            );
                        }

                    } catch(Exception $e){
                        $errorMessage = $e;

                        Log::debug("ChromePlugin Cart Exception ".$errorMessage);

                        $arr_result = array("rcode" => -1, "rmsg" => $e);

                        DB::table('api_logs')->insert(
                            [
                                'al_key' => 'add_dart'
                                , 'ct_id' => '0'
                                , 'al_memo' =>  ' >>>>>>>>>> '. $errorMessage
                            ]
                        );

                        echo json_encode($arr_result,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                        exit;
                    } finally {

                    }
				}
			}
			
			$arr_result = array("rcode" => 1, "rmsg" => '상품이 카트에 저장되었습니다', "raw" => $request->all());
		}
		else
		{
		    /* esummer, 2021-06-24, 메시지 변경 : 존재하지 않는 유저키 ~ -->> 로그인 정보가 확인되지 않습니다. 로그인 후 다시 시도하세요.
			$arr_result = array("rcode" => 0, "rmsg" => '존재하지 않는 유저키입니다');
		    */
			$arr_result = array("rcode" => 0, "rmsg" => '로그인 정보가 확인되지 않습니다. 로그인 후 다시 시도하세요.');
		}
		
		
		echo json_encode($arr_result,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		exit;	
	}
	
	public function cart_test(Request $request)
	{
		$type = $request->input('type'); //스토어 타입  :   value : SS : 스마트스토어 , CU : 쿠팡 , PO : 우체국
		$url = $request->input('url');  //상품 URL
		$option = $request->input('options');  //옵션 및 수량 정보 ( 비정형 )
		
		$mb_email = session('mb_email');
		$thumbnail = 'https://shopping-phinf.pstatic.net/main_2407393/24073931271.20200907232113.jpg?type=f640';
		
		if($type && $url && $option)
		{		
			DB::table('carts')->insert(
				['mb_email' => $mb_email, 'ct_type' => 'out', 'ct_origin' => $type,  'it_url' => $url, 'it_thumbnail_url' => $thumbnail, 'it_name' => '사마글라스 티포트 티머그 커피드리퍼 25종 세트할인전 / SAMAGLAS 필터세트', 'it_option'=>'S-042 세트', 'ct_price'=> 10, 'ct_krw_price' => 10000, 'ct_qty' =>1, 'ct_send_cost' => 0, 'ct_status' => 'shopping', 'created_at' => date("Y-m-d H:i:s")]
			);
			
			$arr_result = array("rcode" => 1, "rmsg" => '상품이 카트에 저장되었습니다', "raw" => $request->all());			
		}
		else
		{
			$arr_result = array("rcode" => 0, "rmsg" => '필수정보없음', "raw" => $request->all());	
		}
		echo json_encode($arr_result,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		exit;	
	}
}

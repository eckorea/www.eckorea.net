@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MY ACCOUNT</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>MESSAGE</h2>

						<div class="row">
							<div class="col-md-12">	
								<div class="table-responsive basic">
									<table class="table table-striped table-hover ">
										<thead>
										<tr>
											<th class="text-center">쪽지내용</th>
											<th class="text-center">도착일</th>
											<th class="text-center">읽은날짜</th>
										</tr>
										</thead>
										<tbody>
                                        @foreach($data as $row)                                        
										<tr>	
											<td onClick="message_view('{{$row->id}}');" style="cursor:pointer;">{{$row->ms_text}}</td>
											<td>{{$row->created_at}}</td>
                                            @if($row->ms_confirm_date)
                                            <td>{{$row->ms_confirm_date}}</td>
                                            @else
                                            <td></td>
                                            @endif											
										</tr>
                                        @endforeach
										</tbody>
									</table>
								</div>  
							</div>
							<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('mypage_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>
<script>
function message_view(id)
{
	window.open('/my/message/view/'+id,'message_view',"width=500,height=200");
}
</script>

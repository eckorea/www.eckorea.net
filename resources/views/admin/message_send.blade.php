@include('admin.top')
<div class="app-main"> @include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>쪽지발송</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                                <div class="card-body">
                                <form method="post" action="/admin/message/send">
                                {{ csrf_field() }}
                                    <div class="position-relative row form-group">
                                        <label class="col-sm-2 col-form-label">이메일</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" name="mb_email" placeholder="다수에게 보낼시 , (콤마)로 구분">
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label class="col-sm-2 col-form-label">쪽지내용</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="ms_text"></textarea>
                                        </div>
                                    </div>
                                    <div class="blank20"></div>
                                    <button type="submit" class="mb-2 mr-2 btn btn-primary">발송</button>
                                    <button class="mb-2 mr-2 btn btn-light">취소</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.footer') 
	</div>
</div>

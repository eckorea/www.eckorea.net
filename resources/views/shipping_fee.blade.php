@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>SHIPPING FEE</h2>

						<div class="row row-sm">
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/4"><div class="tab_list">USA</div></a>
							</div><!-- End .col-xl-3 -->
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/27"><div class="tab_list">UNITED KINGDOM</div></a>
							</div>
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/28"><div class="tab_list">FRANCE</div></a>
							</div>
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/29"><div class="tab_list">GERMANY</div></a>
							</div>
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/30"><div class="tab_list">CANADA</div></a>
							</div>
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/31"><div class="tab_list">SINGAPORE</div></a>
							</div>
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/32"><div class="tab_list">VIETNAM</div></a>
							</div>
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/33"><div class="tab_list">AUSTRALIA</div></a>
							</div>
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/34"><div class="tab_list">NEW ZEALAND</div></a>
							</div>
							<div class="col-6 col-md-4 col-xl-3">
								<a href="/mall/faq/35"><div class="tab_list">MEXICO</div></a>
							</div>
						</div>
						
					</div>

					<!-- End .col-lg-9 -->

					@include('cscenter_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

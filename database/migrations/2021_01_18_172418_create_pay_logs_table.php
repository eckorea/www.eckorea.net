<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pay_logs', function(Blueprint $table)
		{
			$table->integer('pay_id', true);
			$table->string('pay_pg', 32);
			$table->string('pay_method', 32);
			$table->float('pay_amount', 10, 0);
			$table->string('pay_tid', 64)->nullable();
			$table->string('pay_return_code', 32)->nullable();
			$table->string('pay_status', 32)->default('ready');
			$table->text('pay_raw')->nullable()->comment('리턴 RAW 데이터');
			$table->dateTime('created_at');
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			$table->SoftDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pay_logs');
	}

}

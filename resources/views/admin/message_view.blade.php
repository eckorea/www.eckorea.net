@include('admin.top')

<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>쪽지내역</div>
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="tab-content">                            
							<div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
								<div class="card-body">										  
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">이메일</label>
										<div class="col-sm-10"><input class="form-control" value="{{$data[0]->ms_receiver}}" ></div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">쪽지내용</label>
										<div class="col-sm-10"><textarea class="form-control">{{$data[0]->ms_text}}</textarea></div>
									</div>									
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">발송일</label>
										<div class="col-sm-10">{{$data[0]->created_at}}</div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">확인일</label>
										<div class="col-sm-10">{{$data[0]->ms_confirm_date}}</div>
									</div>								
                                        
									<button class="mb-2 mr-2 btn btn-light">목록</button>                                       
								</div>									
							</div>
						</div>						
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>

<aside class="sidebar col-lg-3">
	<div class="widget widget-dashboard">
		<h3 class="widget-title">CS CENTER</h3>
		<ul class="list cs_left">
			<li><a href="/notice">NOTICE</a></li>
			<li><a href="/mall/faq">FAQ</a></li>
            <li><a href="/event">EVENT</a></li>
			<li><a href="/inquiry">INQUIRY</a></li>
			<li><a href="/mall/review">REVIEW</a></li>
			<li><a href="/layout/shipping_fee">SHIPPING FEE</a></li>
			<li><a href="/contact">CONTACT US</a></li>
		</ul>
	</div>
<!-- End .widget --> 
</aside>
<!-- End .col-lg-3 --> 
@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">GUIDE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>Common problems & issues</h2>

						<div class="row">
							<div class="col-md-12">	
								<h4 class="title">1. 회원가입</h4>
								<p>
									● SNS 계정연동이 안 됩니다.<br>
									SNS 계정연동이 안되는 경우,  회원가입을 통해 진행해주시기 바랍니다.<br>
									이씨코리아는 배송 안내를 위한 목적의 이메일, 연락처, 배송지 정보를 제외하고 회원님의 개인정보를 수집하지 않습니다.<br>
									회원가입을 하시게 되면, 첫 구매 도서 반값 배송 혜택을 비롯한 다양한 혜택을 받으실 수 있습니다.<br><br>

									● 이메일로 인증 메일이 안 옵니다.<br>
									인증 메일이 전송되기까지 네트워크 환경에 따라 약 5분정도 소요될 수 있습니다.<br>
									이후에도 메일이 도착하지 않는다면, 스팸메일함을 확인해주시거나 다른 이메일로 작성바랍니다.<br>
									지속적으로 문제가 발생하는 경우, 00000@ecplaza.net 으로 메일을 보내주시면 처리해드리겠습니다.<br><br>

									● 개인 연락처와 이메일을 꼭 입력해야 하나요?<br>
									회원님의 연락처와 이메일은 배송 안내를 목적으로만 활용됩니다.<br>
									추가적으로 동의를 해주신 경우, 이벤트 및 프로모션 알림을 받으실 수 있습니다.<br><br>

									● 크롬 확장프로그램을 꼭 설치해야 하나요?<br>
									이씨코리아에서만 쇼핑을 하신다면 설치하지 않으셔도 됩니다.<br>
									그러나 다른 한국 사이트에서 쇼핑을 하기 위해서는 크롬 브라우저를 사용해주셔야 합니다. <br>
									이씨코리아는 크롬 확장프로그램을 통해 회원님의 주문정보를 자동으로 이씨코리아 장바구니로 담습니다.<br>
									이씨코리아 통합 장바구니 크롬 확장프로그램은 언제든지 비활성화/삭제하실 수 있습니다.
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">2. 이씨코리아에서 쇼핑</h4>
								<p>
									 ● 네이버 스마트스토어, 쿠팡을 제외한 다른 한국 쇼핑몰에서는 쇼핑이 불가능한가요?<br>
									 죄송합니다. 현재 이씨코리아 통합 장바구니와 연계된 한국 사이트는 네이버 스마트스토어, 쿠팡입니다.<br>
									 다른 한국 쇼핑몰들은 추후 서비스될 예정이오니 양해 바랍니다. <br><br>

									 ● 원하는 상품을 못 찾겠어요.<br>
									 이씨코리아는 해외 교민분들과 한류 팬들을 위해 MD들이 주기적으로 상품을 소싱하고 있습니다.<br>
									 마음에 드시는 상품이 없으시다면 네이버 스마트스토어와 쿠팡에서 더 많은 상품을 찾아보세요!<br>
									 배송비 절감과 빠른 배송을 위해, 현재 연계된 네이버 스마트스토어와 쿠팡만 제공되는 점 양해 부탁드립니다.<br>
									 추후 다른 한국 쇼핑몰 들도 점차 추가될 예정이니 많은 기대 부탁드립니다!<br><br>

									 ● 메인 페이지에 게시되는 상품들은 무엇인가요?<br>
									 이씨코리아 MD들이 엄선한 상품(ECK CHOICE : FEATURED PRODUCTS / BEST SELLERS / NEW ARRIVALS)<br>
									 카테고리별 상품(KYOBO BOOKS / K-BEAUTY / K-FASHION / K-LIVING / K-GOODS / K-FOOD)<br>
									 최근 다른 회원들이 구매한 상품(KYOBO BEST SELLER)<br>
									 순으로 소개해드리고 있습니다. 이씨코리아에서 다양한 상품들을 만나보세요!
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">3. 이씨코리아 장바구니 담기</h4>
								<p>
									 ● 화면 우측하단에 크롬 장바구니 확장프로그램 장바구니 담기 버튼이 사라졌습니다.<br>
									 현재 네이버 스마트스토어, 쿠팡을 제외하고는 이씨코리아 통합 장바구니 서비스는 지원되지 않습니다.<br>
									 서비스가 지원되지 않는 쇼핑몰에서는 장바구니 담기가 불가합니다. <br>
									 추후 다른 한국 쇼핑몰들도 추가될 예정입니다.<br><br>

									 ● 크롬 장바구니 확장프로그램에서 장바구니 담기 클릭 시 ‘현재 서비스를 이용하실 수 없습니다’ 메시지가 뜹니다.<br>
									 네이버 스마트스토어와 쿠팡의 일부 판매자가 상품의 정보를 자주 갱신시키는 경우 발생하는 문제입니다.<br>
									 크롬 장바구니 확장프로그램을 재설치해보시거나 잠시 후에 다시 시도해주시기 바랍니다.<br>
									 지속적으로 문제가 발생하는 경우, 00000@ecplaza.net 으로 메일을 보내주시기 바랍니다.<br><br>

									 ● 크롬 장바구니 확장프로그램의 장바구니 담기 버튼이 뜨지 않습니다.<br>
									 네이버 스마트스토어와 쿠팡이 아닌 다른 사이트에서는 크롬 장바구니 확장프로그램이 동작하지 않습니다.<br>
									 만약 네이버 스마트스토어와 쿠팡에서 버튼이 뜨지 않는다면, 재설치해주시기 바랍니다.<br>
									 지속적으로 문제가 발생하는 경우, 00000@ecplaza.net 으로 메일을 보내주시기 바랍니다.<br><br>

									 ● 장바구니에서 취소한 상품을 다시 담을 수 있나요?<br>
									 죄송합니다. 장바구니에서 취소한 상품은 다시 담을 수 없습니다. 추후에 관련 기능이 추가될 예정입니다.<br><br>

									 ● 장바구니에 담아둔 상품의 정보가 변경되었어요.<br>
									 상품의 정보가 변경되었다면, 외부 쇼핑몰의 판매자가 정보를 갱신한 경우로서 오류가 아닙니다.<br>
									 이씨코리아는 외부 쇼핑몰과 연계되어 있기 때문에 해당 판매자가 갱신한 정보가 자동으로 반영됩니다.<br>
									 판매자가 가격과 수량 정보를 수정하였다면, 장바구니에서도 수정된 정보가 보이게 됩니다.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">4. 주문상품 한국 내 배송</h4>
								<p>
									 ● 배송 조회는 어떻게 하나요?<br>
									 회원님의 배송조회는 한국 내 배송 또는 국제배송이 시작된 때부터 MY PAGE > Shipment 에서 송장번호로 조회할 수 있습니다.<br><br>
										  
									 ● 배송 조회가 되지 않습니다.<br>
									 아직 운송사에서 배송이 시작되지 않은 경우, 송장번호가 없기 때문에 조회가 불가능합니다.<br>
									 운송사에서 실제로 배송이 시작되고 나서 운송사의 조회 시스템에 해당 배송 건이 등록된 이후부터 조회가 가능합니다.<br><br>

									 ● 한국 내 배송 중에 발생한 문제는 어떻게 해결하나요?<br>
									 한국 내 배송 중에 문제가 발생하여 이씨코리아 물류창고에 입고가 지연되는 경우, 회원님께 이메일로 안내 메일이 발송됩니다.<br>
									 이후 해당 상품을 취소(환불) 또는 대기하도록 선택해 주시면, 선택하신 방법으로 조치해드립니다. 
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">5. 주문상품 입고</h4>
								<p>
									 ● 입고 안내 메시지가 안 옵니다. <br>
									주문하신 상품이 아직 이씨코리아 물류센터로 도착하지 않은 경우, 메시지가 발송되지 않습니다.<br>
									혹은 등록해주셨던 이메일과 연락처가 맞는지 확인해주시고, 스팸 메일함도 확인해주시기 바랍니다.<br>
									MY PAGE > Shipment 에서 현재 배송 상황을 조회하실 수 있습니다.<br>
									혹은 송장번호로 해당 운송사 홈페이지에서도 조회하실 수 있습니다.<br><br>
									
									● 입고가 너무 늦어요.<br>
									한국 쇼핑몰에서 이씨코리아 물류센터로 배송되는 과정에서 문제가 발생한 경우입니다.<br>
									입고가 지연되는 상품에 대해서는 회원님께 문제 발견 즉시 안내됩니다. <br>
									이후 해당 상품을 취소(환불) 또는 대기하도록 선택해 주시면, 선택하신 방법으로 조치해드립니다. <br><br>

									● 주문을 변경하고 싶어요.<br>
									죄송합니다. 이씨코리아는 정책상 교환이 불가능합니다. <br>
									이미 주문하신 상품에 대해서 오직 취소(환불) 처리만 가능하오니 서비스 이용에 참고해주시기 바랍니다.<br>
									변경하고 싶은 주문에 대해서는 취소(환불) 이후 재주문해 주시기 바랍니다.<br><br>

									● 입고 단계에서 발생한 문제는 어떻게 처리되나요?<br>
									입고 단계에서 문제가 발생되면 회원님께 안내 메시지가 발송됩니다.<br>
									이후 해당 상품을 취소(환불) 또는 대기하도록 선택해 주시면, 선택하신 방법으로 조치해드립니다. <br>
									입고 과정에서 발생한 상품의 손괴/파손의 경우에는 환불 처리됩니다.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">6. 검수/패킹 후 출고</h4>
								<p>
									 ● 더 자세히 검수해줄 수 있나요?<br>
									이씨코리아 물류센터로 입고된 모든 상품은 검수 및 패킹(재포장) 작업을 거치게 됩니다.<br>
									검수를 통해 주문 상품이 맞는지, 수량과 옵션이 맞는지, 손괴/파손이 있는지 확인하고 국제배송비를 측정합니다.<br>
									이후 회원님의 이메일에 사진과 영상으로 검수 결과가 발송됩니다. 추후에 정밀검수 서비스가 추가될 예정입니다.<br><br>

									● 검수/패킹 단계를 더 자세히 알고 싶어요.<br>
									GUIDE > Inspection & Fees 를 참고해주시기 바랍니다.<br><br>

									● 출고가 너무 늦어요.<br>
									이씨코리아 물류센터 또는 국제 운송사에서 문제가 발생한 경우이니 이점 양해 부탁드립니다.<br>
									출고가 지연되는 상품에 대해서는 회원님께 문제 발견 즉시 안내됩니다. <br>
									이후 해당 상품을 취소(환불) 또는 계속 대기 중 선택하신 대로 조치해드립니다. <br><br>

									● 주문을 변경하고 싶어요.<br>
									죄송합니다. 이씨코리아는 정책상 교환이 불가능합니다. <br>
									이미 주문하신 상품에 대해서 오직 취소(환불) 처리만 가능하오니 서비스 이용에 참고해주시기 바랍니다.<br>
									변경하고 싶은 주문에 대해서는 취소(환불) 이후 재주문해 주시기 바랍니다.<br><br>

									● 출고 안내 메시지가 안 옵니다.<br>
									주문하신 상품이 아직 이씨코리아 물류센터에서 출하되지 않은 경우, 메시지가 발송되지 않습니다.<br>
									혹은 등록해주셨던 이메일과 연락처가 맞는지 확인해주시고, 스팸 메일함도 확인해주시기 바랍니다.<br>
									MY PAGE > Shipment 에서 현재 배송 상황을 조회하실 수 있습니다.<br>
									혹은 송장번호로 해당 운송사 홈페이지에서도 조회하실 수 있습니다.<br><br>

									● 출고 단계에서 발생한 문제는 어떻게 처리되나요?<br>
									출고 단계에서 문제가 발생하면 회원님께 안내 메시지가 발송됩니다.<br>
									이후 해당 상품을 취소(환불) 또는 대기하도록 선택해 주시면, 선택하신 방법으로 조치해드립니다. <br>
									출고 과정에서 발생한 상품의 손괴/파손의 경우에는 환불 처리됩니다.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">7. 국제 배송(항공, 해상)</h4>
								<p>
									● 배송 조회가 안 됩니다. <br>
									아직 국제 운송사에서 배송이 시작되지 않은 경우, 송장번호가 없기 때문에 조회가 불가능합니다. <br>
									운송사에서 실제로 배송이 시작되고 나서 운송사의 조회 시스템에 해당 배송 건이 등록된 이후부터 조회가 가능합니다. <br> <br>

									● 국제 배송 단계에서 발생한 문제는 어떻게 처리되나요? <br>
									국제 배송 단계에서 문제가 발생되면 회원님께 안내 메시지가 발송됩니다. <br>
									이후 해당 상품을 취소(환불) 또는 대기하도록 선택해 주시면, 선택하신 방법으로 조치해드립니다.  <br>
									국제 배송 과정에서 발생한 상품의 손괴/파손의 경우에는 저희에게 문의를 주셔도 해결이 어렵습니다. <br>
									해당 운송사에 직접 문의드리는 것을 권장드립니다.	
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">8. 통관 및 관부가세 납부</h4>
								<p>
									● 통관 진행상황은 어디서 확인하나요?<br>
									기본적으로 MY PAGE > Shipment 에서 통관 전 / 중 / 완료 상태를  확인 하실 수 있습니다.<br>
									더 자세한 진행정보는 각 나라 관세청 수입화물 진행정보 혹은 항공사에서 항공운송장 번호를 통해 조회할 수 있습니다.<br><br>

									● 통관 완료를 확인했는데도 배송이 안되고 있습니다.<br>
									통관 심사는 완료되었으나 세관에서 반출이 지연되고 있는 경우입니다.<br>
									세관에서 반출되어 회원님 국가의 현지 배송업체로 인계되기 전까지 기다려주시면 감사하겠습니다.<br><br>

									● 통관 안내 문자나 메일을 받지 못했어요<br>
									주문하신 상품의 통관 심사가 완료되지 않은 경우, 메시지가 발송되지 않습니다.<br>
									혹은 등록해주셨던 이메일과 연락처가 맞는지 확인해주시고, 스팸 메일함도 확인해주세요.<br>
									기본적으로 MY PAGE > Shipment 에서 통관 전 / 중 / 완료 상태를 확인 하실 수 있습니다.<br>
									더 자세한 진행정보는 각 나라 관세청 수입화물 진행정보 혹은 항공사에서 항공운송장 번호를 통해 조회할 수 있습니다.<br><br>

									● 관부가세가 너무 많이 나왔어요<br>
									통관 심사에서 면세 조건을 초과하거나 면세에 해당하지 않는 품목이 있는 경우, 관부가세가 청구될 수 있습니다.<br>
									청구된 비용이 결제되기 전까지 회원님의 상품은 세관에서 반출되지 않습니다.<br>
									따라서 구매 전에 Customs duty rates, List of restricted & prohibited items 를 꼭 확인해주시기 바랍니다.<br><br>

									● 관부가세 결제는 어디에서 어떻게 하나요?<br>
									청구된 관부가세는 회원님의 이메일로 안내드립니다.<br>
									청구된 금액을 확인하시고 이용하시는 은행에 직접 방문하시거나, 인터넷 뱅킹으로 납부하실 수 있습니다.<br><br>

									● 통관금지품목에 없었는데, 수입신고 수리가 거절되었습니다.<br>
									각 나라별 통관금지품목은 보편적인 경우를 설명해드리고 있습니다.<br>
									시기와 상황에 따라서 변동이 있을 수 있습니다. 이 점 양해 부탁드립니다.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">9. 현지 배송</h4>
								<p>
									● 현지 배송업체를 제가 선택할 수 있나요?<br>
									현지 배송업체 선택은 불가능합니다.<br><br>

									● 현지 배송 단계에서 발생한 문제는 어떻게 해결하나요?<br>
									현지 배송 단계에서 문제가 발생되면 회원님께 안내 메시지가 발송됩니다.<br>
									이후 해당 상품을 취소(환불) 또는 대기하도록 선택해 주시면, 선택하신 방법으로 조치해드립니다. <br>
									현지 배송 과정에서 발생한 상품의 손괴/파손의 경우에는 저희에게 문의를 주셔도 해결이 어렵습니다.<br>
									회원님의 국가와 한국 간의 시차로 인해 빠른 문제해결을 위해서는 해당 배송업체에 직접 문의드리는 것을 권장드립니다.<br><br>

									● 현지 배송이 너무 늦어요.<br>
									현지 배송업체의 사정에 따라 배송이 지연될 수 있습니다.<br>
									회원님의 국가와 한국 간의 시차로 인해 빠른 문제해결을 위해서는 해당 배송업체에 직접 문의드리는 것을 권장드립니다.<br><br>
										
									● 안내 문자를 받지 못했습니다.<br>
									회원님의 국가와 한국 간의 시차로 인해 배송 현황이 다소 늦게 반영될 수 있습니다.<br>
									혹은 회원님의 이메일 주소가 맞는지 확인해주시고, 스팸 메일함을 확인해주시기 바랍니다.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">10. 주문상품 수령</h4>
								<p>
									● 주문 상품에 문제가 있어서 교환/환불하고 싶습니다.<br>
									죄송합니다. 이씨코리아는 정책상 교환이 불가능하며 환불만 가능합니다. <br>
									상품을 환불하시려는 경우, 이씨코리아 물류센터로 반송을 해주셔야 합니다.<br>
									또한 반송에 필요한 배송비와 부가 수수료는 회원님께서 부담해주셔야 하는 점 양해 바랍니다.<br><br>

									● 상품은 잘 받았습니다. 리뷰는 어디에서 작성하나요?<br>
									회원님의 소중한 리뷰는 K-Community > Reviews 에서 작성 부탁드립니다.<br>
									또한 회원님께서 작성하신 리뷰와 회원님이 열람하신 리뷰는 My Page > My post 에서 확인 가능합니다. 
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">1. SIGN UP</h4>
								<p>
									● Social media accounts cannot be linked.<br>
									 If your account is unable to link please register through the regular membership sign-up.<br>
									 EC Korea does not collect your personal information except for email, contact information, and shipping address information for the purpose of delivery guidance.<br>
									 When you register as a member, you can receive various benefits such as half-price delivery of your first purchase.<br><br>

									 ● I did not receive a verification email.<br>
									 It may take about 5 minutes for the authentication email to be sent depending on the network environment.<br>
									 If the email still does not arrive afterwards, please check your spam mail or use another email address.<br>
									 If the problem persists, please send an email to 00000@ecplaza.net and we will take care of it.<br><br>

									 ● Do I have to enter my personal contact and email?<br>
									 Your contact information and email will be used only for shipping guidance.<br>
									 If you give us additional consent, you will be notified of the event and promotion.<br><br>

									 ● Do I have to install the Chrome extension?<br>
									 If you are only shopping at EC Korea, there is no need to install it.<br>
									 However, in order to shop on external Korean sites, you must use a Chrome browser.<br>
									 EC Korea automatically puts your order information into the EC Korea shopping cart through the Chrome extension program.<br>
									 If you don't need it anymore, you can disable/delete the ECKorea integrated shopping cart chrome extension at any time.
								</p>
								
								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">2. SHOPPING at EC KOREA</h4>
								<p>
									● Is shopping impossible at other Korean shopping malls except Naver Smart Store and Coupang?<br>
									We apologize as currently the only Korean sites linked to the EC Korea integrated shopping cart are Naver Smart Store and Coupang.<br>
									Please note that other Korean shopping malls will be serviced soon.<br><br>

									● I can't find the product I want.<br>
									At EC Korea, MDs regularly source products for overseas Koreans and K-wave fans.<br>
									If you don't have a product you are looking for, search for more products at Naver Smart Store and Coupang!<br>
									Please understand that currently only Naver Smart Store and Coupang are provided to reduce shipping costs and fast delivery.<br>
									Other Korean shopping malls will be added soon, so please look forward to it!<br><br>

									● What products are posted on the main page?<br>
									Products carefully selected by EC Korea MDs (ECK CHOICE: FEATURED PRODUCTS / BEST SELLERS / NEW ARRIVALS)<br>
									Products by category (KYOBO BOOKS / K-BEAUTY / K-FASHION / K-LIVING / K-GOODS / K-FOOD)<br>
									Products recently purchased by other members (KYOBO BEST SELLER)<br>
									Meet a variety of products at EC Korea!
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">3. EC KOREA ADD TO CART</h4>
								<p>
									● Add to cart button has disappeared at the bottom right corner of the screen.<br>
									currently only Naver Smart Store and Coupang are provided to reduce shipping costs and fast delivery.<br>
									Add to cart is not available in shopping malls where the service is not supported. <br>
									Other Korean shopping malls will be added soon, so please look forward to it!<br><br>

									● When I click ‘Add to Cart' in the Chrome Extension program, a message ‘service unavailable currently' appears.<br>
									This is a problem that occurs when some sellers of Naver Smart Store and Coupang frequently update product information.<br>
									Please try reinstalling the Chrome Shopping Cart extension or try again later.<br>
									If the problem persists, please send an email to 00000@ecplaza.net.<br><br>

									●  ‘Add to Cart’ button in the Chrome extension program does not appear.<br>
									Chrome Shopping Cart extension does not work on sites other than Naver Smart Store and Coupang.<br>
									If the button does not appear in Naver Smart Store and Coupang, please reinstall the Chrome Extension.<br>
									If the problem persists, please send an e-mail to 00000@ecplaza.net.<br><br>

									● Can I put the canceled product back in the shopping cart?<br>
									Sorry. Items canceled from the shopping cart cannot be added again. Related functions will be added soon.<br><br>

									● The product information in the shopping cart has been changed.<br>
									If the product information has been changed, it is not an error the information is just updated by the seller of the external shopping mall.<br>
									Since EC Korea is linked with an external shopping mall, the information updated by the seller is automatically reflected.<br>
									If the seller has edited the price and quantity information, the modified information will also be displayed in the shopping cart.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">4. SHIPPING WITHIN KOREA</h4>
								<p>
									● How do I track shipments?<br>
									You can track your shipment by invoice number on MY PAGE> Shipment from the time your delivery in Korea or international begins.<br><br>

									● Delivery tracking is not available.<br>
									If delivery has not yet started with the carrier, it cannot be viewed because there is no invoice number.<br>
									Tracking service is available after the shipment is actually started by the carrier and the shipment is registered in the tracking system of the carrier.<br><br>

									● How do I solve a  problem during delivery within Korea?<br>
									If a problem occurs during delivery within Korea and the delivery to the EC Korea warehouse is delayed, a notification email will be sent to you by email.<br>
									After that, if you choose to cancel (refund) or wait for the product, we will take action in the method you chose.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">5. RECEIVING ORDERED GOODS</h4>
								<p>
									● I do not receive a stocking information message.<br>
									If the ordered product has not yet arrived at the EC Korea distribution center, a message will not be sent.<br>
									Or, please check that your email information is correct at MY PAGE. If your email is correct, please check your spam mailbox.<br>
									You can check the current shipping status at MY PAGE> Shipment.<br>
									Alternatively, you can search on the carrier's website by using the invoice number.<br><br>

									● Warehousing take so long.<br>
									This is a case of a problem in the process of being delivered from a Korean shopping mall to the EC Korea distribution center.<br>
									For items that are delayed in stocking, you will be notified immediately about the  problem.<br>
									After that, if you choose to cancel (refund) or wait for the product, we will take action in the method you chose.<br><br>

									● I want to change my order.<br>
									Sorry. Due to the policy of EC Korea, exchange is not possible.<br>
									You can only cancel (refund) the products you have already ordered, so please refer to the service usage.<br>
									If you want to change your order, please reorder that after cancellation (refund).<br><br>

									● How is the problem handled during the warehousing phase?<br>
									If a problem occurs during the warehousing phase, an notification message will be sent to you.<br>
									After that, if you choose to cancel (refund) or wait for the product, we will take action in the method you chose.<br>
									In the case of product damage/damage during the warehousing process, a refund will be processed.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">6. INSPECTION PACKING AND SHIPPING</h4>
								<p>
									● Can you inspect it in more detail?<br>
									All products received at the EC Korea distribution Center will be inspected and packed (repackaged).<br>
									Through inspection, we check whether the ordered product is correct, including whether the quantity and the options are correct, and if there is damaged, and measurement of the international shipping cost.<br>
									Afterwards, the inspection results will be sent to your  email as a photo and video. A precision inspection service will be added later. <br><br>

									● I want to know more about the inspection/packing steps.<br>
									Please refer to GUIDE> Inspection & Fees for more information<br><br>

									●The delivery is too late.<br>
									Please note that this is a case of a problem at the EC Korea distribution center or an international shipping company.<br>
									For products that are delayed in shipment, you will be notified as soon as the problem is found.<br>
									After that, if you choose to cancel (refund) or wait for the product, we will take action in the method you chose.<br><br>

									● I want to change my order.<br>
									Sorry. Due to the policy of EC Korea, exchange is not possible.<br>
									You can only cancel (refund) the products you have already ordered, so please refer to the service usage.<br>
									If you want to change your order, please reorder that after cancellation (refund).<br><br>

									● I do not receive a message about the shipment.<br>
									If the ordered product has not yet been shipped from the EC Korea distribution center, no message will be sent.<br>
									Please check that your email information is correct at MY PAGE. If your email is correct, please check your spam mailbox.<br>
									You can check the current shipping status at MY PAGE> Shipment.<br>
									Alternatively, you can search on the carrier's website by using the invoice number.<br><br>

									● How is the problem handled during the shipping process?<br>
									If a problem occurs during the shipping process, a message will be sent to the you.<br>
									After that, if you choose to cancel (refund) or wait for the product, we will take action in the method you chose.<br>
									In the case of product damage/damage in the process of shipment, a refund will be issued.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">7. INTHERNATIONAL SHIPPING (AIR, SEA)</h4>
								<p>
									● I can't track my shipment.<br>
									If delivery has not yet been initiated by an international carrier, the invoice number does not exist, so the tracking is unavailable. <br>
									you can track your order after the shipment is actually started by the carrier and the shipment is registered in the tracking system of the carrier.<br><br>

									● How is the problem handled during the international shipping process?<br>
									If a problem occurs during the international shipping process, a message will be sent to the you.<br>
									After that, if you choose to cancel (refund) or wait for the product, we will take action in the method you chose.<br>
									In the case of product damage during the international delivery process, it is difficult to resolve it even if you contact us.<br>
									We recommend that you contact the carrier directly.
								</p>

								<div class="blank10"></div><div class="blank10"></div>

								<h4 class="title">8. CUSTOMS CLEARANCE AND PAYMENT OF CUSTOMS TAX</h4>
								<p>
									● Where can I check the progress of customs clearance?<br>
									Basically, you can check the status of before / during / after customs clearance process at MY PAGE> Shipment.<br>
									For more detailed progress information, you can check the import cargo progress information of each country or the airline through the air waybill number.<br><br>

									● I checked the customs clearance, but it's not being delivered.<br>
									Customs clearance has been completed, but there is a delay in taking it out of customs.<br>
									We would appreciate it if you could wait until it is taken out of customs and transferred to your country's local shipping company.<br><br>

									● I didn't get any customs clearance messages or emails.<br>
									If the customs inspection of the product you ordered is not completed, no message will be sent.<br>
									Please check if the email and contact number you registered are correct, and also check your spam mailbox.<br>
									You can also check the status of before / during / after customs clearance process at MY PAGE> Shipment.<br>
									For more detailed progress information, you can check the import cargo progress information of each country or the airline through the air waybill number.<br><br>

									● There was too much VAT.<br>
									If there are items that exceed the duty-free conditions or do not qualify for duty-free in customs inspection, you may be charged.<br>
									Your item will not be taken out of customs until the charged cost is paid.<br>
									Therefore, please check the Custom duty rates, List of restricted & protected items before purchasing.<br><br>
									
									● Where and how do I pay for the tax?<br>
									The charged government tax will be sent to your email.<br>
									You can check the requested amount and visit the bank you are using or pay through Internet banking.<br><br>

									● It was not on the customs clearance prohibited item, but the import declaration repair was rejected.<br>
									Customs-free items in each country explain the general case.<br>
									There may be changes depending on the time and situation. Please understand this.
								</p>



							</div>
							<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('guide_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

@include('top')

<body>
    <div class="page-wrapper">
        <main class="main">            

            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">About Us</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

			

            <div class="about-section">
                <div class="container">
					<h2 class="title2"><img src="/css/assets/images/aboutus.jpg"></h2>
					
                    <h2 class="title">What is EC KOREA? </h2>
                    <p>
						The only Cross-border Shopping Platform for K-Wave Fans like YOU! <br>
						We are e-commerce business for more than 20 years. Starting B2B, We step on B2C for Enterprise and general Customer. <br>
						We are confidential that eckorea is the best place for overseas K-Wave Fans to seek substantial and popular K-products. 
					</p>
                </div><!-- End .container -->
            </div><!-- End .about-section -->

			<div class="team-section">
                <div class="container">                  
                    <div class="row">
                        <div class="col-4 col-sm-4 col-lg-4">
                            <div class="member">
                                <img src="/image/team/about_01.png"></a>
                                <h3 class="member-title">Credible career</h3>
                            </div><!-- End .member -->
                        </div>

                        <div class="col-4 col-sm-4 col-lg-4">
                            <div class="member">
                                <img src="/image/team/about_02.png"></a>
                                <h3 class="member-title">Various merchandise</h3>
                            </div><!-- End .member -->
                        </div>

                        <div class="col-4 col-sm-4 col-lg-4">
                            <div class="member">
                                <img src="/image/team/about_03.png"></a>
                                <h3 class="member-title">Easy and quick process</h3>
                            </div><!-- End .member -->
                        </div>  
						 
                    </div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .team-section -->

            
			

			<div class="about-section">
                <div class="container">
				<!--
					<h2 class="title">HOW TO USE</h2>     

					<div class="about_step">
						<p>1. Sign up</p>

						Join us with Chrome Browser or App.<br>
						Download expansion program.<br>
						Only Chrome is supported.
					</div>

					<div class="row">
                        <div class="col-sm-6 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_01.jpg"></a>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_02.jpg"></a>
                            </div>
                        </div>
                              
                    </div>

					<div class="about_step">
						<p>2. Select your K-products</p>
						Enjoy your shopping and add to Cart.
					</div>

					<div class="row">
                        <div class="col-sm-6 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_03.jpg"></a>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_04.jpg"></a>
                            </div>
                        </div>
                              
                    </div>

					<div class="about_step">
						<p>3. Follow Payment guide</p>
						Go after our clear guide. <br>
						Fill out form, pay the price.
					</div>

					<div class="row">
                        <div class="col-sm-6 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_05.jpg"></a>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_06.jpg"></a>
                            </div>
                        </div>
                              
                    </div>

					<div class="about_step">
						<p>3-1. Confirm the shipping fee</p>
						As your goods arrive at warehouse, you will get this message by email or Kakao talk. 
						If you got this message from us, please visit ��MY ORDER�� and make a second payment for the international shipment. 
					</div>

					<div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_07.jpg"></a>
                            </div>
                        </div>
                       
                    </div>

					<div class="about_step">
						<p>4. Deliver and Receive</p>
						Check your ongoing Deliver. <br>
						Receive your goods, Confirm your order.
					</div>

					<div class="row">
                        <div class="col-sm-6 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_08.jpg"></a>
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_09.jpg"></a>
                            </div>
                        </div>
                              
                    </div>

					<div class="about_step">
						<p>5. Review</p>
						Leave your review.
						Get reward and discount coupons.
					</div>

					<div class="row">
                        <div class="col-sm-12 col-lg-6">
                            <div class="member">
                                <img src="/image/team/about_step_10.jpg"></a>
                            </div>
                        </div>
                       
                    </div>
		-->

					<div class="row">
                        <div class="col-sm-12 col-lg-12">
                            <div class="member">
                                <img src="/image/team/about_hongbo_01.jpg"></a>
                            </div>
                        </div>     
						<div class="col-sm-12 col-lg-12">
                            <div class="member">
                                <img src="/image/team/about_hongbo_02.jpg"></a>
                            </div>
                        </div> 
						<div class="col-sm-12 col-lg-12">
                            <div class="member">
                                <img src="/image/team/about_hongbo_03.jpg"></a>
                            </div>
                        </div> 
						<div class="col-sm-12 col-lg-12">
                            <div class="member">
                                <img src="/image/team/about_hongbo_04.jpg"></a>
                            </div>
                        </div> 
						<div class="col-sm-12 col-lg-12">
                            <div class="member">
                                <img src="/image/team/about_hongbo_05.jpg"></a>
                            </div>
                        </div> 
                    </div>

                    <h2 class="title">MORE INFORMATION</h2>                   

					<div class="feature-boxes-container row mt-6 mb-1">
						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/order/list">
									<img src="/image/team/bottom_ic_03.png"></a>
									<div class="feature-box-content">
										<h3 class=" ">MY PAGE</h3>
									</div>
								</a>
							</div>
						</div>
						
						
						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/guide/4">
									<img src="/image/team/bottom_ic_06.png"></a>
									<div class="feature-box-content">
										<h3 class="mb-0 pb-1">GUIDE</h3>
									</div>
								</a>
							</div>
						</div>
						

						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/faq">
									<img src="/image/team/bottom_ic_01.png"></a>
									<div class="feature-box-content">
										<h3 class="mb-0 pb-1">FAQ</h3>
									</div>
								</a>
							</div>
						</div>
						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/review">
									<img src="/image/team/bottom_ic_02.png"></a>
									<div class="feature-box-content">
										<h3 class="mb-0 pb-1">REVIEWS</h3>
									</div>
								</a>
							</div>
						</div>

						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/shipping_fee">
									<img src="/image/team/bottom_ic_04.png"></a>
									<div class="feature-box-content">
										<h3 class="mb-0 pb-1">SHIPPING FEE</h3>
									</div>
								</a>
							</div>
						</div>

						<div class="col-4 col-md-2">
							<div class="feature-box px-sm-5 px-md-4 mx-sm-5 mx-md-3 feature-box-simple text-center">
								<a href="/mall/exchange_rate">
									<img src="/image/team/bottom_ic_05.png"></a>
									<div class="feature-box-content">
										<h3 class="mb-0 pb-1">EXCHANGE RATE</h3>
									</div>
								</a>
							</div>
						</div>
					</div><!-- End .feature-boxes-container.row -->

					 <h2 class="title">FOLLOW US</h2>              


					<div class="row">
                        <div class="col-4 col-md-2">
                            <div class="member">
                                <a href="https://www.instagram.com/ec_korea_mall/" target="_blank"><img src="/image/team/sns_01.png"></a>
                            </div><!-- End .member -->
                        </div>

                        <div class="col-4 col-md-2">
                            <div class="member">
                                <a href="https://www.youtube.com/channel/UC9KXNIRP6nqCTkxulOW86sg" target="_blank"><img src="/image/team/sns_02.png"></a>
                            </div><!-- End .member -->
                        </div>

                        <div class="col-4 col-md-2">
                            <div class="member">
                                <a href="https://blog.naver.com/cbt20korea" target="_blank"><img src="/image/team/sns_03.png"></a>
                            </div><!-- End .member -->
                        </div>     
						
						<div class="col-4 col-md-2">
                            <div class="member">
                                <a href="https://www.facebook.com/EC-KOREA-351853399217828" target="_blank"><img src="/image/team/sns_04.png"></a>
                            </div><!-- End .member -->
                        </div>   

						<div class="col-4 col-md-2">
                            <div class="member">
                                <a href="https://www.quora.com/q/allaboutskorea" target="_blank"><img src="/image/team/sns_05.png"></a>
                            </div><!-- End .member -->
                        </div>  

						<div class="col-4 col-md-2">
                            <div class="member">
                                <a href="http://pf.kakao.com/_UxnFDK" target="_blank"><img src="/image/team/sns_06.png"></a>
                            </div><!-- End .member -->
                        </div>   


                    </div><!-- End .row -->

                </div><!-- End .container -->
            </div><!-- End .about-section -->



            
        </main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	@include('footer')
    
</body>

        
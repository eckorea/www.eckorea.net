@include('top')

<body>
    <div class="page-wrapper">
        <main class="main">            

            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">K-CULTURE</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="about-section">
                <div class="container">
                    <h2 class="title">K-CULTURE</h2>                   
                </div><!-- End .container -->
            </div><!-- End .about-section -->
			
			<div class="team-section">
                <div class="container">                  
                    <div class="row">
                        <div class="col-sm-6 col-lg-3">
                            <div class="member">
                                <a href="/mall/kculture/K-TREND">
								<img src="/css/assets/images/ktrend.jpg">
                                <h3 class="member-title">K-TREND</h3>
								</a>
                            </div><!-- End .member -->
                        </div><!-- End .col-lg-3 -->

                        <div class="col-sm-6 col-lg-3">
                            <div class="member">
								<a href="/mall/kculture/K-BEAUTY">
                                <img src="/css/assets/images/kbeauty.jpg">
                                <h3 class="member-title">K-BEAUTY</h3>
								</a>
                            </div><!-- End .member -->
                        </div><!-- End .col-lg-3 -->

                        <div class="col-sm-6 col-lg-3">
                            <div class="member">
								<a href="/mall/kculture/K-COOK">
                                <img src="/css/assets/images/kcook.jpg">
                                <h3 class="member-title">K-COOK</h3>
								</a>
                            </div><!-- End .member -->
                        </div><!-- End .col-lg-3 -->                      

						<div class="col-sm-6 col-lg-3">
                            <div class="member">
								<a href="/mall/kculture/kstudy">
                                <img src="/css/assets/images/kstudy.jpg">
                                <h3 class="member-title">K-STUDY</h3>
								</a>
                            </div><!-- End .member -->
                        </div><!-- End .col-lg-3 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .team-section -->           
			

            
        </main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	@include('footer')
    
</body>

        
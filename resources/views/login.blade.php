@include('top')

<body>
<div class="page-wrapper">
  <main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
      <div class="container">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Sign In</li>
        </ol>
      </div>
      <!-- End .container --> 
    </nav>
    <div class="page-header">
      <div class="container">
        <h2>Sign In and Create Account</h2>
      </div>
      <!-- End .container --> 
    </div>
    <!-- End .page-header -->
    
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="heading">
            <h2 class="title">Sign In</h2>
            <p>Hello, ECKOREA shopper.</p>
           <!-- <p>로그인 된 계정 명 : {{@session('mb_email')}}</p>
            <p>생성된 유저키(쿠키BASE) : <input type="text" value="{{$SBUSERKEY}}"/></p>-->
          </div>
          <!-- End .heading -->
          
          <form method="post" action="/login">
          {{ csrf_field() }}
              <input type="hidden" name="return_url" value="{{Request::query('return_url')}}">
              <input type="hidden" name="phoneType">
              <input type="hidden" name="deviceUuid">
              <input type="hidden" name="pushKey">
              <input type="email" name="email" class="form-control" placeholder="Email Address" required>
              <input type="password" name="password" class="form-control" placeholder="Password" required>
              <div class="form-footer">
                <button type="submit" class="btn btn-primary">SIGN IN</button>
                <a href="/password_reset" class="forget-pass"> Forgot your password?</a>
              </div>
                <!-- End .form-footer -->
          </form>
         <!-- <div>FACEBOOK LOGIN</div>
          <div>GOOGLE LOGIN</div>
          <div>KAKAOTALK LOGIN</div>
          <div>APPLE LOGIN</div>-->
        </div>
        <!-- End .col-md-6 -->
        
        <div class="col-md-6">
          <div class="heading">
            <h2 class="title">Create An Account</h2>
            <p>Haven’t you made account yet?</p>
          </div>
          <!-- End .heading -->
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
          <form method="post" action="/join" onSubmit="return valid_join();">
              {{ csrf_field() }}
                <input type="text" name="first_name" class="form-control" placeholder="First Name"  pattern="[A-Za-z]+" oninput="handleOnInput(this)" required>
                <input type="text" name="last_name" class="form-control" placeholder="Last Name" pattern="[A-Za-z]+" oninput="handleOnInput(this)" required>
                <input type="email" name="email" class="form-control" placeholder="Email Address" @if(session('email')) style="background-color:yellow;" @endif required>
                <input type="password"  id="password" name="password" class="form-control" placeholder="Password" required>
                <input type="password" id="password_check" class="form-control" placeholder="Confirm Password" required>
                <input type="text" name="recommend_user" class="form-control" placeholder="Recommend User ID(e-mail) (Optional)">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="news_letter" value="1" class="custom-control-input" id="newsletter-signup">
                    <label class="custom-control-label" for="newsletter-signup">Sign Up Our Newsletter</label>
                </div>

                  <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                      <label class="col-md-4 control-label">로봇이 아닙니다!</label>
                      <div class="col-md-6">
                          {!! app('captcha')->display() !!}
                          @if ($errors->has('g-recaptcha-response'))
                          <span class="help-block">
                                  <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                          </span>
                          @endif
                      </div>
                  </div>
            <!-- End .custom-checkbox -->
            
                <div class="form-footer">
                    <button type="submit" class="btn btn-primary">Create Account</button>
                </div>
            <!-- End .form-footer -->
          </form>
        </div>
        <!-- End .col-md-6 --> 
      </div>
      <!-- End .row --> 
    </div>
    <!-- End .container -->
    
    <div class="mb-5"></div>
    <!-- margin --> 
  </main>
  <!-- End .main --> 
</div>
<!-- End .page-wrapper --> 

@include('footer')
</body>
<script>
function handleOnInput(e)  {
  e.value = e.value.replace(/[^A-Za-z]/ig, '')
}

function valid_join()
{
	var password = $("#password").val();
	var password_check = $("#password_check").val();
	
	if(password != password_check)
	{
		alert('Password is different. Please Check.');
		return false;
	}
}

function fn_initDeviceInfo(phoneType, deviceUuid, pushKey) {
    // alert( phoneType+ "|"+deviceUuid + "|" + pushKey);
    $("input[name=phoneType]").val(phoneType);
    $("input[name=deviceUuid]").val(deviceUuid);
    $("input[name=pushKey]").val(pushKey);
};

window.onload = function() {
    var browserInfo = navigator.userAgent.toLowerCase();
    // console.log("My Broswer["+browserInfo+"]");

    if(browserInfo.indexOf("android") > -1) {
        ///alert("안드로이드.");
        /**
         * 안드로이드용 디바이스 정보
         */
        window.ecplaza.getDeviceInfo();
    } else if(browserInfo.indexOf("ip") > -1) {
        ///alert("iOS");
        /**
         * 아이폰용 디바이스 정보
         */
        var postData = {
            "name": "getDeviceInfo"
        };
        window.webkit.messageHandlers.ecplaza.postMessage(postData);
    }
};

$(document).ready(function() {

});

</script>
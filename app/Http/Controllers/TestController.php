<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\PayErom;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use App\Mail\UserRegist;
use Illuminate\Support\Facades\Mail;

class TestController extends Controller
{
    public function abroad_payment()
	{
		//make start log
		$ok = new PayErom();	
		$ok->hard();	
		//make end log
	}
	
	public function domestic_payment()
	{
		echo "국내결제";
		$pay = new PayErom();	
		$pay->hard_domestic();
		
		echo "done";
		
	}
	
	public function sendmail()
	{
		$result = Mail::to('milovetrue@gmail.com')->send(new UserRegist('veriurl21313'));
		//$result = Mail::to('jaeseong3219@hanmail.net')->send(new UserRegist('veriurl2222'));
		
		echo $result;
		//echo mail('milovetrue@gmail.com', '다시 보냄 테스트 이제 잘 작동하나?', '내용이 드간다 야야야야 고고 재');	
		echo "OK";
	}
	
	public function cancel()
	{
		$ok = new PayErom();	
		$result = $ok->refundPayment('20210316326230889006','210316-040738-0680',10, '1st_charge');	
		print_r($result);
	}
	
	public function exchange()
	{
		$ok = new PayErom();	
		$result = $ok->getRate('USD','KRW');
		print_r($result);
	}
	
	public function paser()
	{
		
		//SS
		$buy_amount = 27000;
		$ship_sample = '<div class="pTaPzv0i6H _21BFxdLPm-">택배배송<span class="Y-_Vd4O6dS"><span class="_1_wrVRMvuL">2,500</span>원<span class="_3JrHrGnQ31">(주문시 결제)</span></span></div>
<p class="_1JN0qDXZim">30,000원 이상 구매 시 무료 / 제주,도서지역 추가 2,500원</p>
<div class="_3_kSDK68pw"><a href="/coffeecg/bundle/950215?prevProdId=2031015220" target="_blank" class="_2Ugyq5gjSq _23ZcKYMJAs _2W6umRasi3 N=a:pcs.delivery">배송비 절약상품 보기</a></div>';
		
		$ship_sample2 = '<div class="pTaPzv0i6H _21BFxdLPm-">택배배송<span class="Y-_Vd4O6dS"><span class="_1_wrVRMvuL">3,500</span>원<span class="_3JrHrGnQ31">(주문시 결제)</span></span></div>
<p class="_1JN0qDXZim">제주 추가 4,000원, 제주 외 도서지역 추가 6,000원 / 제주도 4000원 추가, 제주 외 도서산간지역 추가 6000원</p>
<div class="_3_kSDK68pw"><a href="/casadada/bundle/21240456?prevProdId=4699650057" target="_blank" class="_2Ugyq5gjSq _23ZcKYMJAs _2W6umRasi3 N=a:pcs.delivery">배송비 절약상품 보기</a></div>';

		$ship_sample3 = '<div class="_38cnMDbfB_"><span class="_2nozUYDL1J">3,000원</span><span class="_2nozUYDL1J">제주 추가 3,000원, 제주 외 도서지역 추가 3,000원</span><span class="_2nozUYDL1J _2nM_mEpqlR"> 3000</span><a href="/plan-e/bundle/26102392?prevProdId=4891860792" class="_2CEl2aEeO7 N=a:inf.deliver linkAnchor">배송비 절약상품 보기</a></div>'; //모바일

		$ship_sample4 = '<div class=\"pTaPzv0i6H _21BFxdLPm-\">택배배송<span class=\"Y-_Vd4O6dS\">무료배송</span></div>';

		//배송비파싱
		$mobile = 0;
		$arr_ship_cond = array();
		$crawler_shipping = new Crawler($ship_sample2);	//파서에 HTML 주입
		if($mobile)
		{
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('._2nozUYDL1J')->text(''));			
			$arr_raw_ship_cond = explode('/',$crawler_shipping->filter('._2nozUYDL1J')->text(''));
		}
		else
		{
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('._1_wrVRMvuL')->text(''));			
			$arr_raw_ship_cond = explode('/',$crawler_shipping->filter('._1JN0qDXZim')->text(''));
		}
		
		$ship_cond_raw = $arr_raw_ship_cond[0];
		$ship_cond = '';
		$neddle = '원 이상 구매 시';
		
		if(strpos($ship_cond_raw, $neddle))
		{
			//원이상 구매시 앞뒤로 자른다		
			$arr_cond = explode($neddle,$ship_cond_raw);
			//
			$ship_cond = preg_replace("/[^0-9]*/s", "",$arr_cond[0]);			
			$cond_price = preg_replace("/[^0-9]*/s", "",$arr_cond[1]);
			if($buy_amount >= $ship_cond) //조건에 부합하면 해당 조건의 가격으로
			{
				$final_price = $cond_price;
			}
			else
			{
				$final_price = $basic_price;
			}
						
			if($final_price == '') $final_price = 0; //문자열일 경우 무료 라고 판단한다.			
		}
		else
		{
			$final_price = $basic_price;
		}	
		
		if($ship_cond)
		{
			$arr_ship_cond[$ship_cond] = $cond_price;
		}
		
		$arr_shipping_condition = array("basic_price"=> $basic_price, "cond" => $arr_ship_cond);
		$json_shipping_condition = json_encode($arr_shipping_condition);
		
		
		echo "구매금액: $buy_amount // 가격: $final_price // 조건 : $ship_cond";
		echo $json_shipping_condition;
		echo "<br><br>";
		
		//쿠팡
		$buy_amount = 29000;
		$ship_sample = '<div class="prod-delivery-notice-container"> </div>
<div class="prod-shipping-fee-container">
    <div class="prod-shipping-fee ">
        <div class="SHIPPING_FEE_DISPLAY_0">
            <div class="prod-shipping-fee-message"> 
			<span>
			배송비 2,500원
			</span> 
			</div>
            <div class="prod-shipping-consolidation"> </div>
        </div>
        <div class="prod-pre-order-release-container" style="display: none;"> <span class="prod-pre-order-release-msg"></span> </div>
    </div>
</div>
<div class="prod-pdd-container">
    <div class="prod-pdd-display-area">
        <div class="prod-pdd single-pdd-area PDD_DISPLAY_0 delivery-3pl "> <em class="prod-txt-onyx prod-txt-font-14">화요일 3/23</em><em class="prod-txt-onyx"> 도착 예정</em>
            <div class="prod-pdd-delay"></div>
        </div>
    </div>
    <div class="prod-pdd-list" style="display: none;"> </div>
</div>
<div class="shipping-fee-list" style="display: none;"> </div>
<div class="loyalty-reminder-default-address" style="display:none;"> <img class="delivery-type-badge" src="" alt=""><em class="prod-gray-txt">가능 배송지로 변경 시 구매 가능</em> </div>';
		
		$ship_sample2 = '<div class="prod-delivery-notice-container"> </div>
<div class="prod-shipping-fee-container">
    <div class="prod-shipping-fee ">
        <div class="SHIPPING_FEE_DISPLAY_0">
            <div class="prod-shipping-fee-message"> 
			<span> 
			<em class="prod-txt-bold">무료배송</em> 
			<em class="prod-txt-black"> (같은 판매자 상품 100,000원 이상 구매 시)</em> 
			<em class="prod-shipping-fee-divider">그 외 배송비 3,000원</em> 
			</span> 
			</div>
            <div class="prod-shipping-consolidation"> </div>
        </div>
        <div class="prod-pre-order-release-container" style="display: none;"> <span class="prod-pre-order-release-msg"></span> </div>
    </div>
</div>
<div class="prod-pdd-container">
    <div class="prod-pdd-display-area">
        <div class="prod-pdd single-pdd-area PDD_DISPLAY_0 delivery-3pl "> <em class="prod-txt-onyx prod-txt-font-14">모레(토) 3/20</em><em class="prod-txt-onyx"> 도착 예정</em>
            <div class="prod-pdd-delay"></div>
        </div>
    </div>
    <div class="prod-pdd-list" style="display: none;"> </div>
</div>
<div class="shipping-fee-list" style="display: none;"> </div>
<div class="loyalty-reminder-default-address" style="display:none;"> <img class="delivery-type-badge" src="" alt=""><em class="prod-gray-txt">가능 배송지로 변경 시 구매 가능</em> </div>';

$ship_sample3 = '<div class="prod-delivery-notice-container"></div>
<div class="prod-shipping-fee-container">
    <div class="prod-shipping-fee">t
        <div class="SHIPPING_FEE_DISPLAY_0">tt
            <div class="prod-shipping-fee-message">
            <span>
            <em class="prod-txt-bold">무료배송</em>
            </span>
			</div>
           
            <div class="prod-shipping-consolidation"></div>
            </div>
        <div class="prod-pre-order-release-container"> <span class="prod-pre-order-release-msg"></span></div>
    </div>
</div>
<div class="prod-pdd-container">
    <div class="prod-pdd-display-area">
        <div>
            <div class="prod-pdd single-pdd-area  PDD_DISPLAY_0 delivery-3pl "> <em class="prod-txt-onyx prod-txt-font-14">모레(토) 3/20</em><em class="prod-txt-onyx"> 도착 예정</em>
                <div class="prod-pdd-delay"></div>
            </div>
        </div>
    </div>
    <div class="prod-pdd-list" style="display: none;"></div>
</div>
<div class="shipping-fee-list" style="display: none;"> </div>
<div class="loyalty-reminder-default-address" style="display:none;"> <img class="delivery-type-badge" src="" alt=""><em class="prod-gray-txt">가능 배송지로 변경 시 구매 가능</em> </div>
';

$ship_sample4 = '<div class="prod-delivery-notice-container"> </div>
<div class="prod-shipping-fee-container" style="display: none;">
    <div class="prod-shipping-fee ">
        <div class="SHIPPING_FEE_DISPLAY_0">
            <div class="prod-shipping-fee-message"> 
			<span>
			<em class="prod-txt-bold">로켓배송 상품</em>
			<br>
            <em class="prod-txt-bold">19,800원 이상 무료배송</em>
			</span> 
			</div>
            <div class="prod-shipping-consolidation"> </div>
        </div>
        <div class="SHIPPING_FEE_DISPLAY_1">
            <div class="prod-shipping-fee-message"> <span><em class="prod-txt-bold">무료배송 + 무료반품</em><br>
                <em class="prod-txt-gray">(로켓와우 신청 시)</em></span> </div>
            <div class="prod-shipping-consolidation"> </div>
        </div>
        <div class="prod-pre-order-release-container" style="display: none;"> <span class="prod-pre-order-release-msg"></span> </div>
    </div>
</div>
<div class="prod-pdd-container">
    <div class="prod-pdd-display-area">
        <div class="prod-pdd single-pdd-area  PDD_DISPLAY_0  "> <em class="prod-txt-onyx prod-txt-green-2">내일(금) 3/19</em><em class="prod-txt-onyx  prod-txt-green-normal"> 도착 보장</em><em class="prod-txt-onyx"> (</em><em class="prod-txt-onyx">16시간 1분</em><em class="prod-txt-onyx"> 내 주문 시)</em>
            <div class="prod-pdd-delay"></div>
            <div class="prod-other-seller" style="display: inline-block; margin-left: 0px;"><a href="javascript:void(0)" class="prod-other-seller-btn">다른 판매자 보기(<span class="prod-other-seller-count">3</span>)</a></div>
        </div>
        <div class="prod-pdd single-pdd-area  PDD_DISPLAY_1  "> <em class="prod-txt-onyx prod-txt-green-2">내일(금) 3/19</em><em class="prod-txt-onyx  prod-txt-green-normal"> 도착 보장</em><em class="prod-txt-onyx"> (</em><em class="prod-txt-onyx">16시간 1분</em><em class="prod-txt-onyx"> 내 주문 시)</em>
            <div class="prod-pdd-delay"></div>
            <div class="prod-other-seller" style="display: inline-block; margin-left: 0px;"><a href="javascript:void(0)" class="prod-other-seller-btn">다른 판매자 보기(<span class="prod-other-seller-count">3</span>)</a></div>
        </div>
    </div>
    <div class="prod-pdd-list" style="display: none;"> </div>
</div>
<div class="shipping-fee-list" style="display: none;"> </div>
<div class="loyalty-reminder-default-address" style="display:none;"> <img class="delivery-type-badge" src="" alt=""><em class="prod-gray-txt">가능 배송지로 변경 시 구매 가능</em> </div>';

$ship_sample5 = '<div class="prod-delivery-notice-container"> </div>
<div class="prod-shipping-fee-container" style="display: none;">
    <div class="prod-shipping-fee ">
        <div class="SHIPPING_FEE_DISPLAY_0">
            <div class="prod-shipping-fee-message"> 
            <span>
            <em class="prod-txt-bold">로켓배송 상품 19,800원 이상 무료배송</em>
            </span> 
            </div>
            <div class="prod-shipping-consolidation"> </div>
        </div>
        <div class="SHIPPING_FEE_DISPLAY_1">
            <div class="prod-shipping-fee-message"> <span><em class="prod-txt-bold">무료배송 + 무료반품</em><em class="separator">|</em><em class="icon-tx">로켓와우 신청시</em><em><i class="rocket-falcon-icon question-mark-rocket-plus"></i></em></span> </div>
            <div class="prod-shipping-consolidation"> </div>
        </div>
        <div class="prod-pre-order-release-container" style="display: none;"> <span class="prod-pre-order-release-msg"></span> </div>
    </div>
</div>
<div class="prod-pdd-container">
    <div class="prod-pdd-display-area">
        <div class="prod-pdd single-pdd-area  PDD_DISPLAY_0  "> <em class="prod-txt-onyx prod-txt-green-2">내일(금) 3/19</em><em class="prod-txt-onyx  prod-txt-green-normal"> 도착 보장</em><em class="prod-txt-onyx"> (</em><em class="prod-txt-onyx">15시간 44분</em><em class="prod-txt-onyx"> 내 주문 시)</em>
            <div class="prod-pdd-delay"></div>
        </div>
        <div class="prod-pdd single-pdd-area  PDD_DISPLAY_1  "> <em class="prod-txt-onyx prod-txt-green-2">내일(금) 3/19</em><em class="prod-txt-onyx  prod-txt-green-normal"> 도착 보장</em><em class="prod-txt-onyx"> (</em><em class="prod-txt-onyx">15시간 44분</em><em class="prod-txt-onyx"> 내 주문 시)</em>
            <div class="prod-pdd-delay"></div>
        </div>
    </div>
    <div class="prod-pdd-list" style="display: none;"> </div>
</div>
<div class="shipping-fee-list">
    <div class="radio-list-item SHIPPING_FEE_NUDGE_DISPLAY_0 selected"> <span class="radio-button"></span> <span class="shipping-fee-list-item inline-flex-v-center"><em class="prod-txt-bold">로켓배송 상품 19,800원 이상 무료배송</em></span> </div>
    <div class="radio-list-item SHIPPING_FEE_NUDGE_DISPLAY_1 "> <span class="radio-button"></span> <span class="shipping-fee-list-item inline-flex-v-center"><em class="prod-txt-bold">무료배송 + 무료반품</em><em class="separator">|</em><em class="icon-tx">로켓와우 신청시</em><em><i class="rocket-falcon-icon question-mark-rocket-plus"></i></em></span> </div>
</div>
<div class="loyalty-reminder-default-address" style="display:none;"> <img class="delivery-type-badge" src="" alt=""><em class="prod-gray-txt">가능 배송지로 변경 시 구매 가능</em> </div>
';

$ship_sample6 = '<table class="return-policy-table">
    <colgroup>
        <col width="29.1%">
        <col width="*">
    </colgroup>
    <tbody>
                        
    <tr>
       <th>배송방법</th>
       <td>순차배송</td>
    </tr>
                        
    <tr>
       <th>배송사</th>
       <td>CJ 대한통운</td>
    </tr>

    <tr>
        <th>배송비</th>
        <td>
            <p>3,000원</p><p>- 30,000원 이상 구매 시 무료배송</p><p></p>
        </td>
    </tr>

    <tr>
        <th>묶음배송 여부</th>
        <td>가능</td>
    </tr>
    <tr>
        <th>배송기간</th>
        <td><p>· 도서산간 지역 등은 배송에 3-5일이 더 소요될 수 있습니다.</p><p>- 천재지변, 물량 수급 변동 등 예외적인 사유 발생 시, 다소 지연될 수 있는 점 양해 부탁드립니다.</p></td>
    </tr>

</tbody>
</table>'; //모바일

$ship_sample7 = '<table class="return-policy-table">
    <colgroup>
    <col width="29.1%">
    <col width="*">
    </colgroup>
    <tbody>
        <tr>
            <th>배송방법</th>
            <td>순차배송</td>
        </tr>
        <tr>
            <th>배송비</th>
            <td><p>무료배송</p>
                <p>- 로켓배송 상품 중 19,800원 이상 구매 시 무료배송</p>
                <p>- 도서산간 지역 추가비용 없음</p></td>
        </tr>
        <tr>
            <th>묶음배송 여부</th>
            <td>가능</td>
        </tr>
        <tr>
            <th>배송기간</th>
            <td><p>· 쿠팡맨 배송 지역 : 주문 및 결제 완료 후, 1-2일 이내 도착</p>
                <p>· 쿠팡맨 미배송 지역 : 주문 및 결제 완료 후, 2-3일 이내 도착</p>
                <p>- 도서 산간 지역 등은 하루가 더 소요될 수 있습니다. 곧 고객님께도 쿠팡맨이 찾아갈 수 있도록 노력하겠습니다</p>
                <p>· 천재지변, 물량 수급 변동 등 예외적인 사유 발생 시, 다소 지연될 수 있는 점 양해 부탁드립니다.</p></td>
        </tr>
    </tbody>
</table>'; // 모바일

$ship_sample8 = '<div class="prod-delivery-notice-container"> </div>
<div class="prod-shipping-fee-containe...-2">오늘(화)</em><em class="prod-txt-onyx  prod-txt-green-normal"> 도착 보장</em> <span class="loyalty-cut-off-timer-wrapper"> (<span class="loyalty-cut-off-message"><em class="prod-txt-onyx">오전 10시 전 주문 시</em></span>) </span> <span class="icon-txt">로켓와우</span><i class="rocket-falcon-icon question-mark-rocket-plus"></i>
    <div class="prod-pdd-delay"></div>
</div>
</div>
</div>
<div class="shipping-fee-list" style="display: none;"> </div>
<div class="loyalty-reminder-default-address" style="display:none;"> <img class="delivery-type-badge" src="" alt=""><em class="prod-gray-txt">가능 배송지로 변경 시 구매 가능</em> </div>';

$ship_sample9 = '\n    <div class=\"prod-delivery-notice-container\">\n    \n    </div>\n\n    <div class=\"prod-shipping-fee-contain...4/3 새벽 7시 전 </em><em class=\"prod-txt-onyx  prod-txt-green-normal\">도착 보장</em>\n              \n              \n                \n                  <span class=\"loyalty-cut-off-timer-wrapper\">\n                    (<span class=\"loyalty-cut-off-message\"><em class=\"prod-txt-onyx\">오후 7시 전 주문 시</em></span>)\n                  </span>\n                  \n                    <span class=\"icon-txt\">로켓와우</span><i class=\"rocket-falcon-icon question-mark-rocket-plus\"></i>\n                  \n                \n              \n              \n              \n              <div class=\"prod-pdd-delay\"></div>\n            </div>\n          \n        \n      </div>\n    </div>\n\n    <div class=\"shipping-fee-list\" style=\"display: none;\">\n      \n    </div>\n\n    <div class=\"loyalty-reminder-default-address\" style=\"display:none;\">\n      \n        <img class=\"delivery-type-badge\" src=\"\" alt=\"\"><em class=\"prod-gray-txt\">가능 배송지로 변경 시 구매 가능</em>\n      \n    </div>\n  ';

		//배송비파싱
		$arr_ship_cond = array();
		$ship_cond = '';
		$crawler_shipping = new Crawler($ship_sample9);	//파서에 HTML 주입
		
		//기본 배송비 파싱
		if($mobile)
		{			
			$arr_shipping_pointer = $crawler_shipping->filter('tr')->each(function (Crawler $node, $i) { //리턴 데이터는 배열로 넘어온다
				if(strpos($node->text(),'배송비') !== false) return 'Y'; else return 'N';				
			});
			
			$shipping_pointer = array_search('Y',$arr_shipping_pointer); //배송비라는 키워드가 있는 tr index
			
			//기본배송비				
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('tr')->eq($shipping_pointer)->filter('td p')->eq(0)->text());
			if($basic_price == '') $basic_price = 0; //문자열일 경우 무료 라고 판단한다.
			
			//조건분석
			if($crawler_shipping->filter('tr')->eq($shipping_pointer)->filter('td p')->eq(1)->count() > 0)
			{
				$full_ship_cond = explode('원 이상 구매 시',$crawler_shipping->filter('tr')->eq($shipping_pointer)->filter('td p')->eq(1)->text());
				$ship_cond = preg_replace("/[^0-9]*/s", "",$full_ship_cond[0]); //이상 구매시로 split
				$cond_price = preg_replace("/[^0-9]*/s", "",$full_ship_cond[1]);
			}
			else //조건나온 td 없다고 판단되면 공백
			{
				$ship_cond = '';
				$cond_price = '';
			}
			
			//조건에 부합하면 해당 조건의 가격으로
			if($buy_amount >= $ship_cond) 
			{
				$final_price = $cond_price;
			}
			else
			{
				$final_price = $basic_price;
			}			
		}		
		//em 태그가 있는경우 조건부 배송비로 판단
		else if($crawler_shipping->filter('.prod-shipping-fee-message em')->count() == 3) 
		{
			//조건부 로직
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message .prod-shipping-fee-divider')->text(''));
			$ship_cond = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message .prod-txt-black')->text(''));
			$cond_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message .prod-txt-bold')->text(''));
			if($cond_price == '') $cond_price = 0;
			
			if($buy_amount >= $ship_cond) //조건에 부합하면 해당 조건의 가격으로
			{
				$final_price = $cond_price;
			}
			else
			{
				$final_price = $basic_price;
			}
		}
		else if($crawler_shipping->filter('.prod-shipping-fee-message em')->count())
		{						
			if(strpos($crawler_shipping->filter('.prod-shipping-fee-message .prod-txt-bold')->text(''), '로켓배송') > -1)
			{
				$basic_price = 0;
				$final_price = 0;
			}
			else
			{				
				$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message .prod-txt-bold')->text(''));
				if($basic_price == '') $basic_price = 0; //문자열일 경우 무료 라고 판단한다.
				$final_price = $basic_price;
			}		
		}
		else 
		{
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message')->text(''));
			if($basic_price == '') $basic_price = 0; //문자열일 경우 무료 라고 판단한다.
			$final_price = $basic_price;
		}		
					
		if($ship_cond)
		{
			$arr_ship_cond[$ship_cond] = $cond_price;
		}
		
		$arr_shipping_condition = array("basic_price"=> $basic_price, "cond" => $arr_ship_cond);
		$json_shipping_condition = json_encode($arr_shipping_condition);
		
		
		echo "구매금액: $buy_amount // 가격: $final_price // 조건 : $ship_cond ";
		echo $json_shipping_condition;	
		
	}
		
}

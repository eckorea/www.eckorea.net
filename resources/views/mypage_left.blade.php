<aside class="sidebar col-lg-3">
	<div class="widget widget-dashboard">
		<h3 class="widget-title">MY ACCOUNT</h3>
		<ul class="list">
			<li><a href="/order/list">ORDERS</a></li>
			<li><a href="/my/coupon">COUPON</a></li>
			<li><a href="/my/message">MESSAGE</a></li>
			<li><a href="/my/address">SHIPPING ADDRESS</a></li>
			<li class="low"><a href="/my/password/change">CHANGE PASSWORD</a></li>
			<li class="low"><a href="/mall/exchange_rate">EXCHANGE RATE</a></li>
		</ul>
	</div>
<!-- End .widget --> 
</aside>
<!-- End .col-lg-3 --> 
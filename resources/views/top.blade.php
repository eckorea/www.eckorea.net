<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="ROBOTS" content="all" />
    <meta name="author" content="ECPLAZA Network Inc." />
    <meta name="copyright" content="Copyright &#169; ECPLAZA Network Inc. All Rights Reserved." />
    <meta name="reply-to" content="information@ecplaza.net" />

    <title>ECKorea: Korean Shopping Mall &amp; Dropship - eckorea.net</title>
    <meta property="og:title" content="ECKorea: The only Cross-border Shopping Platform for K-Wave Fans like YOU! - eckorea.net" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.eckorea.net" />
    <meta property="og:image" content="https://www.eckorea.net/image/logo.png" />
    <meta property="og:site_name" content="eckorea.net" />
    <meta property="og:description" content="The only Cross-border Shopping Platform for K-Wave Fans like YOU!. Import &amp; Export on eckorea.net" />


	<!--<script type="text/javascript">
		WebFontConfig = {
			google: { families: [ 'Open+Sans:300,400,600,700,800','Poppins:300,400,500,600,700,800' ] }
		};
		(function(d) {
			var wf = d.createElement('script'), s = d.scripts[0];
			wf.src = '/css/assets/js/webfont.js';
			wf.async = true;
			s.parentNode.insertBefore(wf, s);
		})(document);
	</script>-->

	
	<!-- Plugins CSS File -->
	<link rel="stylesheet" href="/css/assets/css/bootstrap.min.css">

    <!-- Main CSS File -->
    <link rel="stylesheet" href="/css/assets/css/style.min.css">
    <link rel="stylesheet" type="text/css" href="/css/assets/vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="/css/assets/css/custom.css">

    @if(Session::has('jsAlert'))
	<script type="text/javascript" >alert('{{ session('jsAlert') }}');</script>
    @endif


    {!! NoCaptcha::renderJs() !!}
</head>

<div class="page-wrapper">
	<div class="scroll" style="z-index:1000000000000000000">
		<img src="/image/team/scroll_00.jpg">
		<?php
		/*
		<a href="https://chrome.google.com/webstore/detail/ec-korea-shopping-cart/kmdkekllcigdinleflokeplcdbaoghjo?hl=ko" target="_blank"><img src="/image/team/scroll_01.jpg"></a>
		*/
		?>
		<?php
		/**
		 * esummer, 2021-05-13
		 * <a href="https://pf.kakao.com/_UxnFDK" target="_blank"><img src="/image/team/scroll_02.jpg"></a>
		 */
		?>

        <a href="https://chrome.google.com/webstore/detail/ec-korea-shopping-cart/kmdkekllcigdinleflokeplcdbaoghjo?hl=ko" target="_blank"><img src="/image/team/scroll_01.jpg"></a>
        <a href="http://pf.kakao.com/_UxnFDK/chat" target="_blank"><img src="/image/team/scroll_02.jpg"></a>
		<a href="/mall/guide/4"><img src="/image/team/scroll_03.jpg"></a>
		<a href="/order/list"><img src="/image/team/scroll_04.jpg"></a>
	</div>
	
<?php
	/*

    <div class="top-notice text-black bg-yellow">
		<div class="container text-center">
			<h5 class="d-inline-block mb-0 mr-1">All the best K-Products, JUST 1 CLICK</h5>
			<a href="https://chrome.google.com/webstore/detail/ec-korea-shopping-cart/kmdkekllcigdinleflokeplcdbaoghjo?hl=ko" class="category ml-2 mr-2" target="_blank">Add to Chrome</a>
		</div><!-- End .container -->
	</div>
	*/
?>
    <div class="top-notice text-black bg-yellow">
        <div class="container text-center">
            <h5 class="d-inline-block mb-0 mr-1">All the best K-Products, JUST 1 CLICK</h5>
            <a href="https://chrome.google.com/webstore/detail/ec-korea-shopping-cart/kmdkekllcigdinleflokeplcdbaoghjo?hl=ko" class="category ml-2 mr-2" target="_blank">Add to Chrome</a>
        </div><!-- End .container -->
    </div>
	<header class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-left d-none d-sm-block">
					<p class="top-message text-uppercase"></p>
				</div><!-- End .header-left -->

				<div class="header-right header-dropdowns ml-0 ml-sm-auto w-sm-100">
					<div class="header-dropdown dropdown-expanded mr-md-3 mr-lg-0">
						<div class="header-menu">
							<a href="/notice">@lang('common.cscenter')</a>&nbsp;&nbsp;
                            @if(!session('mb_email'))
                            <a href="/login">@lang('common.login')</a>&nbsp;&nbsp;                                
                            @else
                            <a href="/order/list">@lang('common.myaccount')</a>&nbsp;&nbsp;															
								@if(session('mb_level') >= 10)
                                <a href="/admin/member">@lang('common.admin')</a>&nbsp;&nbsp;
                                @endif
                                <a href="/logout">@lang('common.logout')</a>&nbsp;&nbsp;
                            @endif
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->

					<!--<span class="separator"></span>-->

					<div class="header-dropdown">
						<a href="#">USD</a>
						<div class="header-menu">
							<ul>
								<li><a href="#">USD</a></li>
							</ul>
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->

					<div class="header-dropdown ml-0 ml-md-4 mr-auto mr-sm-3 mr-md-0">
						<a href="#"><img src="/css/assets/images/flags/@if(session('locale') == '')en.png @else{{@session('locale')}}.png @endif" alt="{{@session('locale')}} flag">
                        @if(session('locale') == '' || session('locale') == 'en')
                        ENG
                        @elseif(session('locale') == 'ko')
                        KOR
                        @elseif(session('locale') == 'vn')
                        VNM
                        @endif
                        </a>
						<div class="header-menu">
							<ul>
								<li><a href="/lang/en"><img src="/css/assets/images/flags/en.png" alt="England flag">ENG</a></li>
								<li><a href="/lang/ko"><img src="/css/assets/images/flags/ko.png" alt="France flag">KOR</a></li>
								<li><a href="/lang/vn"><img src="/css/assets/images/flags/vn.png" alt="France flag">VNM</a></li>
							</ul>
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->

					<span class="separator"></span>
					
				</div><!-- End .header-right -->
			</div><!-- End .container -->
		</div>

		<div class="header-middle">
			<div class="container">
				<div class="header-left col-lg-2 w-auto pl-0">
					<button class="mobile-menu-toggler text-primary mr-2" type="button">
						<i class="icon-menu"></i>
					</button>
					<a href="/" class="logo">
						<img src="/image/logo.png" alt="logo">
					</a>
				</div><!-- End .header-left -->

				<div class="header-right w-lg-max">
					<div class="header-icon header-icon header-search header-search-inline header-search-category w-lg-max ml-3 pl-1 pr-xl-5 mr-xl-4">
						<a href="#" class="search-toggle" role="button"><i class="icon-search-3"></i></a>
						<!--<form action="/mall/search" method="get">-->
                        <script>
						function top_search_enter() {
						if (window.event.keyCode == 13) {
				 			top_search();						
						}
						}
						//$("#q").keyup(function(e){if(e.keyCode == 13)  top_search(); });
						function top_search()
						{
							var keyword = $("#q").val();							
							var cat = $("#cat").val();
							if(keyword == '') alert('검색어를 입력해주세요'); 
							else location.href = '/mall/search/'+cat+'/'+keyword;
						}
						</script>
							<div class="header-search-wrapper">
								<input type="search" class="form-control" name="q" id="q" placeholder="@lang('common.search')..." value="@if(isset($keyword)){{trim($keyword)}}@endif" onKeyUp="top_search_enter();" @if(strpos($_SERVER['HTTP_HOST'], 'dev')) style='background-color: yellow' @endif >
								<div class="select-custom">
									<select id="cat" name="cat">
										<!--<option value="">All Categories</option>-->
										<option value="all" @if(isset($category) && $category == 'all') selected @endif>ALL</option>
										<?php
										/*
										<!--<option value="SS" @if(isset($category) && $category == 'SS') selected @endif>NAVER</option>-->
										*/
										?>
                                        <option value="SS" @if(isset($category) && $category == 'SS') selected @endif>NAVER</option>
                                        <option value="kyobo" @if(isset($category) && $category == 'kyobo') selected @endif>KYOBO BOOK</option>
										<option value="hottracks" @if(isset($category) && $category == 'hottracks') selected @endif>K-GOODS</option>								
										
										<option value="10" @if(isset($category) &&$category == '10') selected @endif>K-POPS</option>
										<option value="03" @if(isset($category) &&$category == '03') selected @endif>K-FASHION</option>
										<option value="04" @if(isset($category) && $category == '04') selected @endif>K-BEAUTY</option>
										<option value="05" @if(isset($category) && $category == '05') selected @endif>K-FOOD</option>
										<option value="06" @if(isset($category) && $category == '06') selected @endif>K-LIVING</option>
									</select>
								</div><!-- End .select-custom -->
								<button class="btn icon-search-3 p-0" type="button" onClick="top_search();"></button>
							</div><!-- End .header-search-wrapper -->
						<!--</form>-->
					</div><!-- End .header-search -->
					
                    @if(session('mb_email'))
                    <a href="/order/list" class="header-icon pb-md-1  pl-1"><i class="icon-user" style="color:orange;"></i></a>
                    @else
                    <a href="/order/list" class="header-icon pb-md-1  pl-1"><i class="icon-user-2"></i></a>
                    @endif
					

					<div class="dropdown cart-dropdown">
						<a href="" onClick="location.href='/order/cart';" class="dropdown-toggle " role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
							<i class="icon-shopping-cart"></i>
						</a>							
					</div><!-- End .dropdown -->
				</div><!-- End .header-right -->
			</div><!-- End .container -->
		</div>

		<div class="header-bottom sticky-header d-none d-lg-block" data-sticky-options="{
			'move': [
				{
					'item': '.header-search',
					'position': 'end',
					'clone': false
				},
				{
					'item': '.header-icon:not(.header-search)',
					'position': 'end',
					'clone': false
				},
				{
					'item': '.cart-dropdown',
					'position': 'end',
					'clone': false
				}
			],
			'moveTo': '.container',
			'changes': [
				{
					'item': '.logo-light',
					'removeClass': 'd-none'
				},
				{
					'item': '.header-icon:not(.header-search)',
					'removeClass': 'pb-md-1'
				},
				{
					'item': '.header-search',
					'removeClass': 'header-search-inline w-lg-max ml-3 pl-1 pr-xl-5 mr-xl-4',
					'addClass': 'header-search-popup ml-auto'
				},
				{
					'item': '.main-nav li.float-right',
					'addClass': 'd-none'
				}
			]
		}">
			<div class="container">
				<a href="/" class="logo logo-light mr-3 pr-xl-3 d-none">
					<img src="/image/logo.png" alt="Logo">
				</a>
				<nav class="main-nav w-lg-max bg-primary">
					<ul class="menu">
                    <!--<li class="active">
                        <a href="/">Home</a>
                    </li>-->
                    
                    <!--교보전용 카테고리-->
                    <li>
						<a href="/mall/category/kyobo/01">KYOBO BOOK</a>                        
                        @foreach(DB::table('categories_kyobo')
                        ->where([                        
                        ['ca_level','=',1]
                        ])->get() as $ca2_row)
                        @if($loop->first)<ul class="kyobomenu">@endif
                        	<li>
                            <a href="/mall/category/kyobo/{{$ca2_row->ca_id}}">{{$ca2_row->ca_name}}</a>                            
                          <!--  @foreach(DB::table('categories_kyobo')
                            ->where([                        
                            ['ca_parent','=',$ca2_row->ca_id]
                            ])->get() as $ca3_row)
                            @if($loop->first)<ul>@endif
                                <li>
                                <a href="/mall/category/kyobo/{{$ca3_row->ca_id}}">{{$ca3_row->ca_name}}</a>                                                           
                                </li>
                            @if($loop->last)</ul>@endif
                            @endforeach  -->  
                            </li>
                            @if($loop->last)</ul>@endif                          
                        @endforeach                        							
					</li>                 
                   
                    <!--일반카테고리-->
                    @foreach(DB::table('categories')
                    ->where([
                    ['ca_level','=',1],
                    ['ca_id','!=','01'],
                    ['ca_id','!=','07'],
                    ['ca_id','!=','08']                   
                    ])->whereNull('deleted_at')->orderBy('ca_order','asc')->get() as $ca1_row)
                    <li>
						<a href="/mall/category/{{$ca1_row->ca_id}}">{{$ca1_row->ca_name}}</a>                        
                        @foreach(DB::table('categories')
                        ->where([                        
                        ['ca_parent','=',$ca1_row->ca_id]
                        ])->whereNull('deleted_at')->get() as $ca2_row)
                        @if($loop->first)<ul>@endif
                        	<li>
                            <a href="/mall/category/{{$ca2_row->ca_id}}">{{$ca2_row->ca_name}}</a>                            
                            @foreach(DB::table('categories')
                            ->where([                        
                            ['ca_parent','=',$ca2_row->ca_id]
                            ])->whereNull('deleted_at')->get() as $ca3_row)
                            @if($loop->first)<ul>@endif
                                <li><a href="/mall/category/{{$ca3_row->ca_id}}">{{$ca3_row->ca_name}}</a></li>
                            @if($loop->last)</ul>@endif
                            @endforeach                            
                            </li>
                            @if($loop->last)</ul>@endif                          
                        @endforeach                        							
					</li>                                        
                    @endforeach
                    	<!--					
						<li>
							<a href="/mall/category/10">KYOBO BOOK</a>								
						</li>
						<li>
							<a href="/mall/category/10">K-GOODS</a>								
						</li>
						<li>
							<a href="/mall/category/10">K-FASHION</a>
							<ul>
								<li><a href="/mall/category/10">SUBMENU</a></li>
								<li><a href="/mall/category/10">SUBMENU</a>
									<ul>
										<li><a href="#">SUBMENU</a></li>
									</ul>
								</li>									
							</ul>
						</li>
						<li>
							<a href="/mall/category/10">K-BEAUTY</a>								
						</li>
						<li>
							<a href="/mall/category/10">K-FOOD</a>								
						</li>
						<li>
							<a href="/mall/category/10">K-LIVING</a>								
						</li>
                        -->						
						
						
						<li class="float-right m-0"><a class="px-4" href="/mall/about">ABOUT US</a></li>
						<li class="float-right m-0 bg-primary"><a class="px-4" href="/mall/kculture">K-CULTURE</a></li>
						<li class="float-right m-0 bg-primary">
							<a class="px-4" href="#">K-COMMUNITY</a>
							<ul>
								<li><a href="#">FORUM</a></li>
								<li><a href="/mall/review">REVIEW</a></li>															
							</ul>
						</li>
					</ul>
				</nav>
			</div><!-- End .container -->
		</div><!-- End .header-bottom -->
	</header><!-- End .header -->
</div>


<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

<div class="mobile-menu-container">
	<div class="mobile-menu-wrapper">
		<span class="mobile-menu-close"><i class="icon-cancel"></i></span>
		<nav class="mobile-nav">
			<ul class="mobile-menu">
				<li class="active"><a href="/">Home</a></li>
				<!--교보전용 카테고리-->
                    <li>
						<a href="/mall/category/kyobo/01">KYOBO BOOK</a>                        
                        @foreach(DB::table('categories_kyobo')
                        ->where([                        
                        ['ca_level','=',1]
                        ])->get() as $ca2_row)
                        @if($loop->first)<ul>@endif
                        	<li>
                            <a href="/mall/category/kyobo/{{$ca2_row->ca_id}}">{{$ca2_row->ca_name}}</a>                            
                            @foreach(DB::table('categories_kyobo')
                            ->where([                        
                            ['ca_parent','=',$ca2_row->ca_id]
                            ])->get() as $ca3_row)
                            @if($loop->first)<ul>@endif
                                <li>
                                <a href="/mall/category/kyobo/{{$ca3_row->ca_id}}">{{$ca3_row->ca_name}}</a>                                                           
                                </li>
                            @if($loop->last)</ul>@endif
                            @endforeach    
                            </li>
                            @if($loop->last)</ul>@endif                          
                        @endforeach                        							
					</li>                 
                   
                    <!--일반카테고리-->
                    @foreach(DB::table('categories')
                    ->where([
                    ['ca_level','=',1],
                    ['ca_id','!=','01'],
                    ['ca_id','!=','07'],
                    ['ca_id','!=','08']
                    ])->get() as $ca1_row)
                    <li>
						<a href="/mall/category/{{$ca1_row->ca_id}}">{{$ca1_row->ca_name}}</a>                        
                        @foreach(DB::table('categories')
                        ->where([                        
                        ['ca_parent','=',$ca1_row->ca_id]
                        ])->get() as $ca2_row)
                        @if($loop->first)<ul>@endif
                        	<li>
                            <a href="/mall/category/{{$ca2_row->ca_id}}">{{$ca2_row->ca_name}}</a>                            
                            @foreach(DB::table('categories')
                            ->where([                        
                            ['ca_parent','=',$ca2_row->ca_id]
                            ])->get() as $ca3_row)
                            @if($loop->first)<ul>@endif
                                <li><a href="/mall/category/{{$ca3_row->ca_id}}">{{$ca3_row->ca_name}}</a></li>
                            @if($loop->last)</ul>@endif
                            @endforeach                            
                            </li>
                            @if($loop->last)</ul>@endif                          
                        @endforeach                        							
					</li>                                        
                    @endforeach
                    	<!--					
						<li>
							<a href="/mall/category/10">KYOBO BOOK</a>								
						</li>
						<li>
							<a href="/mall/category/10">K-GOODS</a>								
						</li>
						<li>
							<a href="/mall/category/10">K-FASHION</a>
							<ul>
								<li><a href="/mall/category/10">SUBMENU</a></li>
								<li><a href="/mall/category/10">SUBMENU</a>
									<ul>
										<li><a href="#">SUBMENU</a></li>
									</ul>
								</li>									
							</ul>
						</li>
						<li>
							<a href="/mall/category/10">K-BEAUTY</a>								
						</li>
						<li>
							<a href="/mall/category/10">K-FOOD</a>								
						</li>
						<li>
							<a href="/mall/category/10">K-LIVING</a>								
						</li>
                        -->						
						
						
						<li><a href="/mall/about">ABOUT US</a></li>
						<li><a href="/mall/kculture">K-CULTURE</a></li>
						<li><a href="#">K-COMMUNITY</a></li>
				
			</ul>
		</nav><!-- End .mobile-nav -->
		
	</div><!-- End .mobile-menu-wrapper -->
</div><!-- End .mobile-menu-container -->
<?php
namespace App\Http\Controllers;

use App\Item;
use App\Board;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class MallController extends Controller
{
    //Contents Controller에서도 쓰고 있음, 변경시 같이 변경해줘야함
	public $kc_boards = array(
	'K-TREND'=>'1',
	'K-BEAUTY'=>'2',
	'K-COOK'=>'3',
	'Simple Tests & Quizzes'=>'41',
	'Ordinary conversations'=>'41',
	'Lectures'=>'41',
	'TOPIK'=>'41',
	'Habits'=>'42',
	'Superstitions'=>'42',
	'Traditions'=>'42',
	'Customs'=>'42',
	'Culture & History'=>'42',
	'Landmarks'=>'43',
	'Shopping'=>'43',
	'Foods'=>'43',
	'Scenery'=>'43',
	'Activities'=>'43'
	);
	
	public $kc_group = array(
	'41'=>'Learning Korean Language',
	'42'=>'Korean Culture',
	'43'=>'Travel Korea'
	);
	 
    public function index(Item $item)
    {
		//배너로드
		$banner_data = DB::table('banners')->orderBy('banner_order','asc')->get();
		
		//상품로드		
		$arr_reco_key = array('hotdeal','featured','bestseller','new','flex','must');
		$arr_result = array();
		foreach($arr_reco_key as $key)
		{
			$arr_result[$key] = $item
			->join('recommend_items','recommend_items.it_id', '=', 'items.it_id')
			->where([
			['items.it_use', '=', 'Y'],
			['recommend_items.reco_type', '=', $key],
			['it_thumbnail_url', '!=', '']
			])
			->get();
		}	
		
		return view('index')->with(['result'=>$arr_result,'banner_data'=>$banner_data]);
    }

    public function indexWithPartner(Request $request, Item $item)
    {
        //배너로드
        $banner_data = DB::table('banners')->orderBy('banner_order','asc')->get();

        //상품로드
        $arr_reco_key = array('featured','hotdeal','bestseller','new','flex','must');
        $arr_result = array();
        foreach($arr_reco_key as $key)
        {
            $arr_result[$key] = $item
                ->join('recommend_items','recommend_items.it_id', '=', 'items.it_id')
                ->where([
                    ['items.it_use', '=', 'Y'],
                    ['recommend_items.reco_type', '=', $key],
                    ['it_thumbnail_url', '!=', '']
                ])
                ->get();
        }

        /**
         * echo "<script> alert('". isset($_GET['PID'])."') ; </script>";
         */

        /**
         * TODO : esummer, 2021-05-06, 사용자 접속시 파트너 시스템으로 결제정보 넘김
         */
        if(isset($_GET['PID'])) {
            $_PID = $request->input('PID');
            Cookie::queue(Cookie::forget('cpsKEY'));
            Cookie::queue('cpsKEY', $_PID, 60*24*3);

            $_OPTION = [];
            $res = sendMarketingData($request, 'C', $_PID, $_OPTION);
        }

        return view('index')->with(['result'=>$arr_result,'banner_data'=>$banner_data]);
    }

    public function index_faq()
    {
		$data = DB::table('faqs')->get();		
		return view('faq')->with(['data'=>$data]);
    }
	
	public function view_faq($faq_id)
    {		
		$lang = app()->getLocale();
					
		//row data
		$data = DB::table('faqs')->where('faq_id',$faq_id)->get();
		
		$title = '';
		$contents = '';
		
		if(isset($data[0]))
		{		
			$title = $data[0]->{"faq_title_".$lang};
			$contents = $data[0]->{"faq_contents_".$lang};
		}		
				
		return view('faq_view')->with(['data'=>$data, 'title'=>$title, 'contents' => $contents]);
    }
	
	public function index_kculture(Request $request, $kc_board)
    {
		$data = DB::table('kcultures')
		->where([
		['kc_board','=',$kc_board]
		])
		->paginate(12)
		->appends($request->query());	
								
		return view('kculture_list')->with(['data'=>$data]);
    }
	
	public function section_kculture()
	{
		return view('kculture');
	}
	
	public function study_kculture()
	{
		return view('kculture_study')->with(['kc_boards'=>$this->kc_boards, 'kc_group'=>$this->kc_group]);
	}
	
	public function view_kculture($kc_id)
    {
		$data = DB::table('kcultures')
		->where([
		['kc_id','=',$kc_id]
		])
		->get();		
		return view('kculture_view')->with(['data'=>$data]);
    }
	
	public function view_board(Board $board, $wr_id)
    {
		$data = $board
		->where([
		['wr_id','=',$wr_id]
		])
		->get();		
		return view('board_view')->with(['data'=>$data]);
    }
	
	public function indextest(Item $item)
    {
		$must_item = $item->where([
		['it_use', '=', 'Y'],
		['it_type', '=', 'out']
		])->limit(6)->get();		
		
		return view('index')->with(['must_item'=>$must_item, 'result'=>array()]);
    }
	
	public function search($category, $keyword)
	{
		return view('search')->with(['category'=>$category, 'keyword'=>$keyword]);		
	}
	
	public function search_data(Request $request, Item $item, $category, $keyword, $page)
	{	
		//AJAX
		if($page == '') $page = 1;	
		
		//스마트 스토어 전용 API
		if($category == 'SS')
		{
			$result = getSSsearchData(urlencode($keyword),$page);		
		}
		else
		{
			$items = array();
			
			//카테고리별 데이터 로드
			if($category == 'all')
			{
				$data = $item->where([
				['it_name', 'LIKE', $keyword.'%']			
				])
				->orderBy('ca_id','asc')
				->paginate(40,['*'],'page',$page);
			}
			else if($category == 'kyobo' || $category == 'hottracks')
			{
				$data = $item->where([
				['it_origin', '=', $category],
				['it_name', 'LIKE', $keyword.'%']			
				])->paginate(40,['*'],'page',$page);
			}
			else
			{
				$data = $item->where([
				['ca_id', 'LIKE', $category.'%'],
				['it_name', 'LIKE', $keyword.'%']			
				])->paginate(40,['*'],'page',$page);
			}
			
			//Json result 생성
			foreach($data as $row)
			{
				$arr_img = json_decode($row->it_img_json);				
				if(isset($arr_img)) $mainImgUrl = $arr_img[0]; else $mainImgUrl = '';				
				$items[] = array('url'=>'/mall/item/'.$row->it_id, 'price'=>$row->it_price, 'mainImgUrl'=>$mainImgUrl, 'name'=>$row->it_name);				
			}
			
			$full_item_data['rcode'] = 1;			
			$full_item_data['entity'] = $items;			
			$result = json_encode($full_item_data);
		}
			
		return response($result,200);
	}
	
	public function category_item(Item $item, Request $request, $ca_id)
	{
		//카테고리 들어가면 최상위 ca_id 찾은뒤 거기서부터 차례대로 훑어내려온다.		
			
		//최상위 ca_id 찾기, 위로 타고 올라감
		$top_ca_id = $ca_id;
		$top_ca_name = '';
		$inner_cnt = 0;
		do
		{
			$category_data = DB::table('categories')
			->where([
			['ca_id','=', $top_ca_id]
			])->get();
			
			$ca_parent = $category_data[0]->ca_parent;
			if($ca_parent) 
			{				
				$top_ca_id = $category_data[0]->ca_parent;				
			}
			else break;
			
			$inner_cnt++;
		}
		while($inner_cnt < 10); //예기치 못한 무한 루프 방지
				
		//최상위 카테고리 이름
		$category_data2 = DB::table('categories')
		->where([
		['ca_id','=', $top_ca_id]
		])
		->limit(1)
		->get();		
		$top_ca_name = $category_data2[0]->ca_name;
		
		//본 카테고리 이름
		$category_data3 = DB::table('categories')
		->where([
		['ca_id','=', $ca_id]
		])
		->limit(1)
		->get();		
		$ca_name = $category_data3[0]->ca_name;
				
		//카테고리 Tree
		$arr_ca_tree = array();	
		$ca_id_level = strlen($ca_id) / 2;
		
		for($ii= 1; $ii <= $ca_id_level; $ii++)
		{
			$temp_ca_id = substr($ca_id, 0, 2*$ii);
			$ca_data = DB::table('categories')->where('ca_id',$temp_ca_id)->limit(1)->get();			
			array_push($arr_ca_tree,array('ca_id'=>$temp_ca_id, 'ca_name'=>$ca_data[0]->ca_name));
		}
		
		//아이템 데이터 로드		
		$data = $item
		//->select('it_id','it_name','it_price','it_img_json','it_url')
		->where([
		['ca_id', 'LIKE', $ca_id.'%'],
		['it_use', '=', 'Y'],
		['it_thumbnail_url', '!=', '']
		]);
		
		//정렬
		$orderby = $request->query('orderby');
		if($orderby)
		{			
			if($orderby == 'popularity') $data = $data->orderBy('review_points', 'desc');
			else if($orderby == 'price') $data = $data->orderBy('it_price', 'asc');
			else if($orderby == 'price-desc') $data = $data->orderBy('it_price', 'desc');		
		}
		else
		{
			$data = $data->orderBy('review_points', 'desc');
		}
		
		//페이징		
		$data = $data->paginate(20)->appends($request->query());		
		
		return view('category')->with(['data'=>$data, 'category_table'=>'categories', 'ca_id'=>$ca_id, 'ca_name'=> $ca_name, 'top_ca_id'=>$top_ca_id, 'top_ca_name' => $top_ca_name, 'add_param'=>'', 'ca_tree'=>$arr_ca_tree]);		
	}
	
	public function category_kyobo_item(Item $item, Request $request, $ca_id)
	{
		//최상위 카테고리 정보
		$top_ca_id = substr($ca_id,0,2);		
		$category_data = DB::table('categories_kyobo')
		->where([
		['ca_id','=', $top_ca_id]
		])
		->limit(1)
		->get();
		$top_ca_name = $category_data[0]->ca_name;	
		
		//본 카테고리 정보
		$category_data2 = DB::table('categories_kyobo')
		->where([
		['ca_id','=', $ca_id]
		])
		->limit(1)
		->get();
		$ca_name = $category_data2[0]->ca_name;		
		
		//카테고리 Tree
		$arr_ca_tree = array();	
		$ca_id_level = strlen($ca_id) / 2;
		
		for($ii= 1; $ii <= $ca_id_level; $ii++)
		{
			$temp_ca_id = substr($ca_id, 0, 2*$ii);
			$ca_data = DB::table('categories_kyobo')->where('ca_id',$temp_ca_id)->limit(1)->get();			
			array_push($arr_ca_tree,array('ca_id'=>$temp_ca_id, 'ca_name'=>$ca_data[0]->ca_name));
		}
							
		//아이템 데이터 로드		
		$data = $item		
		->where([
		['ca_id_extra', 'LIKE', $ca_id.'%'],
		['it_origin', '=', 'kyobo'],
		['it_use', '=', 'Y'],
		['it_thumbnail_url', '!=', '']

		]);
		
		//정렬
		$orderby = $request->query('orderby');
		if($orderby)
		{			
			if($orderby == 'popularity') $data = $data->orderBy('review_points', 'desc');
			else if($orderby == 'price') $data = $data->orderBy('it_price', 'asc');
			else if($orderby == 'price-desc') $data = $data->orderBy('it_price', 'desc');		
		}
		else
		{
			$data = $data->orderBy('review_points', 'desc');
		}
		
		//페이징		
		$data = $data->paginate(20)->appends($request->query());	
					
		return view('category')->with(['data'=>$data, 'category_table'=>'categories_kyobo', 'ca_id'=>$ca_id, 'ca_name'=> $ca_name, 'top_ca_id'=>$top_ca_id, 'top_ca_name' => $top_ca_name, 'add_param'=>'kyobo/', 'ca_tree'=>$arr_ca_tree]);		
	}
	
	public function view_item(Item $item, $it_id)
	{
		$data = $item
		->where('it_id', $it_id)
		->get();
		
		//카테고리 Tree
		$arr_ca_tree = array();
		$ca_id = $data[0]->ca_id;		
		$ca_id_level = strlen($ca_id) / 2;
		
		for($ii= 1; $ii <= $ca_id_level; $ii++)
		{
			$temp_ca_id = substr($ca_id, 0, 2*$ii);
			$ca_data = DB::table('categories')->where('ca_id',$temp_ca_id)->limit(1)->get();			
			array_push($arr_ca_tree,array('ca_id'=>$temp_ca_id, 'ca_name'=>$ca_data[0]->ca_name));
		}
				
		$review_data = DB::table('reviews')->where('it_id',$it_id)->paginate(8);
		$qna_data = DB::table('qnas')->where('it_id',$it_id)->paginate(8);		
		
		return view('mall_view')->with(['data'=>$data, 'review_data'=>$review_data, 'qna_data'=>$qna_data, 'it_id'=>$it_id, 'ca_tree'=>$arr_ca_tree]);		
	}
	
	public function coupon_list()
	{		
		$data = DB::table('coupons')
		->where('mb_email',session('mb_email'))
		->get();
		
		return view('coupon')->with(['data'=>$data]);				
	}
	
	public function message_list()
	{		
		$data = DB::table('messages')
		->where('ms_receiver',session('mb_email'))
		->get();
		
		return view('message')->with(['data'=>$data]);				
	}
	
	public function message_view($id)
	{		
		$data = DB::table('messages')
		->where([
		['ms_receiver','=',session('mb_email')],
		['id','=',$id]
		]);
				
		//읽은날짜 UPDATE
		DB::table('messages')
		->where([
		['ms_receiver','=',session('mb_email')],
		['id','=',$id],
		['ms_confirm_date', '=' ,NULL]
		])
		->update(
		['ms_confirm_date'=> Carbon::now()]
		);
		
		return view('message_view')->with(['data'=>$data->get()]);				
	}
	
	
	public function address_add(Request $request)
	{
		$addr_id = $request->input('addr_id');
		
		if($addr_id)
		{
			DB::table('user_addrs')
			->where([
			['mb_email','=',session('mb_email')],
			['id','=',$addr_id]			
			])
			->update([
			'addr_first_name'=>$request->input('first_name'),
			'addr_last_name'=>$request->input('last_name'),
			'addr_city'=>$request->input('city'),
			'addr_state'=>$request->input('state'),
			'addr_1'=>$request->input('addr1'),
			'addr_2'=>$request->input('addr2'),
			'addr_zip'=>$request->input('zipcode'),
			'addr_country'=>$request->input('country'),
			'addr_phone'=>$request->input('phone')
			]);
		}
		else
		{					
			DB::table('user_addrs')
			->insert([
			'mb_email'=>session('mb_email'),
			'addr_first_name'=>$request->input('first_name'),
			'addr_last_name'=>$request->input('last_name'),
			'addr_city'=>$request->input('city'),
			'addr_state'=>$request->input('state'),
			'addr_1'=>$request->input('addr1'),
			'addr_2'=>$request->input('addr2'),
			'addr_zip'=>$request->input('zipcode'),
			'addr_country'=>$request->input('country'),
			'addr_phone'=>$request->input('phone'),
			'created_at'=>Carbon::now()
			]);
		}
		
		return redirect('/my/address');
	}
	
	public function address_delete($id)
	{		
		DB::table('user_addrs')
		->where([
		['mb_email','=',session('mb_email')],
		['id','=',$id]			
		])
		->delete();
		
		return redirect('/my/address');
	}
	
	public function address_list()
	{		
		$data = DB::table('user_addrs')
		->where('mb_email',session('mb_email'))
		->get();
		
		return view('shippingaddr')->with(['data'=>$data]);				
	}
	
	public function address_view($id)
	{		
		$data = DB::table('user_addrs')
		->where([
		['mb_email','=',session('mb_email')],
		['id','=',$id]
		]);		
		
		return view('shippingaddr_add')->with(['data'=>$data->get()]);				
	}
	
	public function address_default($id)
	{
		//기본주소 전체 해제
		DB::table('user_addrs')
		->where([
		['mb_email','=',session('mb_email')]
		])->update(['addr_default'=>'N']);		
		
		//기본 주소설정		
		DB::table('user_addrs')
		->where([
		['mb_email','=',session('mb_email')],
		['id','=',$id]
		])->update(['addr_default'=>'Y']);		
		
		return redirect('/my/address');
	}
	
	public function password_change(Request $request)
	{
		$old_password = Hash::make($request->input('old_password'));
		$new_password = Hash::make($request->input('new_password'));
		//$hashed_pass = Hash::make($request->input('password'));
				
		$data = DB::table('members')
		->where([
		['mb_email','=',session('mb_email')]
		])->update([
		'mb_password' => $new_password
		]);		
		;		
		
		return redirect('/login');
	}
	public function index_review(Item $item, Request $request)
	{
		$data = $item->join('reviews','reviews.it_id', '=', 'items.it_id')
		->orderBy('reviews.created_at', 'desc')
		->paginate(20);					
		return view('review')->with(['data'=>$data]);
	}
	
	public function post_review(Request $request)
	{
		$it_id = $request->input('it_id');
		$review_photo_path = '';
		$review_photo_path = $request->hasFile('file_photo') ? $request->file_photo->store('review','public') : '';				
		$video_url = $request->input('video_url');
		$review_text = $request->input('review_text');
		$rate = $request->input('rate');
		
		DB::table('reviews')
		->insert([
		'it_id' => $it_id,
		'review_title' => '',
		'review_contents' => $review_text,
		'review_rate' => $rate,
		'mb_email' => session('mb_email'),
		'review_photo' => $review_photo_path,
		'review_video_url' => $video_url,
		'created_at' => now()
		]);
		
		//성공시에 해당 아이템에 point , count update
		/*DB::table('items')->where('it_id',$it_id)
		->update(DB::raw('review_points = review_points+3, review_counts = review_counts+1'));*/
		
		$affected = DB::update("UPDATE items SET review_points = review_points+$rate, review_counts = review_counts+1 where it_id = ?", [$it_id]);
		
		return redirect()->back();
	}
	
	public function delete_review(Request $request, $review_id)
	{
		$cursor = DB::table('reviews')
		->where([
		['review_id', '=', $review_id]		
		]);
		
		//관리자급 아닌경우에는 본인이 쓴 글만 지울수 있게
		if(session('mb_level') < 10)
		{
			$cursor= $cursor->where([
			['mb_email', '=', session('mb_email')]
			]);
		}		
		
		$data = $cursor->get();
		$it_id = $data[0]->it_id;
		$review_rate = $data[0]->review_rate;
		$review_photo = $data[0]->review_photo;
				
		$result = $cursor->delete();		
		
		if($result)
		{
			$file_result = Storage::disk('public')->delete($review_photo);	
			$affected = DB::update("UPDATE items SET review_points = review_points-$review_rate, review_counts = review_counts-1 where it_id = ?", [$it_id]);
		}
				
		return redirect()->back();		
	}
	
	public function post_qna(Request $request)
	{
		$it_id = $request->input('it_id');
		$qna_text = $request->input('qna_text');
		
		DB::table('qnas')
		->insert([
		'it_id' => $it_id,
		'qna_title' => '',
		'qna_contents' => $qna_text,
		'mb_email' => session('mb_email'),
		'created_at' => now()
		]);
			
		return redirect()->back();
	}
	
	public function delete_qna(Request $request, $qna_id)
	{
		$cursor = DB::table('qnas')
		->where([
		['qna_id', '=', $qna_id],
		['mb_email', '=', session('mb_email')]
		]);				
		$result = $cursor->delete();		
					
		return redirect()->back();		
	}
	
	public function view_guide(Request $request, $guide_id)
	{
		$lang = app()->getLocale();
		
		//title datas
		$title_data = DB::table('guides')->whereNull('deleted_at')->get();
		
		//row data
		$data = DB::table('guides')->whereNull('deleted_at')->where('guide_id',$guide_id)->get();
		
		$title = '';
		$contents = '';
		
		if(isset($data[0]))
		{		
			$title = $data[0]->{"guide_title_".$lang};
			$contents = $data[0]->{"guide_contents_".$lang};
		}		
				
		return view('/guide')->with(['data'=>$data, 'title_data'=>$title_data, 'title'=>$title, 'contents' => $contents]);	
	}

    public function view_policy(Request $request)
	{
		$lang = app()->getLocale();
					
		$data = DB::table('polices')->orderBy('created_at','desc')->limit(1)->get();		
		$contents = '';
		
		if(isset($data[0]))
		{		
			$contents = $data[0]->{"policy_".$lang};
		}
				
		return view('/policy')->with(['data'=>$data, 'contents' => $contents]);	
	}
	
	public function view_terms(Request $request)
	{
		$lang = app()->getLocale();
					
		$data = DB::table('terms')->orderBy('created_at','desc')->limit(1)->get();		
		$contents = '';
		
		if(isset($data[0]))
		{		
			$contents = $data[0]->{"terms_".$lang};
		}
				
		return view('/terms')->with(['data'=>$data, 'contents' => $contents]);	
	}
	
	public function img_view(Request $request)
	{
		$url = $request->url;
		return view('/img_view')->with(['url'=>$url]);	
	}
}

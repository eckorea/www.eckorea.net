@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MY ACCOUNT</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>ORDERS</h2>

						<div class="row">
							<div class="col-md-12">	
								<div class="table-responsive basic">

									<div class="form-group">
										<div class="toolbox" style="width:50%">		
                                        <form method="get">					
                                        <!--<input type="hidden" name="page" value="">-->
											<select name="status" class="form-control" onChange="submit();">
												<option value="" @if(Request::query('status') == '') selected @endif >@lang('common.all')</option>
                                                @foreach(getAllStatus() as $each_status)
                                                <option value="{{$each_status}}" @if(Request::query('status') == $each_status) selected @endif >{{convStatusToText($each_status)}}</option>
                                                @endforeach
                                            </select>
                                        </form>
										</div><!-- End .select-custom -->
									</div><!-- End .form-group -->

									<table class="align-middle mb-0 table table-striped table-hover">
										<thead>
											<tr>
												<th class="text-center">Item detail</th>
												<th class="text-center">Shipping</th>
												<th class="text-center">Status</th>
											</tr>
										</thead>
										<tbody> 
                                        @foreach($data as $row)
											<tr>
												<th colspan="3">[{{$row->od_id}}] </th>
											</tr>
											<tr>												
												<td class="center_name2">
													<div>
															 @if($row->it_thumbnail_url) 
                                                <img src="{{$row->it_thumbnail_url}}" alt="product">                                                
                                                @else 
                                                <img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg" alt="product">                                                @endif   
														<div class="center_name_right">
															[{{convOriginToText($row->ct_origin)}}]<br>
															<!--<a href="@if($row->it_url) {{$row->it_url}} @else /mall/item/{{$row->it_id}} @endif">{{$row->it_name}}</a> <br>-->
                                                            <p>{{$row->it_name}}</p>
															{{substr($row->it_option,0,50)}} <br>
															<span>KRW {{number_format($row->ct_price)}} / {{$row->ct_qty}}개</span><br>
                                                            <!--{{convStatusToText($row->ct_house_status)}}  2021.05.03 숨김요청-->
                                                            
														</div>
														<div class="clear"></div>
													</div>
													
												</td>
												<td>
													<b>{{$row->od_country}}</b> <br>
													{{$row->od_hope_carrier}} <br>
													{{$row->od_weight_due}}kg /	$ {{$row->od_send_cost2}}
                                                    
												</td>
												<td>
                                                @if($row->ct_house_status == 'cancel')
                                                {{convStatusToText('cancel')}}
                                                @elseif($row->od_status != 'sended' && $row->ct_ship_no != '')
                                                {{convStatusToText('sending')}}
                                                @else
                                                <b>{{convStatusToText($row->od_status)}}</b>
                                                @endif
                                                <br>
													<b>{{$row->ct_carrier}} {{$row->ct_ship_no}}</b> <br>
													<a href="/order/view/{{$row->od_id}}">[VIEW]</a>
												</td>
											</tr> 
                                        @endforeach 						
										</tbody>
									</table>		
                                   								
								</div>  
                               
							</div>
							<!-- End .col-md-6 --> 
						</div>	
                         {{ $data->links() }} 
					</div>

					<!-- End .col-lg-9 -->

					@include('mypage_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

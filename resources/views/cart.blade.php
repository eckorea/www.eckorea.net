@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav mb-1">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">CART</li>
					</ol>
				</div><!-- End .container -->
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="cart-table-container">							

							<table class="table table-cart">
								<thead>
									<tr>
										<th><input type="checkbox" id="cart_chk_all"></th>
										<th class="product-col">Product</th>
										<th class="price-col">Price</th>
                                        <th class="price-col">Shipping Price</th>
										<th class="qty-col">Qty</th>
										<th>Subtotal</th>
									</tr>
								</thead>

								<tbody>
                                	@php
                                    $subtotal = 0;
                                    $total_shipping = 0;
                                    $total_service_fee = 0;
                                    $item_count = 0;
                                    @endphp
                                    
                                	@foreach($data as $row)
									
                                    @php
                                    if($row->ct_select == 'Y')                                    
                                    {
                                    	$subtotal += ($row->ct_price*$row->ct_qty);
                                        $total_shipping += $row->ct_send_cost;
                                        $total_service_fee += Config::get('service_fee.fee') * Config::get('rate.usd');
                                        $item_count++;
                                    }
                                    @endphp                                  
									<tr class="product-row">
										<td><input type="checkbox" class="chk_ct_id" name="ct_id[]" value="{{$row->ct_id}}" @if($row->ct_select == 'Y') checked @endif></td>
										<td class="product-col">
											<figure class="product-image-container">
												<a href="@if($row->it_url) {{$row->it_url}} @else /mall/item/{{$row->it_id}} @endif" class="product-image">
                                                @if($row->it_thumbnail_url) 
                                                <img src="{{$row->it_thumbnail_url}}" alt="product">                                                
                                                @else 
                                                <img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg" alt="product">                                                @endif                            
                                                
                                                </a>
											</figure>
											<h6 class="product-title">
												<a href="@if($row->it_url) {{$row->it_url}} @else /mall/item/{{$row->it_id}} @endif">{{$row->it_name}}</a>
												<div class="cart_option">
												{{$row->it_option}}
												</div>
											</h6>
										</td>
										<td>₩ {{number_format($row->ct_price)}} <br>($ {{number_format($row->ct_price / Config::get('rate.usd'), 2)}})</td>
                                        <td>₩ {{number_format($row->ct_send_cost)}} <br>($ {{number_format($row->ct_send_cost / Config::get('rate.usd'), 2)}})</td>
										<td>
											<input class="vertical-quantity form-control ct_qty" type="text" value="{{$row->ct_qty}}">
                                            <span class="change_qty" data_ct_id="{{$row->ct_id}}">CHANGE</span>
										</td>
										<td>₩ {{number_format(($row->ct_price*$row->ct_qty)+$row->ct_send_cost)}} <br>($ {{number_format((($row->ct_price*$row->ct_qty)+$row->ct_send_cost) / Config::get('rate.usd'),2)}})  </td>
									</tr>																	
                                    @endforeach                                  
									
                                    @php
                                    //지정된 상품 개수보다 적으면 기본 서비스피로 부과
                                    if($item_count <= Config::get('service_fee.min')) $total_service_fee = Config::get('service_fee.min_price') * Config::get('rate.usd');;
                                    @endphp
								</tbody>
								<tfoot>
									<tr>
										<td colspan="6" class="clearfix">
											<div class="float-left">
												<a href="/" class="btn btn-outline-secondary">Continue Shopping</a>
											</div><!-- End .float-left -->

											<div class="float-right">
												<a href="" class="btn btn-outline-secondary" id="cart_delete" >Delete</a>
											</div><!-- End .float-right -->
										</td>
									</tr>
								</tfoot>
							</table>
						</div><!-- End .cart-table-container -->											
					</div><!-- End .col-lg-8 -->

					<div class="col-lg-4">
						<div class="cart-summary">
							<h3>Summary</h3>	
							<div>동일 판매자로부터 여러개의 제품을 구매하시면 배송비 중복결제가 이루어지나 즉시 환불처리 됩니다.</div>
							<table class="table table-totals">
                                <tbody>
                                    <tr>
                                        <td>Subtotal</td>
                                        <td>KRW {{number_format($subtotal)}}</td>
                                    </tr>

                                    <tr>
                                        <td>Local Shipping</td>
                                        <td>KRW {{number_format($total_shipping)}}</td>
                                    </tr>                                    
                                    <tr>
                                        <td>
											Service Fee
											<div class="tooltip2"><a href="#"><img src="/image/team/g_dot.png"></a>
												<span class="tooltiptext2">Service Fee는 item 갯수로 부과되며 1-3개까지는 $1.9 4-7개까지는 item당 $0.5입니다. </span>
											</div>																					
										</td>
                                        <td>USD {{number_format($total_service_fee / Config::get('rate.usd'),2)}} <!--(USD {{Config::get('service_fee.fee')}} * Item)--></td>
                                    </tr>
                                    <tr>
                                        <td>Item Subtotal</td>
                                        <td>KRW {{number_format($subtotal+$total_shipping+$total_service_fee)}}</td>                                        
                                    </tr>
                                    <tr>
										<td>
											Exchange Rates
											<div class="tooltip2"><a href="#"><img src="/image/team/g_dot.png"></a>
												<span class="tooltiptext2">EC KOREA의 환율정보는 하나은행의 고시환율이 적용된 값임을 알려드립니다. </span>
											</div>
										</td>
										<td>1USD = {{Config::get('rate.usd')}} KRW </td>
                                    </tr>
                                </tbody>
                                <tfoot>                                    
                                    <tr>
                                    	<td>Order Total (USD)</td>
                                        <td>USD {{number_format(($subtotal+$total_shipping+$total_service_fee) /Config::get('rate.usd') ,2)}}</td>
                                    </tr>
                                </tfoot>
                            </table>
							<div class="checkout-methods">
								<a href="/order/checkout" class="btn btn-block btn-sm btn-primary">Go to Checkout</a>
							</div><!-- End .checkout-methods -->
						</div><!-- End .cart-summary -->
					</div><!-- End .col-lg-4 -->
						
				</div><!-- End .row -->
			</div><!-- End .container -->

			<div class="mb-6"></div><!-- margin -->
		</main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	

	@include('footer')

</body>
<script>
$(document).ready(function(e) {
	
	//
	if($(".chk_ct_id").length == $(".chk_ct_id:checked").length)
	{
		$("#cart_chk_all").attr("checked",true);
	}
	
	$("#cart_chk_all").click(function(e) {		
		var chk_yn = 'N';		
		if($(this).is(":checked")) chk_yn = 'Y'; else	chk_yn = 'N';		
        select_all_ct_id(chk_yn);		
		
		location.reload();
    });
    $("#cart_delete").click(function(e) {
        $(".chk_ct_id:checked").each(function(index, element) {
            del_ct_id($(this).val());
        });
		
		location.reload();
    });
	$(".chk_ct_id").click(function(e) {
		var chk_yn = 'N';
		var ct_id = $(this).val();
        if($(this).is(":checked")) chk_yn = 'Y';
		select_ct_id(ct_id, chk_yn); 
		location.reload();
		
    });
	$(".change_qty").click(function(e) {
        var ct_id = $(this).attr("data_ct_id");
		var qty = $(this).parent().find(".ct_qty").val();
		qty_ct_id(ct_id,qty);
    });
});
function del_ct_id(ct_id)
{
	//AJAX
	$.ajax({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	type: 'POST',
	async:false,
	url: '/order/cart/update',
	data: {method:'delete', ct_id:ct_id},
	dataType: 'json',
	success: function(data) {
		//if(data.result == 1) location.reload();
	},
	error: function(data) {
		alert("error" +data.msg);
  }
  });
}
function select_ct_id(ct_id, select_yn)
{
	//AJAX
	$.ajax({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	type: 'POST',
	async:false,
	url: '/order/cart/update',
	data: {method:'select', ct_id:ct_id, select_yn : select_yn},
	dataType: 'json',
	success: function(data) {
		if(data.result == 1) return true; else return false;
	},
	error: function(data) {
		alert("error : " +data.msg);
  }
  });
}
function select_all_ct_id(select_yn)
{
	//AJAX
	$.ajax({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	type: 'POST',
	async:false,
	url: '/order/cart/update',
	data: {method:'select_all', select_yn : select_yn},
	dataType: 'json',
	success: function(data) {
		if(data.result == 1) return true; else return false;
	},
	error: function(data) {
		alert("error : " +data.msg);
  }
  });
}
function qty_ct_id(ct_id, qty)
{
	//AJAX
	$.ajax({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	type: 'POST',
	async:false,
	url: '/order/cart/update',
	data: {method:'edit_qty', ct_id:ct_id, qty : qty},
	dataType: 'json',
	success: function(data) {
		if(data.result == 1) location.reload();
	},
	error: function(data) {
		alert("error" +data.msg);
  }
  });
}
</script>
        
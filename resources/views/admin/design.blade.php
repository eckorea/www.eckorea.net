@include('admin.top')
<div class="app-main"> 
	@include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>기본설정</div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">이미지</th>
                                        <th class="text-center">링크경로</th>
										<th class="text-center">노출여부</th>
                                        <th class="text-center">비고</th>
                                    </tr>
                                </thead>
                                <tbody>                              
                                    <tr>
										<td><img src=""></td>
										<td>http://</td>
										<td>노출</td>
										<td><a href="/admin/design_view">[수정]</a></td>                              
                                    </tr>  
									<tr>
										<td><img src=""></td>
										<td>http://</td>
										<td>노출</td>
										<td><a href="/admin/design_view">[수정]</a></td>                              
                                    </tr>  
									<tr>
										<td><img src=""></td>
										<td>http://</td>
										<td>노출</td>
										<td><a href="/admin/design_view">[수정]</a></td>                              
                                    </tr>  
									<tr>
										<td><img src=""></td>
										<td>http://</td>
										<td>노출</td>
										<td><a href="/admin/design_view">[수정]</a></td>                              
                                    </tr>  
									<tr>
										<td><img src=""></td>
										<td>http://</td>
										<td>노출</td>
										<td><a href="/admin/design_view">[수정]</a></td>                              
                                    </tr>  
                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        @include('admin.footer') 
	</div>
</div>

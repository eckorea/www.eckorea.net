<aside class="sidebar col-lg-3">
	<div class="widget widget-dashboard">
		<h3 class="widget-title">GUIDE</h3>
		<ul class="list">
        @foreach($title_data as $title_row)
			<li><a href="/mall/guide/{{$title_row->guide_id}}">{{$title_row->{"guide_title_".app()->getLocale()} }} </a></li>            
        @endforeach
		</ul>
        
       
	</div>
<!-- End .widget --> 
</aside>
<!-- End .col-lg-3 --> 
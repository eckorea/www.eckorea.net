@include('admin.top')
<div class="app-main"> @include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>주문서 상세</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">주문번호</th>
                                        <th class="text-center">상품정보</th>
                                        <th class="text-center">배송정보</th>
                                        <th class="text-center">주문인</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="center_order"> {{$data[0]->od_id}} </td>
                                        <td class="center_name2">
                                        @foreach($data as $row)
                                        <div> 
											<img src="{{$row->it_thumbnail_url}}">
											<div class="center_name_right"> 
												{{$row->od_id}}-{{$row->ct_id}} <br>
												[{{$row->ct_origin}}]<br>
												{{$row->it_name}} <br>
												{{$row->it_option}} <span>{{$row->ct_krw_price}}원 / {{$row->ct_qty}}개</span> </div>
											<div class="clear"></div>
										</div>
										<div style="clear:both"></div>
                                        @endforeach              
                                        </td>
                                        <td>
                                            @foreach($data as $row)
                                            <div>
												<form method="post" action="/admin/cart/edit">
                                                @csrf
                                                <input type="hidden" name="od_id" value="{{$row->od_id}}" />
												<input type="hidden" name="ct_id" value="{{$row->ct_id}}" />
                                                <div style="padding-bottom:10px;">
                                                <input type="text" name="ct_local_ship_carrier" placeholder="국내배송회사" value="{{$row->ct_local_ship_carrier}}" style="width:178px;" />
                                                <input type="text" name="ct_local_ship_no" placeholder="국내송장번호" value="{{$row->ct_local_ship_no}}" />
                                                <button type="submit">국내송장 업데이트</button>
                                                </div>
                                                <div style="border-top:solid 1px green; padding-top:10px;">
													<select name="ct_carrier">
													<option value="">배송업체선택</option>
                                                    @foreach($carrier_data as $carrier_row)
													<option value="{{$carrier_row->carrier_name}}" 
                                                    @if($row->ct_carrier == '' && $carrier_row->carrier_name == $data[0]->od_hope_carrier) selected  
                                                    @elseif($row->ct_carrier == $carrier_row->carrier_name) selected @endif > {{$carrier_row->carrier_name}} 
                                                    </option>
                                                    @endforeach
													</select>
													<input type="text" name="ct_ship_no" placeholder="해외송장번호" value="{{$row->ct_ship_no}}" />
													<button type="submit">해외송장 업데이트</button>
                                                </div>
												</form>
                                            </div>
                                            @endforeach                
                                        </td>
                                        <td> {{$data[0]->mb_email}} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <form method="post" action="/admin/order/edit">
            <input type="hidden" name="od_id" value="{{$data[0]->od_id}}" />
            {{ csrf_field() }}
            <div class="row">            
                <div class="col-md-12">                
                    <h5 class="card-title">수취인정보 </h5>{{$data[0]->mb_email}}
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <div class="form-row">
                           		 <div class="col-md-6">
                                    <h5 class="card-title">배송옵션(한국전용) eck : ECK 입고후 일괄배송 , direct : 소비자주소지로 직배송</h5>
                                        <input name="od_delivery_option" id="" value="{{$data[0]->od_delivery_option}}" type="text" class="form-control">
                                    <!--<select class="mb-2 form-control">
                                        <option>EMS 항공</option>
                                    </select>-->
                                </div>
                                <div class="col-md-6">
                                    <h5 class="card-title">고객희망 운송방법</h5>
                                        <input name="od_ship_method" id="" value="{{$data[0]->od_hope_carrier}}" type="text" class="form-control">
                                    <!--<select class="mb-2 form-control">
                                        <option>EMS 항공</option>
                                    </select>-->
                                </div>
                                <div class="col-md-6">
                                    <h5 class="card-title">배송국가</h5>
                                    <input name="od_country" id="" value="{{$data[0]->od_country}}" type="text" class="form-control">
                                    <!--<select class="mb-2 form-control">
                                        <option>일본</option>
                                    </select>-->
                                </div>
                            </div>
                             <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="" class="">First Name</label>
                                        <input name="od_first_name" id="" value="{{$data[0]->od_first_name}}" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="" class="">Last Name</label>
                                        <input name="od_last_name" id="" value="{{$data[0]->od_last_name}}" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">                                
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="" class="">Tel</label>
                                        <input name="od_tel" id="" value="{{$data[0]->od_tel}}" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="" class="">Address 1</label>
                                        <input name="od_addr1" id="" type="text" value="{{$data[0]->od_addr1}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="" class="">Address 2</label>
                                        <input name="od_addr2" id="" type="text" value="{{$data[0]->od_addr2}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="" class="">City</label>
                                        <input name="od_city" id="" type="text" value="{{$data[0]->od_city}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="" class="">State</label>
                                        <input name="od_state" id="" type="text" value="{{$data[0]->od_state}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="position-relative form-group">
                                        <label for="" class="">Zip</label>
                                        <input name="od_zip" id="" type="text" value="{{$data[0]->od_zip}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="mb-2 mr-2 btn btn-light">저장</button>
                        </div>
                    </div>
              
                </div>
                
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="card-title">무게측정</h5>
                    <div class="main-card mb-3 card">
                        <div class="table-responsive card-body ">
                            <table class="align-middle mb-0 table table-borderless">
                                <thead>
                                    <tr>
                                        <th class="text-center">실무게(kg)</th>
                                        <th class="text-center">가로*세로*높이(cm)</th>
                                        <th class="text-center">적용무게(kg)</th>
                                        <th class="text-center">배송비(KRW)</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input class="form-control" name="od_weight" value="{{$data[0]->od_weight}}"></td>
                                        <td class="center_name2">                                        	
                                            <input class="form-control" name="od_width" value="{{$data[0]->od_width}}">
                                            <input class="form-control" name="od_length" value="{{$data[0]->od_length}}">
                                            <input class="form-control" name="od_height" value="{{$data[0]->od_height}}">
                                            </td>
                                        <td><input class="form-control" name="od_weight_due" value="{{$data[0]->od_weight_due}}"></td>
                                        <td><input class="form-control" name="od_send_cost2" value="{{$data[0]->od_send_cost2}}"></td>
                                    </tr>
                                    <tr>
                                    <td>Sales Tax(KRW) <input class="form-control" name="od_sales_tax" value="{{$data[0]->od_sales_tax}}"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <button onclick="return make_ship_invoice('{{$data[0]->od_id}}')" class="mb-2 mr-2 btn btn-primary">견적제출</button>
                            <button type="submit" class="mb-2 mr-2 btn btn-light">저장</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="card-title">배송비 / 결제정보</h5>
                    <div class="main-card mb-3 card">
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                                <div class="card-body">
                                    <div class="position-relative row form-group">
                                        <label class="col-sm-2 col-form-label">1차 결제액</label>
                                        <div class="col-sm-10">KRW {{number_format($data[0]->od_receipt_price1)}}</div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label class="col-sm-2 col-form-label">2차 배송비 결제액(+Sales Tax)</label>
                                        <div class="col-sm-10">KRW {{number_format($data[0]->od_receipt_price2)}} 
                                        @if($data[0]->od_ship_paid == 'Y') 
                                        <button type="button" onclick="refund_shipping('{{$data[0]->od_id}}');">환불처리</button> 
                                        @elseif($data[0]->od_ship_paid == 'C') 
                                        (환불완료)
                                        @endif                                        
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label class="col-sm-2 col-form-label">결제수단</label>
                                        <div class="col-sm-10">CARD (쿠폰사용 $ {{$data[0]->od_cart_coupon}})</div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label class="col-sm-2 col-form-label">1차 결제일</label>
                                        <div class="col-sm-10">{{$data[0]->created_at}}</div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label class="col-sm-2 col-form-label">2차 결제일</label>
                                        <div class="col-sm-10">@if(isset($ship_pay_log[0]->created_at)){{$ship_pay_log[0]->created_at}}@endif</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <h5 class="card-title">주문서 변경로그</h5>
                    <div class="main-card mb-3 card">
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                                <div class="card-body">
                                    <table>
                                    <thead>
                                    <th>기능</th><th>변경인</th><th>변경시간</th><th>IP</th>
                                    </thead>
                                    <tbody>
                                    @foreach($mod_log as $log)                                    
                                    <tr><td>{{$log->method}}</td><td>{{$log->writer}}</td><td>{{$log->created_at}}</td><td>{{$log->ip}}</td></tr>
                                    @endforeach
                                    </tbody>
                                    </table>                               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="row">
                <div class="col-md-12">
                    <h5 class="card-title">운송장번호</h5>
                    <div class="main-card mb-3 card">
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                                <div class="card-body">
                                    <div class="position-relative row form-group">
                                        <label for="" class="col-sm-2 col-form-label">운송회사</label>
                                        <div class="col-sm-10">
                                            <select name="select" id="" class="form-control">
                                                <option>DHL</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label class="col-sm-2 col-form-label">운송장번호</label>
                                        <div class="col-sm-10">
                                            <input class="form-control" name="">
                                        </div>
                                    </div>
                                    <button type="submit" class="mb-2 mr-2 btn btn-primary">저장</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            -->
              </form>
        </div>
        @include('admin.footer') </div>
</div>
<script>
function make_ship_invoice(od_id)
{
	if(confirm('견젹제출 하시겠습니까?\n2차 배송료 결제가 설정됩니다'))
	{			
		//AJAX
		$.ajax({
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: 'POST',
		url: '/admin/order/make_ship_invoice',
		data: {od_id : od_id, od_weight : $("input[name='od_weight']").val(), od_width : $("input[name='od_width']").val(), od_length : $("input[name='od_length']").val(), od_height : $("input[name='od_height']").val(), od_weight_due : $("input[name='od_weight_due']").val(), od_send_cost2 :$("input[name='od_send_cost2']").val()},
		dataType: 'json',
		success: function(data) {
			alert(data.msg);
		},
		error: function(data) {
			alert("error" +data);
	  }
	});
	}
	
	return false;
}
function refund_shipping(od_id)
{
	if(!confirm('PG사에 2차 결제 금액이 환불 요청됩니다. 진행하시겠습니까?'))
	{
		return false;
	}	
		
	//AJAX
	$.ajax({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	type: 'POST',
	url: '/admin/shipping/refund/'+od_id,
	data: {},
	dataType: 'json',
	success: function(data) {
		alert(data.msg);
		location.relaod();
	},
	error: function(data) {
		alert("error" +data);
  }
});
}
</script>
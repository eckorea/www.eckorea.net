@include('top')
<style>
.domestic_card
{
	display:none;
}
</style>
<body>
	<div class="page-wrapper">
		<main class="main">
            <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Checkout</li>
                    </ol>
                </div><!-- End .container -->
            </nav>
			<form id="payment_form" method="post" action="/order/payment">
            {{ csrf_field() }}
            <div class="container">               
                <div class="row">
                    <div class="col-lg-8">
                        <ul class="checkout-steps">
                            <li>
                                <h2 class="step-title">Shipping Address <a href="/my/address">[Manage Shipping Address]</a> </h2>                                  
									<div class="blank20">&nbsp;</div>
                                    <div class="form-group required-field">
                                        <label>First Name </label>
                                        <input type="text" class="form-control eng_input" name="od_first_name" value="@if(isset($default_addr_data[0])) {{$default_addr_data[0]->addr_first_name}} @endif" required>
                                    </div><!-- End .form-group -->

                                    <div class="form-group required-field">
                                        <label>Last Name </label>
                                        <input type="text" class="form-control eng_input" name="od_last_name"  value="@if(isset($default_addr_data[0])) {{$default_addr_data[0]->addr_last_name}} @endif" required>
                                    </div><!-- End .form-group -->

                                    <!--<div class="form-group">
                                        <label>Company </label>
                                        <input type="text" class="form-control" name="od_company">
                                    </div><!-- End .form-group -->

                                    <!--<div class="form-group">
                                        <label>Street Address </label>
                                        <input type="text" class="form-control" name="od_addr1" required>
                                    </div><!-- End .form-group -->

									<!--미국, 캐나다, 멕시코, 호주, 뉴질랜드, 영국, 독일, 프랑스, 베트남, 싱가폴, 한국-->
									<div class="form-group required-field">
                                        <label>Country</label>
                                        <div class="toolbox" >	
                                            <select class="form-control" id="od_country" name="od_country" required>
                                            	<option value="">SELECT COUNTRY</option>
                                                @foreach(DB::table('define_countries')->get() as $country_row)
                                                <option carrier_allows="{{$country_row->carrier_allows}}" phone_code="{{$country_row->country_phone_code}}" value="{{$country_row->country_name}}" @if(isset($default_addr_data[0])) @if($default_addr_data[0]->addr_country == $country_row->country_name) selected @endif @endif">{{$country_row->country_name}}</option>                                                
                                                @endforeach                                                
                                            </select>
                                        </div><!-- End .select-custom -->
                                    </div><!-- End .form-group -->
                                    
                                    <div id="delivery_option" class="form-group">
                                        <label>배송옵션(Delivery Option)</label><br>
                                        <input type="radio" id="delivery_direct" name="od_delivery_option" value="direct" checked> <label for="delivery_direct">먼저 도착한 제품부터 개별 배송</label> 
                                        <input type="radio" id="delivery_eck" name="od_delivery_option" value="eck"> <label for="delivery_eck">한번에 모아서 배송 (추가비용 발생)</label>
                                    </div><!-- End .form-group -->

									<div class="form-group required-field">
                                        <label>State/Province</label>
                                        <input type="text" class="form-control eng_input" name="od_state"  value="@if(isset($default_addr_data[0])) {{$default_addr_data[0]->addr_state}} @endif" required>                                               
                                    </div><!-- End .form-group -->

                                    <div class="form-group required-field">
                                        <label>City</label>
                                        <input type="text" class="form-control eng_input" name="od_city"  value="@if(isset($default_addr_data[0])) {{$default_addr_data[0]->addr_city}} @endif" required>
                                    </div><!-- End .form-group -->

									<div class="form-group required-field">
                                        <label>Address1</label>
                                        <input type="text" class="form-control" name="od_addr1"  value="" required>
                                    </div><!-- End .form-group -->

									<div class="form-group ">
                                        <label>Address2</label>
                                        <input type="text" class="form-control" name="od_addr2"  value="">
                                    </div><!-- End .form-group -->                                   

                                    <div class="form-group required-field">
                                        <label>Zip/Postal Code </label>
                                        <input type="text" class="form-control" name="od_zip"  value="@if(isset($default_addr_data[0])) {{$default_addr_data[0]->addr_zip}} @endif" required>
                                    </div><!-- End .form-group -->                                 
                                    
									
                                    <div class="form-group required-field">
                                        <label>Phone Number </label>
                                        <div class="form-control-tooltip">
                                            <input type="tel" class="form-control" id="od_phone" name="od_phone"  value="@if(isset($default_addr_data[0])) {{$default_addr_data[0]->addr_phone}} @endif" required>
                                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                                        </div><!-- End .form-control-tooltip -->
                                    </div><!-- End .form-group -->
                                    
									<div class="form-group">
                                        <label>COUPON</label>
                                        <div class="toolbox" >	
                                            <select class="form-control" id="od_coupon" name="od_coupon">
                                                <option value="" data-price="0">== SELECT COUPON ==</option>
                                                @foreach($cp_data as $cp_row)
                                                <option value="{{$cp_row->id}}" data-price="{{$cp_row->cp_amount}}">{{$cp_row->cp_name}} (${{$cp_row->cp_amount}})</option>
                                                @endforeach
                                            </select>
                                        </div><!-- End .select-custom -->
                                    </div><!-- End .form-group -->
                                    
                                    <div class="form-group required-field">
                                        <label>카드타입 </label>
                                        <div class="form-control-tooltip">
                                            <input type="radio" id="rdo_abroad" name="rdo_pay_type" class="rdo_pay_type" checked> <label for="rdo_abroad">해외카드</label>
                                            <input type="radio" id="rdo_domestic" name="rdo_pay_type" class="rdo_pay_type"> <label for="rdo_domestic">국내카드(한국) [팝업해제필수]</label>
                                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                                        </div><!-- End .form-control-tooltip -->
                                    </div><!-- End .form-group -->
                                    
                                    <div class="form-group required-field domestic_card">
                                        <label>카드사 선택</label>
                                        <div class="form-control-tooltip">
                                            <select id="card_company" name="card_code">
                                            <option value="361">BC</option>
                                            <option value="364">광주</option>
                                            <option value="366">신한</option>
                                            <option value="365">삼성</option>
                                            <option value="367">현대</option>
                                            <option value="368">롯데</option>
                                            <option value="369">수협</option>
                                            <option value="370">씨티</option>
                                            <option value="371">농협</option>
                                            <option value="372">전북</option>
                                            <option value="373">제주</option>
                                            <option value="374">하나</option>
                                            <option value="381">국민</option>
                                            <option value="041">우리</option>
                                            <option value="044">외환</option>
                                            </select>
                                            
                                            <input type="hidden" id="installment_months" name="installment_months" value="00">
                                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                                        </div><!-- End .form-control-tooltip -->
                                    </div><!-- End .form-group -->
                                    
                                    
                                    <div class="form-group required-field abroad_card">
                                        <label>Card Number </label>
                                        <div class="form-control-tooltip">
                                            <input type="text" class="form-control abroad_payment" required placeholder="1234-1234-1234-1234" name="card_number" maxlength="20">
                                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                                        </div><!-- End .form-control-tooltip -->
                                    </div><!-- End .form-group -->
                                    
                                    <div class="form-group required-field abroad_card">
                                        <label>Card Expire / CVV </label>
                                        <div class="form-control-tooltip">
                                            <input type="text" class="form-control abroad_payment" required placeholder="MM" name="card_mm" maxlength="2" style="width:80px; display:inline-block;">/ 
                                            <input type="text" class="form-control abroad_payment" required placeholder="YY" name="card_yy" maxlength="2" style="width:80px; display:inline-block;">
                                            <input type="text" class="form-control abroad_payment" required placeholder="CVV" name="card_cvv" maxlength="4" style="width:80px; display:inline-block;">
                                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                                        </div><!-- End .form-control-tooltip -->
                                    </div><!-- End .form-group -->
                                    
                                    <div class="form-group required-field abroad_card">
                                        <label>Card Holder </label>
                                        <div class="form-control-tooltip">
                                            <input type="text" class="form-control eng_input abroad_payment" required placeholder="JHON DOE" name="card_holder">
                                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                                        </div><!-- End .form-control-tooltip -->
                                    </div><!-- End .form-group -->                                   
                                
                            </li>
                            <li>
                                <div class="checkout-step-shipping">
                                    <h2 class="step-title">Shipping Methods ( <!--In Item Only : {{$flag_in_item}},--> Weight : {{$total_weight}}kg )</h2>

                                    <table id="carrier_list" class="table table-step-shipping">
                                        <tbody>
                                        <!--dynamic carrier list data-->
                                        </tbody>
                                    </table>
                                </div><!-- End .checkout-step-shipping -->
                            </li>
                        </ul>
                    </div><!-- End .col-lg-8 -->

                    <div class="col-lg-4">
                        <div class="order-summary">
                            <h3>Summary</h3>

                            <div class="" >
                                <table class="table table-mini-cart">
                                    <tbody>                                    
                                    @php
                                    $subtotal = 0;
                                    $total_shipping = 0;
                                    $total_service_fee = 0;
                                    $item_count = 0;
                                    @endphp
                                    
                                    @foreach($data as $row)
                                    
                                    @php
                                    if($row->ct_select == 'Y')                                    
                                    {
                                    	$subtotal += ($row->ct_price*$row->ct_qty);
                                        $total_shipping += $row->ct_send_cost;
                                        $total_service_fee += ((Config::get('service_fee.fee') * Config::get('rate.usd')));
                                        $item_count++;
                                    }
                                    @endphp  
                                        <tr>
                                            <td class="product-col">
                                                <figure class="product-image-container">
                                                    <a href="@if($row->it_url) {{$row->it_url}} @else /mall/item/{{$row->it_id}} @endif" class="product-image">
                                                        @if($row->it_thumbnail_url) 
                                                <img src="{{$row->it_thumbnail_url}}" alt="product">                                                
                                                @else 
                                                <img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg" alt="product">                                                @endif 
                                                    </a>
                                                </figure>
                                                <div>
                                                    <div class="product-title">
                                                        <a href="@if($row->it_url) {{$row->it_url}} @else /mall/item/{{$row->it_id}} @endif">{{$row->it_name}}</a>
                                                    </div>

                                                    <span class="product-qty">Qty: {{$row->ct_qty}} / KRW {{number_format($row->ct_price*$row->ct_qty)}} (USD {{number_format(($row->ct_price*$row->ct_qty)/Config::get('rate.usd'),2)}})</span>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach  
                                        
                                        @php
                                        //지정된 상품 개수보다 적으면 기본 서비스피로 부과
                                        if($item_count <= Config::get('service_fee.min')) $total_service_fee = Config::get('service_fee.min_price') * Config::get('rate.usd');
                                        $total_service_fee = round($total_service_fee);
                                        @endphp
                                        
                                        <tr>
                                        <td>
											<div class="cart-summary">
											<table class="table table-totals">
												<tbody>
													<tr>
														<td>Subtotal</td>
														<td>KRW {{number_format($subtotal)}}</td>
													</tr>
													<tr>
														<td>Local Shipping</td>
														<td>KRW {{number_format($total_shipping)}}</td>
													</tr>
													<tr>
														<td>Int'l Shipping</td>
														<td><span id="show_intl_shipping">0</span></td>
													</tr>
													<tr>
														<td>Sales Tax</td>
														<td>KRW <span id="show_sale_tax">0</span></td>
													</tr>
													<tr>
														<td>
															Service Fee
															<div class="tooltip2"><a href="#"><img src="/image/team/g_dot.png"></a>
																<span class="tooltiptext2">Service Fee는 item 갯수로 부과되며 1-3개까지는 $1.9 4-7개까지는 item당 $0.5입니다. </span>
															</div>																					
														</td>
														<td>USD {{number_format($total_service_fee/Config::get('rate.usd'),2)}} <!--(USD {{Config::get('service_fee.fee')}} * Item)--></td>
													</tr>
													<tr>
														<td>Item Subtotal</td>
														<td>
															KRW {{number_format($subtotal+$total_shipping+$total_service_fee)}}<br>
															USD <span id="show_order_total_usd">{{number_format(($subtotal+$total_shipping+$total_service_fee) / Config::get('rate.usd') ,2)}}</span>
														</td>
													</tr>
                                                    <tr>
														<td>Coupon</td>
														<td>USD <span id="show_coupon_price">0</span></td>
													</tr>
													<tr>
														<td>
															Exchange Rates
															<div class="tooltip2"><a href="#"><img src="/image/team/g_dot.png"></a>
																<span class="tooltiptext2">EC KOREA의 환율정보는 하나은행의 고시환율이 적용된 값임을 알려드립니다. </span>
															</div>
														</td>
														<td>1USD = {{Config::get('rate.usd')}} KRW</td>
													</tr>
											</table>
											</div>
                                                                              
                                        
                                        <div style="font-size:15px; font-weight:bold; border-top:1px dotted #000; padding-top:10px">Order Total : USD 
                                        <span id="show_due">{{number_format(($subtotal+$total_shipping+$total_service_fee) / Config::get('rate.usd') ,2)}}</span></div>                                        
                                        </td>
                                        </tr>                                 
                                    </tbody>    
                                </table>
                            </div><!-- End #order-cart-section -->
                        </div><!-- End .order-summary -->
                    </div><!-- End .col-lg-4 -->
                </div><!-- End .row -->
				
                <div class="row">
                    <div class="col-lg-8">
                        <div class="checkout-steps-action">
                            <!--<a href="/payment" class="btn btn-primary float-right">NEXT</a>-->
                            <span id="payment_loading_img" style="display:none;"><img src='/image/hori_loading.gif'> Payment Processing... Please wait...</span>
                            <button type="submit" id="order_payment" class="btn btn-primary float-right">NEXT</button>
                        </div><!-- End .checkout-steps-action -->
                    </div><!-- End .col-lg-8 -->
                </div><!-- End .row -->
            </div><!-- End .container -->
            <input type="hidden" id="amount_due_naked" name="amount_due_naked" value="{{$subtotal+$total_shipping+$total_service_fee}}">
            <input type="hidden" id="amount_due" name="amount_due" value="{{round($subtotal+$total_shipping+$total_service_fee)}}">
            <input type="hidden" id="intl_shipping" name="od_intl_shipping" value="0">
            <input type="hidden" id="sales_tax" name="od_sales_tax" value="0">
			</form>
            <div class="mb-6"></div><!-- margin -->
        </main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	@include('footer')
<script>
function handleOnInput(e)  {
  e.value = e.value.replace(/[^A-Za-z]/ig, '')
}

function valid_order()
{
	var flag = true;
	$("input[type='text']").each(function(index, element) {
        if($(this).prop('required'))
		{
			if($("#rdo_domestic").is(":checked") && $(this).hasClass('abroad_payment')) return;
						
			if($(this).val() == '')
			{				
				$(this).focus();	
				alert('Please Input Requirements.');
				flag = false;			
				return false;
			}						
		}		
    });
	
	//배송방식 선택 여부 확인
	var country = $("#od_country option:selected").val();	
	if(country != 'South Korea') //한국배송은 제외
	{
		if($("input[name='od_hope_carrier']:checked").length != 1)
		{
			alert('Please Select Shipping Method.');
			flag = false;			
			return false;
		}
	}
	
	return flag;
}

function request_payment(card_code, install_month)
{
	var params = $("#payment_form").serialize();
	var amount_due = $("#amount_due").val();
	
	//AJAX
	$.ajax({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	type: 'POST',
	//async:false,
	url: '/order/payment/domestic',
	data: params,
	dataType: 'json',
	beforeSend: function()
	{
		//로딩 이미지
		/*
		$("#order_payment").hide();
		$("#payment_loading_img").show();		
		*/
	},
	success: function(data) {
		//alert(JSON.stringify( data));
		if(data.result == 1)
		{
			// window.open(data.payment_url,'pay_child','width=10, height=10, menubar=no, status=no, toolbar=no, location=no');
			location.href = data.payment_url;
		}
		else
		{
			alert("Error:" +data.msg);
		}
		
		//로딩 이미지
		$("#order_payment").show();
		$("#payment_loading_img").hide();
	},
	error: function(data) {
		alert("Error:" +data.msg);
  }
  });
}

function make_carrier_list()
{	
	//리스트 초기화
	$("#carrier_list tbody").empty();
	
	var type = '{{$flag_in_item}}';
	var country = $("#od_country option:selected").val();	
	var weight = '{{$total_weight}}';
	
	//AJAX
	$.ajax({
	headers: {
	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	},
	type: 'POST',
	async:false,
	url: '/order/shipping_rate',
	data: {type:type, country:country, weight:weight},
	dataType: 'json',
	success: function(data) {
		if(data.result == 1)
		{
			console.log(data);
			var inner_cnt = 1;		
			$.each(data.shipping_data, function(key,value)
			{
				var carrier_name = value.sr_carrier;
				var carrier_rate = value.sr_price;
				
				if(type)
				{
					var result_html = '<tr><td><input id="carrier_'+inner_cnt+'" type="radio" name="od_hope_carrier" data-price="'+carrier_rate+'" value="'+carrier_name+'"></td><td><label for="carrier_'+inner_cnt+'"><strong>'+carrier_name+'(KRW '+carrier_rate+')</strong></label></td></tr>';
				}
				else
				{
					var result_html = '<tr><td><input id="carrier_'+inner_cnt+'" type="radio" name="od_hope_carrier" data-price="0" value="'+carrier_name+'"></td><td><label for="carrier_'+inner_cnt+'"><strong>'+carrier_name+'</strong></label></td></tr>';
				}				
				
				$("#carrier_list tbody").append(result_html);	
				
				inner_cnt++;			
			});
		}
		else
		{
			alert("Error:" +data.msg);
		}
	},
	error: function(data) {
		alert("error" +data.msg);
  }
  });	
}
function checkout_cal_init()
{
	var amount_due = $("#amount_due").val();		
	$("#show_intl_shipping").text(0);
	$("#show_sale_tax").text(0);
	$("#show_due").text($("#show_order_total_usd").text());
}
/*
function make_carrier_list()
{	
	//리스트 초기화
	$("#carrier_list tbody").empty();
	
	var country = $("#od_country option:selected").val();	
	var weight = '{{$total_weight}}';
	
	get_shipping_rate(country,weight);
	
	
	
	var carrier = $("#od_country option:selected").attr('carrier_allows')
	if(!carrier) return;
	
	var arr_carrier = carrier.split(',');
	for(idx in arr_carrier)
	{
		var carrier_name = arr_carrier[idx];
		var result_html = '<tr><td><input id="carrier_'+idx+'" type="radio" name="od_hope_carrier" value="'+carrier_name+'"></td><td><label for="carrier_'+idx+'"><strong>'+carrier_name+'($23.55)</strong></label></td></tr>';
		
		$("#carrier_list tbody").append(result_html);
	}
	
}*/
$(document).ready(function(e) {
	//Init
	if($("#od_country option:selected").val() != 'South Korea') $("#delivery_option").hide();
	make_carrier_list();
	alert('영국, 유럽, 호주 뉴질랜드 등 특정 국가에 대해서는 2차 결제 시\n물품 대금과 배송비를 합산한 금액의 * %가 소비세로 부과될 수 있습니다.');
	
	//배송국가변경
	$("#od_country").change(function(e) {
		//계산 초기화
		checkout_cal_init();
		var type = '{{$flag_in_item}}';
		var mixed_type = '{{$flag_mixed_item}}';
		var phone_code = $("#od_country option:selected").attr('phone_code');
		
		if($(this).val() == 'South Korea')
		{
			if(type == 1 || mixed_type == 1)
			{
				//합배송만 보여줌
				//<input type="radio" name="od_delivery_option" value="direct" checked> <label>소비자주소지로 직배송</label> 
				$("#delivery_option input:eq(0), #delivery_option label:eq(1)").hide();
				$("#delivery_option input:eq(1)").attr("checked",true);
				$("#delivery_option").show();
			}
			else
			{
				$("#delivery_option input:eq(0), #delivery_option label:eq(1)").show();
				$("#delivery_option input:eq(0)").attr("checked",true);
				$("#delivery_option").show();
			}
		}
		else
		{
			$("#delivery_option").hide();
		}
		
		//전화번호 국가 번호 추가
		$("#od_phone").val('+'+phone_code);
		
		//배송사 리스팅
        make_carrier_list();
    });
	
	//캐리어 선택	
	$("#carrier_list").on('change',"input[name='od_hope_carrier']",function()
	{
		var type = '{{$flag_in_item}}';
		var selected_country = $("#od_country option:selected").val();
		var amount_due = $("#amount_due").val();		
		var price = $(this).attr('data-price');
		var total_price = price*1 + amount_due*1;
		var sales_tax = 0;
		
		//국가별 sales tax 계산
		
		if(type == 1)
		{
			if(selected_country == 'New Zealand')
			{
				sales_tax = Math.round(total_price * 0.15);
			}
			else if(selected_country == 'United Kingdom' || selected_country == 'Australia' || selected_country == 'Germany' || selected_country == 'France')
			{
				sales_tax = Math.round(total_price * 0.1);			
			}
			else
			{
				sales_tax = 0;
			}
		}
				
		var final_due = total_price + sales_tax;
		$("#show_intl_shipping").text(price);
		$("#intl_shipping").val(price);
		$("#show_sale_tax").text(sales_tax);
		$("#sales_tax").val(sales_tax);
		$("#show_due").text((final_due /{{Config::get('rate.usd')}}).toFixed(2));
	});
	
	//결제버튼 클릭	
    $("#order_payment").click(function(e) {		
		if(valid_order()) //필수 확인
		{
			//로딩 이미지
			$("#order_payment").hide();
			$("#payment_loading_img").show();	
		
			//결제 방식별 submit 분기
			if($("#rdo_domestic").is(":checked"))
			{				
				request_payment($("#card_company").val(), '00');
			}
			else
			{
       			$("#payment_form").submit();
			}
		}
    });
	
	//결제방식 변경
	/*
	$(".rdo_pay_type").change(function(e) {
		
		$(".abroad_card, .domestic_card").toggle();
    });
	*/
	$("#rdo_abroad").click(function(e) {
        $(".abroad_card").show();
		$(".domestic_card").hide();
    });
	
	$("#rdo_domestic").click(function(e) {
        $(".abroad_card").hide();
		$(".domestic_card").show();
    });
	
	//쿠폰변경
	$("#od_coupon").change(function(e) {
		var amount_due = $("#amount_due_naked").val();
		var amount_due_usd = $("#amount_due_naked").val() / {{Config::get('rate.usd')}};
		var cp_price = $(this).find(":selected").attr('data-price');
		var final_due = (amount_due_usd - cp_price).toFixed(2);		
     	$("#show_coupon_price").text(cp_price);   
		$("#show_due").text(final_due);
    });
	
	
	//영문만 입력되게
	$(".eng_input").on('keyup',function()
	{
		handleOnInput(this);
	});
});
</script>
</body>
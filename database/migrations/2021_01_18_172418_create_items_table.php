<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('items', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->charset = 'utf8';
			$table->collation = 'utf8_general_ci';			
            $table->bigIncrements('id');
			$table->enum('it_type', array('in','out'));
			$table->string('it_origin',64)->comment('상품출처');
			$table->string('it_id', 20)->unique('it_id');
			$table->string('ca_id')->nullable();
			$table->string('it_name');
			$table->float('it_price', 10, 0);
			$table->enum('it_use', array('Y','N'))->default('N');
			$table->integer('it_stock_qty')->comment('재고수량');
			$table->integer('it_order')->comment('정렬순서');
			$table->text('it_img_json');
			$table->string('it_url',256);
			$table->dateTime('created_at');
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
			$table->SoftDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('items');
	}

}

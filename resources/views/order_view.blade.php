@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MY ACCOUNT</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>ORDER DETAIL</h2>

						<div class="row">
							<div class="col-md-12">	
								<div class="table-responsive basic">									
									<table class="align-middle mb-0 table table-borderless table-striped table-hover">										
										<tbody> 
											<tr>
												<th colspan="3">[{{$data[0]->od_id}}] </th>
											</tr>
                                            @foreach($data as $row)
											<tr>												
												<td class="center_name2">
													<div>
														 @if($row->it_thumbnail_url) 
                                                <img src="{{$row->it_thumbnail_url}}" alt="product">                                                
                                                @else 
                                                <img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg" alt="product">                                                @endif   
														<div class="center_name_right">
															[{{$row->ct_origin}}] <br>
															<!--<a href="@if($row->it_url) {{$row->it_url}} @else /mall/item/{{$row->it_id}} @endif">{{$row->it_name}}</a> <br>-->
                                                            {{$row->it_name}} <br>
															{{$row->it_option}} <span>KRW {{number_format($row->ct_price)}} / {{$row->ct_qty}}개<br>
                                                            {{convStatusToText($row->ct_house_status)}}                                                            
                                                            </span>
														</div>
														<div class="clear"></div>
													</div>													
												</td>												
											</tr>
                                           @endforeach   						
										</tbody>
									</table>										
								</div>  
							</div>
							<!-- End .col-md-6 --> 
						</div>	

						<div class="blank30">&nbsp;</div>

						<div class="row">
							<div class="table-responsive basic col-md-12">	
								<h5 class="step-title">Shipping Fee</h5>   
                                <div class="blank10">&nbsp;</div>
								<table class="align-middle mb-0 table table-borderless table-striped table-hover">										
									<tbody> 
										<tr>
											<th>Type</th>
                                            <th>Weight</th>
											<th>Shiping Fee</th>
                                            <th>Paid</th>
										</tr>
										<tr>												
											<td>Local Shipping</td>									
                                            <td>-</td>
											<td>KRW {{$data[0]->od_send_cost1}}</td>
                                            <td>Paid</td>
										</tr>  						
									</tbody>
								</table>	                                
                                <div class="blank10">&nbsp;</div>
								<table class="align-middle mb-0 table table-borderless table-striped table-hover">										
									<tbody> 
										<tr>
                                        	<th>Type</th>
											<th>Weight</th>
											<th>Shiping Fee</th>
                                            <th>Sales Tax</th>
                                            <th>Paid</th>
										</tr>
										<tr>
                                        	<td>Export</td>												
											<td>{{$data[0]->od_weight_due}}KG</td>									
											<td>KRW {{$data[0]->od_send_cost2}}</td>
                                            <td>KRW {{$data[0]->od_sales_tax}}</td>
                                            <td>
                                            @if($data[0]->od_ship_paid == 'N')
                                            Waiting For Shipping Information Check
                                            @elseif($data[0]->od_ship_paid == 'R')                                            
                                            <a href="/order/checkout/shipping/{{$data[0]->od_id}}">Waiting For Payment [Go to Pay]
                                            @elseif($data[0]->od_ship_paid == 'C')                                                  
                                            Canceled
                                            @else
                                            Paid
                                            @endif
                                            </a>
                                            </td>
										</tr>  						
									</tbody>
								</table>										
							</div>  
							<div class="blank30">&nbsp;</div>

							<div class="table-responsive basic col-md-12">	
								<h5 class="step-title">Payment Info</h5>   
                                <table style="text-align:center;">
                                <thead>
                                <tr>
                                <th>TID</th><th>METHOD</th><th>TYPE</th><th>PAY FOR</th><th>PAY AMOUNT</th><th>PAY DATE</th>
                                </tr>
                                </thead>
                                <tbody>                                
								@foreach($pay_data as $pay_row)
                                <tr>
                                <td>{{$pay_row->pay_tid}}</td><td>{{$pay_row->pay_method}}</td><td>{{$pay_row->pay_type}}</td><td>{{$pay_row->pay_for}}</td><td>KRW {{number_format($pay_row->pay_amount)}}</td><td>{{$pay_row->created_at}}</td>
                                </tr>                                                          
                                @endforeach
                                </tbody>   
                                </table>  
							</div>  
							<div class="blank30">&nbsp;</div>
							
							<div class="col-md-12">	
                            <form method="post" action="/order/edit/address" onSubmit="return confirm('주소정보 변경시 배송비측정이 다시 요청됩니다. 변경하시겠습니까?')">
                            @csrf                                                        
                            <input type="hidden" name="od_id" value="{{$data[0]->od_id}}">
								<h5 class="step-title">Shipping Address</h5>     
								<div class="blank30">&nbsp;</div>
								<div class="form-group">
									<label for="">First Name</label>
									<input type="text" class="form-control" id="" name="od_first_name" value="{{$data[0]->od_first_name}}" required >
								</div><!-- End .form-group -->
								<div class="form-group">
									<label for="">Last Name</label>
									<input type="text" class="form-control" id="" name="od_last_name" value="{{$data[0]->od_last_name}}" required >
								</div><!-- End .form-group -->
								<div class="form-group">
									<label for="">City</label>
									<input type="text" class="form-control" id="" name="od_city" value="{{$data[0]->od_city}}" required >
								</div><!-- End .form-group -->
								<div id="state_group" class="form-group">
									<label>State/Province</label>
									<div class="toolbox">		
										<input type="text" class="form-control" id="od_state" name="od_state" value="{{$data[0]->od_state}}" required >
									</div><!-- End .select-custom -->
								</div><!-- End .form-group -->
                                
                                <div class="form-group">
									<label for="">Addr1</label>
									<input type="text" class="form-control" id="" name="od_addr1" value="{{$data[0]->od_addr1}}" required >
								</div><!-- End .form-group -->
                                
                                <div class="form-group">
									<label for="">Addr2</label>
									<input type="text" class="form-control" id="" name="od_addr2" value="{{$data[0]->od_addr2}}" required >
								</div><!-- End .form-group -->
                                
								<div class="form-group">
									<label for="">Zip/Postal Code</label>
									<input type="text" class="form-control" id="" name="od_zip" value="{{$data[0]->od_zip}}" required >
								</div><!-- End .form-group -->
								<div class="form-group">
									<label>Country</label>
									<div class="toolbox">		
										<select class="form-control" id="od_country" name="od_country">
											@foreach(DB::table('define_countries')->get() as $country_row)
                                                <option carrier_allows="{{$country_row->carrier_allows}}" value="{{$country_row->country_name}}" @if($data[0]->od_country == $country_row->country_name) selected @endif">{{$country_row->country_name}}</option>                                                
                                                @endforeach    
										</select>
									</div><!-- End .select-custom -->
								</div><!-- End .form-group -->
								<div class="form-group">
									<label for="">Phone Number</label>
									<input type="text" class="form-control" id="" name="od_tel" value="{{$data[0]->od_tel}}" required >
								</div><!-- End .form-group -->
							</div>
							<!-- End .col-md-6 --> 
                            <input type="submit" value="EDIT">
                            </form>
						</div>
					
					</div>

					<!-- End .col-lg-9 -->

					@include('mypage_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
<script>
$(document).ready(function(e) {
//배송국가변경
$("#od_country").change(function(e) {
	if($(this).val() == 'South Korea')
	{
		$("#od_state").val('');
		$("#state_group").hide();		
	}
	else
	{
		$("#state_group").show();
	}
});
    
});

</script>
</body>

@include('admin.top')

<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>기본설정</div>
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="tab-content">                            
							<div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
								<div class="card-body">	
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">이미지</label>
										<div class="col-sm-10"><input type="file"></div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">링크경로</label>
										<div class="col-sm-10"><input class="form-control" name="first_name" value=""></div>
									</div>
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">노출여부</label>
										<div class="col-sm-10"><input type="checkbox"></div>
									</div>
									
									<div class="blank20"></div>
                                        
									<button type="submit" class="mb-2 mr-2 btn btn-primary">저장</button>                                        
									<button class="mb-2 mr-2 btn btn-light">취소</button>
								</div>									
							</div>
						</div>						
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>

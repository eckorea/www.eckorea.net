@include('admin.top')
<div class="app-main"> @include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>카테고리 관리</div>
                    </div>
                </div>
            </div>
            <!--
            <div class="main-card mb-3 card">
                <div class="card-body">
                
                    <div class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <form method="get" action="">
                                <select name="qkind" class="select_css">                                      
                                    <option value="email" @if(Request::query('qkind') == 'email') selected @endif>이메일</option>                                    
                                </select>                                
                            </div>
                           <input type="text" class="form-control" name="q" value="{{Request::query('q')}}">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">검색</button>
                            </div>
                            </form>
                        </div>
                    </div>
                
                </div>
            </div>
                -->
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">카테고리 ID</th>
                                        <th class="text-center">카테고리명</th>                                       
                                        <th class="text-center">수정</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)            
                                
                                    <tr>
                                        <td>{{$row->ca_id}}</td>
                                        <td>
                                        @for($i=1; $i < $row->ca_level; $i++)
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        @endfor
                                        {{$row->ca_name}}</td>                                      
                                        <td>
                                        <a href="category/add/{{$row->ca_id}}">[추가]</a>
                                        <a href="category/edit/{{$row->ca_id}}">[수정]</a>
                                        <a href="category/delete/{{$row->ca_id}}" onclick="return confirm('삭제하시겠습니까?');">[삭제]</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div>
                            <button><a href="/admin/category/new/new">카테고리 추가</a></button>
                            </div>
                        </div>
                        <div style="text-align:center">
                            <div class="card-body">
                                <nav>
                                    <ul class="pagination">
                                    {{ $data->links() }} 
                                    <!--
                                        <li class="page-item"><a href="javascript:void(0);" class="page-link" ><span aria-hidden="true">«</span><span class="sr-only">Previous</span></a></li>
                                        <li class="page-item"><a href="javascript:void(0);" class="page-link">1</a></li>
                                        <li class="page-item active"><a href="javascript:void(0);" class="page-link">2</a></li>
                                        <li class="page-item"><a href="javascript:void(0);" class="page-link">3</a></li>
                                        <li class="page-item"><a href="javascript:void(0);" class="page-link">4</a></li>
                                        <li class="page-item"><a href="javascript:void(0);" class="page-link">5</a></li>
                                        <li class="page-item"><a href="javascript:void(0);" class="page-link" ><span aria-hidden="true">»</span><span class="sr-only">Next</span></a></li>-->
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('admin.footer') </div>
</div>

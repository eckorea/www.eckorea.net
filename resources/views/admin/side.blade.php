<div class="app-sidebar sidebar-shadow">
	<div class="app-header__logo">
		<div class="logo-src"></div>
		<div class="header__pane ml-auto">
			<div>
				<button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
			</div>
		</div>
	</div>
	<div class="app-header__mobile-menu">
		<div>
			<button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>
		</div>
	</div>
	<div class="app-header__menu">
		<span>
			<button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
				<span class="btn-icon-wrapper">
					<i class="fa fa-ellipsis-v fa-w-6"></i>
				</span>
			</button>
		</span>
	</div>    
	<div class="scrollbar-sidebar">
		<div class="app-sidebar__inner">
			<ul class="vertical-nav-menu">				
				<li class="app-sidebar__heading">관리자</li>
				<li>
					<a href="">						
						기본관리
						<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
					</a>
					<ul>
						<li>
							<a href="#"><i class="metismenu-icon"></i>기본설정</a>
							<a href="/admin/category"><i class="metismenu-icon"></i>카테고리관리</a>
                            <a href=""><i class="metismenu-icon"></i>배송비관리</a>
                            
						</li>						
					</ul>
				</li>
				<li>
						<a href="/admin/item"><i class="metismenu-icon"></i>상품관리</a>
				</li>
				<li>
					<a href="">						
						회원관리
						<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
					</a>
					<ul>
						<li>
							<a href="/admin/member"><i class="metismenu-icon"></i>회원관리</a>
						</li>
						<li>
							<a href="/admin/message"><i class="metismenu-icon"></i>쪽지내역</a>
						</li>   
						<li>
							<a href="/admin/coupon"><i class="metismenu-icon"></i>쿠폰내역</a>
						</li>   
					</ul>
				</li>
				
				<li>
					<a href="">
						구매관리
						<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
					</a>
					<ul>
						<li>
							<a href="/admin/buy?status=buy_wait"><i class="metismenu-icon"></i>주문전</a>
						</li>
						<li>
							<a href="/admin/buy?status=buy_done"><i class="metismenu-icon"></i>주문완료</a>
						</li>	
						<li>
							<a href="/admin/buy?status=buy_hold"><i class="metismenu-icon"></i>주문대기</a>
						</li>
						<li>
							<a href="/admin/buy?status=buy_cancel"><i class="metismenu-icon"></i>주문취소</a>
						</li>
					</ul>
				</li>	
				<li>
					<a>
						입고관리
						<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
					</a>
					<ul>
						<li>
							<a href="/admin/warehouse?status=buy_done"><i class="metismenu-icon"></i>입고대기</a>
						</li>
						<li>
							<a href="/admin/warehouse?status=warehouse_stock"><i class="metismenu-icon"></i>입고확인</a>
						</li>
						<li>
							<a href="/admin/warehouse?status=warehouse_hold"><i class="metismenu-icon"></i>입고보류</a>
						</li>
						<li>
							<a href="/admin/warehouse?status=warehouse_cancel"><i class="metismenu-icon"></i>부분취소</a>
						</li>
					</ul>
				</li>	
				<li>
					<a>
						창고관리
						<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
					</a>
					<ul>
						<li>
							<a href="/admin/packing?status=warehouse_stock"><i class="metismenu-icon"></i>검수/패킹</a>
						</li>
						<li>
							<a href="/admin/shipping?status=R"><i class="metismenu-icon"></i>결제대기</a>
						</li>
						<li>
							<a href="/admin/shipping?status=Y"><i class="metismenu-icon"></i>(결제확인)발송대기</a>
						</li>
						<li>
							<a href="/admin/shipping?status=sended"><i class="metismenu-icon"></i>발송완료</a>
						</li>
					</ul>
				</li>	
                
                <li>
					<a>
						컨텐츠관리
						<i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
					</a>
					<ul>
						<li>
							<a href="/admin/faq"><i class="metismenu-icon"></i>FAQ</a>
						</li>
						<li>
							<a href="/admin/kculture"><i class="metismenu-icon"></i>K-CULTURE</a>
						</li>
                        <li>
							<a href="/admin/guide"><i class="metismenu-icon"></i>GUIDE</a>
						</li>
                        <li>
							<a href="/admin/board?wr_board=event"><i class="metismenu-icon"></i>EVENT</a>
						</li>							
                        <li>
							<a href="/admin/terms?lang=ko"><i class="metismenu-icon"></i>TERMS OF USE</a>
						</li>							
                        <li>
							<a href="/admin/policy?lang=ko"><i class="metismenu-icon"></i>PRIVATE POLICY</a>
						</li>
                        <li>
							<a href="/admin/banner"><i class="metismenu-icon"></i>배너관리</a>
						</li>							
					</ul>
				</li>
                
                <li>
                <a href="/" target="_blank">쇼핑몰</a>
                </li>	
				
			</ul>
		</div>
	</div>
</div> 
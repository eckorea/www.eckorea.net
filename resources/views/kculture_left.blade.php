<aside class="sidebar col-lg-3">
	<div class="widget widget-dashboard">
		<h3 class="widget-title">K-CULTURE</h3>
		<ul class="list">
			<li><a href="/mall/kculture/K-TREND">K-TREND</a></li>
			<li><a href="/mall/kculture/K-BEAUTY">K-BEAUTY</a></li>
            <li><a href="/mall/kculture/K-COOK">K-COOK</a></li>
            <li><a href="/mall/kculture/kstudy">K-STUDY</a></li>
		</ul>
	</div>
<!-- End .widget --> 
</aside>
<!-- End .col-lg-3 --> 
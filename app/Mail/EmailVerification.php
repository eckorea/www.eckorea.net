<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;

	public $veri_url, $sub_title;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($veri_url, $sub_title = 'VERIFICATION')
    {
        //
		$this->veri_url = $veri_url;
		$this->sub_title = $sub_title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
		->subject('ECKOREA VERIFICATION MAIL')
		->from(['address'=>'no-reply@eckorea.net', 'name'=>'ECKOREA'])
		->view('emails.email_verification');
    }
}

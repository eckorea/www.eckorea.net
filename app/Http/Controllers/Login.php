<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Login extends Controller
{
    public function login(Request $request)
	{
		$email = $request->input('email');
		$password = $request->input('password');
		$is_exist = DB::table('members')->where([
			['email', '=', $email],
			['password', '=', $password],
		])->exists();
		
		if($is_exist)
		{
			echo "계정정보가 일치합니다";		
		}
		else
		{
			echo "계정정보가 일치하지 않습니다";
		}
	}
}

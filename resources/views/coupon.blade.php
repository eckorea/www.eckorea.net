@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MY ACCOUNT</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>COUPON</h2>

						<div class="row">
							<div class="col-md-12">	
								<div class="table-responsive basic">
									<table class="table table-striped table-hover ">
										<thead>
										<tr>
											<th class="text-center">@lang('common.coupons')</th>
											<th class="text-center">@lang('common.amount')</th>
											<th class="text-center">@lang('common.receive_date')</th>
											<th class="text-center">@lang('common.state')</th>
										</tr>
										</thead>
										<tbody>
                                        @foreach($data as $row)
                                        <tr>	
											<td>{{$row->cp_name}}</td>
											<td>${{$row->cp_amount}}</td>
											<td>{{date("Y-m-d",strtotime($row->created_at))}}</td>
											<td>
                                            @if($row->cp_used_date)
												사용완료<br>
												{{date("Y-m-d",strtotime($row->cp_used_date))}}
                                            @endif
											</td>
										</tr>
                                        @endforeach
										
										</tbody>
									</table>
								</div>  
							</div>
							<!-- End .col-md-6 --> 
						</div>
						
						
					</div>

					<!-- End .col-lg-9 -->

					@include('mypage_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

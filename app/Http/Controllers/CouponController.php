<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Coupon $coupon, Request $request)
    {
        $qs = $request->query(); 
		
		$qkind = array_key_exists('qkind',$qs) ? $qs['qkind'] : '';
		$q = array_key_exists('q',$qs) ? $qs['q'] : '';
				
		if($qkind == 'email') $data = $coupon->where('mb_email','LIKE',$q.'%');			
		else $data = $coupon;
		
		$data = $data
		->orderBy('created_at', 'desc')
		->paginate(5);		
        return view('/admin/coupon')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Coupon::create([		
		'mb_email' => $request->input('cp_email'),
		'cp_name' => $request->input('cp_name'),
		'cp_amount' => $request->input('cp_amount')
		]);
				
		return redirect('/admin/coupon');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        $data = $message		
		->where('id', $id)
		->get();
		return view('/admin/coupon_view')->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon, $id)
    {
        $deletedRows = $coupon->where('id', $id)->delete();
		return redirect('/admin/coupon');
    }
}

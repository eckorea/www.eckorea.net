<?php
namespace App\Libraries;
use Illuminate\Support\Facades\DB;

class PayErom
{
	/**
	 * real
	 * https://payment.erompay.com
	 * $merchant_id 	= '21030001'
	 * $merchant_pw 	= 'whbsb5gocjiglwj3'
	 * $transaction_key = 'vpxpwhgocjiglwjj';
	 */

	/**
	 * develop
	 * https://paymenttest.erompay.com
	 * $merchant_id 	= '21010001'
	 * $merchant_pw 	= '14mf3nngmmyrmesu'
	 * $transaction_key = '691idugmmyrmesv1';
	 */

	private $production			= 'real';	
	private $test_host 			= 'https://paymenttest.erompay.com';
	private $real_host 			= 'https://payment.erompay.com';
    private $merchant_id 		= '21030001';
	private	$merchant_pw 		= 'whbsb5gocjiglwj3';
	private	$transaction_key 	= 'vpxpwhgocjiglwjj';
	/* 개발용
	private $merchant_id 		= '21010001';
	private	$merchant_pw 		= '14mf3nngmmyrmesu';
	private	$transaction_key 	= '691idugmmyrmesv1';
	*/
    private $aPayPostUrl        = Array();
	private $cApprovalUrl       = 'https://www.eckorea.net/order/payment/domestic/approve'; // 결제 성공 URL
	private $cApprovalUrl2       = 'https://www2.eckorea.net/order/payment/domestic/approve'; // 결제 성공 URL
    /* 개발용
    private $cApprovalUrl       = 'http://eckorea.test/order/payment/domestic/approve'; // 결제 성공 URL
    */

    private $cHeaderInfo        = Array();
	
    public function __construct()
	{
		if($this->production == 'real') $pay_host = $this->real_host; else $pay_host = $this->test_host;			
        $this->aPayPostUrl = Array(
            'no_verify_pay'		=> $pay_host.'/payment/sale',    //비인증결제
			'verify_card_pay'	=> $pay_host.'/v2/payment/card/auth',  //인증결제 카드
			'refund'			=> $pay_host.'/payment/refund',    //환불
			'exchange'			=> $pay_host.'/exchange'    //환율
            );
    }
			
	public function approvePayment($card_number, $card_month, $card_year, $card_cvv, $card_holder, $order_id, $processing_amount)
	{			
		$card_holder = urlencode($card_holder);		
		$transaction_type = '0040';		
					
		$encryption    = [
			'encryption_code'   => $this->hashEncryption($this->merchant_id, $this->merchant_pw, $this->transaction_key, $transaction_type, $order_id, $processing_amount)
			,'card_number'      => urlencode($this->cardEncryption($card_number, $this->transaction_key))
			,'card_month'       => urlencode($this->cardEncryption($card_month, $this->transaction_key))
			,'card_year'        => urlencode($this->cardEncryption($card_year, $this->transaction_key))
			,'card_cvv'         => urlencode($this->cardEncryption($card_cvv, $this->transaction_key))
		];
				
		$aPostData = "version=2&transaction_type=$transaction_type&merchant_id={$this->merchant_id}&order_id=$order_id&product_name=product%20name&processing_currency=KRW&processing_amount=$processing_amount&card_number={$encryption['card_number']}&card_year={$encryption['card_year']}&card_month={$encryption['card_month']}&card_cvv={$encryption['card_cvv']}&card_holder=$card_holder&encryption_code={$encryption['encryption_code']}";		
		
		$aSendData = Array(
		'sUrl'  => $this->aPayPostUrl['no_verify_pay'],
		'sData' => $aPostData
		);

		$aResult = $this->initCurl($aSendData);
		return $aResult;		
	}
	
	public function domesticPayment($customer_id, $card_code, $installment_months, $order_id, $processing_amount, $product_name, $od_coupon, $order_type)
	{
		//$processing_amount = 1000;					
		$transaction_type = '0040';					
		$encryption    = [
			'encryption_code'   => $this->hashEncryption($this->merchant_id, $this->merchant_pw, $this->transaction_key, $transaction_type, $order_id, $processing_amount)			
		];
		
		//$custom_field = array('custom_field1'=>$od_coupon,'cusrom_field2'=>$order_type);
		//$json_custom_field = json_encode($custom_field);

		if (strpos($_SERVER['SERVER_NAME'], "www2") !== false)
			$return_url = $this->cApprovalUrl2;
		else
			$return_url = $this->cApprovalUrl;

		$aPostData = array(
		'version'=>2,
		'transaction_type'=> $transaction_type,
		'merchant_id'=> $this->merchant_id,
		'order_id'=> $order_id,
		'product_name'=> $product_name,
		'processing_currency'=> 'KRW',
		'processing_amount'=> $processing_amount,
		'card_code'=> $card_code,
		'installment_months'=> $installment_months,
		'return_url'=> $return_url,
		'encryption_code'=> $encryption['encryption_code'],
		'card_code'=> $card_code,
		'customer_ip'=> $_SERVER['REMOTE_ADDR'],
		'customer_id'=> $customer_id
		//"custom_fields['custom_field1']"=> $od_coupon,
		//"user_details['phone']"=> '01023020'
		);
		
		$aSendData = Array(
		'sUrl'  => $this->aPayPostUrl['verify_card_pay'],
		'sData' => $aPostData
		);

		$aResult = $this->initCurl2($aSendData);
		
		return $aResult;	
	}
	
	public function refundPayment($transaction_id, $order_id, $processing_amount, $pay_for)
	{
		//시작 로그
		$log_result = payLogger($order_id, 'refund', $pay_for, $processing_amount, $transaction_id, '', 'ready', '');
				
		$transaction_type = '0050';							
		$encryption    = [
			'encryption_code'   => $this->hashEncryption($this->merchant_id, $this->merchant_pw, $this->transaction_key, $transaction_type, $order_id, $processing_amount)			
		];
		
		$aPostData = "version=2&transaction_type=$transaction_type&transaction_id=$transaction_id&merchant_id={$this->merchant_id}&order_id=$order_id&processing_currency=KRW&processing_amount=$processing_amount&encryption_code={$encryption['encryption_code']}";		
		
		$aSendData = Array(
		'sUrl'  => $this->aPayPostUrl['refund'],
		'sData' => $aPostData
		);

		$aResult = $this->initCurl($aSendData);
				
		//결과값 결제로그 삽입	
		$arr_pay_result = json_decode($aResult,true);
		$pay_tid = $arr_pay_result['transaction_id'];
		$pay_result_code = $arr_pay_result['result_code'];
		$log_result = payLogger($order_id, 'refund', $pay_for, $processing_amount, $transaction_id, $pay_result_code, 'done', $aResult);
		
		return $aResult;		
	}
	
	public function getRate($curr1, $curr2)
	{
		$transaction_type = '0810';		
		
		$aPostData = "version=2&transaction_type=$transaction_type&merchant_id={$this->merchant_id}&display_currency={$curr1}&processing_currency={$curr2}&display_amount=1";		
		
		$aSendData = Array(
		'sUrl'  => $this->aPayPostUrl['exchange'],
		'sData' => $aPostData
		);

		$aResult = $this->initCurl($aSendData);

		return $aResult;		
	}
	
    public function initCurl($aData)
    {	
		/*
        $aPostData = '';
        foreach( $aData['sData'] As $sKey => $val )
        {
            $aPostData .= $aPostData == '' ? $sKey.'='.urlencode($val) : '&'.$sKey.'='.urlencode($val);
        }*/
		
		$aPostData = $aData['sData'];
		
        $curl = null;
        $curl = curl_init();
		//curl_setopt( $curl, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt( $curl, CURLOPT_URL, $aData['sUrl'] );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $aPostData );
		curl_setopt( $curl, CURLOPT_POST, true );   
		//curl_setopt( $curl, CURLOPT_HEADER, true);
		
		
        $gData = curl_exec( $curl );
        curl_close($curl);		
        return $gData;
    }
	
	public function initCurl2($aData)
    {	
		
        $aPostData = '';
        foreach( $aData['sData'] As $sKey => $val )
        {
            $aPostData .= $aPostData == '' ? $sKey.'='.urlencode($val) : '&'.$sKey.'='.urlencode($val);
        }		
		
        $curl = null;
        $curl = curl_init();
		//curl_setopt( $curl, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt( $curl, CURLOPT_URL, $aData['sUrl'] );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $aPostData );
		curl_setopt( $curl, CURLOPT_POST, true );   
		//curl_setopt( $curl, CURLOPT_HEADER, true);
		
		
        $gData = curl_exec( $curl );
        curl_close($curl);		
        return $gData;
    }
    
	//Encryption
	public static function hashEncryption($merchant_id, $merchant_pw, $transaction_key, $transaction_type, $order_id, $processing_amount){
		$match = null;
		preg_match_all('/[a-fA-F0-9]/', $order_id, $match);
		//$hex = substr(implode($match[0], ''), -8);
		$hex = substr(implode('', $match[0]), -8);
		
		$validationNo = abs(hexdec($hex) % 6) + 1;
		switch($validationNo){
	        case '1':
	               $validationStr = $merchant_pw.$transaction_key.$transaction_type.$merchant_id.$order_id.$processing_amount;
	               break;
	        case '2':
	               $validationStr = $processing_amount.$merchant_pw.$transaction_key.$transaction_type.$merchant_id.$order_id;
	               break;
	        case '3':
	               $validationStr = $order_id.$processing_amount.$merchant_pw.$transaction_key.$transaction_type.$merchant_id;
	               break;
	        case '4':
	               $validationStr = $merchant_id.$order_id.$processing_amount.$merchant_pw.$transaction_key.$transaction_type;
	               break;
	        case '5':
	               $validationStr = $transaction_type.$merchant_id.$order_id.$processing_amount.$merchant_pw.$transaction_key;
	               break;
	        case '6':
	               $validationStr = $transaction_key.$transaction_type.$merchant_id.$order_id.$processing_amount.$merchant_pw;
	               break;
		}

		$enctyprionCd = null;
		$validationNo = abs(hexdec($hex) % 2) + 1;
		switch($validationNo){
	        case '1':
	               $enctyprionCd = hash( 'sha256', hash('sha256', $merchant_pw.$validationStr).$transaction_key );
	               break;
	        case '2':
	               $enctyprionCd = hash( 'sha256', hash('sha256', $transaction_key.$validationStr).$merchant_pw );
	               break;
		}
		
		return $enctyprionCd;
	}

	public static function cardEncryption($data, $transaction_key){
		return base64_encode(openssl_encrypt($data, "AES-128-ECB", $transaction_key, OPENSSL_RAW_DATA));
	}
	
	public static function cardDecryption($data, $transaction_key){
		return openssl_decrypt(base64_decode($data), "AES-128-ECB", $transaction_key, OPENSSL_RAW_DATA);
	}
		
	public function hard()
	{	
		$merchant_id = '21010001';
		$merchant_pw = '14mf3nngmmyrmesu';
		$transaction_key = '691idugmmyrmesv1';
		$transaction_type = '0040';
		$order_id = '123456778';
		$processing_amount = '1000';
						
		$encryption    = [
        'encryption_code'   => ''
        ,'card_number'      => ''
        ,'card_month'       => ''
        ,'card_year'        => ''
        ,'card_cvv'         => ''
		];
				
		$encryption    = [
			'encryption_code'   => $this->hashEncryption($merchant_id, $merchant_pw, $transaction_key, $transaction_type, $order_id, $processing_amount)
			,'card_number'      => urlencode($this->cardEncryption($card_number, $transaction_key))
			,'card_month'       => urlencode($this->cardEncryption($card_month, $transaction_key))
			,'card_year'        => urlencode($this->cardEncryption($card_year, $transaction_key))
			,'card_cvv'         => urlencode($this->cardEncryption($card_cvv, $transaction_key))
		];
		
		
		/*
		
		$encryption    = [
			'encryption_code'   => $this->hashEncryption($merchant_id, $merchant_pw, $transaction_key, $transaction_type, $order_id, $processing_amount)
			,'card_number'      => 'imTEVdsilyZEa1TPxsnxvSc2UIdN7XNuZxIoPt6PcQA='
			,'card_month'       => 'oOVZLBaHsBUvZvWeM7nsfw=='
			,'card_year'        => '8vnZFoSdz6MNj9v2CFmi2Q=='
			,'card_cvv'         => 'LGMMh3gIWS/y6t9qSTECg=='
		];
		
		*/
		
		
		//$enc = $this->hashEncryption($merchant_id, $merchant_pw, $transaction_key, $transaction_type, $order_id, $processing_amount);
				
		$aPostData = "version=2&transaction_type=$transaction_type&merchant_id=$merchant_id&order_id=$order_id&product_name=product%20name&processing_currency=KRW&processing_amount=$processing_amount&card_number={$encryption['card_number']}&card_year={$encryption['card_year']}&card_month={$encryption['card_month']}&card_cvv={$encryption['card_cvv']}&card_holder=$card_holder&encryption_code={$encryption['encryption_code']}";
		
		//echo $aPostData; 
		
		/*$aPostData = "version=2&transaction_type=$transaction_type&merchant_id=$merchant_id&order_id=$order_id&product_name=product%20name&processing_currency=USD&processing_amount=$processing_amount&card_number=HlcvitFp81%2BT4T3grJBXols4OyRhCOjTjof0j5l7Quw%3D&card_year=qZmOoqthjgyZBAZaGxHoDA%3D%3D&card_month=nqjcVk%2B2Opco6G1JLWZXCA%3D%3D&card_cvv=YedrH3tZ33m9KQ8CD252hg%3D%3D&card_holder=test%20card%20holder&encryption_code={$encryption['encryption_code']}";
		
		echo "<br><br>".$aPostData;exit;*/
				
		$aSendData = Array(
		'sUrl'  => $this->aPayPostUrl['no_verify_pay'],
		'sData' => $aPostData
		);

		$aResult = $this->initCurl($aSendData);
		
		echo "RESULT:".$aResult;
		
	}
	
	public function hard_domestic()
	{				
		$merchant_id = '21010001';
		$merchant_pw = '14mf3nngmmyrmesu';
		$transaction_key = '691idugmmyrmesv1';
		$transaction_type = '0040';
		$order_id = '123456778';
		$processing_amount = 1000+rand(1,5);
		$transaction_token = '1sakjdhaskdj9812823';
		$product_name = 'pdname';
		$installment_months = '00';
		$return_url = 'http://www.eckorea.net/payment/approve';
		$card_code = '361';
			
		$encryption    = [
			'encryption_code'   => $this->hashEncryption($merchant_id, $merchant_pw, $transaction_key, $transaction_type, $order_id, $processing_amount)			
		];
		
		$aPostData = array(
		'version'=>2,
		'transaction_type'=> $transaction_type,
		'merchant_id'=> $merchant_id,
		'order_id'=> $order_id,
		'product_name'=> $product_name,
		'processing_currency'=> 'KRW',
		'processing_amount'=> $processing_amount,
		'transaction_token'=> $transaction_token,
		'card_code'=> $card_code,
		'installment_months'=> $installment_months,
		'return_url'=> $return_url,
		'encryption_code'=> $encryption['encryption_code'],
		'card_code'=> $card_code,
		'customer_ip'=> $_SERVER['REMOTE_ADDR'],
		'customer_id'=> 'test'
		);
				
		$aSendData = Array(
		'sUrl'  => $this->aPayPostUrl['verify_card_pay'],
		'sData' => $aPostData
		);

		$aResult = $this->initCurl2($aSendData);
		
		//return $aResult;
		echo "RESULT:".stripcslashes($aResult);
		
	}
}
?>

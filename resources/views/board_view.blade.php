@include('top')

<body>
<div class="page-wrapper">
    <main class="main">
        <nav aria-label="breadcrumb" class="breadcrumb-nav">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
                </ol>
            </div>
            <!-- End .container --> 
        </nav>
        <div class="container">
            <div class="row">
                <div class="col-lg-9 order-lg-last dashboard-content">
                    <h2>{{$data[0]->wr_title}}</h2>
                    <div class="row pt-3">
                        <div class="col-sm-6 col-lg-12">
                            <div class="widget"> {!!$data[0]->wr_content!!} </div>
                            <!-- End .widget --> 
                        </div>
                        <!-- End .col-sm-6 --> 
                        
                    </div>
                    <!-- End .row --> 
                    
                </div>
                
                <!-- End .col-lg-9 --> 
                
                @include('cscenter_left') </div>
            <!-- End .row --> 
        </div>
        <!-- End .container -->
        
        <div class="mb-5"></div>
        <!-- margin --> 
    </main>
    <!-- End .main --> 
</div>
<!-- End .page-wrapper --> 
<script>
function notice_delete(wr_id)
{
	if(confirm('Delete?'))
	{
		location.href = '/notice_delete/'+wr_id;
	}
}

function notice_modify(wr_id)
{
	location.href = '/notice_edit/'+wr_id;
}
</script> 
@include('footer')
</body>

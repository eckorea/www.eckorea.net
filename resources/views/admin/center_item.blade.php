@include('admin.top')
<div class="app-main"> 
	@include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>입고관리</div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <form method="get">
                                <select name="qkind" class="select_css">                                   
                                    <option value="od_id" @if(Request::query('qkind') == 'od_id') selected @endif>주문번호</option>
                                    <option value="mb_email" @if(Request::query('qkind') == 'mb_email') selected @endif>주문인</option>
                                    <option value="it_name" @if(Request::query('qkind') == 'it_name') selected @endif>상품명</option>                                    
                                </select>
                            </div>
                            <input type="text" class="form-control" name="q" value="{{Request::query('q')}}" placeholder="검색어">                                       
                            
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">검색</button>								
                            </div>
                            <div class="input-group-append">
                                &nbsp;<button type="button" class="btn btn-secondary" onclick="location.href='/admin/warehouse?status={{Request::query('status')}}';">초기화</button>								
                            </div>
                            
                            <div>
                            <style>
							.warehouse_rdo_list li, .warehouse_rdo_list li label
							{
								display:inline-block;								
							}
							</style>
                            <ul class="warehouse_rdo_list">
                            <li><input type="radio" id="warehouse_wait" name="status" value="buy_done" @if($query['status'] == 'buy_done') checked="checked" @endif /> <label for="warehouse_wait">입고대기</label></li>
                            <li><input type="radio" id="warehouse_stock" name="status" value="warehouse_stock" @if($query['status'] == 'warehouse_stock') checked="checked" @endif /> <label for="warehouse_stock">입고확인</label></li>
                            <li><input type="radio" id="warehouse_hold" name="status" value="warehouse_hold" @if($query['status'] == 'warehouse_hold') checked="checked" @endif /> <label for="warehouse_hold">입고보류</label></li>
                            <li><input type="radio" id="warehouse_cancel" name="status" value="warehouse_cancel" @if($query['status'] == 'warehouse_cancel') checked="checked" @endif /> <label for="warehouse_cancel">부분취소</label></li>
                            </ul>          
                            
                            
                            </div>
                         </form>                
                         
                           					
                        </div>                      
                        <button onclick="location.href='/admin/exceldown/order';" style="margin-left:20px;">발주서 다운로드</button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
										<th class="text-center"><input type="checkbox" onclick="warehouse_chk_all();"></th>
                                        <th class="text-center">이미지</th>
                                        <th class="text-center">상품정보</th>
                                        <th class="text-center">주문번호</th>
                                        <th class="text-center">입고상태</th>
                                        <th class="text-center">랙번호</th>
                                        <th class="text-center">주문인</th>
                                        <th class="text-center">신청일</th>
                                    </tr>
                                </thead>
                                <tbody>     
                                @foreach($data as $row)      
                                
                                @php
                                //json decode
                                $arr_status_time = json_decode($row->ct_house_status_time_json, true);
                                $qstatus = Request::query('status');
                                $arr_status_memo = json_decode($row->ct_house_memo_json,true);
                                @endphp                                
                                
									<tr>
										<td><input type="checkbox" class="ct_id" value="{{$row->ct_id}}"></td>
										<td class="center_img"><img src="{{$row->it_thumbnail_url}}" style="width:200px;"></td>
										<td class="center_name">
											[{{convOriginToText($row->ct_origin)}}]<br>
											{{$row->it_name}} <br>
											<span>{{$row->it_option}}</span> <br>
											<p>{{$row->ct_qty}}개</p>
										</td>
										<td><a href="/admin/order/view/{{$row->od_id}}">{{$row->od_id}}</a></td>
                                        <td>
                                        {{$row->ct_house_status}}
                                        @if(isset($arr_status_time[$qstatus]))
                                        <br />
                                        {{date("Y-m-d",strtotime($arr_status_time[$qstatus]))}}
                                        @endif
                                        </td>
                                        <td>{{$row->ct_rack_no}}</td>
										<td>{{$row->mb_email}}</td>
										<td>{{$row->created_at}}</td>
									</tr>  
									<tr>
										<td colspan="8">입고메모<input type="text" class="form-control ct_house_memo_{{$row->ct_id}}" name="ct_house_memo" value="@if(isset($arr_status_memo['warehouse'])) {{$arr_status_memo['warehouse']}} @endif"></td>
									</tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        
                         <nav class="toolbox toolbox-pagination">
							<ul class="pagination">
								{{ $data->links() }}  
								<!--
								<li class="page-item disabled"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a> </li>
								<li class="page-item active"> <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a> </li>
								<li class="page-item"><a class="page-link" href="#">2</a></li>
								<li class="page-item"><a class="page-link" href="#">3</a></li>
								<li class="page-item"><a class="page-link" href="#">4</a></li>
								<li class="page-item"><a class="page-link" href="#">5</a></li>
								<li class="page-item"> <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a> </li>
								-->
							</ul>
						</nav>
                                                
                    </div>
                </div>
            </div>
			<button class="mb-2 mr-2 btn btn-primary" onclick="update_warehouse_status('warehouse_stock');">검수/패킹으로 넘기기</button>
			<button class="mb-2 mr-2 btn btn-secondary" onclick="update_warehouse_status('warehouse_hold');">입고보류</button>
        </div>
        @include('admin.footer') 
	</div>
</div>
<script>
function warehouse_chk_all()
{
	$(".ct_id").attr("checked","checked");
}
function update_warehouse_status(status)
{
	var method = 'warehouse';	
	
	if($(".ct_id:checked").length == 0) alert('변경하려는 상품에 체크를 해주세요');
	
	$(".ct_id:checked").each(function(index, element) {

        var ct_id = $(this).val();
		var warehouse_memo = $(".ct_house_memo_"+ct_id).val();
	
		//AJAX
		$.ajax({
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: 'POST',
		async:false,
		url: '/admin/status/update',
		data: {method:method, ct_id : ct_id, status : status, ct_house_memo:warehouse_memo},
		dataType: 'json',
		success: function(data) {
			console.log(data.msg);
			console.log(ct_id + '//'+status);
		},
		error: function(data) {
			alert("error" +data);
	  	}
		}).done(function()
		{
			location.reload();		
		}); //ajax end
	});
}

$(document).ready(function(e) {    
	
});
</script>
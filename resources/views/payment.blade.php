@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
            <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PAYMENTS</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="container">
               
                <div class="row">
                    <div class="col-lg-8">
                        <ul class="checkout-steps">
                            <li>
								<div class="blank20"></div>
                                <h2 class="step-title">Payments</h2>                                
								
                                <form action="#">
                                    <div class="form-group2">
                                        ORDER <span style="color:#000; font-size:17px">$ 18</span> + SHIPPING <span style="color:#000; font-size:17px">$ 2</span>
										= TOTAL <span style="color:#f00; font-size:17px; font-weight:bold">$ 20</span>
                                    </div><!-- End .form-group -->
                                  
                                    <div class="form-group">
                                        <label>COUPON</label>
                                        <div class="select-custom">
                                            <select class="form-control">
                                                <option> == 나의 쿠폰 목록 ==</option>
                                                
                                            </select>
                                        </div><!-- End .select-custom -->
                                    </div><!-- End .form-group -->

                                   
                                </form>
                            </li>                           
                        </ul>
                    </div><!-- End .col-lg-8 -->

                    <div class="col-lg-4">
                        <div class="order-summary">
                            <h3>Summary</h3>

                            <div class="" >
                                <table class="table table-mini-cart">
                                    <tbody>
                                        <tr>
                                            <td class="product-col">
                                                <figure class="product-image-container">
                                                    <a href="mall_view" class="product-image">
                                                        <img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg" alt="product">
                                                    </a>
                                                </figure>
                                                <div>
                                                    <h2 class="product-title">
                                                        <a href="mall_view">퍼실 딥클린 테크놀로지 라벤더젤 드럼용 리필</a>
                                                    </h2>

                                                    <span class="product-qty">Qty: 4 / $ 15</span>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="product-col">
                                                <figure class="product-image-container">
                                                    <a href="mall_view" class="product-image">
                                                        <img src="https://thumbnail6.coupangcdn.com/thumbnails/remote/230x230ex/image/retail/images/2019/05/11/3/0/adbcf29b-9468-4a85-8420-10c5e053db15.jpg" alt="product">
                                                    </a>
                                                </figure>
                                                <div>
                                                    <h2 class="product-title">
                                                        <a href="mall_view">퍼실 딥클린 테크놀로지 라벤더젤 드럼용 리필</a>
                                                    </h2>

                                                    <span class="product-qty">Qty: 4 / $ 15</span>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>    
                                </table>
                            </div><!-- End #order-cart-section -->
                        </div><!-- End .order-summary -->

						<div class="checkout-info-box">
                            <h3 class="step-title">Ship To
                                <a href="#" title="Edit" class="step-title-edit"></a>
                            </h3>

                            <address>
                                Desmond Mason <br>
                                123 Street Name, City, USA <br>
                                Los Angeles, California 03100 <br>
                                United States <br>
                                (123) 456-7890
                            </address>
                        </div><!-- End .checkout-info-box -->

                        <div class="checkout-info-box">
                            <h3 class="step-title">Shipping Method
                                <a href="#" title="Edit" class="step-title-edit"></a>
                            </h3>

                            <p>DHL AIR</p>
                        </div><!-- End .checkout-info-box -->
                    </div><!-- End .col-lg-4 -->
                </div><!-- End .row -->

                <div class="row">
                    <div class="col-lg-8">
                        <div class="checkout-steps-action">
                            <a href="checkout-review.html" class="btn btn-primary float-right">NEXT</a>
                        </div><!-- End .checkout-steps-action -->
                    </div><!-- End .col-lg-8 -->
                </div><!-- End .row -->
            </div><!-- End .container -->

            <div class="mb-6"></div><!-- margin -->
        </main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	@include('footer')

</body>

        
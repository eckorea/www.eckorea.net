@include('admin.top')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>
<style>
.ck-editor__editable {
    min-height: 500px;
}
</style>
<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>게시판</div>
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="tab-content">                            
							<div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                            <form method="post" action="/admin/board/edit" onsubmit="return before_board_submit();">
                             {{ csrf_field() }}
                            <input type="hidden" name="wr_board" value="{{Request::query('wr_board')}}" />
                            <input type="hidden" name="wr_id" value="@if(isset($data[0])){{$data[0]->wr_id}}@endif" />
                            <input type="hidden" id="wr_contents" name="wr_contents" value="" />
                            
								<div class="card-body">
                                <!--										  
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">GUIDE GROUP</label>
										<div class="col-sm-10"><input class="form-control" name="guide_group" value="@if(isset($data[0])){{$data[0]->guide_group}}@endif" ></div>
									</div>-->                                    
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">TITLE</label>
										<div class="col-sm-10"><input class="form-control" name="wr_title" value="{{$title}}" ></div>
									</div>
                                    
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">CONTENTS</label>
										<div class="col-sm-10">
                                        <div id="toolbar-container">
                                        </div>
                                        
                                        <div id="toolbar-container"></div>
                                        <div id="editor" style="border:solid 1px;">
                                           {!!$contents!!}
                                        </div>                                        
                                        </div>
									</div>                      					
									
									<button class="mb-2 mr-2 btn btn-light" type="button"><a href="/admin/board?wr_board={{Request::query('wr_board')}}">목록</a></button>                                       
                                    <button class="mb-2 mr-2 btn btn-light" type="submit">저장</button>
								</div>
                             </form>									
							</div>
						</div>						
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>
<script>
class UploadAdapter {
    constructor(loader) {
        this.loader = loader;
    }

    upload() {
        return this.loader.file.then( file => new Promise(((resolve, reject) => {
            this._initRequest();
            this._initListeners( resolve, reject, file );
            this._sendRequest( file );
        })))
    }

    _initRequest() {
        const xhr = this.xhr = new XMLHttpRequest();
        xhr.open('POST', '/editor/upload', true);
        xhr.responseType = 'json';
    }

    _initListeners(resolve, reject, file) {
        const xhr = this.xhr;
        const loader = this.loader;
        const genericErrorText = '파일 업로드에 실패했습니다.'

        xhr.addEventListener('error', () => {reject(genericErrorText)})
        xhr.addEventListener('abort', () => reject())
        xhr.addEventListener('load', () => {
            const response = xhr.response
            if(!response || response.error) {
                return reject( response && response.error ? response.error.message : genericErrorText );
            }
			console.log(response);
            resolve({
                default: response.url //업로드된 파일 주소
            })
        })
    }

    _sendRequest(file) {
        const data = new FormData()
        data.append('upload',file)
		this.xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}');
        this.xhr.send(data);		
    }
}

function MyCustomUploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        return new UploadAdapter(loader)
    }
}

var myEditor;

DecoupledEditor
  .create( document.querySelector( '#editor' ),
  {
	  extraPlugins: [MyCustomUploadAdapterPlugin]
  })
  .then( editor => {
	 const toolbarContainer = document.querySelector( '#toolbar-container' ); 
	 toolbarContainer.appendChild( editor.ui.view.toolbar.element );
	 myEditor = editor;
  } )
  .catch( error => {
	 console.error( error );
  } );

function before_board_submit()
{
	var contents = myEditor.getData();
	console.log(contents);
	$("#wr_contents").val(contents);
}
</script>
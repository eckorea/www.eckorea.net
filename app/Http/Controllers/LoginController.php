<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use	Illuminate\Support\Facades\Cookie;

class LoginController extends Controller
{
    public function login(Request $request)
	{
		$return_url = $request->input('return_url');
		$email = $request->input('email');
		$password = $request->input('password');
		$user_data = DB::table('members')->where([
			['mb_email', '=', $email]			
		])->get();
		
		if(!isset($user_data[0])) //아이디 자체가 검색이 안되었을때
		{
			return redirect()->back()->with('jsAlert', '입력하신 정보가 일치하지 않습니다. 다시 확인하여 주십시오.\\nIncorrect ID or Password. Please retry.');
		}
				
		//이메일 인증 확인
		$mb_verified = $user_data[0]->mb_verified;
		if($mb_verified != 'Y')
		{
			$title = __('common.need_email_verification1');
			$msg = __('common.email_not_verified');
			$btn_name = 'Resend Email';
			$btn_url = "/verify/resend?email=$email&type=join";
			
			return view('register_msg')->with([
			'title'=>$title,
			'msg'=>$msg,
			'btn_name'=>$btn_name,
			'btn_url'=>$btn_url
			]);
		}
		
		//이메일 인증 되었으면 비번 확인						
		$saved_hash = $user_data[0]->mb_password;
		if (Hash::check($password, $saved_hash)) {
   			//echo "해쉬비번맞음 (로그인처리)";
			$request->session()->put('mb_email', $email);
			$request->session()->put('mb_level', $user_data[0]->mb_level);
			
			//userkey 생성
			$chrome_auth_key = Hash::make($email.time());
			
			//DB chrome_auth_key set
			DB::table('members')->where([
			['mb_email', '=', $email]			
			])->update(['chrome_auth_key'=>$chrome_auth_key, 'chrome_auth_mktime'=>date("Y-m-d H:i:s")]);

            /**
             * 디바이스 토큰값 저장
             */
            $pushKey = $request->input('pushKey');
            $deviceUuid = $request->input('deviceUuid');
            $phoneType = $request->input('phoneType');

            if ($pushKey != null && $deviceUuid != null) {
                $device_data = DB::table('members_device')
                    ->where('pushKey', '=', $pushKey)
                    ->where('deviceUuid', '=', $deviceUuid)
                    ->get();

                if(!isset($device_data[0])) {
                    $result = DB::table('members_device')->insert([
                        'mb_email' => $email,
                        'pushKey' => $pushKey,
                        'deviceUuid' => $deviceUuid,
                        'phoneType' => $phoneType,
                        'mb_ip'=>$_SERVER['REMOTE_ADDR'],
                        'created_at'=>now(),
                        'updated_at'=>now()
                    ]);
                } else {
                    $result = DB::table('members_device')
                        ->where('pushKey', '=', $pushKey)
                        ->update([
                            'mb_ip'=>$_SERVER['REMOTE_ADDR'],
                            'updated_at'=>now()
                        ]);
                }
            }
            Cookie::forget('SBUSERKEY');
			//echo "<br> userkey : $chrome_auth_key<br>";			
			//echo "<a href='/login'>로그인페이지로</a>";				
			//Cookie::make('SBUSERKEY','ok',1);
			//echo "<br>쿠키에 설정된 값 : ".$request->cookie('SBUSERKEY');			
			//return response('',200)->cookie('SBUSERKEY', $chrome_auth_key,0);
			
			if($return_url == '') $return_url = '/';			
			//return redirect($return_url)->withCookie(cookie('SBUSERKEY', $chrome_auth_key));
			return redirect($return_url)->withCookie('SBUSERKEY', $chrome_auth_key);
			
		}
		else
		{				
			$request->session()->flush();
			return redirect()->back()->with('jsAlert', '입력하신 정보가 일치하지 않습니다. 다시 확인하여 주십시오.\\nIncorrect ID or Password. Please retry.');
		}
		
	}
	
	public function logout(Request $request)
	{
		Cookie::forget('SBUSERKEY');
		$request->session()->flush();
		return redirect('/')->withCookie('SBUSERKEY', '');
	}
}

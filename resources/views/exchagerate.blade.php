@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MY ACCOUNT</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>EXCHANGE RATE</h2>

						<div class="about-section">
							<div class="container">								
								
								<div class="exchange_table"> 
									<table>
										<col width="20%">
										<col>
										<col width="30%">
										<tr>
											<td class="gon_color"><img src="/image/team/flag_us.jpg"></a></td>
											<td class="gon_color">US Dollar</td>
											<td class="RT gon_color">1</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_ko.jpg"></a></td>
											<td>South Korea Won</td>
											<td class="RT">1,104.1</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_en.jpg"></a></td>
											<td>British Pound</td>
											<td class="RT">-</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_er.jpg"></a></td>
											<td>EURO</td>
											<td class="RT">-</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_ca.jpg"></a></td>
											<td>CANADA Dollar</td>
											<td class="RT">-</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_si.jpg"></a></td>
											<td>SINGAPORE Dollar</td>
											<td class="RT">-</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_vn.jpg"></a></td>
											<td>VIETNAMESE DONG</td>
											<td class="RT">-</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_au.jpg"></a></td>
											<td>AUSTRAILA Dollar</td>
											<td class="RT">-</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_nz.jpg"></a></td>
											<td>NEW ZEALAND Dollar</td>
											<td class="RT">-</td>
										</tr>
										<tr>
											<td><img src="/image/team/flag_mx.jpg"></a></td>
											<td>MEXICO Peso</td>
											<td class="RT">-</td>
										</tr>
									</table>
								</div>
							</div><!-- End .container -->
						</div><!-- End .about-section -->
									
						
					</div>

					<!-- End .col-lg-9 -->

					@include('mypage_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

@include('top')

<body>
    <div class="page-wrapper">
        <main class="main">            
            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Login</li>
                    </ol>
                </div><!-- End .container -->
            </nav>

            <div class="about-section">
                <div class="container">
                    <h2 class="title">{{$title}}</h2>
                    <p>
						{{$msg}}
					</p>

					<div>
                        <a href="{{$btn_url}}"><button type="button" class="btn btn-primary">{{$btn_name}}</button></a>
                    </div><!-- End .form-footer -->
                </div><!-- End .container -->
            </div><!-- End .about-section --> 
        </main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	@include('footer')
    
</body>

        
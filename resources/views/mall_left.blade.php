<aside class="sidebar-shop col-lg-3 order-lg-first">
	<div class="sidebar-wrapper">
		<div class="widget">
			<h3 class="widget-title">
				<a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true" aria-controls="widget-body-2">{{$top_ca_name}}</a>
			</h3>

			<div class="collapse show" id="widget-body-2">
				<div class="widget-body">
					<ul class="cat-list">                    
                    @foreach(DB::table($category_table)
                    ->where([
                    ['ca_parent','=',$top_ca_id]
                    ])->whereNull('deleted_at')->get() as $ca1_row)
                    <li>
						<a href="/mall/category/{{$add_param}}{{$ca1_row->ca_id}}" @if($ca_id == $ca1_row->ca_id) style="text-decoration:underline; font-weight:bold; @endif">{{$ca1_row->ca_name}}</a>
                        @foreach(DB::table($category_table)
                        ->where([                        
                        ['ca_parent','=',$ca1_row->ca_id]
                        ])->whereNull('deleted_at')->get() as $ca2_row)
                        <p><a href="/mall/category/{{$add_param}}{{$ca2_row->ca_id}}" @if($ca_id == $ca2_row->ca_id) style="text-decoration:underline; font-weight:bold; @endif">{{$ca2_row->ca_name}}</a></p>
                        @endforeach
                    </li>                                        
                    @endforeach                   
					</ul>
				</div><!-- End .widget-body -->
			</div><!-- End .collapse -->
		</div><!-- End .widget -->			
	</div><!-- End .sidebar-wrapper -->
</aside><!-- End .col-lg-3 -->
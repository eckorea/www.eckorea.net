@include('admin.top')
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/decoupled-document/ckeditor.js"></script>
<style>
.ck-editor__editable {
    min-height: 500px;
}
</style>
<div class="app-main">
	   
	@include('admin.side')

	<div class="app-main__outer">
		<div class="app-main__inner"> 
			<div class="app-page-title">
				<div class="page-title-wrapper">
					<div class="page-title-heading">
						<div>TERMS OF USE</div>
					</div>
				</div>
			</div>		
			
			<div class="row">
				<div class="col-md-12">
					<div class="main-card mb-3 card">                                    
						<div class="tab-content">                            
							<div class="tab-pane tabs-animation fade show active" id="tab-content-3" role="tabpanel">
                            <form method="post" action="/admin/terms" onsubmit="return before_terms_submit();">
                             {{ csrf_field() }}
                            <input type="hidden" name="terms_id" value="@if(isset($data[0])){{$data[0]->terms_id}}@endif" />
                            <input type="hidden" id="terms_contents" name="terms_contents" value="" />
								<div class="card-body">                                      
                                    <div class="position-relative row form-group"><label class="col-sm-2 col-form-label">TERMS Language</label>
										<div class="col-sm-10">
                                        <select name="terms_lang" onchange="return terms_chnage_lang(this);">
                                        <option value="ko" @if(Request::query('lang') == 'ko') selected="selected" @endif >KO </option>
                                        <option value="en" @if(Request::query('lang') == 'en') selected="selected" @endif>EN</option>
                                        <option value="vn" @if(Request::query('lang') == 'vn') selected="selected" @endif>VN</option>
                                        </select>
                                        </div>
									</div>                                   
									<div class="position-relative row form-group"><label class="col-sm-2 col-form-label">TERMS CONTENTS</label>
										<div class="col-sm-10">
                                        <div id="toolbar-container">
                                        </div>
                                        
                                        <div id="toolbar-container"></div>
                                        <div id="editor" style="border:solid 1px;">
                                           {!!$contents!!}
                                        </div>
                                        </div>
									</div>						
                                    <button class="mb-2 mr-2 btn btn-light" type="submit">저장</button>
								</div>
                             </form>									
							</div>
						</div>						
					</div>
				</div>
			</div>			
		</div>

		@include('admin.footer')   
	</div>
</div>
<script>
function terms_chnage_lang(e)
{
	if(confirm('언어를 변경하면, 저장되지 않은 내용은 잃게 됩니다. 계속진행하시겠습니까?'))
	{
		location.href = '?lang='+e.value;
	}
	else
	{
		return false;
	}	
}

class UploadAdapter {
    constructor(loader) {
        this.loader = loader;
    }

    upload() {
        return this.loader.file.then( file => new Promise(((resolve, reject) => {
            this._initRequest();
            this._initListeners( resolve, reject, file );
            this._sendRequest( file );
        })))
    }

    _initRequest() {
        const xhr = this.xhr = new XMLHttpRequest();
        xhr.open('POST', '/editor/upload', true);
        xhr.responseType = 'json';
    }

    _initListeners(resolve, reject, file) {
        const xhr = this.xhr;
        const loader = this.loader;
        const genericErrorText = '파일 업로드에 실패했습니다.'

        xhr.addEventListener('error', () => {reject(genericErrorText)})
        xhr.addEventListener('abort', () => reject())
        xhr.addEventListener('load', () => {
            const response = xhr.response
            if(!response || response.error) {
                return reject( response && response.error ? response.error.message : genericErrorText );
            }
			console.log(response);
            resolve({
                default: response.url //업로드된 파일 주소
            })
        })
    }

    _sendRequest(file) {
        const data = new FormData()
        data.append('upload',file)
		this.xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}');
        this.xhr.send(data);		
    }
}

function MyCustomUploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        return new UploadAdapter(loader)
    }
}

var myEditor;

DecoupledEditor
  .create( document.querySelector( '#editor' ),
  {
	  extraPlugins: [MyCustomUploadAdapterPlugin]
  })
  .then( editor => {
	 const toolbarContainer = document.querySelector( '#toolbar-container' ); 
	 toolbarContainer.appendChild( editor.ui.view.toolbar.element );
	 myEditor = editor;
  } )
  .catch( error => {
	 console.error( error );
  } );

function before_terms_submit()
{
	var contents = myEditor.getData();
	console.log(contents);
	$("#terms_contents").val(contents);
}
/*		
ClassicEditor
	.create( document.querySelector( '#terms_contents' ),
	{
		 extraPlugins: [MyCustomUploadAdapterPlugin]
		
		 
	})
	.then(function()
	{
		console.log('ckeditor loaded');
	}
	)
	.catch( error => {
		console.log( error );
	});
	*/
</script>
@include('top')

<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">MY ACCOUNT</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>SHIPPING ADDRESS</h2>
						<form method="post" action="/my/address/add">
                        {{ csrf_field() }}
                        <input type="hidden" name="addr_id" value="@if(isset($data[0]->id)) {{$data[0]->id}} @endif">
						<div class="row">
                        
							<div class="col-md-12">	
								<div class="form-group">
									<label for="">First Name</label>
									<input type="text" class="form-control" id="" name="first_name" value="@if(isset($data[0]->addr_first_name)) {{$data[0]->addr_first_name}} @endif" required>
								</div><!-- End .form-group -->
								<div class="form-group">
									<label for="">Last Name</label>
									<input type="text" class="form-control" id="" name="last_name" value="@if(isset($data[0]->addr_last_name)) {{$data[0]->addr_last_name}} @endif"  required>
								</div><!-- End .form-group -->
								<div class="form-group">
									<label for="">City</label>
									<input type="text" class="form-control" id="" name="city" value="@if(isset($data[0]->addr_city)) {{$data[0]->addr_city}} @endif" required>
								</div><!-- End .form-group -->
								<div class="form-group">
									<label>State/Province</label>
									<div class="toolbox">							
										<input type="text" name="state" class="form-control" value="@if(isset($data[0]->addr_state)) {{$data[0]->addr_state}} @endif">											
									</div><!-- End .select-custom -->
								</div><!-- End .form-group -->
                                <div class="form-group">
									<label>Addr1</label>
									<div class="toolbox">							
										<input type="text" name="addr1" class="form-control" placeholder="" value="@if(isset($data[0]->addr_1)) {{$data[0]->addr_1}} @endif">											
									</div><!-- End .select-custom -->
								</div><!-- End .form-group -->
                                <div class="form-group">
									<label>Addr2</label>
									<div class="toolbox">							
										<input type="text" name="addr2" class="form-control" placeholder="" value="@if(isset($data[0]->addr_2)) {{$data[0]->addr_2}} @endif">											
									</div><!-- End .select-custom -->
								</div><!-- End .form-group -->
								<div class="form-group">
									<label for="">Zip/Postal Code</label>
									<input type="text" class="form-control" id="" name="zipcode" value="@if(isset($data[0]->addr_zip)) {{$data[0]->addr_zip}} @endif" required>
								</div><!-- End .form-group -->
								<div class="form-group">
									<label>Country</label>
									<div class="toolbox">							
										<select name="country" class="form-control">
											<option value="">SELECT COUNTRY</option>
                                                @foreach(DB::table('define_countries')->get() as $country_row)
                                                <option value="{{$country_row->country_name}}" @if(isset($data[0])) @if($data[0]->addr_country == $country_row->country_name) selected @endif @endif">{{$country_row->country_name}}</option>                                                
                                                @endforeach     
										</select>
									</div><!-- End .select-custom -->
								</div><!-- End .form-group -->
								<div class="form-group">
									<label for="">Phone Number</label>
									<input type="text" class="form-control" id="" name="phone" value="@if(isset($data[0]->addr_phone)) {{$data[0]->addr_phone}} @endif" required>
								</div><!-- End .form-group -->
							</div>
							<!-- End .col-md-6 --> 
						</div>
						
                       

						<div class="blank10">&nbsp;</div>

						<div>
							<button type="submit" class="btn btn-primary">SAVE</button>
                            @if(isset($data[0]->id))
							<button type="button" class="btn" onClick="location.href='/my/address/delete/@if(isset($data[0]->id)) {{$data[0]->id}} @endif'">Delete</button>
                            @endif
						</div><!-- End .form-footer -->
						
						
					</div>
                     </form>

					<!-- End .col-lg-9 -->

					@include('mypage_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
</body>

@include('top')
<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">CS CENTER</li>
					</ol>
				</div><!-- End .container -->
			</nav>

            <div class="container">
                <div class="row">
                    <div class="col-lg-9 order-lg-last dashboard-content">
                        <h2>CONTACT US</h2>
                        
                        <div class="row">
							<div class="col-md-12">
								<form method="post" action="contact">
									{{ csrf_field() }}
									<div class="form-group required-field">
										<label for="contact-name">Name</label>
										<input type="text" class="form-control" id="contact-name" name="contact-name" required>
									</div><!-- End .form-group -->

									<div class="form-group required-field">
										<label for="contact-email">Email</label>
										<input type="email" class="form-control" id="contact-email" name="contact-email" required>
									</div><!-- End .form-group -->

									<div class="form-group">
										<label for="contact-phone">Phone Number</label>
										<input type="tel" class="form-control" id="contact-phone" name="contact-phone">
									</div><!-- End .form-group -->

									<div class="form-group required-field">
										<label for="contact-message">What’s on your mind?</label>
										<textarea cols="30" rows="1" id="contact-message" class="form-control" name="contact-message" required></textarea>
									</div><!-- End .form-group -->

									<div class="form-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div><!-- End .form-footer -->
								</form>
							</div><!-- End .col-md-8 -->

							<div class="col-md-4">
								<h2 class="light-title">Contact <strong>Details</strong></h2>

								<div class="contact-info">
									<div>
										<i class="icon-phone"></i>
										<p><a href="tel:">+82) 02-3433-4242 <br>+82) 02-3433-4252 </a></p>
									</div>
									
									<div>
										<i class="icon-mail-alt"></i>
										<p>
											tskim@ecplaza.net <br>
											jlim4103@ecplaza.net
										</p>
									</div>									
								</div><!-- End .contact-info -->
							</div><!-- End .col-md-4 -->
						</div><!-- End .row -->
                    </div><!-- End .col-lg-9 -->

                    <aside class="sidebar col-lg-3">
                        <div class="widget widget-dashboard">
                            <h3 class="widget-title">CS CENTER</h3>

                            <ul class="list">
								<li><a href="/notice">NOTICE</a></li>
								<li><a href="/layout/faq">FAQ</a></li>
								<li><a href="/inquiry">INQUIRY</a></li>
								<li><a href="/layout/review">REVIEW</a></li>
								<li class="active"><a href="/contact">CONTACT US</a></li>                                
                            </ul>
                        </div><!-- End .widget -->
                    </aside><!-- End .col-lg-3 -->
                </div><!-- End .row -->
            </div><!-- End .container -->

            <div class="mb-5"></div><!-- margin -->
        </main><!-- End .main -->
	</div><!-- End .page-wrapper -->

	@include('footer')
    
</body>

        
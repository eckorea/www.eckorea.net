@include('top')
<script async charset="utf-8" src="http://cdn.embedly.com/widgets/platform.js"></script>
<style>
oembed, .embedly-card-hug, .embedly-card
{
	width:100%;
	max-width:none !important;
}
</style>
<body>
	<div class="page-wrapper">
		<main class="main">
			<nav aria-label="breadcrumb" class="breadcrumb-nav">
				<div class="container">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="index.html">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">K-CULTURE</li>
					</ol>
				</div>
				<!-- End .container --> 
			</nav>
			<div class="container">
				<div class="row">
					<div class="col-lg-9 order-lg-last dashboard-content">
						<h2>{{$data[0]->kc_title}}</h2>
						
						<div class="row pt-3">
							<div class="col-sm-6 col-lg-12">
								<div class="widget">
									{!!$data[0]->kc_contents!!}							
								</div><!-- End .widget -->							
							</div><!-- End .col-sm-6 -->
							
						</div><!-- End .row -->							
						
					</div>

					<!-- End .col-lg-9 -->

					@include('kculture_left')
				</div>
				<!-- End .row --> 
			</div>
			<!-- End .container -->

			<div class="mb-5"></div>
			<!-- margin --> 
		</main>
	<!-- End .main --> 
	</div>
	<!-- End .page-wrapper --> 

@include('footer')
<script>
    document.querySelectorAll( 'oembed[url]' ).forEach( element => {
        // Create the <a href="..." class="embedly-card"></a> element that Embedly uses
        // to discover the media.
        const anchor = document.createElement( 'a' );

        anchor.setAttribute( 'href', element.getAttribute( 'url' ) );
        anchor.className = 'embedly-card';

        element.appendChild( anchor );
    } );
</script>
</body>

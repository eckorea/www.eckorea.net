<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AdminController extends Controller
{
	public function add_category(Request $request)
	{		
		$ca_parent = '0245';
		$ca_level = strlen($ca_parent) / 2;
		//0201 0202 0203 ... 0245 .... 0245 01
		//레벨단위로 split 해서 마지막 배열에 +1 한다
		$query = "select max(ca_id) WHERE ca_parent = $ca_parent";
		
		$tgt_no = (int)substr('024501',-2,2);
		$new_category_no = $tgt_no+1;				
		$new_category_str = $ca_parent.str_pad($new_category_no,2,'0',STR_PAD_LEFT);
		echo $new_category_str;
		
	}
}

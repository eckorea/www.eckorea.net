<?php

namespace App\Http\Middleware;

use Closure, Session;

class LoginCheck
{
    public function handle($request, Closure $next)
	{
		$current_url = $_SERVER['REQUEST_URI'];
		if(!session('mb_email'))
		{
			return redirect('/login?return_url='.$current_url);
		}
		return $next($request);
	}	
}

@include('admin.top')
<div class="app-main"> 
	@include('admin.side')
    <div class="app-main__outer">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading">
                        <div>
                        @if(Request::query('status') == 'R') 
                        결제대기
                        @elseif(Request::query('status') == 'Y') 
                        발송대기
                        @elseif(Request::query('status') == 'sended') 
                        발송완료
                        @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <div class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">                            
                                <form method="get">
                                <input type="hidden" name="status" value="{{Request::query('status')}}" />
                                <select name="qkind" class="select_css">                                   
                                    <option value="od_id" @if(Request::query('qkind') == 'od_id') selected @endif>주문번호</option>
                                    <option value="mb_email" @if(Request::query('qkind') == 'mb_email') selected @endif>주문인</option>                                                           
                                </select>
                            </div>
                            <input type="text" class="form-control" name="q" value="">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">검색</button>								
                            </div>
                            <div class="input-group-append">
                                &nbsp;<button type="button" class="btn btn-secondary" onclick="location.href='/admin/packing?status={{Request::query('status')}}';">초기화</button>								
                            </div>
                            </form>							
                        </div>
                        @if(Request::query('status') == 'Y') 
                        <button onclick="location.href='/admin/exceldown/send_wait';" style="margin-left:20px;">발송대기 목록 다운로드</button>
                        @elseif(Request::query('status') == 'sended') 
                        <form method="post" action="/admin/excelup/sended" enctype="multipart/form-data" style="margin-left:20px;">
                        @csrf
                        <input type="file" name="excel_file" />
                        <button type="submit" style="margin-left:20px;">발송완료 목록 업로드</button>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="main-card mb-3 card">
                        <div class="table-responsive">
                            <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                                <thead>
                                    <tr>
                                    	<th class="text-center"><input type="checkbox" onclick="shipping_chk_all();"></th>
                                        <th class="text-center">주문번호</th>    
                                        <th class="text-center">결제상태</th>                                      
                                        <th class="text-center">주문인</th>
										<th class="text-center">배송</th>
                                    </tr>
                                </thead>
                                <tbody> 
									@foreach($data as $row)
									<tr>
                                    	<td><input type="checkbox" class="od_id" value="{{$row->od_id}}"></td>
										<td class="center_order">
											{{$row->od_id}}<br>
											<a href="/admin/order/view/{{$row->od_id}}">[보기]</a>
										</td>
										<td>
                                        @if(Request::query('status') == 'R') 
                                        결제대기중
                                        @elseif(Request::query('status') == 'Y') 
                                        결제완료
                                        @endif
                                        </td>
										<td>
											{{$row->mb_email}} <br>
											<b>{{$row->ct_carrier}} {{$row->ct_ship_no}}</b>
										</td>
										<td>
											<b>{{$row->od_country}}</b> <br>
											{{$row->ct_carrier}} <br>
											{{$row->od_weight_due}}kg /	$ {{$row->od_send_cost2}}
										</td>
									</tr>  	
                                    
                                    @endforeach					
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                </div>
            </div>
            <button class="mb-2 mr-2 btn btn-secondary" onclick="update_shipping_status('sended');">발송완료</button>
        </div>
        @include('admin.footer') 
	</div>
</div>
<script>
function shipping_chk_all()
{
	$(".od_id").attr("checked","checked");
}
function update_shipping_status(status)
{
	var method = 'shipping';	
	
	if($(".od_id:checked").length == 0) alert('변경하려는 주문에 체크를 해주세요');
	
	$(".od_id:checked").each(function(index, element) {

        var od_id = $(this).val();
	
		//AJAX
		$.ajax({
		headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: 'POST',
		async:false,
		url: '/admin/status/update_order',
		data: {method:method, od_id : od_id, status : status},
		dataType: 'json',
		success: function(data) {
			console.log(data.msg);
			console.log(od_id + '//'+status);
		},
		error: function(data) {
			alert("error" +data);
	  	}
		}).done(function()
		{
			location.reload();		
		}); //ajax end
	});
}

</script>

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegist extends Mailable
{
    use Queueable, SerializesModels;

	public $veri_url;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($veri_url)
    {
        //
		$this->veri_url = $veri_url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
		->subject('ECKOREA VERIFICATION MAIL')
		->from(['address'=>'no-reply@eckorea.net', 'name'=>'ECKOREA'])
		->view('emails.email_verification.blade');
    }
}

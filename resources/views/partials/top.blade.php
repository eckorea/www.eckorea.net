<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Porto - Bootstrap eCommerce Template TEST">
    <meta name="author" content="SW-THEMES">        
  
    
    <script type="text/javascript">
        WebFontConfig = {
            google: { families: [ 'Open+Sans:300,400,600,700,800','Poppins:300,400,500,600,700' ] }
        };
        (function(d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = '/js/assets/js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>
    
    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="/css/assets/css/bootstrap.min.css">

    <!-- Main CSS File -->
    <link rel="stylesheet" href="/css/assets/css/style.min.css">
    <link rel="stylesheet" type="text/css" href="/css/assets/vendor/fontawesome-free/css/all.min.css">
</head>

<div class="page-wrapper">
	<header class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-left header-dropdowns">
					<div class="header-dropdown">
						<a href="#">USD</a>
						<div class="header-menu">
							<ul>
								<li><a href="#">EUR</a></li>
								<li><a href="#">USD</a></li>
							</ul>
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->

					<div class="header-dropdown">
						<a href="#"><img src="/image/flags/en.png" alt="England flag">ENG</a>
						<div class="header-menu">
							<ul>
								<li><a href="#"><img src="/image/flags/en.png" alt="England flag">ENG</a></li>
								<li><a href="#"><img src="/image/flags/fr.png" alt="France flag">CHN</a></li>
							</ul>
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->                       
				</div><!-- End .header-left -->

				<div class="header-right">
					<p class="welcome-msg">[apple] WELCOME! </p>

					<div class="header-dropdown dropdown-expanded">
						<a href="#">Links</a>
						<div class="header-menu">
							<ul>
								<li><a href="my-account.html">MY ACCOUNT </a></li>
								<li><a href="#">DAILY DEAL</a></li>
								<li><a href="#">MY WISHLIST </a></li>
								<li><a href="blog.html">BLOG</a></li>
								<li><a href="contact.html">Contact</a></li>
								<li><a href="login">LOG IN</a></li>
							</ul>
						</div><!-- End .header-menu -->
					</div><!-- End .header-dropown -->
				</div><!-- End .header-right -->
			</div><!-- End .container -->
		</div><!-- End .header-top -->

		<div class="header-middle sticky-header">
			<div class="container">
				<div class="header-left">
					<a href="index" class="logo">
						<img src="/image/logo.png" alt="Porto Logo">
					</a>

					<nav class="main-nav">
						<ul class="menu sf-arrows">
							<li>
								<a href="category.html" class="sf-with-ul">MALL</a>
								<div class="megamenu megamenu-fixed-width">
									<div class="row">
										<div class="col-lg-8">
										<div class="row">
											<div class="col-lg-6">
												<div class="menu-title">
													<a href="#">Variations 1<span class="tip tip-new">New!</span></a>
												</div>
												<ul>
													<li><a href="category.html">Fullwidth Banner<span class="tip tip-hot">Hot!</span></a></li>
													<li><a href="category-banner-boxed-slider.html">Boxed Slider Banner</a></li>
													<li><a href="category-banner-boxed-image.html">Boxed Image Banner</a></li>
													<li><a href="category.html">Left Sidebar</a></li>
													<li><a href="category-sidebar-right.html">Right Sidebar</a></li>
													<li><a href="category-flex-grid.html">Product Flex Grid</a></li>
													<li><a href="category-horizontal-filter1.html">Horizontal Filter1</a></li>
													<li><a href="category-horizontal-filter2.html">Horizontal Filter2</a></li>
												</ul>
											</div><!-- End .col-lg-6 -->
											<div class="col-lg-6">
												<div class="menu-title">
													<a href="#">Variations 2</a>
												</div>
												<ul>
													<li><a href="#">Product List Item Types</a></li>
													<li><a href="category-infinite-scroll.html">Ajax Infinite Scroll</a></li>
													<li><a href="category-3col.html">3 Columns Products</a></li>
													<li><a href="category.html">4 Columns Products <span class="tip tip-new">New</span></a></li>
													<li><a href="category-5col.html">5 Columns Products</a></li>
													<li><a href="category-6col.html">6 Columns Products</a></li>
													<li><a href="category-7col.html">7 Columns Products</a></li>
													<li><a href="category-8col.html">8 Columns Products</a></li>
												</ul>
											</div><!-- End .col-lg-6 -->
										</div><!-- End .row -->
									</div><!-- End .col-lg-8 -->
										<div class="col-lg-4">
											<div class="banner">
												<a href="#">
													<img src="/image/menu-banner-2.jpg" alt="Menu banner">
												</a>
											</div><!-- End .banner -->
										</div><!-- End .col-lg-4 -->
									</div>
								</div><!-- End .megamenu -->
							</li>
							<li class=""><a href="product.html" class="sf-with-ul">REVIEW</a></li>								
							<li class=""><a href="product.html" class="sf-with-ul">BLOG</a></li>						
							<li class=""><a href="product.html" class="sf-with-ul">GUIDE</a></li>						
							<li class=""><a href="product.html" class="sf-with-ul">ABOUT US</a></li>						
						</ul>
					</nav>

					<div class="header-search">
						<a href="#" class="search-toggle" role="button"><i class="icon-search"></i></a>
						<form action="#" method="get">
							<div class="header-search-wrapper">
								<div class="select-custom">
									<select id="cat" name="cat">
										<option value="4">BOOK</option>
										<option value="12">NO BOOK</option>										
									</select>
								</div><!-- End .select-custom -->
								<input type="search" class="form-control" name="q" id="q" placeholder="Search..." required>
								
								<button class="btn" type="submit"><i class="icon-search"></i></button>
							</div><!-- End .header-search-wrapper -->
						</form>
					</div><!-- End .header-search -->
				</div><!-- End .header-left -->

				<div class="header-right">
					<button class="mobile-menu-toggler" type="button">
						<i class="icon-menu"></i>
					</button>
					<div class="header-contact">
						<span>Call us now</span>
						<a href="tel:#"><strong>+000 1584 2578</strong></a>
					</div><!-- End .header-contact -->

					<div class="dropdown cart-dropdown">
						<a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
							<span class="cart-count">2</span>
						</a>

						<div class="dropdown-menu" >
							<div class="dropdownmenu-wrapper">
								<div class="dropdown-cart-products">
									<div class="product">
										<div class="product-details">
											<h4 class="product-title">
												<a href="product.html">Woman Ring</a>
											</h4>

											<span class="cart-product-info">
												<span class="cart-product-qty">1</span>
												x $99.00
											</span>
										</div><!-- End .product-details -->

										<figure class="product-image-container">
											<a href="product.html" class="product-image">
												<img src="/image/products/cart/product-1.jpg" alt="product">
											</a>
											<a href="#" class="btn-remove" title="Remove Product"><i class="icon-cancel"></i></a>
										</figure>
									</div><!-- End .product -->

									<div class="product">
										<div class="product-details">
											<h4 class="product-title">
												<a href="product.html">Woman Necklace</a>
											</h4>

											<span class="cart-product-info">
												<span class="cart-product-qty">1</span>
												x $35.00
											</span>
										</div><!-- End .product-details -->

										<figure class="product-image-container">
											<a href="product.html" class="product-image">
												<img src="/image/products/cart/product-2.jpg" alt="product">
											</a>
											<a href="#" class="btn-remove" title="Remove Product"><i class="icon-cancel"></i></a>
										</figure>
									</div><!-- End .product -->
								</div><!-- End .cart-product -->

								<div class="dropdown-cart-total">
									<span>Total</span>

									<span class="cart-total-price">$134.00</span>
								</div><!-- End .dropdown-cart-total -->

								<div class="dropdown-cart-action">
									<a href="cart.html" class="btn">View Cart</a>
									<a href="checkout-shipping.html" class="btn">Checkout</a>
								</div><!-- End .dropdown-cart-total -->
							</div><!-- End .dropdownmenu-wrapper -->
						</div><!-- End .dropdown-menu -->
					</div><!-- End .dropdown -->
				</div><!-- End .header-right -->
			</div><!-- End .container -->
		</div><!-- End .header-middle -->
	</header><!-- End .header -->
</div><!-- End .page-wrapper -->


<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->
    <div class="mobile-menu-container">
        <div class="mobile-menu-wrapper">
            <span class="mobile-menu-close"><i class="icon-cancel"></i></span>
            <nav class="mobile-nav">
                <ul class="mobile-menu">
                    <li>
                        <a href="category.html">MALL</a>
                        <ul>
                            <li><a href="category.html">Full Width Banner</a></li>
                            <li><a href="category-banner-boxed-slider.html">Boxed Slider Banner</a></li>
                            <li><a href="category-banner-boxed-image.html">Boxed Image Banner</a></li>
                            <li><a href="category.html">Left Sidebar</a></li>
                            <li><a href="category-sidebar-right.html">Right Sidebar</a></li>
                            <li><a href="category-flex-grid.html">Product Flex Grid</a></li>
                            <li><a href="category-horizontal-filter1.html">Horizontal Filter 1</a></li>
                            <li><a href="category-horizontal-filter2.html">Horizontal Filter 2</a></li>
                            <li><a href="#">Product List Item Types</a></li>
                            <li><a href="category-infinite-scroll.html">Ajax Infinite Scroll<span class="tip tip-new">New</span></a></li>
                            <li><a href="category-3col.html">3 Columns Products</a></li>
                            <li><a href="category.html">4 Columns Products</a></li>
                            <li><a href="category-5col.html">5 Columns Products</a></li>
                            <li><a href="category-6col.html">6 Columns Products</a></li>
                            <li><a href="category-7col.html">7 Columns Products</a></li>
                            <li><a href="category-8col.html">8 Columns Products</a></li>
                        </ul>
                    </li>                    
                    <li class=""><a href="product.html">REVIEW</a></li>								
					<li class=""><a href="product.html">BLOG</a></li>						
					<li class=""><a href="product.html">GUIDE</a></li>						
					<li class=""><a href="product.html">ABOUT US</a></li>	
                   
                </ul>
            </nav><!-- End .mobile-nav -->

            <div class="social-icons">
                <a href="#" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
                <a href="#" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
                <a href="#" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
            </div><!-- End .social-icons -->
        </div><!-- End .mobile-menu-wrapper -->
    </div><!-- End .mobile-menu-container -->
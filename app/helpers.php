<?php

use Carbon\Carbon;
use Goutte\Client;
use Illuminate\Support\Facades\DB;
use Symfony\Component\DomCrawler\Crawler;

function getIfSet(&$var) {
	if (isset($var)) {
		return $var;
    }
    return null;
}

function getAllStatus()
{
	$arr = array('buy_wait','buy_done','buy_hold','buy_cancel','warehouse_stock','warehouse_hold','warehouse_cancel','send_payment_wait','send_paid','sending','sended');
	return $arr;
}

function convStatusToText($status)
{
	$result = $status;
	switch($status)
	{
		case "buy_wait" :
		$result = __('common.buy_wait');
		break;
		case "buy_done" :
		$result = __('common.buy_done');
		break;
		case "buy_hold" :
		$result = __('common.buy_hold');
		break;
		case "buy_cancel" :
		$result = __('common.buy_cancel');
		break;
		case "warehouse_stock" :
		$result = __('common.warehouse_stock');
		break;
		case "warehouse_hold" :
		$result = __('common.warehouse_hold');
		break;
		case "warehouse_cancel" :
		$result = __('common.warehouse_cancel');
		break;
		case "send_payment_wait" :
		$result = __('common.send_payment_wait');
		break;
		case "send_paid" :
		$result = __('common.send_paid');
		break;
		case "sending" :
		$result = __('common.sending');
		break;
		case "sended" :
		$result = __('common.sended');
		break;
		case "paid" :
		$result = __('common.paid');
		break;			
	}
	
	return $result;
}

function convOriginToText($origin)
{
	$result = $origin;
	switch($origin)
	{
		case "SS" :
		$result = '스마트스토어';
		break;
		case "CU" :
		$result = '쿠팡';
		break;
		case "PO" :
		$result = '우체국';
		break;
		case "kyobo" :
		$result = '교보문고';
		break;					
	}	
	return $result;
}

function parserCoupang_v1($data)
{
	//$html = '[{"img":"//thumbnail9.coupangcdn.com/thumbnails/remote/492x492ex/image/product/image/vendoritem/2019/02/21/3390609386/ddd12871-c50e-4732-8218-da9898bae17b.jpg","name":"다이슨 무선 청소기 V8 카본 파이버","price":"479,000원\n            \n        \n            원","option":"색상\n                혼합색상","count":"1","shipping":"\n              무료배송\n              \n            "}]';
	//$html = '[{"img":"https://shop-phinf.pstatic.net/20201130_140/1606728612960dooTA_JPEG/7864455667332643_1417205514.jpg?type=m510","name":"카멜 듀얼모터 전동 높이조절 책상 모션데스크 PSD-1","price":"2","option":"<div class=\"ABlRhvBGMr\"><p class=\"_3HIhLg5kOy\">우드화이트 / 지방방문설치(7~15일소요)제주도서산간지역제외</p><div class=\"_2ctp4_oSla\"><div class=\"_2d9PkUsVhA\"><button type=\"button\" class=\"_3gsRID8KP2 _7DNjn2XDP0\"><span class=\"blind\">수량 빼기</span></button><input type=\"text\" class=\"_3Cu3cwWYel\" value=\"1\"><button type=\"button\" class=\"_3gsRID8KP2 _31ra1B7laj\"><span class=\"blind\">수량 추가</span></button></div><span class=\"qT8CN3xTzY\"><span class=\"_1jsVii8y4-\">499,000</span>원</span></div></div><button class=\"_22ko9ygWLP\"><span class=\"blind\">구매목록에서 삭제</span></button>","count":"2","shipping":"무료배송"}]';
	
	$platform = $data['platform'];
	$arr_options = json_decode($data['options'],true);
	
	$inner_cnt = 0;
	foreach($arr_options as $options)
	{			
		$min_price = getMinPrice(explode("|",$options['price']));	
		
		//======================= 배송비 파싱 시작=====================
		$arr_ship_cond = array();
		$ship_cond = '';
		$crawler_shipping = new Crawler($options['shipping']);	//파서에 HTML 주입
		
		//기본 배송비 파싱
		if($platform == 'mobile') //mobile 경우
		{
			$arr_shipping_pointer = $crawler_shipping->filter('tr')->each(function (Crawler $node, $i) { //리턴 데이터는 배열로 넘어온다
				if(strpos($node->text(),'배송비') !== false) return 'Y'; else return 'N';				
			});
			
			$shipping_pointer = array_search('Y',$arr_shipping_pointer); //배송비라는 키워드가 있는 tr index
			
			//기본배송비				
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('tr')->eq($shipping_pointer)->filter('td p')->eq(0)->text());
			if($basic_price == '') $basic_price = 0; //문자열일 경우 무료 라고 판단한다.
			
			//조건분석
			if($crawler_shipping->filter('tr')->eq($shipping_pointer)->filter('td p')->eq(1)->count() > 0)
			{
				$cond_full_txt = $crawler_shipping->filter('tr')->eq($shipping_pointer)->filter('td p')->eq(1)->text();
				if(strpos($cond_full_txt,'원 이상 구매 시') !== false)
				{
					$full_ship_cond = explode('원 이상 구매 시',$crawler_shipping->filter('tr')->eq($shipping_pointer)->filter('td p')->eq(1)->text());
					$ship_cond = preg_replace("/[^0-9]*/s", "",$full_ship_cond[0]); //이상 구매시로 split
					$cond_price = preg_replace("/[^0-9]*/s", "",$full_ship_cond[1]);
				}
				else
				{
					$ship_cond = '';
					$cond_price = '';
				}
			}
			else //조건나온 td 없다고 판단되면 공백
			{
				$ship_cond = '';
				$cond_price = '';
			}
			
			//조건에 부합하면 해당 조건의 가격으로
			if($min_price >= $ship_cond) 
			{
				$final_price = $cond_price;
			}
			else
			{
				$final_price = $basic_price;
			}
		}
		//em 태그가 3개 있는경우 조건부 배송비로 판단, PC
		else if($crawler_shipping->filter('.prod-shipping-fee-message em')->count() == 3) 
		{
			//조건부 로직
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message .prod-shipping-fee-divider')->text(''));
			if($basic_price == '') $basic_price = 0; //문자열일 경우 무료 라고 판단한다.
			
			$ship_cond = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message .prod-txt-black')->text(''));
			$cond_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message .prod-txt-bold')->text(''));
			if($cond_price == '') $cond_price = 0;
			
			if($min_price >= $ship_cond) //조건에 부합하면 해당 조건의 가격으로
			{
				$final_price = $cond_price;
			}
			else
			{
				$final_price = $basic_price;
			}
		}
		else if($crawler_shipping->filter('.prod-shipping-fee-message em')->count())
		{			
			if(strpos($crawler_shipping->filter('.prod-shipping-fee-message .prod-txt-bold')->text(), '로켓배송') > -1)
			{
				$basic_price = 0;
				$final_price = 0;
			}
			else
			{				
				$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message .prod-txt-bold')->text(''));
				if($basic_price == '') $basic_price = 0; //문자열일 경우 무료 라고 판단한다.
				$final_price = $basic_price;
			}		
		}
		else 
		{
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('.prod-shipping-fee-message')->text(''));
			if($basic_price == '') $basic_price = 0; //문자열일 경우 무료 라고 판단한다.
			$final_price = $basic_price;
		}			
					
		if($ship_cond)
		{
			$arr_ship_cond[$ship_cond] = $cond_price;
		}
		
		//최종 배송비가 null 일 경우 0으로 지정
		if(!$final_price) $final_price = 0;
		
		$arr_shipping_condition = array("basic_price"=> $basic_price, "cond" => $arr_ship_cond);
		$json_shipping_condition = json_encode($arr_shipping_condition);	
		//======================= 배송비 파싱 종료=====================	
			
		$arr_options[$inner_cnt]['price'] = $min_price;
		$arr_options[$inner_cnt]['option'] = preg_replace("/\s{2,}/"," ",preg_replace('/\r\n|\r|\n/','',$options['option'])); //개행문자제거, 공백2개 이상 1개로 변환		
		//$arr_options[$inner_cnt]['shipping'] = preg_replace("/[^0-9]*/s", "", $options['shipping']);		 
		$arr_options[$inner_cnt]['shipping'] = $final_price;		
		$arr_options[$inner_cnt]['shipping_cond'] = trim(preg_replace("/\s{2,}/"," ",preg_replace('/\r\n|\r|\n/','',$options['shipping'])));
		$arr_options[$inner_cnt]['shipping_cond2'] = $json_shipping_condition;
		
		$inner_cnt++;
	}
	
	return $arr_options;	
}

function parserSmartStore_v1($data)
{	
	//$sample = '<li class="_1hXttefmex"><div class="ABlRhvBGMr"><p class="_3HIhLg5kOy">11인치 / 블랙</p><div class="_2ctp4_oSla"><div class="_2d9PkUsVhA"><button type="button" class="_3gsRID8KP2 _7DNjn2XDP0"><span class="blind">수량 빼기</span></button><input type="text" class="_3Cu3cwWYel" value="1"><button type="button" class="_3gsRID8KP2 _31ra1B7laj"><span class="blind">수량 추가</span></button></div><span class="qT8CN3xTzY"><span class="_1jsVii8y4-">9,800</span>원</span></div></div><button class="_22ko9ygWLP"><span class="blind">구매목록에서 삭제</span></button></li><li class="_1hXttefmex"><div class="ABlRhvBGMr"><p class="_3HIhLg5kOy">12.9 / 13인치 / 블랙</p><div class="_2ctp4_oSla"><div class="_2d9PkUsVhA"><button type="button" class="_3gsRID8KP2 _7DNjn2XDP0"><span class="blind">수량 빼기</span></button><input type="text" class="_3Cu3cwWYel" value="1"><button type="button" class="_3gsRID8KP2 _31ra1B7laj"><span class="blind">수량 추가</span></button></div><span class="qT8CN3xTzY"><span class="_1jsVii8y4-">11,900</span>원</span></div></div><button class="_22ko9ygWLP"><span class="blind">구매목록에서 삭제</span></button></li><li class="_1hXttefmex"><div class="ABlRhvBGMr"><p class="_3HIhLg5kOy">핑크</p><div class="_2ctp4_oSla"><div class="_2d9PkUsVhA"><button type="button" class="_3gsRID8KP2 _7DNjn2XDP0"><span class="blind">수량 빼기</span></button><input type="text" class="_3Cu3cwWYel" value="1"><button type="button" class="_3gsRID8KP2 _31ra1B7laj"><span class="blind">수량 추가</span></button></div><span class="qT8CN3xTzY"><span class="_1jsVii8y4-">8,900</span>원</span></div></div><button class="_22ko9ygWLP"><span class="blind">구매목록에서 삭제</span></button></li>';
	
	$ship_sample = '<div class="pTaPzv0i6H _21BFxdLPm-">택배배송<span class="Y-_Vd4O6dS"><span class="_1_wrVRMvuL">2,500</span>원<span class="_3JrHrGnQ31">(주문시 결제)</span></span></div>
<p class="_1JN0qDXZim">30,000원 이상 구매 시 무료 / 제주,도서지역 추가 2,500원</p>
<div class="_3_kSDK68pw"><a href="/coffeecg/bundle/950215?prevProdId=2031015220" target="_blank" class="_2Ugyq5gjSq _23ZcKYMJAs _2W6umRasi3 N=a:pcs.delivery">배송비 절약상품 보기</a></div>';
	
	$platform = $data['platform'];
	$arr_options = json_decode($data['options'],true);
		
	$inner_cnt = 0;
	foreach($arr_options as $options)
	{
		//스마트스토어 옵션 데이터 파싱		
		$filtered_option = '';
		$crawler = new Crawler($options['option']);	//파서에 HTML 주입					
		$arr_filtered_option = $crawler->filter('li')->each(function (Crawler $node, $i) //리턴 데이터는 배열로 넘어온다
		{
			$option_name = '';
			$option_count = '';
			$option_price = '';
			if($node->filter('p')->count()) $option_name = $node->filter('p')->text();
			if($node->filter("input[type='text']")->count()) $option_count = $node->filter("input[type='text']")->attr('value').'개';
			if($option_name) //개별 옵션네임이 있는경우에는 따로 가격이 붙지만, 없을때는 선택옵션이 없고 상품명 자체로 주문하기 때문에 옵션별 가격정보가 없으므로 공백처리한다
			{
				if($node->filter('div span')->last()->count()) $option_price = $node->filter('div span')->last()->text().'원';
			}
				
			$foption = '['.$option_name.'|'.$option_count.'|'.$option_price.']';				
			return $foption;			
		});	
					
		//다수개 옵션 텍스트화
		foreach((array)$arr_filtered_option as $foption)
		{
			$filtered_option .= $foption;
		}
		
		$min_price = getMinPrice(explode("|",$options['price']));				
		
		//======================= 배송비 파싱 시작=====================
		$arr_ship_cond = array();
		$crawler_shipping = new Crawler($options['shipping']);	//파서에 HTML 주입
		
		if($platform == 'mobile')
		{
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('._2nozUYDL1J')->text(''));	
			if($basic_price == '') $basic_price = 0; //문자열,공백일 경우 무료 라고 판단한다.		
			
			$arr_raw_ship_cond = explode('/',$crawler_shipping->filter('._2nozUYDL1J')->text(''));
		}
		else
		{
			$basic_price = preg_replace("/[^0-9]*/s", "",$crawler_shipping->filter('._1_wrVRMvuL')->text(''));			
			if($basic_price == '') $basic_price = 0; //문자열,공백일 경우 무료 라고 판단한다.		
			
			$arr_raw_ship_cond = explode('/',$crawler_shipping->filter('._1JN0qDXZim')->text(''));
		}
		
		$ship_cond_raw = $arr_raw_ship_cond[0];
		$ship_cond = '';
		$neddle = '원 이상 구매 시';
		
		if(strpos($ship_cond_raw, $neddle))
		{
			//원이상 구매시 앞뒤로 자른다		
			$arr_cond = explode($neddle,$ship_cond_raw);
			//
			$ship_cond = preg_replace("/[^0-9]*/s", "",$arr_cond[0]);			
			$cond_price = preg_replace("/[^0-9]*/s", "",$arr_cond[1]);
			
			if($min_price >= $ship_cond) //조건에 부합하면 해당 조건의 가격으로
			{
				$final_price = $cond_price;
			}
			else
			{
				$final_price = $basic_price;
			}
						
			if($final_price == '') $final_price = 0; //문자열일 경우 무료 라고 판단한다.	
		}
		else
		{
			$final_price = $basic_price;
		}	
		
		if($ship_cond)
		{
			$arr_ship_cond[$ship_cond] = $cond_price;
		}
		
		//최종 배송비가 null 일 경우 0으로 지정
		if(!$final_price) $final_price = 0;
		
		$arr_shipping_condition = array("basic_price"=> $basic_price, "cond" => $arr_ship_cond);
		$json_shipping_condition = json_encode($arr_shipping_condition);
		//======================= 배송비 파싱 종료=====================		
		
		$arr_options[$inner_cnt]['price'] = $min_price;		
		$arr_options[$inner_cnt]['count'] = 1;
		$arr_options[$inner_cnt]['option'] = $filtered_option;
		$arr_options[$inner_cnt]['shipping'] = $final_price;
		$arr_options[$inner_cnt]['shipping_cond'] = trim(preg_replace("/\s{2,}/"," ",preg_replace('/\r\n|\r|\n/','',$options['shipping'])));
		$arr_options[$inner_cnt]['shipping_cond2'] = $json_shipping_condition;
		
		$inner_cnt++;
	}
	
	return $arr_options;	
}

function checkItem($url)
{
	$chck = 'http://49.247.0.111:21001/?cmd=getProduct&type=CU&urlBase64=aHR0cHM6Ly93d3cuY291cGFuZy5jb20vdnAvcHJvZHVjdHMvMTcyMjQ0NjU5ND92ZW5kb3JJdGVtSWQ9NzA5MjAyOTc1OTQmc291cmNlVHlwZT1IT01FX1RSRU5ESU5HX0FEUyZzZWFyY2hJZD1mZWVkLTE2MTI4NTM0MTU2OTMtdHJlbmRpbmdfYWRzLTYzNDQ4JmlzQWRkZWRDYXJ0PQ&secTimeout=40';
	return;
}

function getSSsearchData($keyword, $page) //스마트스토어 검색 API
{
	$timeout = 15;	
	$url = "http://49.247.0.111:21001/?cmd=searchSS&page=$page&searchValue=$keyword&secTimeout=$timeout";	
	
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}

function getMinPrice($arr_data) //복수개 가격있을시 최저가격 리턴
{
	$arr_price = array();	
	foreach((array)$arr_data as $data)
	{
		$striped_price = preg_replace("/[^0-9]*/s", "", $data);
		if($striped_price != '') array_push($arr_price, $striped_price);
	}

	$min_price = min($arr_price);
	return $min_price;
}

function payLogger($od_id, $pay_type, $pay_for, $pay_amount, $pay_tid, $pay_return_code, $pay_status, $pay_raw)
{
	$result = DB::table('pay_logs')
	->insert([
	'od_id' => $od_id,
	'pay_pg'=> 'erompay',
	'pay_type'=> $pay_type,
	'pay_method'=>'card', 
	'pay_for'=> $pay_for,
	'pay_amount'=>$pay_amount,
	'pay_tid'=>$pay_tid, 
	'pay_return_code'=>$pay_return_code, 
	'pay_status'=>$pay_status, 
	'pay_raw'=>$pay_raw, 			
	'created_at'=>now()
	]);
	
	if($result) return true;
	else return false;
}

function setOrderPartners($_PID, $new_od_id, $mb_email) {
    /**
     * TODO : esummer, 2021-05-06, 사용자 결제시 파트너 시스템으로 결제정보 넘김
     */
    DB::table('orders_partner')
        ->insert(['od_id'=>$new_od_id,
            'partner_id'=>$_PID,
            'mb_email'=>$mb_email,
            'od_amt' => 0,
            'comm_amt' => 0,
            'od_status' => 'od_regist',
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
        ]);
}

/**
 * @param $_TYPE
 * @param $_PID
 * @param $_OPTION
 * @return bool|string
 */
function sendMarketingData($request, $_TYPE, $_PID, $_OPTION) //스마트스토어 검색 API
{
    $option = '';
    switch ( $_TYPE ) {
        case 'C':
            $option = '&';
            break;
        case 'J':
            $option = '&UID='.$_OPTION['UID'];
            break;
        case 'T':
            $option = '&UID='.$_OPTION['UID'].'&ORDERID='.$_OPTION['ORDERID'].'&MYMONEY='.$_OPTION['MYMONEY'];
            break;
        case 'P':
            $option = '&UID='.$_OPTION['UID'].'&ORDERID='.$_OPTION['ORDERID'].'&MYMONEY='.$_OPTION['MYMONEY'].'&';
            break;
    }

    $url = "https://partners.eckorea.net/marketing_acc/acc_data.php?TYPE=".$_TYPE."&PID=".$_PID."&UIP=".$_SERVER["REMOTE_ADDR"].$option;

    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_FAILONERROR => true,

        CURLOPT_ENCODING => "",
        CURLOPT_TIMEOUT => 30000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            // Set Here Your Requesred Headers
            'Content-Type: application/json',
        ),
    ));
    $result = "";
    try{
        $result = curl_exec($curl);
        $err = curl_error($curl);
    } catch(Exception $e){
        $errorMessage = $e;
    } finally {
        curl_close($curl);
    }
    return $result;
}

/**
 * 모바일 푸시메시지 발송
 * @param $request
 * @param $mb_email
 * @param $ms_code
 * @param $ms_text
 * @param $link_url
 */
function sendMobileNotification($mb_email, $ms_code, $ms_title, $ms_text, $link_url)
{
    $devices = DB::table('members_device')->where('mb_email',$mb_email)->whereNull('deleted_at')->get();
    $result = "";
    foreach($devices as $row) {
        $message_id = DB::table('messages')->insertGetId(
            [
                'ms_sender'  =>'SYSTEM',
                'ms_receiver'=> $mb_email,
                'ms_title'=> $ms_title,
                'ms_text' => $ms_text,
                'linkUrl' => $link_url,
                'ms_code' => $ms_code,
                'status'  => 'A',
                'device_no' => $row->device_no,
                'created_at'=>Carbon::now()
            ]
        );

        $api_token = env('API_TOKEN', '0bc048074bca6e9a305c1408661c3cad-e687bab4-339a4fbc');
        $api_host = env('API_HOST', 'http://api.eckorea.net:7272');

        $url = $api_host."/eckorea/fcm/send?api_token=".$api_token."&message_id=".$message_id."&device_no=".$row->device_no."&UIP=".$_SERVER["REMOTE_ADDR"];

        $curl = curl_init();
        try{
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_FAILONERROR => true,

                CURLOPT_ENCODING => "",
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    // Set Here Your Requesred Headers
                    'Content-Type: application/json',
                ),
            ));
            $result = curl_exec($curl);
            if ($result === false) {
                $curl_msg = curl_error($curl);
                $remark = array("curl_error"=>$curl_msg, "curl_error_no"=>curl_errno($curl), "curl_exec"=> $result);
                DB::table('sys_error')->insert(
                    [
                        'code'  =>'FCM-PUSH;;'.$ms_code,
                        'ref'=> $url,
                        'remark'=> json_encode($remark),
                        'status'  => 'E',
                        'created_at'=>Carbon::now()
                    ]
                );
            }
        } catch(Exception $e){
            $remark = $e;
            DB::table('sys_error')->insert(
                [
                    'code'  =>'FCM-PUSH;;'.$ms_code,
                    'ref'=> $url,
                    'remark'=> $remark,
                    'status'  => 'E',
                    'created_at'=>Carbon::now()
                ]
            );
        } finally {
            curl_close($curl);
        }
    }

}

function sendSlackNotification($ms_code, $ms_title, $ms_link)
{
    /**
     *  curl -X POST -H 'Content-type: application/json' --data '{"text":"Hello, World!"}' https://hooks.slack.com/services/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
     */
    $api_token = 'Nx59jX9T4DVkcgppXmrdV4Jk';
    $api_host = 'https://hooks.slack.com/';

    $url = $api_host."services/T01E2ERGD6H/B01ULCT73UG/".$api_token;

    $ch = curl_init();
    try{

        $data =
           '{"text":":collision: ECKorea Webserver notification ! \n'.$ms_title.'\n'.$ms_link.'"}'
        ;
                              //curl 초기화
        curl_setopt($ch, CURLOPT_URL, $url);               //URL 지정하기
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    //요청 결과를 문자열로 반환
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);      //connection timeout 10초
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);   //원격 서버의 인증서가 유효한지 검사 안함
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);       //POST data
        curl_setopt($ch, CURLOPT_POST, true);              //true시 post 전송

        $result = curl_exec($ch);
        if ($result === false) {
            $curl_msg = curl_error($ch);
            $remark = array("curl_error"=>$curl_msg, "curl_error_no"=>curl_errno($ch), "curl_exec"=> $result);
            DB::table('sys_error')->insert(
                [
                    'code'  =>'SLACK-PUSH;;'.$ms_code,
                    'ref'=> $url,
                    'remark'=> json_encode($remark),
                    'status'  => 'E',
                    'created_at'=>Carbon::now()
                ]
            );
        }
    } catch(Exception $e){
        $remark = $e;
        DB::table('sys_error')->insert(
            [
                'code'  =>'SLACK-PUSH;;'.$ms_code,
                'ref'=> $url,
                'remark'=> $remark,
                'status'  => 'E',
                'created_at'=>Carbon::now()
            ]
        );
    } finally {
        curl_close($ch);
    }

}